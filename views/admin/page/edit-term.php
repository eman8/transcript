<?php
head_css();
echo head(array('title' => 'Transcript - Edit Term'));

echo flash();
?>
<script src="<?= WEB_ROOT ?>/plugins/Transcript/javascripts/tinymce/tinymce-mod.min.js"></script>
<script src="<?= WEB_ROOT ?>/plugins/Transcript/javascripts/transcript-admin.js"></script>

<br /><br />
<?php
  echo $content;

  echo foot();
?>

