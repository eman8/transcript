<?php
queue_css_url(WEB_ROOT . '/plugins/Transcript/css/transcript.css');
queue_css_url('https://eman-archives.org/exist/apps/tei-publisher/transform/EMAN.css');
queue_css_url(WEB_ROOT . '/plugins/Transcript/javascripts/codemirror/lib/codemirror.css');
queue_css_url(WEB_ROOT . '/plugins/Transcript/javascripts/codemirror/addon/hint/show-hint.css');
queue_css_url(WEB_ROOT . '/plugins/Transcript/javascripts/codemirror/addon/fold/foldgutter.css');
queue_css_url(WEB_ROOT . '/plugins/Transcript/javascripts/codemirror/addon/dialog/dialog.css');

// Codemirror

queue_js_url(WEB_ROOT . '/plugins/Transcript/javascripts/codemirror/lib/codemirror.js');
queue_js_url(WEB_ROOT . '/plugins/Transcript/javascripts/codemirror/addon/fold/foldcode.js');
queue_js_url(WEB_ROOT . '/plugins/Transcript/javascripts/codemirror/addon/fold/foldgutter.js');
queue_js_url(WEB_ROOT . '/plugins/Transcript/javascripts/codemirror/addon/fold/brace-fold.js');
queue_js_url(WEB_ROOT . '/plugins/Transcript/javascripts/codemirror/addon/fold/xml-fold.js');
queue_js_url(WEB_ROOT . '/plugins/Transcript/javascripts/codemirror/addon/fold/indent-fold.js');
queue_js_url(WEB_ROOT . '/plugins/Transcript/javascripts/codemirror/addon/fold/comment-fold.js');
queue_js_url(WEB_ROOT . '/plugins/Transcript/javascripts/codemirror/addon/edit/matchtags.js');
queue_js_url(WEB_ROOT . '/plugins/Transcript/javascripts/codemirror/addon/edit/closebrackets.js');
queue_js_url(WEB_ROOT . '/plugins/Transcript/javascripts/codemirror/addon/edit/closetag.js');
queue_js_url(WEB_ROOT . '/plugins/Transcript/javascripts/codemirror/addon/display/autorefresh.js');
queue_js_url(WEB_ROOT . '/plugins/Transcript/javascripts/codemirror/addon/selection/mark-selection.js');
queue_js_url(WEB_ROOT . '/plugins/Transcript/javascripts/codemirror/addon/search/match-highlighter.js');
queue_js_url(WEB_ROOT . '/plugins/Transcript/javascripts/codemirror/addon/mode/overlay.js');
queue_js_url(WEB_ROOT . '/plugins/Transcript/javascripts/codemirror/addon/dialog/dialog.js');
queue_js_url(WEB_ROOT . '/plugins/Transcript/javascripts/codemirror/addon/search/searchcursor.js');
queue_js_url(WEB_ROOT . '/plugins/Transcript/javascripts/codemirror/addon/search/search.js');
queue_js_url(WEB_ROOT . '/plugins/Transcript/javascripts/codemirror/addon/scroll/annotatescrollbar.js');
queue_js_url(WEB_ROOT . '/plugins/Transcript/javascripts/codemirror/addon/search/matchesonscrollbar.js');
queue_js_url(WEB_ROOT . '/plugins/Transcript/javascripts/codemirror/addon/search/jump-to-line.js');
queue_js_url(WEB_ROOT . '/plugins/Transcript/javascripts/codemirror/addon/hint/show-hint.js');
queue_js_url(WEB_ROOT . '/plugins/Transcript/javascripts/codemirror/addon/hint/xml-hint.js');
queue_js_url(WEB_ROOT . '/plugins/Transcript/javascripts/codemirror/mode/xml/xml.js');
queue_js_url(WEB_ROOT . '/plugins/Transcript/javascripts/codemirror/addon/xml4tei/js/xml4tei.js');
queue_js_url(WEB_ROOT . '/plugins/Transcript/javascripts/codemirror/addon/xml4tei/js/xml4teiSchema2json.js');
queue_js_url(WEB_ROOT . '/plugins/Transcript/javascripts/codemirror/addon/xml4tei/js/xml4teiNte.js');
queue_js_url(WEB_ROOT . '/plugins/Transcript/javascripts/codemirror/addon/xml4tei/js/xml4teiHelp.js');

queue_js_url(WEB_ROOT . '/plugins/Transcript/javascripts/functions.js');

// TEI Publisher
queue_js_url('https://unpkg.com/@webcomponents/webcomponentsjs@2.4.3/webcomponents-loader.js');
queue_js_url('https://unpkg.com/@teipublisher/pb-components@latest/dist/pb-components-bundle.js');

head_css();
head_js(false);

echo head(array('title' => 'Browse transcriptions', 'bodyclass' => 'transcript browse'));
echo flash();
if ($content) :
  echo $content;
else :
?>
  <div id='toolbar'><?=$toolbar ?><div id='import-output'></div></div>

  <?php if (current_user()) : ?>
    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
      <span class="sr-only">Toggle navigation</span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
    </button>

    <div id='transcript-wrapper'>

    <div id='file-info'></div>
    <div id='messages'></div>
    <div id='termes-info'></div>

      <div id='transcript-zoom'>
        <div id='transcript-image-container'>
        </div>
      </div>

      <div id="transcript-form">
        <button id="transcript-show-source">Source</button>
        <button id="transcript-validate">Valider</button>
        <div class="buttons-nav"></div>
        <div id='currenttag'></div>
        <div id='code-mirror-wrapper'>
          <div id="validation-response"></div>
          <textarea id='codemirror-edit' class='tei-editor' data-xmlschema="<?= WEB_ROOT ?>/plugins/Transcript/resources/cm-tei-schema.xml"></textarea>
        </div>
      	<?php echo $form; ?>
      </div>

    </div>
  <?php endif; ?>

  <div id='transcript-rendition'>
    <?php if (! current_user()) : ?>
      <div id='file-info'></div>
      <div id='termes-info'></div>
      <div id="trancript-pictures"><img src='<?= WEB_ROOT ?>/plugins/Transcript/resources/no-image.jpg' /></div>
      <div id="trancript-rendered"></div>
    <?php endif; ?>
  </div>

<?php if (current_user()) : ?>
    <script>
    $ = jQuery;
    // Transcript preferences
    <?php echo "var tag_context = ". $controles . ";\n";?>

    var contentDivStyles = $('#content').css(["width", "max-width", "height", "top", "left", "margin", "padding"]);
    var fullscreen = false;
    </script>
  <?php endif; ?>

  <div id="phpWebRoot" style="display:none;"><?php echo WEB_ROOT; ?></div>
  <div id="userRole" style="display:none;"><?php echo $userRole; ?></div>
   <?php echo $comments ?>
  </div>

<?php endif; ?>
<!-- TinyMCE -->
<!-- <script type="text/javascript" src="<?= WEB_ROOT ?>/plugins/Transcript/javascripts/tinymce/tinymce-mod.js"></script> -->
<script type="text/javascript" src="<?= WEB_ROOT ?>/plugins/Transcript/javascripts/tinymce/tinymce.min.js"></script>
<?php if (current_user()) : ?>
  <script type="text/javascript" src="<?= WEB_ROOT ?>/plugins/Transcript/javascripts/transcript.js"></script>
<?php else : ?>
  <script type="text/javascript" src="<?= WEB_ROOT ?>/plugins/Transcript/javascripts/transcript-public.js"></script>
<?php endif; ?>

<?php echo foot(); ?>