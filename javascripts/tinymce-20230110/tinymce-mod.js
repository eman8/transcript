var $jscomp = {
    scope: {},
    findInternal: function(E, K, Y) {
        E instanceof String && (E = String(E));
        for (var z = E.length, ga = 0; ga < z; ga++) {
            var k = E[ga];
            if (K.call(Y, k, ga, E)) return {
                i: ga,
                v: k
            }
        }
        return {
            i: -1,
            v: void 0
        }
    }
};
$jscomp.defineProperty = "function" == typeof Object.defineProperties ? Object.defineProperty : function(E, K, Y) {
    if (Y.get || Y.set) throw new TypeError("ES3 does not support getters and setters.");
    E != Array.prototype && E != Object.prototype && (E[K] = Y.value)
};
$jscomp.getGlobal = function(E) {
    return "undefined" != typeof window && window === E ? E : "undefined" != typeof global && null != global ? global : E
};
$jscomp.global = $jscomp.getGlobal(this);
$jscomp.polyfill = function(E, K, Y, z) {
    if (K) {
        Y = $jscomp.global;
        E = E.split(".");
        for (z = 0; z < E.length - 1; z++) {
            var ga = E[z];
            ga in Y || (Y[ga] = {});
            Y = Y[ga]
        }
        E = E[E.length - 1];
        z = Y[E];
        K = K(z);
        K != z && null != K && $jscomp.defineProperty(Y, E, {
            configurable: !0,
            writable: !0,
            value: K
        })
    }
};
$jscomp.polyfill("Array.prototype.find", function(E) {
    return E ? E : function(E, Y) {
        return $jscomp.findInternal(this, E, Y).v
    }
}, "es6-impl", "es3");
$jscomp.polyfill("Array.prototype.findIndex", function(E) {
    return E ? E : function(E, Y) {
        return $jscomp.findInternal(this, E, Y).i
    }
}, "es6-impl", "es3");
$jscomp.SYMBOL_PREFIX = "jscomp_symbol_";
$jscomp.initSymbol = function() {
    $jscomp.initSymbol = function() {};
    $jscomp.global.Symbol || ($jscomp.global.Symbol = $jscomp.Symbol)
};
$jscomp.symbolCounter_ = 0;
$jscomp.Symbol = function(E) {
    return $jscomp.SYMBOL_PREFIX + (E || "") + $jscomp.symbolCounter_++
};
$jscomp.initSymbolIterator = function() {
    $jscomp.initSymbol();
    var E = $jscomp.global.Symbol.iterator;
    E || (E = $jscomp.global.Symbol.iterator = $jscomp.global.Symbol("iterator"));
    "function" != typeof Array.prototype[E] && $jscomp.defineProperty(Array.prototype, E, {
        configurable: !0,
        writable: !0,
        value: function() {
            return $jscomp.arrayIterator(this)
        }
    });
    $jscomp.initSymbolIterator = function() {}
};
$jscomp.arrayIterator = function(E) {
    var K = 0;
    return $jscomp.iteratorPrototype(function() {
        return K < E.length ? {
            done: !1,
            value: E[K++]
        } : {
            done: !0
        }
    })
};
$jscomp.iteratorPrototype = function(E) {
    $jscomp.initSymbolIterator();
    E = {
        next: E
    };
    E[$jscomp.global.Symbol.iterator] = function() {
        return this
    };
    return E
};
$jscomp.array = $jscomp.array || {};
$jscomp.iteratorFromArray = function(E, K) {
    $jscomp.initSymbolIterator();
    E instanceof String && (E += "");
    var Y = 0,
        z = {
            next: function() {
                if (Y < E.length) {
                    var ga = Y++;
                    return {
                        value: K(ga, E[ga]),
                        done: !1
                    }
                }
                z.next = function() {
                    return {
                        done: !0,
                        value: void 0
                    }
                };
                return z.next()
            }
        };
    z[Symbol.iterator] = function() {
        return z
    };
    return z
};
$jscomp.polyfill("Array.prototype.values", function(E) {
    return E ? E : function() {
        return $jscomp.iteratorFromArray(this, function(E, Y) {
            return Y
        })
    }
}, "es6", "es3");
(function(E, K) {
    function Y(k, g) {
        for (var f, e = [], a = 0; a < k.length; ++a) {
            if (!(f = ga[k[a]])) a: {
                f = E;
                for (var c = k[a].split(/[.\/]/), b = 0; b < c.length; ++b) {
                    if (!f[c[b]]) {
                        f = void 0;
                        break a
                    }
                    f = f[c[b]]
                }
            }
            if (!f) throw "module definition dependecy not found: " + k[a];
            e.push(f)
        }
        g.apply(null, e)
    }

    function z(k, g, f) {
        if ("string" !== typeof k) throw "invalid module definition, module id must be defined and be a string";
        if (g === K) throw "invalid module definition, dependencies must be specified";
        if (f === K) throw "invalid module definition, definition function must be specified";
        Y(g, function() {
            ga[k] = f.apply(null, arguments)
        })
    }
    var ga = {};
    z("tinymce/geom/Rect", [], function() {
        function k(c, b, d) {
            var e, f, q, h;
            e = b.x;
            f = b.y;
            q = c.w;
            c = c.h;
            h = b.w;
            b = b.h;
            d = (d || "").split("");
            "b" === d[0] && (f += b);
            "r" === d[1] && (e += h);
            "c" === d[0] && (f += a(b / 2));
            "c" === d[1] && (e += a(h / 2));
            "b" === d[3] && (f -= c);
            "r" === d[4] && (e -= q);
            "c" === d[3] && (f -= a(c / 2));
            "c" === d[4] && (e -= a(q / 2));
            return g(e, f, q, c)
        }

        function g(a, b, d, e) {
            return {
                x: a,
                y: b,
                w: d,
                h: e
            }
        }
        var f = Math.min,
            e = Math.max,
            a = Math.round;
        return {
            inflate: function(a, b, d) {
                return g(a.x - b, a.y -
                    d, a.w + 2 * b, a.h + 2 * d)
            },
            relativePosition: k,
            findBestRelativePosition: function(a, b, d, e) {
                var c, f;
                for (f = 0; f < e.length; f++)
                    if (c = k(a, b, e[f]), c.x >= d.x && c.x + c.w <= d.w + d.x && c.y >= d.y && c.y + c.h <= d.h + d.y) return e[f];
                return null
            },
            intersect: function(a, b) {
                var c, l, v;
                c = e(a.x, b.x);
                l = e(a.y, b.y);
                v = f(a.x + a.w, b.x + b.w);
                a = f(a.y + a.h, b.y + b.h);
                return 0 > v - c || 0 > a - l ? null : g(c, l, v - c, a - l)
            },
            clamp: function(a, b, d) {
                var c, f, q, h, n, p;
                q = a.x;
                h = a.y;
                n = a.x + a.w;
                p = a.y + a.h;
                c = b.x + b.w;
                f = b.y + b.h;
                a = e(0, b.x - q);
                b = e(0, b.y - h);
                c = e(0, n - c);
                f = e(0, p - f);
                q += a;
                h += b;
                d && (n += a, p += b, q -= c, h -= f);
                return g(q, h, n - c - q, p - f - h)
            },
            create: g,
            fromClientRect: function(a) {
                return g(a.left, a.top, a.width, a.height)
            }
        }
    });
    z("tinymce/util/Promise", [], function() {
        function k(a, b) {
            return function() {
                a.apply(b, arguments)
            }
        }

        function g(b) {
            if ("object" !== typeof this) throw new TypeError("Promises must be constructed via new");
            if ("function" !== typeof b) throw new TypeError("not a function");
            this._value = this._state = null;
            this._deferreds = [];
            d(b, k(e, this), k(a, this))
        }

        function f(a) {
            var b = this;
            null === this._state ?
                this._deferreds.push(a) : l(function() {
                    var c = b._state ? a.onFulfilled : a.onRejected;
                    if (null === c)(b._state ? a.resolve : a.reject)(b._value);
                    else {
                        var d;
                        try {
                            d = c(b._value)
                        } catch (w) {
                            a.reject(w);
                            return
                        }
                        a.resolve(d)
                    }
                })
        }

        function e(b) {
            try {
                if (b === this) throw new TypeError("A promise cannot be resolved with itself.");
                if (b && ("object" === typeof b || "function" === typeof b)) {
                    var f = b.then;
                    if ("function" === typeof f) {
                        d(k(f, b), k(e, this), k(a, this));
                        return
                    }
                }
                this._state = !0;
                this._value = b;
                c.call(this)
            } catch (n) {
                a.call(this, n)
            }
        }

        function a(a) {
            this._state = !1;
            this._value = a;
            c.call(this)
        }

        function c() {
            for (var a = 0, b = this._deferreds.length; a < b; a++) f.call(this, this._deferreds[a]);
            this._deferreds = null
        }

        function b(a, b, c, d) {
            this.onFulfilled = "function" === typeof a ? a : null;
            this.onRejected = "function" === typeof b ? b : null;
            this.resolve = c;
            this.reject = d
        }

        function d(a, b, c) {
            var d = !1;
            try {
                a(function(a) {
                    d || (d = !0, b(a))
                }, function(a) {
                    d || (d = !0, c(a))
                })
            } catch (w) {
                d || (d = !0, c(w))
            }
        }
        if (window.Promise) return window.Promise;
        var l = g.immediateFn || "function" === typeof setImmediate && setImmediate ||
            function(a) {
                setTimeout(a, 1)
            },
            v = Array.isArray || function(a) {
                return "[object Array]" === Object.prototype.toString.call(a)
            };
        g.prototype["catch"] = function(a) {
            return this.then(null, a)
        };
        g.prototype.then = function(a, c) {
            var d = this;
            return new g(function(e, h) {
                f.call(d, new b(a, c, e, h))
            })
        };
        g.all = function() {
            var a = Array.prototype.slice.call(1 === arguments.length && v(arguments[0]) ? arguments[0] : arguments);
            return new g(function(b, c) {
                function d(m, f) {
                    try {
                        if (f && ("object" === typeof f || "function" === typeof f)) {
                            var h = f.then;
                            if ("function" ===
                                typeof h) {
                                h.call(f, function(a) {
                                    d(m, a)
                                }, c);
                                return
                            }
                        }
                        a[m] = f;
                        0 === --e && b(a)
                    } catch (B) {
                        c(B)
                    }
                }
                if (0 === a.length) return b([]);
                for (var e = a.length, f = 0; f < a.length; f++) d(f, a[f])
            })
        };
        g.resolve = function(a) {
            return a && "object" === typeof a && a.constructor === g ? a : new g(function(b) {
                b(a)
            })
        };
        g.reject = function(a) {
            return new g(function(b, c) {
                c(a)
            })
        };
        g.race = function(a) {
            return new g(function(b, c) {
                for (var d = 0, e = a.length; d < e; d++) a[d].then(b, c)
            })
        };
        return g
    });
    z("tinymce/util/Delay", ["tinymce/util/Promise"], function(k) {
        function g(a, b) {
            function c(a) {
                window.setTimeout(a,
                    0)
            }
            var e, f = window.requestAnimationFrame,
                q = ["ms", "moz", "webkit"];
            for (e = 0; e < q.length && !f; e++) f = window[q[e] + "RequestAnimationFrame"];
            f || (f = c);
            f(a, b)
        }

        function f(a, b) {
            "number" != typeof b && (b = 0);
            return setTimeout(a, b)
        }

        function e(a, b) {
            "number" != typeof b && (b = 0);
            return setInterval(a, b)
        }
        var a;
        return {
            requestAnimationFrame: function(c, b) {
                a ? a.then(c) : a = (new k(function(a) {
                    b || (b = document.body);
                    g(a, b)
                })).then(c)
            },
            setTimeout: f,
            setInterval: e,
            setEditorTimeout: function(a, b, d) {
                return f(function() {
                    a.removed || b()
                }, d)
            },
            setEditorInterval: function(a,
                b, d) {
                var c;
                return c = e(function() {
                    a.removed ? clearInterval(c) : b()
                }, d)
            },
            throttle: function(a, b) {
                var c, e;
                e = function() {
                    var d = arguments;
                    clearTimeout(c);
                    c = f(function() {
                        a.apply(this, d)
                    }, b)
                };
                e.stop = function() {
                    clearTimeout(c)
                };
                return e
            },
            clearInterval: function(a) {
                return clearInterval(a)
            },
            clearTimeout: function(a) {
                return clearTimeout(a)
            }
        }
    });
    z("tinymce/Env", [], function() {
        var k = navigator,
            g = k.userAgent,
            f, e, a, c, b, d, l, v, q, h, n;
        f = window.opera && window.opera.buildNumber;
        l = /Android/.test(g);
        e = /WebKit/.test(g);
        a = (a = !e && !f &&
            /MSIE/gi.test(g) && /Explorer/gi.test(k.appName)) && /MSIE (\w+)\./.exec(g)[1];
        c = -1 == g.indexOf("Trident/") || -1 == g.indexOf("rv:") && -1 == k.appName.indexOf("Netscape") ? !1 : 11;
        k = -1 == g.indexOf("Edge/") || a || c ? !1 : 12;
        a = a || c || k;
        c = !e && !c && /Gecko/.test(g);
        b = -1 != g.indexOf("Mac");
        d = /(iPad|iPhone)/.test(g);
        v = "FormData" in window && "FileReader" in window && "URL" in window && !!URL.createObjectURL;
        q = ("matchMedia" in window ? matchMedia("only screen and (max-device-width: 480px)").matches : !1) && (l || d);
        h = ("matchMedia" in window ? matchMedia("only screen and (min-width: 800px)").matches :
            !1) && (l || d);
        n = -1 != g.indexOf("Windows Phone");
        k && (e = !1);
        g = !d || v || 534 <= g.match(/AppleWebKit\/(\d*)/)[1];
        return {
            opera: f,
            webkit: e,
            ie: a,
            gecko: c,
            mac: b,
            iOS: d,
            android: l,
            contentEditable: g,
            transparentSrc: "data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7",
            caretAfter: 8 != a,
            range: window.getSelection && "Range" in window,
            documentMode: a && !k ? document.documentMode || 7 : 10,
            fileApi: v,
            ceFalse: !1 === a || 8 < a,
            desktop: !q && !h,
            windowsPhone: n
        }
    });
    z("tinymce/dom/EventUtils", ["tinymce/util/Delay", "tinymce/Env"],
        function(k, g) {
            function f(a, b, c, d) {
                a.addEventListener ? a.addEventListener(b, c, d || !1) : a.attachEvent && a.attachEvent("on" + b, c)
            }

            function e(a, b, c, d) {
                a.removeEventListener ? a.removeEventListener(b, c, d || !1) : a.detachEvent && a.detachEvent("on" + b, c)
            }

            function a(a, b) {
                var c = b;
                (b = a.path) && 0 < b.length && (c = b[0]);
                a.deepPath && (b = a.deepPath()) && 0 < b.length && (c = b[0]);
                return c
            }

            function c(b, c) {
                function d() {
                    return !1
                }

                function e() {
                    return !0
                }
                var f, h = c || {};
                for (f in b) v[f] || (h[f] = b[f]);
                h.target || (h.target = h.srcElement || document);
                g.experimentalShadowDom &&
                    (h.target = a(b, h.target));
                b && l.test(b.type) && void 0 === b.pageX && void 0 !== b.clientX && (f = h.target.ownerDocument || document, c = f.documentElement, f = f.body, h.pageX = b.clientX + (c && c.scrollLeft || f && f.scrollLeft || 0) - (c && c.clientLeft || f && f.clientLeft || 0), h.pageY = b.clientY + (c && c.scrollTop || f && f.scrollTop || 0) - (c && c.clientTop || f && f.clientTop || 0));
                h.preventDefault = function() {
                    h.isDefaultPrevented = e;
                    b && (b.preventDefault ? b.preventDefault() : b.returnValue = !1)
                };
                h.stopPropagation = function() {
                    h.isPropagationStopped = e;
                    b && (b.stopPropagation ?
                        b.stopPropagation() : b.cancelBubble = !0)
                };
                h.stopImmediatePropagation = function() {
                    h.isImmediatePropagationStopped = e;
                    h.stopPropagation()
                };
                h.isDefaultPrevented || (h.isDefaultPrevented = d, h.isPropagationStopped = d, h.isImmediatePropagationStopped = d);
                "undefined" == typeof h.metaKey && (h.metaKey = !1);
                return h
            }

            function b(a, b, c) {
                function d() {
                    c.domLoaded || (c.domLoaded = !0, b(l))
                }

                function h() {
                    if ("complete" === m.readyState || "interactive" === m.readyState && m.body) e(m, "readystatechange", h), d()
                }

                function n() {
                    try {
                        m.documentElement.doScroll("left")
                    } catch (y) {
                        k.setTimeout(n);
                        return
                    }
                    d()
                }
                var m = a.document,
                    l = {
                        type: "ready"
                    };
                c.domLoaded ? b(l) : (m.addEventListener ? "complete" === m.readyState ? d() : f(a, "DOMContentLoaded", d) : (f(m, "readystatechange", h), m.documentElement.doScroll && a.self === a.top && n()), f(a, "load", d))
            }

            function d() {
                function a(a, b) {
                    var c, d, u;
                    if (b = (b = n[b]) && b[a.type])
                        for (c = 0, d = b.length; c < d && ((u = b[c]) && !1 === u.func.call(u.scope, a) && a.preventDefault(), !a.isImmediatePropagationStopped()); c++);
                }
                var d = this,
                    n = {},
                    p, l, A, m, t;
                l = "mce-data-" + (+new Date).toString(32);
                m = "onmouseenter" in
                    document.documentElement;
                A = "onfocusin" in document.documentElement;
                t = {
                    mouseenter: "mouseover",
                    mouseleave: "mouseout"
                };
                p = 1;
                d.domLoaded = !1;
                d.events = n;
                d.bind = function(e, h, w, r) {
                    function u(b) {
                        a(c(b || k.event), F)
                    }
                    var F, y, q, x, B, v, g, k = window;
                    if (e && 3 !== e.nodeType && 8 !== e.nodeType) {
                        e[l] ? F = e[l] : (F = p++, e[l] = F, n[F] = {});
                        r = r || e;
                        h = h.split(" ");
                        for (q = h.length; q--;) x = h[q], v = u, B = g = !1, "DOMContentLoaded" === x && (x = "ready"), d.domLoaded && "ready" === x && "complete" == e.readyState ? w.call(r, c({
                            type: x
                        })) : (m || (B = t[x]) && (v = function(b) {
                            var d,
                                r;
                            d = b.currentTarget;
                            if ((r = b.relatedTarget) && d.contains) r = d.contains(r);
                            else
                                for (; r && r !== d;) r = r.parentNode;
                            r || (b = c(b || k.event), b.type = "mouseout" === b.type ? "mouseleave" : "mouseenter", b.target = d, a(b, F))
                        }), A || "focusin" !== x && "focusout" !== x || (g = !0, B = "focusin" === x ? "focus" : "blur", v = function(b) {
                            b = c(b || k.event);
                            b.type = "focus" === b.type ? "focusin" : "focusout";
                            a(b, F)
                        }), (y = n[F][x]) ? "ready" === x && d.domLoaded ? w({
                            type: x
                        }) : y.push({
                            func: w,
                            scope: r
                        }) : (n[F][x] = y = [{
                                func: w,
                                scope: r
                            }], y.fakeName = B, y.capture = g, y.nativeHandler = v,
                            "ready" === x ? b(e, v, d) : f(e, B || x, v, g)));
                        e = y = 0;
                        return w
                    }
                };
                d.unbind = function(a, b, c) {
                    var r, u, f, m, h, p;
                    if (!a || 3 === a.nodeType || 8 === a.nodeType) return d;
                    if (r = a[l]) {
                        p = n[r];
                        if (b)
                            for (b = b.split(" "), f = b.length; f--;) {
                                if (h = b[f], u = p[h]) {
                                    if (c)
                                        for (m = u.length; m--;)
                                            if (u[m].func === c) {
                                                var w = u.nativeHandler,
                                                    y = u.fakeName,
                                                    x = u.capture;
                                                u = u.slice(0, m).concat(u.slice(m + 1));
                                                u.nativeHandler = w;
                                                u.fakeName = y;
                                                u.capture = x;
                                                p[h] = u
                                            }
                                    c && 0 !== u.length || (delete p[h], e(a, u.fakeName || h, u.nativeHandler, u.capture))
                                }
                            } else {
                                for (h in p) u = p[h], e(a, u.fakeName ||
                                    h, u.nativeHandler, u.capture);
                                p = {}
                            }
                        for (h in p) return d;
                        delete n[r];
                        try {
                            delete a[l]
                        } catch (I) {
                            a[l] = null
                        }
                    }
                    return d
                };
                d.fire = function(b, e, f) {
                    if (!b || 3 === b.nodeType || 8 === b.nodeType) return d;
                    f = c(null, f);
                    f.type = e;
                    f.target = b;
                    do(e = b[l]) && a(f, e), b = b.parentNode || b.ownerDocument || b.defaultView || b.parentWindow; while (b && !f.isPropagationStopped());
                    return d
                };
                d.clean = function(a) {
                    var b, c, r = d.unbind;
                    if (!a || 3 === a.nodeType || 8 === a.nodeType) return d;
                    a[l] && r(a);
                    a.getElementsByTagName || (a = a.document);
                    if (a && a.getElementsByTagName)
                        for (r(a),
                            c = a.getElementsByTagName("*"), b = c.length; b--;) a = c[b], a[l] && r(a);
                    return d
                };
                d.destroy = function() {
                    n = {}
                };
                d.cancel = function(a) {
                    a && (a.preventDefault(), a.stopImmediatePropagation());
                    return !1
                }
            }
            var l = /^(?:mouse|contextmenu)|click/,
                v = {
                    keyLocation: 1,
                    layerX: 1,
                    layerY: 1,
                    returnValue: 1,
                    webkitMovementX: 1,
                    webkitMovementY: 1,
                    keyIdentifier: 1
                };
            d.Event = new d;
            d.Event.bind(window, "ready", function() {});
            return d
        });
    z("tinymce/dom/Sizzle", [], function() {
        function k(a, b, c, d) {
            var r, u, e, f, m;
            (b ? b.ownerDocument || b : P) !== z && R(b);
            b = b ||
                z;
            c = c || [];
            if (!a || "string" !== typeof a) return c;
            if (1 !== (f = b.nodeType) && 9 !== f) return [];
            if (L && !d) {
                if (r = xa.exec(a))
                    if (e = r[1])
                        if (9 === f)
                            if ((u = b.getElementById(e)) && u.parentNode) {
                                if (u.id === e) return c.push(u), c
                            } else return c;
                else {
                    if (b.ownerDocument && (u = b.ownerDocument.getElementById(e)) && C(b, u) && u.id === e) return c.push(u), c
                } else {
                    if (r[2]) return J.apply(c, b.getElementsByTagName(a)), c;
                    if ((e = r[3]) && B.getElementsByClassName) return J.apply(c, b.getElementsByClassName(e)), c
                }
                if (B.qsa && (!T || !T.test(a))) {
                    u = r = D;
                    e = b;
                    m =
                        9 === f && a;
                    if (1 === f && "object" !== b.nodeName.toLowerCase()) {
                        f = F(a);
                        (r = b.getAttribute("id")) ? u = r.replace(ya, "\\$\x26"): b.setAttribute("id", u);
                        u = "[id\x3d'" + u + "'] ";
                        for (e = f.length; e--;) f[e] = u + h(f[e]);
                        e = ta.test(a) && v(b.parentNode) || b;
                        m = f.join(",")
                    }
                    if (m) try {
                        return J.apply(c, e.querySelectorAll(m)), c
                    } catch (Da) {} finally {
                        r || b.removeAttribute("id")
                    }
                }
            }
            return W(a.replace(ja, "$1"), b, c, d)
        }

        function g() {
            function a(c, d) {
                b.push(c + " ") > x.cacheLength && delete a[b.shift()];
                return a[c + " "] = d
            }
            var b = [];
            return a
        }

        function f(a) {
            a[D] = !0;
            return a
        }

        function e(a) {
            var b = z.createElement("div");
            try {
                return !!a(b)
            } catch (wa) {
                return !1
            } finally {
                b.parentNode && b.parentNode.removeChild(b)
            }
        }

        function a(a, b) {
            var c = a.split("|");
            for (a = a.length; a--;) x.attrHandle[c[a]] = b
        }

        function c(a, b) {
            var c = b && a,
                d = c && 1 === a.nodeType && 1 === b.nodeType && (~b.sourceIndex || -2147483648) - (~a.sourceIndex || -2147483648);
            if (d) return d;
            if (c)
                for (; c = c.nextSibling;)
                    if (c === b) return -1;
            return a ? 1 : -1
        }

        function b(a) {
            return function(b) {
                return "input" === b.nodeName.toLowerCase() && b.type === a
            }
        }

        function d(a) {
            return function(b) {
                var c = b.nodeName.toLowerCase();
                return ("input" === c || "button" === c) && b.type === a
            }
        }

        function l(a) {
            return f(function(b) {
                b = +b;
                return f(function(c, d) {
                    for (var r, u = a([], c.length, b), e = u.length; e--;) c[r = u[e]] && (c[r] = !(d[r] = c[r]))
                })
            })
        }

        function v(a) {
            return a && typeof a.getElementsByTagName !== ca && a
        }

        function q() {}

        function h(a) {
            for (var b = 0, c = a.length, d = ""; b < c; b++) d += a[b].value;
            return d
        }

        function n(a, b, c) {
            var d = b.dir,
                r = c && "parentNode" === d,
                u = Q++;
            return b.first ? function(b, c, u) {
                for (; b = b[d];)
                    if (1 ===
                        b.nodeType || r) return a(b, c, u)
            } : function(b, c, e) {
                var f, m, h = [da, u];
                if (e)
                    for (; b = b[d];) {
                        if ((1 === b.nodeType || r) && a(b, c, e)) return !0
                    } else
                        for (; b = b[d];)
                            if (1 === b.nodeType || r) {
                                m = b[D] || (b[D] = {});
                                if ((f = m[d]) && f[0] === da && f[1] === u) return h[2] = f[2];
                                m[d] = h;
                                if (h[2] = a(b, c, e)) return !0
                            }
            }
        }

        function p(a) {
            return 1 < a.length ? function(b, c, d) {
                for (var r = a.length; r--;)
                    if (!a[r](b, c, d)) return !1;
                return !0
            } : a[0]
        }

        function w(a, b, c, d, r) {
            for (var u, e = [], f = 0, m = a.length, h = null != b; f < m; f++)
                if (u = a[f])
                    if (!c || c(u, d, r)) e.push(u), h && b.push(f);
            return e
        }

        function A(a, b, c, d, r, u) {
            d && !d[D] && (d = A(d));
            r && !r[D] && (r = A(r, u));
            return f(function(u, e, f, m) {
                var h, p, n = [],
                    l = [],
                    F = e.length,
                    t;
                if (!(t = u)) {
                    t = b || "*";
                    for (var q = f.nodeType ? [f] : f, x = [], y = 0, A = q.length; y < A; y++) k(t, q[y], x);
                    t = x
                }
                t = !a || !u && b ? t : w(t, n, a, f, m);
                q = c ? r || (u ? a : F || d) ? [] : e : t;
                c && c(t, q, f, m);
                if (d)
                    for (h = w(q, l), d(h, [], f, m), f = h.length; f--;)
                        if (p = h[f]) q[l[f]] = !(t[l[f]] = p);
                if (u) {
                    if (r || a) {
                        if (r) {
                            h = [];
                            for (f = q.length; f--;)(p = q[f]) && h.push(t[f] = p);
                            r(null, q = [], h, m)
                        }
                        for (f = q.length; f--;)(p = q[f]) && -1 < (h = r ? E.call(u, p) : n[f]) &&
                            (u[h] = !(e[h] = p))
                    }
                } else q = w(q === e ? q.splice(F, q.length) : q), r ? r(null, e, q, m) : J.apply(e, q)
            })
        }

        function m(a) {
            var b, c, d, r = a.length,
                u = x.relative[a[0].type];
            c = u || x.relative[" "];
            for (var e = u ? 1 : 0, f = n(function(a) {
                    return a === b
                }, c, !0), l = n(function(a) {
                    return -1 < E.call(b, a)
                }, c, !0), F = [function(a, c, d) {
                    return !u && (d || c !== U) || ((b = c).nodeType ? f(a, c, d) : l(a, c, d))
                }]; e < r; e++)
                if (c = x.relative[a[e].type]) F = [n(p(F), c)];
                else {
                    c = x.filter[a[e].type].apply(null, a[e].matches);
                    if (c[D]) {
                        for (d = ++e; d < r && !x.relative[a[d].type]; d++);
                        return A(1 <
                            e && p(F), 1 < e && h(a.slice(0, e - 1).concat({
                                value: " " === a[e - 2].type ? "*" : ""
                            })).replace(ja, "$1"), c, e < d && m(a.slice(e, d)), d < r && m(a = a.slice(d)), d < r && h(a))
                    }
                    F.push(c)
                }
            return p(F)
        }

        function t(a, b) {
            var c = 0 < b.length,
                d = 0 < a.length,
                r = function(r, u, e, f, m) {
                    var h, p, n, l = 0,
                        F = "0",
                        q = r && [],
                        t = [],
                        y = U,
                        A = r || d && x.find.TAG("*", m),
                        B = da += null == y ? 1 : Math.random() || .1,
                        v = A.length;
                    for (m && (U = u !== z && u); F !== v && null != (h = A[F]); F++) {
                        if (d && h) {
                            for (p = 0; n = a[p++];)
                                if (n(h, u, e)) {
                                    f.push(h);
                                    break
                                }
                            m && (da = B)
                        }
                        c && ((h = !n && h) && l--, r && q.push(h))
                    }
                    l += F;
                    if (c && F !==
                        l) {
                        for (p = 0; n = b[p++];) n(q, t, u, e);
                        if (r) {
                            if (0 < l)
                                for (; F--;) q[F] || t[F] || (t[F] = ia.call(f));
                            t = w(t)
                        }
                        J.apply(f, t);
                        m && !r && 0 < t.length && 1 < l + b.length && k.uniqueSort(f)
                    }
                    m && (da = B, U = y);
                    return q
                };
            return c ? f(r) : r
        }
        var y, B, x, r, u, F, O, W, U, G, N, R, z, H, L, T, ha, M, C, D = "sizzle" + -new Date,
            P = window.document,
            da = 0,
            Q = 0,
            Z = g(),
            aa = g(),
            X = g(),
            V = function(a, b) {
                a === b && (N = !0);
                return 0
            },
            ca = typeof K,
            ea = {}.hasOwnProperty,
            fa = [],
            ia = fa.pop,
            ba = fa.push,
            J = fa.push,
            S = fa.slice,
            E = fa.indexOf || function(a) {
                for (var b = 0, c = this.length; b < c; b++)
                    if (this[b] === a) return b;
                return -1
            },
            ja = /^[\x20\t\r\n\f]+|((?:^|[^\\])(?:\\.)*)[\x20\t\r\n\f]+$/g,
            na = /^[\x20\t\r\n\f]*,[\x20\t\r\n\f]*/,
            pa = /^[\x20\t\r\n\f]*([>+~]|[\x20\t\r\n\f])[\x20\t\r\n\f]*/,
            qa = /=[\x20\t\r\n\f]*([^\]'"]*?)[\x20\t\r\n\f]*\]/g,
            ua = /:((?:\\.|[\w-]|[^\x00-\xa0])+)(?:\((('((?:\\.|[^\\'])*)'|"((?:\\.|[^\\"])*)")|((?:\\.|[^\\()[\]]|\[[\x20\t\r\n\f]*((?:\\.|[\w-]|[^\x00-\xa0])+)(?:[\x20\t\r\n\f]*([*^$|!~]?=)[\x20\t\r\n\f]*(?:'((?:\\.|[^\\'])*)'|"((?:\\.|[^\\"])*)"|((?:\\.|[\w-]|[^\x00-\xa0])+))|)[\x20\t\r\n\f]*\])*)|.*)\)|)/,
            ra = /^(?:\\.|[\w-]|[^\x00-\xa0])+$/,
            sa = {
                ID: /^#((?:\\.|[\w-]|[^\x00-\xa0])+)/,
                CLASS: /^\.((?:\\.|[\w-]|[^\x00-\xa0])+)/,
                TAG: /^((?:\\.|[\w-]|[^\x00-\xa0])+|[*])/,
                ATTR: /^\[[\x20\t\r\n\f]*((?:\\.|[\w-]|[^\x00-\xa0])+)(?:[\x20\t\r\n\f]*([*^$|!~]?=)[\x20\t\r\n\f]*(?:'((?:\\.|[^\\'])*)'|"((?:\\.|[^\\"])*)"|((?:\\.|[\w-]|[^\x00-\xa0])+))|)[\x20\t\r\n\f]*\]/,
                PSEUDO: /^:((?:\\.|[\w-]|[^\x00-\xa0])+)(?:\((('((?:\\.|[^\\'])*)'|"((?:\\.|[^\\"])*)")|((?:\\.|[^\\()[\]]|\[[\x20\t\r\n\f]*((?:\\.|[\w-]|[^\x00-\xa0])+)(?:[\x20\t\r\n\f]*([*^$|!~]?=)[\x20\t\r\n\f]*(?:'((?:\\.|[^\\'])*)'|"((?:\\.|[^\\"])*)"|((?:\\.|[\w-]|[^\x00-\xa0])+))|)[\x20\t\r\n\f]*\])*)|.*)\)|)/,
                CHILD: /^:(only|first|last|nth|nth-last)-(child|of-type)(?:\([\x20\t\r\n\f]*(even|odd|(([+-]|)(\d*)n|)[\x20\t\r\n\f]*(?:([+-]|)[\x20\t\r\n\f]*(\d+)|))[\x20\t\r\n\f]*\)|)/i,
                bool: /^(?:checked|selected|async|autofocus|autoplay|controls|defer|disabled|hidden|ismap|loop|multiple|open|readonly|required|scoped)$/i,
                needsContext: /^[\x20\t\r\n\f]*[>+~]|:(even|odd|eq|gt|lt|nth|first|last)(?:\([\x20\t\r\n\f]*((?:-\d)?\d*)[\x20\t\r\n\f]*\)|)(?=[^-]|$)/i
            },
            za = /^(?:input|select|textarea|button)$/i,
            va = /^h\d$/i,
            oa =
            /^[^{]+\{\s*\[native \w/,
            xa = /^(?:#([\w-]+)|(\w+)|\.([\w-]+))$/,
            ta = /[+~]/,
            ya = /'|\\/g,
            la = /\\([\da-f]{1,6}[\x20\t\r\n\f]?|([\x20\t\r\n\f])|.)/ig,
            ma = function(a, b, c) {
                a = "0x" + b - 65536;
                return a !== a || c ? b : 0 > a ? String.fromCharCode(a + 65536) : String.fromCharCode(a >> 10 | 55296, a & 1023 | 56320)
            };
        try {
            J.apply(fa = S.call(P.childNodes), P.childNodes), fa[P.childNodes.length].nodeType
        } catch (Aa) {
            J = {
                apply: fa.length ? function(a, b) {
                    ba.apply(a, S.call(b))
                } : function(a, b) {
                    for (var c = a.length, d = 0; a[c++] = b[d++];);
                    a.length = c - 1
                }
            }
        }
        B = k.support = {};
        u = k.isXML = function(a) {
            return (a = a && (a.ownerDocument || a).documentElement) ? "HTML" !== a.nodeName : !1
        };
        R = k.setDocument = function(a) {
            var b, d = a ? a.ownerDocument || a : P;
            a = d.defaultView;
            if (d === z || 9 !== d.nodeType || !d.documentElement) return z;
            z = d;
            H = d.documentElement;
            L = !u(d);
            var r;
            if (r = a) {
                a: {
                    try {
                        b = a.top;
                        break a
                    } catch (Ca) {}
                    b = null
                }
                r = a !== b
            }
            r && (a.addEventListener ? a.addEventListener("unload", function() {
                R()
            }, !1) : a.attachEvent && a.attachEvent("onunload", function() {
                R()
            }));
            B.attributes = e(function(a) {
                a.className = "i";
                return !a.getAttribute("className")
            });
            B.getElementsByTagName = e(function(a) {
                a.appendChild(d.createComment(""));
                return !a.getElementsByTagName("*").length
            });
            B.getElementsByClassName = oa.test(d.getElementsByClassName);
            B.getById = e(function(a) {
                H.appendChild(a).id = D;
                return !d.getElementsByName || !d.getElementsByName(D).length
            });
            B.getById ? (x.find.ID = function(a, b) {
                    if (typeof b.getElementById !== ca && L) return (a = b.getElementById(a)) && a.parentNode ? [a] : []
                }, x.filter.ID = function(a) {
                    var b = a.replace(la, ma);
                    return function(a) {
                        return a.getAttribute("id") === b
                    }
                }) :
                (delete x.find.ID, x.filter.ID = function(a) {
                    var b = a.replace(la, ma);
                    return function(a) {
                        return (a = typeof a.getAttributeNode !== ca && a.getAttributeNode("id")) && a.value === b
                    }
                });
            x.find.TAG = B.getElementsByTagName ? function(a, b) {
                if (typeof b.getElementsByTagName !== ca) return b.getElementsByTagName(a)
            } : function(a, b) {
                var c = [],
                    d = 0;
                b = b.getElementsByTagName(a);
                if ("*" === a) {
                    for (; a = b[d++];) 1 === a.nodeType && c.push(a);
                    return c
                }
                return b
            };
            x.find.CLASS = B.getElementsByClassName && function(a, b) {
                if (L) return b.getElementsByClassName(a)
            };
            ha = [];
            T = [];
            if (B.qsa = oa.test(d.querySelectorAll)) e(function(a) {
                a.innerHTML = "\x3cselect msallowcapture\x3d''\x3e\x3coption selected\x3d''\x3e\x3c/option\x3e\x3c/select\x3e";
                a.querySelectorAll("[msallowcapture^\x3d'']").length && T.push("[*^$]\x3d[\\x20\\t\\r\\n\\f]*(?:''|\"\")");
                a.querySelectorAll("[selected]").length || T.push("\\[[\\x20\\t\\r\\n\\f]*(?:value|checked|selected|async|autofocus|autoplay|controls|defer|disabled|hidden|ismap|loop|multiple|open|readonly|required|scoped)");
                a.querySelectorAll(":checked").length ||
                    T.push(":checked")
            }), e(function(a) {
                var b = d.createElement("input");
                b.setAttribute("type", "hidden");
                a.appendChild(b).setAttribute("name", "D");
                a.querySelectorAll("[name\x3dd]").length && T.push("name[\\x20\\t\\r\\n\\f]*[*^$|!~]?\x3d");
                a.querySelectorAll(":enabled").length || T.push(":enabled", ":disabled");
                a.querySelectorAll("*,:x");
                T.push(",.*:")
            });
            (B.matchesSelector = oa.test(M = H.matches || H.webkitMatchesSelector || H.mozMatchesSelector || H.oMatchesSelector || H.msMatchesSelector)) && e(function(a) {
                B.disconnectedMatch =
                    M.call(a, "div");
                M.call(a, "[s!\x3d'']:x");
                ha.push("!\x3d", ":((?:\\\\.|[\\w-]|[^\\x00-\\xa0])+)(?:\\((('((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\")|((?:\\\\.|[^\\\\()[\\]]|\\[[\\x20\\t\\r\\n\\f]*((?:\\\\.|[\\w-]|[^\\x00-\\xa0])+)(?:[\\x20\\t\\r\\n\\f]*([*^$|!~]?\x3d)[\\x20\\t\\r\\n\\f]*(?:'((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\"|((?:\\\\.|[\\w-]|[^\\x00-\\xa0])+))|)[\\x20\\t\\r\\n\\f]*\\])*)|.*)\\)|)")
            });
            T = T.length && new RegExp(T.join("|"));
            ha = ha.length && new RegExp(ha.join("|"));
            C = (b =
                oa.test(H.compareDocumentPosition)) || oa.test(H.contains) ? function(a, b) {
                var c = 9 === a.nodeType ? a.documentElement : a;
                b = b && b.parentNode;
                return a === b || !!(b && 1 === b.nodeType && (c.contains ? c.contains(b) : a.compareDocumentPosition && a.compareDocumentPosition(b) & 16))
            } : function(a, b) {
                if (b)
                    for (; b = b.parentNode;)
                        if (b === a) return !0;
                return !1
            };
            V = b ? function(a, b) {
                if (a === b) return N = !0, 0;
                var c = !a.compareDocumentPosition - !b.compareDocumentPosition;
                if (c) return c;
                c = (a.ownerDocument || a) === (b.ownerDocument || b) ? a.compareDocumentPosition(b) :
                    1;
                return c & 1 || !B.sortDetached && b.compareDocumentPosition(a) === c ? a === d || a.ownerDocument === P && C(P, a) ? -1 : b === d || b.ownerDocument === P && C(P, b) ? 1 : G ? E.call(G, a) - E.call(G, b) : 0 : c & 4 ? -1 : 1
            } : function(a, b) {
                if (a === b) return N = !0, 0;
                var r = 0,
                    u = a.parentNode,
                    e = b.parentNode,
                    f = [a],
                    m = [b];
                if (!u || !e) return a === d ? -1 : b === d ? 1 : u ? -1 : e ? 1 : G ? E.call(G, a) - E.call(G, b) : 0;
                if (u === e) return c(a, b);
                for (; a = a.parentNode;) f.unshift(a);
                for (a = b; a = a.parentNode;) m.unshift(a);
                for (; f[r] === m[r];) r++;
                return r ? c(f[r], m[r]) : f[r] === P ? -1 : m[r] === P ? 1 : 0
            };
            return d
        };
        k.matches = function(a, b) {
            return k(a, null, null, b)
        };
        k.matchesSelector = function(a, b) {
            (a.ownerDocument || a) !== z && R(a);
            b = b.replace(qa, "\x3d'$1']");
            if (!(!B.matchesSelector || !L || ha && ha.test(b) || T && T.test(b))) try {
                var c = M.call(a, b);
                if (c || B.disconnectedMatch || a.document && 11 !== a.document.nodeType) return c
            } catch (Ba) {}
            return 0 < k(b, z, null, [a]).length
        };
        k.contains = function(a, b) {
            (a.ownerDocument || a) !== z && R(a);
            return C(a, b)
        };
        k.attr = function(a, b) {
            (a.ownerDocument || a) !== z && R(a);
            var c = x.attrHandle[b.toLowerCase()],
                c = c &&
                ea.call(x.attrHandle, b.toLowerCase()) ? c(a, b, !L) : K;
            return c !== K ? c : B.attributes || !L ? a.getAttribute(b) : (c = a.getAttributeNode(b)) && c.specified ? c.value : null
        };
        k.error = function(a) {
            throw Error("Syntax error, unrecognized expression: " + a);
        };
        k.uniqueSort = function(a) {
            var b, c = [],
                d = 0,
                r = 0;
            N = !B.detectDuplicates;
            G = !B.sortStable && a.slice(0);
            a.sort(V);
            if (N) {
                for (; b = a[r++];) b === a[r] && (d = c.push(r));
                for (; d--;) a.splice(c[d], 1)
            }
            G = null;
            return a
        };
        r = k.getText = function(a) {
            var b, c = "",
                d = 0;
            b = a.nodeType;
            if (!b)
                for (; b = a[d++];) c +=
                    r(b);
            else if (1 === b || 9 === b || 11 === b) {
                if ("string" === typeof a.textContent) return a.textContent;
                for (a = a.firstChild; a; a = a.nextSibling) c += r(a)
            } else if (3 === b || 4 === b) return a.nodeValue;
            return c
        };
        x = k.selectors = {
            cacheLength: 50,
            createPseudo: f,
            match: sa,
            attrHandle: {},
            find: {},
            relative: {
                "\x3e": {
                    dir: "parentNode",
                    first: !0
                },
                " ": {
                    dir: "parentNode"
                },
                "+": {
                    dir: "previousSibling",
                    first: !0
                },
                "~": {
                    dir: "previousSibling"
                }
            },
            preFilter: {
                ATTR: function(a) {
                    a[1] = a[1].replace(la, ma);
                    a[3] = (a[3] || a[4] || a[5] || "").replace(la, ma);
                    "~\x3d" ===
                    a[2] && (a[3] = " " + a[3] + " ");
                    return a.slice(0, 4)
                },
                CHILD: function(a) {
                    a[1] = a[1].toLowerCase();
                    "nth" === a[1].slice(0, 3) ? (a[3] || k.error(a[0]), a[4] = +(a[4] ? a[5] + (a[6] || 1) : 2 * ("even" === a[3] || "odd" === a[3])), a[5] = +(a[7] + a[8] || "odd" === a[3])) : a[3] && k.error(a[0]);
                    return a
                },
                PSEUDO: function(a) {
                    var b, c = !a[6] && a[2];
                    if (sa.CHILD.test(a[0])) return null;
                    a[3] ? a[2] = a[4] || a[5] || "" : c && ua.test(c) && (b = F(c, !0)) && (b = c.indexOf(")", c.length - b) - c.length) && (a[0] = a[0].slice(0, b), a[2] = c.slice(0, b));
                    return a.slice(0, 3)
                }
            },
            filter: {
                TAG: function(a) {
                    var b =
                        a.replace(la, ma).toLowerCase();
                    return "*" === a ? function() {
                        return !0
                    } : function(a) {
                        return a.nodeName && a.nodeName.toLowerCase() === b
                    }
                },
                CLASS: function(a) {
                    var b = Z[a + " "];
                    return b || (b = new RegExp("(^|[\\x20\\t\\r\\n\\f])" + a + "([\\x20\\t\\r\\n\\f]|$)"), Z(a, function(a) {
                        return b.test("string" === typeof a.className && a.className || typeof a.getAttribute !== ca && a.getAttribute("class") || "")
                    }))
                },
                ATTR: function(a, b, c) {
                    return function(d) {
                        d = k.attr(d, a);
                        if (null == d) return "!\x3d" === b;
                        if (!b) return !0;
                        d += "";
                        return "\x3d" === b ? d === c :
                            "!\x3d" === b ? d !== c : "^\x3d" === b ? c && 0 === d.indexOf(c) : "*\x3d" === b ? c && -1 < d.indexOf(c) : "$\x3d" === b ? c && d.slice(-c.length) === c : "~\x3d" === b ? -1 < (" " + d + " ").indexOf(c) : "|\x3d" === b ? d === c || d.slice(0, c.length + 1) === c + "-" : !1
                    }
                },
                CHILD: function(a, b, c, d, r) {
                    var u = "nth" !== a.slice(0, 3),
                        e = "last" !== a.slice(-4),
                        f = "of-type" === b;
                    return 1 === d && 0 === r ? function(a) {
                        return !!a.parentNode
                    } : function(b, c, m) {
                        var h, p, n, l, F;
                        c = u !== e ? "nextSibling" : "previousSibling";
                        var w = b.parentNode,
                            q = f && b.nodeName.toLowerCase();
                        m = !m && !f;
                        if (w) {
                            if (u) {
                                for (; c;) {
                                    for (p =
                                        b; p = p[c];)
                                        if (f ? p.nodeName.toLowerCase() === q : 1 === p.nodeType) return !1;
                                    F = c = "only" === a && !F && "nextSibling"
                                }
                                return !0
                            }
                            F = [e ? w.firstChild : w.lastChild];
                            if (e && m)
                                for (m = w[D] || (w[D] = {}), h = m[a] || [], l = h[0] === da && h[1], n = h[0] === da && h[2], p = l && w.childNodes[l]; p = ++l && p && p[c] || (n = l = 0) || F.pop();) {
                                    if (1 === p.nodeType && ++n && p === b) {
                                        m[a] = [da, l, n];
                                        break
                                    }
                                } else if (m && (h = (b[D] || (b[D] = {}))[a]) && h[0] === da) n = h[1];
                                else
                                    for (;
                                        (p = ++l && p && p[c] || (n = l = 0) || F.pop()) && ((f ? p.nodeName.toLowerCase() !== q : 1 !== p.nodeType) || !++n || (m && ((p[D] || (p[D] = {}))[a] = [da, n]), p !== b)););
                            n -= r;
                            return n === d || 0 === n % d && 0 <= n / d
                        }
                    }
                },
                PSEUDO: function(a, b) {
                    var c, d = x.pseudos[a] || x.setFilters[a.toLowerCase()] || k.error("unsupported pseudo: " + a);
                    return d[D] ? d(b) : 1 < d.length ? (c = [a, a, "", b], x.setFilters.hasOwnProperty(a.toLowerCase()) ? f(function(a, c) {
                        for (var r, u = d(a, b), e = u.length; e--;) r = E.call(a, u[e]), a[r] = !(c[r] = u[e])
                    }) : function(a) {
                        return d(a, 0, c)
                    }) : d
                }
            },
            pseudos: {
                not: f(function(a) {
                    var b = [],
                        c = [],
                        d = O(a.replace(ja, "$1"));
                    return d[D] ? f(function(a, b, c, r) {
                        r = d(a, null, r, []);
                        for (var u =
                                a.length; u--;)
                            if (c = r[u]) a[u] = !(b[u] = c)
                    }) : function(a, r, u) {
                        b[0] = a;
                        d(b, null, u, c);
                        return !c.pop()
                    }
                }),
                has: f(function(a) {
                    return function(b) {
                        return 0 < k(a, b).length
                    }
                }),
                contains: f(function(a) {
                    a = a.replace(la, ma);
                    return function(b) {
                        return -1 < (b.textContent || b.innerText || r(b)).indexOf(a)
                    }
                }),
                lang: f(function(a) {
                    ra.test(a || "") || k.error("unsupported lang: " + a);
                    a = a.replace(la, ma).toLowerCase();
                    return function(b) {
                        var c;
                        do
                            if (c = L ? b.lang : b.getAttribute("xml:lang") || b.getAttribute("lang")) return c = c.toLowerCase(), c === a ||
                                0 === c.indexOf(a + "-"); while ((b = b.parentNode) && 1 === b.nodeType);
                        return !1
                    }
                }),
                target: function(a) {
                    var b = window.location && window.location.hash;
                    return b && b.slice(1) === a.id
                },
                root: function(a) {
                    return a === H
                },
                focus: function(a) {
                    return a === z.activeElement && (!z.hasFocus || z.hasFocus()) && !!(a.type || a.href || ~a.tabIndex)
                },
                enabled: function(a) {
                    return !1 === a.disabled
                },
                disabled: function(a) {
                    return !0 === a.disabled
                },
                checked: function(a) {
                    var b = a.nodeName.toLowerCase();
                    return "input" === b && !!a.checked || "option" === b && !!a.selected
                },
                selected: function(a) {
                    a.parentNode && a.parentNode.selectedIndex;
                    return !0 === a.selected
                },
                empty: function(a) {
                    for (a = a.firstChild; a; a = a.nextSibling)
                        if (6 > a.nodeType) return !1;
                    return !0
                },
                parent: function(a) {
                    return !x.pseudos.empty(a)
                },
                header: function(a) {
                    return va.test(a.nodeName)
                },
                input: function(a) {
                    return za.test(a.nodeName)
                },
                button: function(a) {
                    var b = a.nodeName.toLowerCase();
                    return "input" === b && "button" === a.type || "button" === b
                },
                text: function(a) {
                    var b;
                    return "input" === a.nodeName.toLowerCase() && "text" === a.type && (null ==
                        (b = a.getAttribute("type")) || "text" === b.toLowerCase())
                },
                first: l(function() {
                    return [0]
                }),
                last: l(function(a, b) {
                    return [b - 1]
                }),
                eq: l(function(a, b, c) {
                    return [0 > c ? c + b : c]
                }),
                even: l(function(a, b) {
                    for (var c = 0; c < b; c += 2) a.push(c);
                    return a
                }),
                odd: l(function(a, b) {
                    for (var c = 1; c < b; c += 2) a.push(c);
                    return a
                }),
                lt: l(function(a, b, c) {
                    for (b = 0 > c ? c + b : c; 0 <= --b;) a.push(b);
                    return a
                }),
                gt: l(function(a, b, c) {
                    for (c = 0 > c ? c + b : c; ++c < b;) a.push(c);
                    return a
                })
            }
        };
        x.pseudos.nth = x.pseudos.eq;
        for (y in {
                radio: !0,
                checkbox: !0,
                file: !0,
                password: !0,
                image: !0
            }) x.pseudos[y] =
            b(y);
        for (y in {
                submit: !0,
                reset: !0
            }) x.pseudos[y] = d(y);
        q.prototype = x.filters = x.pseudos;
        x.setFilters = new q;
        F = k.tokenize = function(a, b) {
            var c, d, r, u, e, f, m;
            if (e = aa[a + " "]) return b ? 0 : e.slice(0);
            e = a;
            f = [];
            for (m = x.preFilter; e;) {
                if (!c || (d = na.exec(e))) d && (e = e.slice(d[0].length) || e), f.push(r = []);
                c = !1;
                if (d = pa.exec(e)) c = d.shift(), r.push({
                    value: c,
                    type: d[0].replace(ja, " ")
                }), e = e.slice(c.length);
                for (u in x.filter) !(d = sa[u].exec(e)) || m[u] && !(d = m[u](d)) || (c = d.shift(), r.push({
                    value: c,
                    type: u,
                    matches: d
                }), e = e.slice(c.length));
                if (!c) break
            }
            return b ? e.length : e ? k.error(a) : aa(a, f).slice(0)
        };
        O = k.compile = function(a, b) {
            var c, d = [],
                r = [],
                u = X[a + " "];
            if (!u) {
                b || (b = F(a));
                for (c = b.length; c--;) u = m(b[c]), u[D] ? d.push(u) : r.push(u);
                u = X(a, t(r, d));
                u.selector = a
            }
            return u
        };
        W = k.select = function(a, b, c, d) {
            var r, u, e, f, m = "function" === typeof a && a,
                p = !d && F(a = m.selector || a);
            c = c || [];
            if (1 === p.length) {
                u = p[0] = p[0].slice(0);
                if (2 < u.length && "ID" === (e = u[0]).type && B.getById && 9 === b.nodeType && L && x.relative[u[1].type]) {
                    b = (x.find.ID(e.matches[0].replace(la, ma), b) || [])[0];
                    if (!b) return c;
                    m && (b = b.parentNode);
                    a = a.slice(u.shift().value.length)
                }
                for (r = sa.needsContext.test(a) ? 0 : u.length; r--;) {
                    e = u[r];
                    if (x.relative[f = e.type]) break;
                    if (f = x.find[f])
                        if (d = f(e.matches[0].replace(la, ma), ta.test(u[0].type) && v(b.parentNode) || b)) {
                            u.splice(r, 1);
                            a = d.length && h(u);
                            if (!a) return J.apply(c, d), c;
                            break
                        }
                }
            }(m || O(a, p))(d, b, !L, c, ta.test(a) && v(b.parentNode) || b);
            return c
        };
        B.sortStable = D.split("").sort(V).join("") === D;
        B.detectDuplicates = !!N;
        R();
        B.sortDetached = e(function(a) {
            return a.compareDocumentPosition(z.createElement("div")) &
                1
        });
        e(function(a) {
            a.innerHTML = "\x3ca href\x3d'#'\x3e\x3c/a\x3e";
            return "#" === a.firstChild.getAttribute("href")
        }) || a("type|href|height|width", function(a, b, c) {
            if (!c) return a.getAttribute(b, "type" === b.toLowerCase() ? 1 : 2)
        });
        B.attributes && e(function(a) {
            a.innerHTML = "\x3cinput/\x3e";
            a.firstChild.setAttribute("value", "");
            return "" === a.firstChild.getAttribute("value")
        }) || a("value", function(a, b, c) {
            if (!c && "input" === a.nodeName.toLowerCase()) return a.defaultValue
        });
        e(function(a) {
                return null == a.getAttribute("disabled")
            }) ||
            a("checked|selected|async|autofocus|autoplay|controls|defer|disabled|hidden|ismap|loop|multiple|open|readonly|required|scoped", function(a, b, c) {
                var d;
                if (!c) return !0 === a[b] ? b.toLowerCase() : (d = a.getAttributeNode(b)) && d.specified ? d.value : null
            });
        return k
    });
    z("tinymce/util/Arr", [], function() {
        function k(e, a, c) {
            var b, d;
            if (!e) return 0;
            c = c || e;
            if (e.length !== K)
                for (b = 0, d = e.length; b < d; b++) {
                    if (!1 === a.call(c, e[b], b, e)) return 0
                } else
                    for (b in e)
                        if (e.hasOwnProperty(b) && !1 === a.call(c, e[b], b, e)) return 0;
            return 1
        }

        function g(e,
            a, c) {
            var b, d;
            b = 0;
            for (d = e.length; b < d; b++)
                if (a.call(c, e[b], b, e)) return b;
            return -1
        }
        var f = Array.isArray || function(e) {
            return "[object Array]" === Object.prototype.toString.call(e)
        };
        return {
            isArray: f,
            toArray: function(e) {
                var a = e,
                    c, b;
                if (!f(e))
                    for (a = [], c = 0, b = e.length; c < b; c++) a[c] = e[c];
                return a
            },
            each: k,
            map: function(e, a) {
                var c = [];
                k(e, function(b, d) {
                    c.push(a(b, d, e))
                });
                return c
            },
            filter: function(e, a) {
                var c = [];
                k(e, function(b, d) {
                    a && !a(b, d, e) || c.push(b)
                });
                return c
            },
            indexOf: function(e, a) {
                var c, b;
                if (e)
                    for (c = 0, b = e.length; c <
                        b; c++)
                        if (e[c] === a) return c;
                return -1
            },
            reduce: function(e, a, c, b) {
                var d = 0;
                for (3 > arguments.length && (c = e[0]); d < e.length; d++) c = a.call(b, c, e[d], d);
                return c
            },
            findIndex: g,
            find: function(e, a, c) {
                a = g(e, a, c);
                return -1 !== a ? e[a] : K
            },
            last: function(e) {
                return e[e.length - 1]
            }
        }
    });
    z("tinymce/util/Tools", ["tinymce/Env", "tinymce/util/Arr"], function(k, g) {
        function f(a) {
            return null === a || a === K ? "" : ("" + a).replace(c, "")
        }

        function e(a, c) {
            return c ? "array" == c && g.isArray(a) ? !0 : typeof a == c : a !== K
        }

        function a(b, c, e, f) {
            f = f || this;
            b && (e && (b = b[e]),
                g.each(b, function(b, d) {
                    if (!1 === c.call(f, b, d, e)) return !1;
                    a(b, c, e, f)
                }))
        }
        var c = /^\s*|\s*$/g;
        return {
            trim: f,
            isArray: g.isArray,
            is: e,
            toArray: g.toArray,
            makeMap: function(a, c, e) {
                a = a || [];
                "string" == typeof a && (a = a.split(c || ","));
                e = e || {};
                for (c = a.length; c--;) e[a[c]] = {};
                return e
            },
            each: g.each,
            map: g.map,
            grep: g.filter,
            inArray: g.indexOf,
            extend: function(a, c) {
                var b, d, e, f = arguments,
                    n;
                b = 1;
                for (d = f.length; b < d; b++)
                    for (e in c = f[b], c) c.hasOwnProperty(e) && (n = c[e], n !== K && (a[e] = n));
                return a
            },
            create: function(a, c, e) {
                var b, d, f, n, p,
                    l = 0;
                a = /^((static) )?([\w.]+)(:([\w.]+))?/.exec(a);
                f = a[3].match(/(^|\.)(\w+)$/i)[2];
                d = this.createNS(a[3].replace(/\.\w+$/, ""), e);
                if (!d[f])
                    if ("static" == a[2]) {
                        if (d[f] = c, this.onCreate) this.onCreate(a[2], a[3], d[f])
                    } else c[f] || (c[f] = function() {}, l = 1), d[f] = c[f], this.extend(d[f].prototype, c), a[5] && (b = this.resolve(a[5]).prototype, n = a[5].match(/\.(\w+)$/i)[1], p = d[f], d[f] = l ? function() {
                        return b[n].apply(this, arguments)
                    } : function() {
                        this.parent = b[n];
                        return p.apply(this, arguments)
                    }, d[f].prototype[f] = d[f], this.each(b,
                        function(a, c) {
                            d[f].prototype[c] = b[c]
                        }), this.each(c, function(a, c) {
                        b[c] ? d[f].prototype[c] = function() {
                            this.parent = b[c];
                            return a.apply(this, arguments)
                        } : c != f && (d[f].prototype[c] = a)
                    })), this.each(c["static"], function(a, b) {
                        d[f][b] = a
                    })
            },
            walk: a,
            createNS: function(a, c) {
                var b, d;
                c = c || window;
                a = a.split(".");
                for (b = 0; b < a.length; b++) d = a[b], c[d] || (c[d] = {}), c = c[d];
                return c
            },
            resolve: function(a, c) {
                var b, d;
                c = c || window;
                a = a.split(".");
                b = 0;
                for (d = a.length; b < d && (c = c[a[b]], c); b++);
                return c
            },
            explode: function(a, c) {
                return !a || e(a,
                    "array") ? a : g.map(a.split(c || ","), f)
            },
            _addCacheSuffix: function(a) {
                var b = k.cacheSuffix;
                b && (a += (-1 === a.indexOf("?") ? "?" : "\x26") + b);
                return a
            }
        }
    });
    z("tinymce/dom/DomQuery", ["tinymce/dom/EventUtils", "tinymce/dom/Sizzle", "tinymce/util/Tools", "tinymce/Env"], function(k, g, f, e) {
        function a(a) {
            return "undefined" !== typeof a
        }

        function c(a, b) {
            var c;
            b = b || y;
            c = b.createElement("div");
            b = b.createDocumentFragment();
            for (c.innerHTML = a; a = c.firstChild;) b.appendChild(a);
            return b
        }

        function b(a, d, r, u) {
            var e;
            if ("string" === typeof d) d =
                c(d, p(a[0]));
            else if (d.length && !d.nodeType) {
                d = v.makeArray(d);
                if (u)
                    for (e = d.length - 1; 0 <= e; e--) b(a, d[e], r, u);
                else
                    for (e = 0; e < d.length; e++) b(a, d[e], r, u);
                return a
            }
            if (d.nodeType)
                for (e = a.length; e--;) r.call(a[e], d);
            return a
        }

        function d(a, b) {
            return a && b && -1 !== (" " + a.className + " ").indexOf(" " + b + " ")
        }

        function l(a, b, c) {
            var d, r;
            b = v(b)[0];
            a.each(function() {
                c && d == this.parentNode || (d = this.parentNode, r = b.cloneNode(!1), this.parentNode.insertBefore(r, this));
                r.appendChild(this)
            });
            return a
        }

        function v(a, b) {
            return new v.fn.init(a,
                b)
        }

        function q(a) {
            return null === a || a === F ? "" : ("" + a).replace(H, "")
        }

        function h(a, b) {
            var c, d, r;
            if (a)
                if (c = a.length, void 0 === c)
                    for (d in a) {
                        if (a.hasOwnProperty(d) && (r = a[d], !1 === b.call(r, d, r))) break
                    } else
                        for (d = 0; d < c && (r = a[d], !1 !== b.call(r, d, r)); d++);
            return a
        }

        function n(a, b) {
            var c = [];
            h(a, function(a, d) {
                b(d, a) && c.push(d)
            });
            return c
        }

        function p(a) {
            return a ? 9 == a.nodeType ? a : a.ownerDocument : y
        }

        function w(a, b, c) {
            var d = [];
            a = a[b];
            for ("string" != typeof c && c instanceof v && (c = c[0]); a && 9 !== a.nodeType;) {
                if (c !== K) {
                    if (a === c) break;
                    if ("string" == typeof c && v(a).is(c)) break
                }
                1 === a.nodeType && d.push(a);
                a = a[b]
            }
            return d
        }

        function A(a, b, c, d) {
            var r = [];
            for (d instanceof v && (d = d[0]); a; a = a[b])
                if (!c || a.nodeType === c) {
                    if (d !== K) {
                        if (a === d) break;
                        if ("string" == typeof d && v(a).is(d)) break
                    }
                    r.push(a)
                }
            return r
        }

        function m(a, b, c) {
            for (a = a[b]; a; a = a[b])
                if (a.nodeType == c) return a;
            return null
        }

        function t(a, b, c) {
            h(c, function(c, d) {
                a[c] = a[c] || {};
                a[c][b] = d
            })
        }
        var y = document,
            B = Array.prototype.push,
            x = Array.prototype.slice,
            r = /^(?:[^#<]*(<[\w\W]+>)[^>]*$|#([\w\-]*)$)/,
            u = k.Event,
            F, O = f.makeMap("children,contents,next,prev"),
            W = f.makeMap("fillOpacity fontWeight lineHeight opacity orphans widows zIndex zoom", " "),
            U = f.makeMap("checked compact declare defer disabled ismap multiple nohref noshade nowrap readonly selected", " "),
            G = {
                "for": "htmlFor",
                "class": "className",
                readonly: "readOnly"
            },
            N = {
                "float": "cssFloat"
            },
            z = {},
            I = {},
            H = /^\s*|\s*$/g;
        v.fn = v.prototype = {
            constructor: v,
            selector: "",
            context: null,
            length: 0,
            init: function(a, b) {
                var d;
                if (!a) return this;
                if (a.nodeType) return this.context =
                    this[0] = a, this.length = 1, this;
                if (b && b.nodeType) this.context = b;
                else {
                    if (b) return v(a).attr(b);
                    this.context = b = document
                }
                if ("string" === typeof a)
                    if (this.selector = a, d = "\x3c" === a.charAt(0) && "\x3e" === a.charAt(a.length - 1) && 3 <= a.length ? [null, a, null] : r.exec(a))
                        if (d[1])
                            for (b = c(a, p(b)).firstChild; b;) B.call(this, b), b = b.nextSibling;
                        else {
                            b = p(b).getElementById(d[2]);
                            if (!b) return this;
                            if (b.id !== d[2]) return this.find(a);
                            this.length = 1;
                            this[0] = b
                        }
                else return v(b).find(a);
                else this.add(a, !1);
                return this
            },
            toArray: function() {
                return f.toArray(this)
            },
            add: function(a, b) {
                if ("string" === typeof a) return this.add(v(a));
                if (!1 !== b)
                    for (a = v.unique(this.toArray().concat(v.makeArray(a))), this.length = a.length, b = 0; b < a.length; b++) this[b] = a[b];
                else B.apply(this, v.makeArray(a));
                return this
            },
            attr: function(b, c) {
                var d = this,
                    r;
                if ("object" === typeof b) h(b, function(a, b) {
                    d.attr(a, b)
                });
                else if (a(c)) this.each(function() {
                    var a;
                    1 === this.nodeType && ((a = z[b]) && a.set ? a.set(this, c) : null === c ? this.removeAttribute(b, 2) : this.setAttribute(b, c, 2))
                });
                else {
                    if (d[0] && 1 === d[0].nodeType) {
                        if ((r =
                                z[b]) && r.get) return r.get(d[0], b);
                        if (U[b]) return d.prop(b) ? b : F;
                        c = d[0].getAttribute(b, 2);
                        null === c && (c = F)
                    }
                    return c
                }
                return d
            },
            removeAttr: function(a) {
                return this.attr(a, null)
            },
            prop: function(b, c) {
                var d = this;
                b = G[b] || b;
                if ("object" === typeof b) h(b, function(a, b) {
                    d.prop(a, b)
                });
                else if (a(c)) this.each(function() {
                    1 == this.nodeType && (this[b] = c)
                });
                else return d[0] && d[0].nodeType && b in d[0] ? d[0][b] : c;
                return d
            },
            css: function(b, c) {
                function d(a) {
                    return a.replace(/-(\D)/g, function(a, b) {
                        return b.toUpperCase()
                    })
                }

                function r(a) {
                    return a.replace(/[A-Z]/g,
                        function(a) {
                            return "-" + a
                        })
                }
                var u = this,
                    e, f;
                if ("object" === typeof b) h(b, function(a, b) {
                    u.css(a, b)
                });
                else if (a(c)) b = d(b), "number" !== typeof c || W[b] || (c += "px"), u.each(function() {
                    var a = this.style;
                    if ((f = I[b]) && f.set) f.set(this, c);
                    else {
                        try {
                            this.style[N[b] || b] = c
                        } catch (Q) {}
                        if (null === c || "" === c) a.removeProperty ? a.removeProperty(r(b)) : a.removeAttribute(b)
                    }
                });
                else {
                    e = u[0];
                    if ((f = I[b]) && f.get) return f.get(e);
                    if (e.ownerDocument.defaultView) try {
                        return e.ownerDocument.defaultView.getComputedStyle(e, null).getPropertyValue(r(b))
                    } catch (da) {
                        return F
                    } else if (e.currentStyle) return e.currentStyle[d(b)]
                }
                return u
            },
            remove: function() {
                for (var a, b = this.length; b--;) a = this[b], u.clean(a), a.parentNode && a.parentNode.removeChild(a);
                return this
            },
            empty: function() {
                for (var a, b = this.length; b--;)
                    for (a = this[b]; a.firstChild;) a.removeChild(a.firstChild);
                return this
            },
            html: function(b) {
                var c;
                if (a(b)) {
                    c = this.length;
                    try {
                        for (; c--;) this[c].innerHTML = b
                    } catch (ha) {
                        v(this[c]).empty().append(b)
                    }
                    return this
                }
                return this[0] ? this[0].innerHTML : ""
            },
            text: function(b) {
                var c;
                if (a(b)) {
                    for (c = this.length; c--;) "innerText" in this[c] ? this[c].innerText =
                        b : this[0].textContent = b;
                    return this
                }
                return this[0] ? this[0].innerText || this[0].textContent : ""
            },
            append: function() {
                return b(this, arguments, function(a) {
                    (1 === this.nodeType || this.host && 1 === this.host.nodeType) && this.appendChild(a)
                })
            },
            prepend: function() {
                return b(this, arguments, function(a) {
                    (1 === this.nodeType || this.host && 1 === this.host.nodeType) && this.insertBefore(a, this.firstChild)
                }, !0)
            },
            before: function() {
                return this[0] && this[0].parentNode ? b(this, arguments, function(a) {
                        this.parentNode.insertBefore(a, this)
                    }) :
                    this
            },
            after: function() {
                return this[0] && this[0].parentNode ? b(this, arguments, function(a) {
                    this.parentNode.insertBefore(a, this.nextSibling)
                }, !0) : this
            },
            appendTo: function(a) {
                v(a).append(this);
                return this
            },
            prependTo: function(a) {
                v(a).prepend(this);
                return this
            },
            replaceWith: function(a) {
                return this.before(a).remove()
            },
            wrap: function(a) {
                return l(this, a)
            },
            wrapAll: function(a) {
                return l(this, a, !0)
            },
            wrapInner: function(a) {
                this.each(function() {
                    v(this).contents().wrapAll(a)
                });
                return this
            },
            unwrap: function() {
                return this.parent().each(function() {
                    v(this).replaceWith(this.childNodes)
                })
            },
            clone: function() {
                var a = [];
                this.each(function() {
                    a.push(this.cloneNode(!0))
                });
                return v(a)
            },
            addClass: function(a) {
                return this.toggleClass(a, !0)
            },
            removeClass: function(a) {
                return this.toggleClass(a, !1)
            },
            toggleClass: function(a, b) {
                var c = this;
                if ("string" != typeof a) return c; - 1 !== a.indexOf(" ") ? h(a.split(" "), function() {
                    c.toggleClass(this, b)
                }) : c.each(function(c, r) {
                    var u;
                    u = d(r, a);
                    u !== b && (c = r.className, r.className = u ? q((" " + c + " ").replace(" " + a + " ", " ")) : r.className + (c ? " " + a : a))
                });
                return c
            },
            hasClass: function(a) {
                return d(this[0],
                    a)
            },
            each: function(a) {
                return h(this, a)
            },
            on: function(a, b) {
                return this.each(function() {
                    u.bind(this, a, b)
                })
            },
            off: function(a, b) {
                return this.each(function() {
                    u.unbind(this, a, b)
                })
            },
            trigger: function(a) {
                return this.each(function() {
                    "object" == typeof a ? u.fire(this, a.type, a) : u.fire(this, a)
                })
            },
            show: function() {
                return this.css("display", "")
            },
            hide: function() {
                return this.css("display", "none")
            },
            slice: function() {
                return new v(x.apply(this, arguments))
            },
            eq: function(a) {
                return -1 === a ? this.slice(a) : this.slice(a, +a + 1)
            },
            first: function() {
                return this.eq(0)
            },
            last: function() {
                return this.eq(-1)
            },
            find: function(a) {
                var b, c, d = [];
                b = 0;
                for (c = this.length; b < c; b++) v.find(a, this[b], d);
                return v(d)
            },
            filter: function(a) {
                return "function" == typeof a ? v(n(this.toArray(), function(b, c) {
                    return a(c, b)
                })) : v(v.filter(a, this.toArray()))
            },
            closest: function(a) {
                var b = [];
                a instanceof v && (a = a[0]);
                this.each(function(c, d) {
                    for (; d;) {
                        if ("string" == typeof a && v(d).is(a)) {
                            b.push(d);
                            break
                        } else if (d == a) {
                            b.push(d);
                            break
                        }
                        d = d.parentNode
                    }
                });
                return v(b)
            },
            offset: function(a) {
                var b, c, d = 0,
                    r = 0;
                if (!a) {
                    if (b =
                        this[0]) a = b.ownerDocument, c = a.documentElement, b.getBoundingClientRect && (r = b.getBoundingClientRect(), d = r.left + (c.scrollLeft || a.body.scrollLeft) - c.clientLeft, r = r.top + (c.scrollTop || a.body.scrollTop) - c.clientTop);
                    return {
                        left: d,
                        top: r
                    }
                }
                return this.css(a)
            },
            push: B,
            sort: [].sort,
            splice: [].splice
        };
        f.extend(v, {
            extend: f.extend,
            makeArray: function(a) {
                return a && a == a.window || a.nodeType ? [a] : f.toArray(a)
            },
            inArray: function(a, b) {
                var c;
                if (b.indexOf) return b.indexOf(a);
                for (c = b.length; c--;)
                    if (b[c] === a) return c;
                return -1
            },
            isArray: f.isArray,
            each: h,
            trim: q,
            grep: n,
            find: g,
            expr: g.selectors,
            unique: g.uniqueSort,
            text: g.getText,
            contains: g.contains,
            filter: function(a, b, c) {
                var d = b.length;
                for (c && (a = ":not(" + a + ")"); d--;) 1 != b[d].nodeType && b.splice(d, 1);
                return b = 1 === b.length ? v.find.matchesSelector(b[0], a) ? [b[0]] : [] : v.find.matches(a, b)
            }
        });
        h({
            parent: function(a) {
                return (a = a.parentNode) && 11 !== a.nodeType ? a : null
            },
            parents: function(a) {
                return w(a, "parentNode")
            },
            next: function(a) {
                return m(a, "nextSibling", 1)
            },
            prev: function(a) {
                return m(a, "previousSibling",
                    1)
            },
            children: function(a) {
                return A(a.firstChild, "nextSibling", 1)
            },
            contents: function(a) {
                return f.toArray(("iframe" === a.nodeName ? a.contentDocument || a.contentWindow.document : a).childNodes)
            }
        }, function(a, b) {
            v.fn[a] = function(c) {
                var d = [];
                this.each(function() {
                    var a = b.call(d, this, c, d);
                    a && (v.isArray(a) ? d.push.apply(d, a) : d.push(a))
                });
                1 < this.length && (O[a] || (d = v.unique(d)), 0 === a.indexOf("parents") && (d = d.reverse()));
                d = v(d);
                return c ? d.filter(c) : d
            }
        });
        h({
            parentsUntil: function(a, b) {
                return w(a, "parentNode", b)
            },
            nextUntil: function(a,
                b) {
                return A(a, "nextSibling", 1, b).slice(1)
            },
            prevUntil: function(a, b) {
                return A(a, "previousSibling", 1, b).slice(1)
            }
        }, function(a, b) {
            v.fn[a] = function(c, d) {
                var r = [];
                this.each(function() {
                    var a = b.call(r, this, c, r);
                    a && (v.isArray(a) ? r.push.apply(r, a) : r.push(a))
                });
                1 < this.length && (r = v.unique(r), 0 === a.indexOf("parents") || "prevUntil" === a) && (r = r.reverse());
                r = v(r);
                return d ? r.filter(d) : r
            }
        });
        v.fn.is = function(a) {
            return !!a && 0 < this.filter(a).length
        };
        v.fn.init.prototype = v.fn;
        v.overrideDefaults = function(a) {
            function b(d, r) {
                c =
                    c || a();
                0 === arguments.length && (d = c.element);
                r || (r = c.context);
                return new b.fn.init(d, r)
            }
            var c;
            v.extend(b, this);
            return b
        };
        e.ie && 8 > e.ie && (t(z, "get", {
            maxlength: function(a) {
                a = a.maxLength;
                return 2147483647 === a ? F : a
            },
            size: function(a) {
                a = a.size;
                return 20 === a ? F : a
            },
            "class": function(a) {
                return a.className
            },
            style: function(a) {
                a = a.style.cssText;
                return 0 === a.length ? F : a
            }
        }), t(z, "set", {
            "class": function(a, b) {
                a.className = b
            },
            style: function(a, b) {
                a.style.cssText = b
            }
        }));
        e.ie && 9 > e.ie && (N["float"] = "styleFloat", t(I, "set", {
            opacity: function(a,
                b) {
                a = a.style;
                null === b || "" === b ? a.removeAttribute("filter") : (a.zoom = 1, a.filter = "alpha(opacity\x3d" + 100 * b + ")")
            }
        }));
        v.attrHooks = z;
        v.cssHooks = I;
        return v
    });
    z("tinymce/html/Styles", [], function() {
        return function(k, g) {
            function f(a, b, c, d) {
                function e(a) {
                    a = parseInt(a, 10).toString(16);
                    return 1 < a.length ? a : "0" + a
                }
                return "#" + e(b) + e(c) + e(d)
            }
            var e = /rgb\s*\(\s*([0-9]+)\s*,\s*([0-9]+)\s*,\s*([0-9]+)\s*\)/gi,
                a = /(?:url(?:(?:\(\s*\"([^\"]+)\"\s*\))|(?:\(\s*\'([^\']+)\'\s*\))|(?:\(\s*([^)\s]+)\s*\))))|(?:\'([^\']+)\')|(?:\"([^\"]+)\")/gi,
                c = /\s*([^:]+):\s*([^;]+);?/g,
                b = /\s+$/,
                d, l = {},
                v, q;
            k = k || {};
            g && (v = g.getValidStyles(), q = g.getInvalidStyles());
            g = "\\\" \\' \\; \\: ; : \ufeff".split(" ");
            for (d = 0; d < g.length; d++) l[g[d]] = "\ufeff" + d, l["\ufeff" + d] = g[d];
            return {
                toHex: function(a) {
                    return a.replace(e, f)
                },
                parse: function(h) {
                    function n(a, b, c) {
                        var r, u, e, f;
                        if (r = y[a + "-top" + b])
                            if (u = y[a + "-right" + b])
                                if (e = y[a + "-bottom" + b])
                                    if (f = y[a + "-left" + b]) {
                                        r = [r, u, e, f];
                                        for (d = r.length - 1; d-- && r[d] === r[d + 1];); - 1 < d && c || (y[a + b] = -1 == d ? r[0] : r.join(" "), delete y[a + "-top" + b], delete y[a +
                                            "-right" + b], delete y[a + "-bottom" + b], delete y[a + "-left" + b])
                                    }
                    }

                    function p(a) {
                        var b = y[a],
                            c;
                        if (b) {
                            b = b.split(" ");
                            for (c = b.length; c--;)
                                if (b[c] !== b[0]) return !1;
                            y[a] = b[0];
                            return !0
                        }
                    }

                    function w(a, b, c, d) {
                        p(b) && p(c) && p(d) && (y[a] = y[b] + " " + y[c] + " " + y[d], delete y[b], delete y[c], delete y[d])
                    }

                    function q(a) {
                        u = !0;
                        return l[a]
                    }

                    function m(a, b) {
                        u && (a = a.replace(/\uFEFF[0-9]/g, function(a) {
                            return l[a]
                        }));
                        b || (a = a.replace(/\\([\'\";:])/g, "$1"));
                        return a
                    }

                    function t(a, b, c, d, r, u) {
                        if (r = r || u) return r = m(r), "'" + r.replace(/\'/g, "\\'") +
                            "'";
                        b = m(b || c || d);
                        if (!k.allow_script_urls && (a = b.replace(/[\s\r\n]+/, ""), /(java|vb)script:/i.test(a) || !k.allow_svg_data_urls && /^data:image\/svg/i.test(a))) return "";
                        F && (b = F.call(v, b, "style"));
                        return "url('" + b.replace(/\'/g, "\\'") + "')"
                    }
                    var y = {},
                        B, x, r, u, F = k.url_converter,
                        v = k.url_converter_scope || this;
                    if (h) {
                        h = h.replace(/[\u0000-\u001F]/g, "");
                        for (h = h.replace(/\\[\"\';:\uFEFF]/g, q).replace(/\"[^\"]+\"|\'[^\']+\'/g, function(a) {
                                return a.replace(/[;:]/g, q)
                            }); B = c.exec(h);) {
                            x = B[1].replace(b, "").toLowerCase();
                            r =
                                B[2].replace(b, "");
                            r = r.replace(/\\[0-9a-f]+/g, function(a) {
                                return String.fromCharCode(parseInt(a.substr(1), 16))
                            });
                            if (x && 0 < r.length) {
                                if (!k.allow_script_urls && ("behavior" == x || /expression\s*\(|\/\*|\*\//.test(r))) continue;
                                if ("font-weight" === x && "700" === r) r = "bold";
                                else if ("color" === x || "background-color" === x) r = r.toLowerCase();
                                r = r.replace(e, f);
                                r = r.replace(a, t);
                                y[x] = u ? m(r, !0) : r
                            }
                            c.lastIndex = B.index + B[0].length
                        }
                        n("border", "", !0);
                        n("border", "-width");
                        n("border", "-color");
                        n("border", "-style");
                        n("padding", "");
                        n("margin", "");
                        w("border", "border-width", "border-style", "border-color");
                        "medium none" === y.border && delete y.border;
                        "none" === y["border-image"] && delete y["border-image"]
                    }
                    return y
                },
                serialize: function(a, b) {
                    function c(b) {
                        var c, d, r, u;
                        if (c = v[b])
                            for (d = 0, r = c.length; d < r; d++) b = c[d], u = a[b], void 0 !== u && 0 < u.length && (e += (0 < e.length ? " " : "") + b + ": " + u + ";")
                    }

                    function d(a, b) {
                        var c;
                        return (c = q["*"]) && c[a] ? !1 : (c = q[b]) && c[a] ? !1 : !0
                    }
                    var e = "",
                        f, h;
                    if (b && v) c("*"), c(b);
                    else
                        for (f in a) h = a[f], void 0 !== h && 0 < h.length && (!q || d(f, b)) &&
                            (e += (0 < e.length ? " " : "") + f + ": " + h + ";");
                    return e
                }
            }
        }
    });
    z("tinymce/dom/TreeWalker", [], function() {
        return function(k, g) {
            function f(a, c, b, d) {
                if (a) {
                    if (!d && a[c]) return a[c];
                    if (a != g) {
                        if (c = a[b]) return c;
                        for (a = a.parentNode; a && a != g; a = a.parentNode)
                            if (c = a[b]) return c
                    }
                }
            }
            var e = k;
            this.current = function() {
                return e
            };
            this.next = function(a) {
                return e = f(e, "firstChild", "nextSibling", a)
            };
            this.prev = function(a) {
                return e = f(e, "lastChild", "previousSibling", a)
            };
            this.prev2 = function(a) {
                a: {
                    var c;
                    if (e) {
                        c = e.previousSibling;
                        if (g && c === g) {
                            e =
                                void 0;
                            break a
                        }
                        if (c) {
                            if (!a)
                                for (a = c.lastChild; a; a = a.lastChild)
                                    if (!a.lastChild) {
                                        e = a;
                                        break a
                                    }
                            e = c;
                            break a
                        }
                        if ((c = e.parentNode) && c !== g) {
                            e = c;
                            break a
                        }
                    }
                    e = void 0
                }
                return e
            }
        }
    });
    z("tinymce/dom/Range", ["tinymce/util/Tools"], function(k) {
        function g(f) {
            function e(a, b) {
                q(!0, a, b)
            }

            function a(a, b) {
                q(!1, a, b)
            }

            function c(a) {
                e(a.parentNode, x(a))
            }

            function b(b) {
                a(b.parentNode, x(b) + 1)
            }

            function d() {
                return h(1)
            }

            function l(a, b) {
                var c;
                if (3 == a.nodeType || 0 > b) return a;
                for (c = a.firstChild; c && 0 < b;) --b, c = c.nextSibling;
                return c ? c : a
            }

            function v(a,
                b, c, d) {
                var r, e;
                if (a == c) return b == d ? 0 : b < d ? -1 : 1;
                for (r = c; r && r.parentNode != a;) r = r.parentNode;
                if (r) {
                    e = 0;
                    for (c = a.firstChild; c != r && e < b;) e++, c = c.nextSibling;
                    return b <= e ? -1 : 1
                }
                for (r = a; r && r.parentNode != c;) r = r.parentNode;
                if (r) {
                    e = 0;
                    for (c = c.firstChild; c != r && e < d;) e++, c = c.nextSibling;
                    return e < d ? -1 : 1
                }
                for (b = f.findCommonAncestor(a, c); a && a.parentNode != b;) a = a.parentNode;
                a || (a = b);
                for (d = c; d && d.parentNode != b;) d = d.parentNode;
                d || (d = b);
                if (a == d) return 0;
                for (c = b.firstChild; c;) {
                    if (c == a) return -1;
                    if (c == d) return 1;
                    c = c.nextSibling
                }
            }

            function q(a, b, c) {
                a ? (t.startContainer = b, t.startOffset = c) : (t.endContainer = b, t.endOffset = c);
                for (b = t.endContainer; b.parentNode;) b = b.parentNode;
                for (c = t.startContainer; c.parentNode;) c = c.parentNode;
                c == b ? 0 < v(t.startContainer, t.startOffset, t.endContainer, t.endOffset) && t.collapse(a) : t.collapse(a);
                t.collapsed = t.startContainer == t.endContainer && t.startOffset == t.endOffset;
                t.commonAncestorContainer = f.findCommonAncestor(t.startContainer, t.endContainer)
            }

            function h(a) {
                var b, c = 0,
                    d = 0,
                    r;
                if (t.startContainer == t.endContainer) return n(a);
                b = t.endContainer;
                for (r = b.parentNode; r; b = r, r = r.parentNode) {
                    if (r == t.startContainer) {
                        var e = void 0,
                            d = a;
                        2 != d && (e = y.createDocumentFragment());
                        r = p(b, d);
                        e && e.appendChild(r);
                        c = x(b) - t.startOffset;
                        if (!(0 >= c))
                            for (r = b.previousSibling; 0 < c;) a = r.previousSibling, r = m(r, d), e && e.insertBefore(r, e.firstChild), --c, r = a;
                        1 != d && (t.setEndBefore(b), t.collapse(!1));
                        a = e;
                        return a
                    }++c
                }
                b = t.startContainer;
                for (r = b.parentNode; r; b = r, r = r.parentNode) {
                    if (r == t.endContainer) {
                        e = void 0;
                        2 != a && (e = y.createDocumentFragment());
                        r = w(b, a);
                        e && e.appendChild(r);
                        c = x(b);
                        ++c;
                        d = t.endOffset - c;
                        for (r = b.nextSibling; r && 0 < d;) c = r.nextSibling, r = m(r, a), e && e.appendChild(r), --d, r = c;
                        1 != a && (t.setStartAfter(b), t.collapse(!0));
                        return e
                    }++d
                }
                d -= c;
                for (b = t.startContainer; 0 < d;) b = b.parentNode, d--;
                for (c = t.endContainer; 0 > d;) c = c.parentNode, d++;
                d = b.parentNode;
                for (r = c.parentNode; d != r; d = d.parentNode, r = r.parentNode) b = d, c = r;
                var f;
                2 != a && (e = y.createDocumentFragment());
                f = w(b, a);
                e && e.appendChild(f);
                d = x(b);
                r = x(c);
                ++d;
                d = r - d;
                for (f = b.nextSibling; 0 < d;) r = f.nextSibling, f = m(f, a), e && e.appendChild(f),
                    f = r, --d;
                f = p(c, a);
                e && e.appendChild(f);
                1 != a && (t.setStartAfter(b), t.collapse(!0));
                return e
            }

            function n(a) {
                var b, c, d, r, e;
                2 != a && (b = y.createDocumentFragment());
                if (t.startOffset == t.endOffset) return b;
                if (3 == t.startContainer.nodeType) {
                    c = t.startContainer.nodeValue;
                    d = c.substring(t.startOffset, t.endOffset);
                    1 != a && (c = t.startContainer, r = t.startOffset, e = t.endOffset - t.startOffset, 0 === r && e >= c.nodeValue.length - 1 ? c.parentNode.removeChild(c) : c.deleteData(r, e), t.collapse(!0));
                    if (2 == a) return;
                    0 < d.length && b.appendChild(y.createTextNode(d));
                    return b
                }
                c = l(t.startContainer, t.startOffset);
                for (d = t.endOffset - t.startOffset; c && 0 < d;) r = c.nextSibling, c = m(c, a), b && b.appendChild(c), --d, c = r;
                1 != a && t.collapse(!0);
                return b
            }

            function p(a, b) {
                var c = l(t.endContainer, t.endOffset - 1),
                    d, r, e, f = c != t.endContainer;
                if (c == a) return A(c, f, !1, b);
                d = c.parentNode;
                for (r = A(d, !1, !1, b); d;) {
                    for (; c;) e = c.previousSibling, c = A(c, f, !1, b), 2 != b && r.insertBefore(c, r.firstChild), f = !0, c = e;
                    if (d == a) return r;
                    c = d.previousSibling;
                    d = d.parentNode;
                    e = A(d, !1, !1, b);
                    2 != b && e.appendChild(r);
                    r = e
                }
            }

            function w(a,
                b) {
                var c = l(t.startContainer, t.startOffset),
                    d = c != t.startContainer,
                    r, e, f;
                if (c == a) return A(c, d, !0, b);
                r = c.parentNode;
                for (e = A(r, !1, !0, b); r;) {
                    for (; c;) f = c.nextSibling, c = A(c, d, !0, b), 2 != b && e.appendChild(c), d = !0, c = f;
                    if (r == a) return e;
                    c = r.nextSibling;
                    r = r.parentNode;
                    f = A(r, !1, !0, b);
                    2 != b && f.appendChild(e);
                    e = f
                }
            }

            function A(a, b, c, d) {
                var r;
                if (b) return m(a, d);
                if (3 == a.nodeType) {
                    b = a.nodeValue;
                    c ? (r = t.startOffset, c = b.substring(r), b = b.substring(0, r)) : (r = t.endOffset, c = b.substring(0, r), b = b.substring(r));
                    1 != d && (a.nodeValue =
                        b);
                    if (2 == d) return;
                    a = f.clone(a, !1);
                    a.nodeValue = c;
                    return a
                }
                if (2 != d) return f.clone(a, !1)
            }

            function m(a, b) {
                if (2 != b) return 1 == b ? f.clone(a, !0) : a;
                a.parentNode.removeChild(a)
            }
            var t = this,
                y = f.doc,
                B = k.extend,
                x = f.nodeIndex;
            B(t, {
                startContainer: y,
                startOffset: 0,
                endContainer: y,
                endOffset: 0,
                collapsed: !0,
                commonAncestorContainer: y,
                START_TO_START: 0,
                START_TO_END: 1,
                END_TO_END: 2,
                END_TO_START: 3,
                setStart: e,
                setEnd: a,
                setStartBefore: c,
                setStartAfter: function(a) {
                    e(a.parentNode, x(a) + 1)
                },
                setEndBefore: function(b) {
                    a(b.parentNode, x(b))
                },
                setEndAfter: b,
                collapse: function(a) {
                    a ? (t.endContainer = t.startContainer, t.endOffset = t.startOffset) : (t.startContainer = t.endContainer, t.startOffset = t.endOffset);
                    t.collapsed = !0
                },
                selectNode: function(a) {
                    c(a);
                    b(a)
                },
                selectNodeContents: function(b) {
                    e(b, 0);
                    a(b, 1 === b.nodeType ? b.childNodes.length : b.nodeValue.length)
                },
                compareBoundaryPoints: function(a, b) {
                    var c = t.startContainer,
                        d = t.startOffset,
                        r = t.endContainer,
                        e = t.endOffset,
                        f = b.startContainer,
                        u = b.startOffset,
                        m = b.endContainer;
                    b = b.endOffset;
                    if (0 === a) return v(c, d, f,
                        u);
                    if (1 === a) return v(r, e, f, u);
                    if (2 === a) return v(r, e, m, b);
                    if (3 === a) return v(c, d, m, b)
                },
                deleteContents: function() {
                    h(2)
                },
                extractContents: function() {
                    return h(0)
                },
                cloneContents: d,
                insertNode: function(a) {
                    var b = this.startContainer,
                        c = this.startOffset,
                        d;
                    3 !== b.nodeType && 4 !== b.nodeType || !b.nodeValue ? (0 < b.childNodes.length && (d = b.childNodes[c]), d ? b.insertBefore(a, d) : 3 == b.nodeType ? f.insertAfter(a, b) : b.appendChild(a)) : c ? c >= b.nodeValue.length ? f.insertAfter(a, b) : (c = b.splitText(c), b.parentNode.insertBefore(a, c)) : b.parentNode.insertBefore(a,
                        b)
                },
                surroundContents: function(a) {
                    var b = t.extractContents();
                    t.insertNode(a);
                    a.appendChild(b);
                    t.selectNode(a)
                },
                cloneRange: function() {
                    return B(new g(f), {
                        startContainer: t.startContainer,
                        startOffset: t.startOffset,
                        endContainer: t.endContainer,
                        endOffset: t.endOffset,
                        collapsed: t.collapsed,
                        commonAncestorContainer: t.commonAncestorContainer
                    })
                },
                toStringIE: function() {
                    return f.create("body", null, d()).outerText
                }
            });
            return t
        }
        g.prototype.toString = function() {
            return this.toStringIE()
        };
        return g
    });
    z("tinymce/html/Entities", ["tinymce/util/Tools"], function(k) {
        function g(b, c) {
            var d, e, f, p = {};
            if (b) {
                b = b.split(",");
                c = c || 10;
                for (d = 0; d < b.length; d += 2) e = String.fromCharCode(parseInt(b[d], c)), a[e] || (f = "\x26" + b[d + 1] + ";", p[e] = f, p[f] = e);
                return p
            }
        }
        var f = k.makeMap,
            e, a, c, b = /[&<>\"\u0060\u007E-\uD7FF\uE000-\uFFEF]|[\uD800-\uDBFF][\uDC00-\uDFFF]/g,
            d = /[<>&\u007E-\uD7FF\uE000-\uFFEF]|[\uD800-\uDBFF][\uDC00-\uDFFF]/g,
            l = /[<>&\"\']/g,
            v = /&#([a-z0-9]+);?|&([a-z0-9]+);/gi,
            q = {
                128: "\u20ac",
                130: "\u201a",
                131: "\u0192",
                132: "\u201e",
                133: "\u2026",
                134: "\u2020",
                135: "\u2021",
                136: "\u02c6",
                137: "\u2030",
                138: "\u0160",
                139: "\u2039",
                140: "\u0152",
                142: "\u017d",
                145: "\u2018",
                146: "\u2019",
                147: "\u201c",
                148: "\u201d",
                149: "\u2022",
                150: "\u2013",
                151: "\u2014",
                152: "\u02dc",
                153: "\u2122",
                154: "\u0161",
                155: "\u203a",
                156: "\u0153",
                158: "\u017e",
                159: "\u0178"
            };
        a = {
            '"': "\x26quot;",
            "'": "\x26#39;",
            "\x3c": "\x26lt;",
            "\x3e": "\x26gt;",
            "\x26": "\x26amp;",
            "`": "\x26#96;"
        };
        c = {
            "\x26lt;": "\x3c",
            "\x26gt;": "\x3e",
            "\x26amp;": "\x26",
            "\x26quot;": '"',
            "\x26apos;": "'"
        };
        e = g("50,nbsp,51,iexcl,52,cent,53,pound,54,curren,55,yen,56,brvbar,57,sect,58,uml,59,copy,5a,ordf,5b,laquo,5c,not,5d,shy,5e,reg,5f,macr,5g,deg,5h,plusmn,5i,sup2,5j,sup3,5k,acute,5l,micro,5m,para,5n,middot,5o,cedil,5p,sup1,5q,ordm,5r,raquo,5s,frac14,5t,frac12,5u,frac34,5v,iquest,60,Agrave,61,Aacute,62,Acirc,63,Atilde,64,Auml,65,Aring,66,AElig,67,Ccedil,68,Egrave,69,Eacute,6a,Ecirc,6b,Euml,6c,Igrave,6d,Iacute,6e,Icirc,6f,Iuml,6g,ETH,6h,Ntilde,6i,Ograve,6j,Oacute,6k,Ocirc,6l,Otilde,6m,Ouml,6n,times,6o,Oslash,6p,Ugrave,6q,Uacute,6r,Ucirc,6s,Uuml,6t,Yacute,6u,THORN,6v,szlig,70,agrave,71,aacute,72,acirc,73,atilde,74,auml,75,aring,76,aelig,77,ccedil,78,egrave,79,eacute,7a,ecirc,7b,euml,7c,igrave,7d,iacute,7e,icirc,7f,iuml,7g,eth,7h,ntilde,7i,ograve,7j,oacute,7k,ocirc,7l,otilde,7m,ouml,7n,divide,7o,oslash,7p,ugrave,7q,uacute,7r,ucirc,7s,uuml,7t,yacute,7u,thorn,7v,yuml,ci,fnof,sh,Alpha,si,Beta,sj,Gamma,sk,Delta,sl,Epsilon,sm,Zeta,sn,Eta,so,Theta,sp,Iota,sq,Kappa,sr,Lambda,ss,Mu,st,Nu,su,Xi,sv,Omicron,t0,Pi,t1,Rho,t3,Sigma,t4,Tau,t5,Upsilon,t6,Phi,t7,Chi,t8,Psi,t9,Omega,th,alpha,ti,beta,tj,gamma,tk,delta,tl,epsilon,tm,zeta,tn,eta,to,theta,tp,iota,tq,kappa,tr,lambda,ts,mu,tt,nu,tu,xi,tv,omicron,u0,pi,u1,rho,u2,sigmaf,u3,sigma,u4,tau,u5,upsilon,u6,phi,u7,chi,u8,psi,u9,omega,uh,thetasym,ui,upsih,um,piv,812,bull,816,hellip,81i,prime,81j,Prime,81u,oline,824,frasl,88o,weierp,88h,image,88s,real,892,trade,89l,alefsym,8cg,larr,8ch,uarr,8ci,rarr,8cj,darr,8ck,harr,8dl,crarr,8eg,lArr,8eh,uArr,8ei,rArr,8ej,dArr,8ek,hArr,8g0,forall,8g2,part,8g3,exist,8g5,empty,8g7,nabla,8g8,isin,8g9,notin,8gb,ni,8gf,prod,8gh,sum,8gi,minus,8gn,lowast,8gq,radic,8gt,prop,8gu,infin,8h0,ang,8h7,and,8h8,or,8h9,cap,8ha,cup,8hb,int,8hk,there4,8hs,sim,8i5,cong,8i8,asymp,8j0,ne,8j1,equiv,8j4,le,8j5,ge,8k2,sub,8k3,sup,8k4,nsub,8k6,sube,8k7,supe,8kl,oplus,8kn,otimes,8l5,perp,8m5,sdot,8o8,lceil,8o9,rceil,8oa,lfloor,8ob,rfloor,8p9,lang,8pa,rang,9ea,loz,9j0,spades,9j3,clubs,9j5,hearts,9j6,diams,ai,OElig,aj,oelig,b0,Scaron,b1,scaron,bo,Yuml,m6,circ,ms,tilde,802,ensp,803,emsp,809,thinsp,80c,zwnj,80d,zwj,80e,lrm,80f,rlm,80j,ndash,80k,mdash,80o,lsquo,80p,rsquo,80q,sbquo,80s,ldquo,80t,rdquo,80u,bdquo,810,dagger,811,Dagger,81g,permil,81p,lsaquo,81q,rsaquo,85c,euro",
            32);
        var h = {
            encodeRaw: function(c, e) {
                return c.replace(e ? b : d, function(b) {
                    return a[b] || b
                })
            },
            encodeAllRaw: function(b) {
                return ("" + b).replace(l, function(b) {
                    return a[b] || b
                })
            },
            encodeNumeric: function(c, e) {
                return c.replace(e ? b : d, function(b) {
                    return 1 < b.length ? "\x26#" + (1024 * (b.charCodeAt(0) - 55296) + (b.charCodeAt(1) - 56320) + 65536) + ";" : a[b] || "\x26#" + b.charCodeAt(0) + ";"
                })
            },
            encodeNamed: function(c, f, h) {
                h = h || e;
                return c.replace(f ? b : d, function(b) {
                    return a[b] || h[b] || b
                })
            },
            getEncodeFunc: function(c, p) {
                function n(c, e) {
                    return c.replace(e ?
                        b : d,
                        function(b) {
                            return a[b] || p[b] || "\x26#" + b.charCodeAt(0) + ";" || b
                        })
                }

                function l(a, b) {
                    return h.encodeNamed(a, b, p)
                }
                p = g(p) || e;
                c = f(c.replace(/\+/g, ","));
                return c.named && c.numeric ? n : c.named ? p ? l : h.encodeNamed : c.numeric ? h.encodeNumeric : h.encodeRaw
            },
            decode: function(a) {
                return a.replace(v, function(a, b) {
                    if (b) return b = "x" === b.charAt(0).toLowerCase() ? parseInt(b.substr(1), 16) : parseInt(b, 10), 65535 < b ? (b -= 65536, String.fromCharCode(55296 + (b >> 10), 56320 + (b & 1023))) : q[b] || String.fromCharCode(b);
                    (b = c[a] || e[a]) || (b = document.createElement("div"),
                        b.innerHTML = a, b = b.textContent || b.innerText || a);
                    return b
                })
            }
        };
        return h
    });
    z("tinymce/dom/StyleSheetLoader", ["tinymce/util/Tools", "tinymce/util/Delay"], function(k, g) {
        return function(f, e) {
            function a(a) {
                f.getElementsByTagName("head")[0].appendChild(a)
            }
            var c = 0,
                b = {},
                d;
            e = e || {};
            d = e.maxLoadTime || 5E3;
            this.load = function(e, v, q) {
                function h() {
                    for (var a = x.passed, b = a.length; b--;) a[b]();
                    x.status = 2;
                    x.passed = [];
                    x.failed = []
                }

                function n() {
                    for (var a = x.failed, b = a.length; b--;) a[b]();
                    x.status = 3;
                    x.passed = [];
                    x.failed = []
                }

                function p() {
                    var a =
                        navigator.userAgent.match(/WebKit\/(\d*)/);
                    return !!(a && 536 > a[1])
                }

                function l(a, b) {
                    a() || ((new Date).getTime() - B < d ? g.setTimeout(b) : n())
                }

                function A() {
                    l(function() {
                        for (var a = f.styleSheets, b, c = a.length; c--;)
                            if (b = a[c], (b = b.ownerNode ? b.ownerNode : b.owningElement) && b.id === t.id) return h(), !0
                    }, A)
                }

                function m() {
                    l(function() {
                        try {
                            var a = y.sheet.cssRules;
                            h();
                            return !!a
                        } catch (u) {}
                    }, m)
                }
                var t, y, B, x;
                e = k._addCacheSuffix(e);
                b[e] ? x = b[e] : (x = {
                    passed: [],
                    failed: []
                }, b[e] = x);
                v && x.passed.push(v);
                q && x.failed.push(q);
                if (1 != x.status)
                    if (2 ==
                        x.status) h();
                    else if (3 == x.status) n();
                else {
                    x.status = 1;
                    t = f.createElement("link");
                    t.rel = "stylesheet";
                    t.type = "text/css";
                    t.id = "u" + c++;
                    t.async = !1;
                    t.defer = !1;
                    B = (new Date).getTime();
                    if ("onload" in t && !p()) t.onload = A, t.onerror = n;
                    else {
                        if (0 < navigator.userAgent.indexOf("Firefox")) {
                            y = f.createElement("style");
                            y.textContent = '@import "' + e + '"';
                            m();
                            a(y);
                            return
                        }
                        A()
                    }
                    a(t);
                    t.href = e
                }
            }
        }
    });
    z("tinymce/dom/DOMUtils", "tinymce/dom/Sizzle tinymce/dom/DomQuery tinymce/html/Styles tinymce/dom/EventUtils tinymce/dom/TreeWalker tinymce/dom/Range tinymce/html/Entities tinymce/Env tinymce/util/Tools tinymce/dom/StyleSheetLoader".split(" "),
        function(k, g, f, e, a, c, b, d, l, v) {
            function q(a, b) {
                var c = {},
                    d = b.keep_values,
                    e;
                e = {
                    set: function(c, d, e) {
                        b.url_converter && (d = b.url_converter.call(b.url_converter_scope || a, d, e, c[0]));
                        c.attr("data-mce-" + e, d).attr(e, d)
                    },
                    get: function(a, b) {
                        return a.attr("data-mce-" + b) || a.attr(b)
                    }
                };
                c = {
                    style: {
                        set: function(a, b) {
                            null !== b && "object" === typeof b ? a.css(b) : (d && a.attr("data-mce-style", b), a.attr("style", b))
                        },
                        get: function(b) {
                            var c = b.attr("data-mce-style") || b.attr("style");
                            return c = a.serializeStyle(a.parseStyle(c), b[0].nodeName)
                        }
                    }
                };
                d && (c.href = c.src = e);
                return c
            }

            function h(a, b) {
                var c = b.attr("style");
                (c = a.serializeStyle(a.parseStyle(c), b[0].nodeName)) || (c = null);
                b.attr("data-mce-style", c)
            }

            function n(a, b) {
                var c = 0,
                    d, e;
                if (a)
                    for (d = a.nodeType, a = a.previousSibling; a; a = a.previousSibling)
                        if (e = a.nodeType, !b || 3 != e || e != d && a.nodeValue.length) c++, d = e;
                return c
            }

            function p(a, b) {
                var c = this,
                    d;
                c.doc = a;
                c.win = window;
                c.files = {};
                c.counter = 0;
                c.stdMode = !y || 8 <= a.documentMode;
                c.boxModel = !y || "CSS1Compat" == a.compatMode || c.stdMode;
                c.styleSheetLoader = new v(a);
                c.boundEvents = [];
                c.settings = b = b || {};
                c.schema = b.schema;
                c.styles = new f({
                    url_converter: b.url_converter,
                    url_converter_scope: b.url_converter_scope
                }, b.schema);
                c.fixDoc(a);
                c.events = b.ownEvents ? new e(b.proxy) : e.Event;
                c.attrHooks = q(c, b);
                d = b.schema ? b.schema.getBlockElements() : {};
                c.$ = g.overrideDefaults(function() {
                    return {
                        context: a,
                        element: c.getRoot()
                    }
                });
                c.isBlock = function(a) {
                    if (!a) return !1;
                    var b = a.nodeType;
                    return b ? !(1 !== b || !d[a.nodeName]) : !!d[a]
                }
            }
            var w = l.each,
                A = l.is,
                m = l.grep,
                t = l.trim,
                y = d.ie,
                B = /^([a-z0-9],?)+$/i,
                x = /^[ \t\r\n]*$/;
            p.prototype = {
                $$: function(a) {
                    "string" == typeof a && (a = this.get(a));
                    return this.$(a)
                },
                root: null,
                fixDoc: function(a) {
                    var b = this.settings,
                        c;
                    if (y && b.schema)
                        for (c in "abbr article aside audio canvas details figcaption figure footer header hgroup mark menu meter nav output progress section summary time video".replace(/\w+/g, function(b) {
                                a.createElement(b)
                            }), b.schema.getCustomElements()) a.createElement(c)
                },
                clone: function(a, b) {
                    var c = this,
                        d, e;
                    if (!y || 1 !== a.nodeType || b) return a.cloneNode(b);
                    e = c.doc;
                    return b ? d.firstChild : (d = e.createElement(a.nodeName), w(c.getAttribs(a), function(b) {
                        c.setAttrib(d, b.nodeName, c.getAttrib(a, b.nodeName))
                    }), d)
                },
                getRoot: function() {
                    return this.settings.root_element || this.doc.body
                },
                getViewPort: function(a) {
                    var b;
                    a = a ? a : this.win;
                    b = a.document;
                    b = this.boxModel ? b.documentElement : b.body;
                    return {
                        x: a.pageXOffset || b.scrollLeft,
                        y: a.pageYOffset || b.scrollTop,
                        w: a.innerWidth || b.clientWidth,
                        h: a.innerHeight || b.clientHeight
                    }
                },
                getRect: function(a) {
                    var b;
                    a = this.get(a);
                    b = this.getPos(a);
                    a = this.getSize(a);
                    return {
                        x: b.x,
                        y: b.y,
                        w: a.w,
                        h: a.h
                    }
                },
                getSize: function(a) {
                    var b, c;
                    a = this.get(a);
                    b = this.getStyle(a, "width");
                    c = this.getStyle(a, "height"); - 1 === b.indexOf("px") && (b = 0); - 1 === c.indexOf("px") && (c = 0);
                    return {
                        w: parseInt(b, 10) || a.offsetWidth || a.clientWidth,
                        h: parseInt(c, 10) || a.offsetHeight || a.clientHeight
                    }
                },
                getParent: function(a, b, c) {
                    return this.getParents(a, b, c, !1)
                },
                getParents: function(a, b, c, d) {
                    var e = this,
                        f, r = [];
                    a = e.get(a);
                    d = d === K;
                    c = c || ("BODY" != e.getRoot().nodeName ? e.getRoot().parentNode : null);
                    A(b, "string") && (f = b,
                        b = "*" === b ? function(a) {
                            return 1 == a.nodeType
                        } : function(a) {
                            return e.is(a, f)
                        });
                    for (; a && a != c && a.nodeType && 9 !== a.nodeType;) {
                        if (!b || b(a))
                            if (d) r.push(a);
                            else return a;
                        a = a.parentNode
                    }
                    return d ? r : null
                },
                get: function(a) {
                    var b;
                    return a && this.doc && "string" == typeof a && (b = a, (a = this.doc.getElementById(a)) && a.id !== b) ? this.doc.getElementsByName(b)[1] : a
                },
                getNext: function(a, b) {
                    return this._findSib(a, b, "nextSibling")
                },
                getPrev: function(a, b) {
                    return this._findSib(a, b, "previousSibling")
                },
                select: function(a, b) {
                    return k(a, this.get(b) ||
                        this.settings.root_element || this.doc, [])
                },
                is: function(a, b) {
                    var c;
                    if (a.length === K) {
                        if ("*" === b) return 1 == a.nodeType;
                        if (B.test(b)) {
                            b = b.toLowerCase().split(/,/);
                            a = a.nodeName.toLowerCase();
                            for (c = b.length - 1; 0 <= c; c--)
                                if (b[c] == a) return !0;
                            return !1
                        }
                    }
                    if (a.nodeType && 1 != a.nodeType) return !1;
                    a = a.nodeType ? [a] : a;
                    return 0 < k(b, a[0].ownerDocument || a[0], null, a).length
                },
                add: function(a, b, c, d, e) {
                    var f = this;
                    return this.run(a, function(a) {
                        var r;
                        r = A(b, "string") ? f.doc.createElement(b) : b;
                        f.setAttribs(r, c);
                        d && (d.nodeType ? r.appendChild(d) :
                            f.setHTML(r, d));
                        return e ? r : a.appendChild(r)
                    })
                },
                create: function(a, b, c) {
                    return this.add(this.doc.createElement(a), a, b, c, 1)
                },
                createHTML: function(a, b, c) {
                    var d, e;
                    d = "\x3c" + a;
                    for (e in b) b.hasOwnProperty(e) && null !== b[e] && "undefined" != typeof b[e] && (d += " " + e + '\x3d"' + this.encode(b[e]) + '"');
                    return "undefined" != typeof c ? d + "\x3e" + c + "\x3c/" + a + "\x3e" : d + " /\x3e"
                },
                createFragment: function(a) {
                    var b;
                    b = this.doc;
                    var c;
                    c = b.createElement("div");
                    b = b.createDocumentFragment();
                    a && (c.innerHTML = a);
                    for (; a = c.firstChild;) b.appendChild(a);
                    return b
                },
                remove: function(a, b) {
                    a = this.$$(a);
                    b ? a.each(function() {
                        for (var a; a = this.firstChild;) 3 == a.nodeType && 0 === a.data.length ? this.removeChild(a) : this.parentNode.insertBefore(a, this)
                    }).remove() : a.remove();
                    return 1 < a.length ? a.toArray() : a[0]
                },
                setStyle: function(a, b, c) {
                    a = this.$$(a).css(b, c);
                    this.settings.update_styles && h(this, a)
                },
                getStyle: function(a, b, c) {
                    a = this.$$(a);
                    if (c) return a.css(b);
                    b = b.replace(/-(\D)/g, function(a, b) {
                        return b.toUpperCase()
                    });
                    "float" == b && (b = d.ie && 12 > d.ie ? "styleFloat" : "cssFloat");
                    return a[0] && a[0].style ? a[0].style[b] : K
                },
                setStyles: function(a, b) {
                    a = this.$$(a).css(b);
                    this.settings.update_styles && h(this, a)
                },
                removeAllAttribs: function(a) {
                    return this.run(a, function(a) {
                        var b, c = a.attributes;
                        for (b = c.length - 1; 0 <= b; b--) a.removeAttributeNode(c.item(b))
                    })
                },
                setAttrib: function(a, b, c) {
                    var d, e, f = this.settings;
                    "" === c && (c = null);
                    a = this.$$(a);
                    d = a.attr(b);
                    if (a.length && ((e = this.attrHooks[b]) && e.set ? e.set(a, c, b) : a.attr(b, c), d != c && f.onSetAttrib)) f.onSetAttrib({
                        attrElm: a,
                        attrName: b,
                        attrValue: c
                    })
                },
                setAttribs: function(a,
                    b) {
                    var c = this;
                    c.$$(a).each(function(a, d) {
                        w(b, function(a, b) {
                            c.setAttrib(d, b, a)
                        })
                    })
                },
                getAttrib: function(a, b, c) {
                    var d;
                    a = this.$$(a);
                    a.length && (d = (d = this.attrHooks[b]) && d.get ? d.get(a, b) : a.attr(b));
                    "undefined" == typeof d && (d = c || "");
                    return d
                },
                getPos: function(a, b) {
                    var c = 0,
                        d = 0,
                        e;
                    e = this.doc;
                    var f = e.body;
                    a = this.get(a);
                    b = b || f;
                    if (a) {
                        if (b === f && a.getBoundingClientRect && "static" === g(f).css("position")) return a = a.getBoundingClientRect(), b = this.boxModel ? e.documentElement : f, c = a.left + (e.documentElement.scrollLeft || f.scrollLeft) -
                            b.clientLeft, d = a.top + (e.documentElement.scrollTop || f.scrollTop) - b.clientTop, {
                                x: c,
                                y: d
                            };
                        for (e = a; e && e != b && e.nodeType;) c += e.offsetLeft || 0, d += e.offsetTop || 0, e = e.offsetParent;
                        for (e = a.parentNode; e && e != b && e.nodeType;) c -= e.scrollLeft || 0, d -= e.scrollTop || 0, e = e.parentNode
                    }
                    return {
                        x: c,
                        y: d
                    }
                },
                parseStyle: function(a) {
                    return this.styles.parse(a)
                },
                serializeStyle: function(a, b) {
                    return this.styles.serialize(a, b)
                },
                addStyle: function(a) {
                    var b = this.doc,
                        c, d;
                    if (this !== p.DOM && b === document) {
                        c = (c = p.DOM.addedStyles) || [];
                        if (c[a]) return;
                        c[a] = !0;
                        p.DOM.addedStyles = c
                    }
                    d = b.getElementById("mceDefaultStyles");
                    d || (d = b.createElement("style"), d.id = "mceDefaultStyles", d.type = "text/css", c = b.getElementsByTagName("head")[0], c.firstChild ? c.insertBefore(d, c.firstChild) : c.appendChild(d));
                    d.styleSheet ? d.styleSheet.cssText += a : d.appendChild(b.createTextNode(a))
                },
                loadCSS: function(a) {
                    var b = this,
                        c = b.doc,
                        d;
                    b !== p.DOM && c === document ? p.DOM.loadCSS(a) : (a || (a = ""), d = c.getElementsByTagName("head")[0], w(a.split(","), function(a) {
                        var e;
                        a = l._addCacheSuffix(a);
                        b.files[a] ||
                            (b.files[a] = !0, e = b.create("link", {
                                rel: "stylesheet",
                                href: a
                            }), y && c.documentMode && c.recalc && (e.onload = function() {
                                c.recalc && c.recalc();
                                e.onload = null
                            }), d.appendChild(e))
                    }))
                },
                addClass: function(a, b) {
                    this.$$(a).addClass(b)
                },
                removeClass: function(a, b) {
                    this.toggleClass(a, b, !1)
                },
                hasClass: function(a, b) {
                    return this.$$(a).hasClass(b)
                },
                toggleClass: function(a, b, c) {
                    this.$$(a).toggleClass(b, c).each(function() {
                        "" === this.className && g(this).attr("class", null)
                    })
                },
                show: function(a) {
                    this.$$(a).show()
                },
                hide: function(a) {
                    this.$$(a).hide()
                },
                isHidden: function(a) {
                    return "none" == this.$$(a).css("display")
                },
                uniqueId: function(a) {
                    return (a ? a : "mce_") + this.counter++
                },
                setHTML: function(a, b) {
                    a = this.$$(a);
                    y ? a.each(function(a, c) {
                        if (!1 !== c.canHaveHTML) {
                            for (; c.firstChild;) c.removeChild(c.firstChild);
                            try {
                                c.innerHTML = "\x3cbr\x3e" + b, c.removeChild(c.firstChild)
                            } catch (W) {
                                g("\x3cdiv\x3e").html("\x3cbr\x3e" + b).contents().slice(1).appendTo(c)
                            }
                            return b
                        }
                    }) : a.html(b)
                },
                getOuterHTML: function(a) {
                    a = this.get(a);
                    return 1 == a.nodeType && "outerHTML" in a ? a.outerHTML : g("\x3cdiv\x3e").append(g(a).clone()).html()
                },
                setOuterHTML: function(a, b) {
                    var c = this;
                    c.$$(a).each(function() {
                        try {
                            if ("outerHTML" in this) {
                                this.outerHTML = b;
                                return
                            }
                        } catch (O) {}
                        c.remove(g(this).html(b), !0)
                    })
                },
                decode: b.decode,
                encode: b.encodeAllRaw,
                insertAfter: function(a, b) {
                    b = this.get(b);
                    return this.run(a, function(a) {
                        var c, d;
                        c = b.parentNode;
                        (d = b.nextSibling) ? c.insertBefore(a, d): c.appendChild(a);
                        return a
                    })
                },
                replace: function(a, b, c) {
                    return this.run(b, function(b) {
                        A(b, "array") && (a = a.cloneNode(!0));
                        c && w(m(b.childNodes), function(b) {
                            a.appendChild(b)
                        });
                        return b.parentNode.replaceChild(a,
                            b)
                    })
                },
                rename: function(a, b) {
                    var c = this,
                        d;
                    a.nodeName != b.toUpperCase() && (d = c.create(b), w(c.getAttribs(a), function(b) {
                        c.setAttrib(d, b.nodeName, c.getAttrib(a, b.nodeName))
                    }), c.replace(d, a, 1));
                    return d || a
                },
                findCommonAncestor: function(a, b) {
                    for (var c = a, d; c;) {
                        for (d = b; d && c != d;) d = d.parentNode;
                        if (c == d) break;
                        c = c.parentNode
                    }
                    return !c && a.ownerDocument ? a.ownerDocument.documentElement : c
                },
                toHex: function(a) {
                    return this.styles.toHex(l.trim(a))
                },
                run: function(a, b, c) {
                    var d = this,
                        e;
                    "string" === typeof a && (a = d.get(a));
                    if (!a) return !1;
                    c = c || this;
                    return a.nodeType || !a.length && 0 !== a.length ? b.call(c, a) : (e = [], w(a, function(a, f) {
                        a && ("string" == typeof a && (a = d.get(a)), e.push(b.call(c, a, f)))
                    }), e)
                },
                getAttribs: function(a) {
                    var b;
                    a = this.get(a);
                    if (!a) return [];
                    if (y) {
                        b = [];
                        if ("OBJECT" == a.nodeName) return a.attributes;
                        "OPTION" === a.nodeName && this.getAttrib(a, "selected") && b.push({
                            specified: 1,
                            nodeName: "selected"
                        });
                        a.cloneNode(!1).outerHTML.replace(/<\/?[\w:\-]+ ?|=[\"][^\"]+\"|=\'[^\']+\'|=[\w\-]+|>/gi, "").replace(/[\w:\-]+/gi, function(a) {
                            b.push({
                                specified: 1,
                                nodeName: a
                            })
                        });
                        return b
                    }
                    return a.attributes
                },
                isEmpty: function(b, c) {
                    var d, e, f, m, r, p = 0;
                    if (b = b.firstChild) {
                        m = new a(b, b.parentNode);
                        c = c || (this.schema ? this.schema.getNonEmptyElements() : null);
                        do {
                            f = b.nodeType;
                            if (1 === f) {
                                if (b.getAttribute("data-mce-bogus")) continue;
                                r = b.nodeName.toLowerCase();
                                if (c && c[r]) {
                                    if ("br" === r) {
                                        p++;
                                        continue
                                    }
                                    return !1
                                }
                                e = this.getAttribs(b);
                                for (d = e.length; d--;)
                                    if (r = e[d].nodeName, "name" === r || "data-mce-bookmark" === r) return !1
                            }
                            if (8 == f || 3 === f && !x.test(b.nodeValue)) return !1
                        } while (b = m.next())
                    }
                    return 1 >=
                        p
                },
                createRng: function() {
                    var a = this.doc;
                    return a.createRange ? a.createRange() : new c(this)
                },
                nodeIndex: n,
                split: function(a, b, c) {
                    function d(a) {
                        var b, c = a.childNodes,
                            f = a.nodeType;
                        if (1 != f || "bookmark" != a.getAttribute("data-mce-type")) {
                            for (b = c.length - 1; 0 <= b; b--) d(c[b]);
                            if (9 != f) {
                                if (3 == f && 0 < a.nodeValue.length) {
                                    b = t(a.nodeValue).length;
                                    if (!(c = !e.isBlock(a.parentNode) || 0 < b)) {
                                        if (b = 0 === b) b = a.nextSibling && "SPAN" == a.nextSibling.nodeName, b = a.previousSibling && "SPAN" == a.previousSibling.nodeName && b;
                                        c = b
                                    }
                                    if (c) return
                                } else if (1 ==
                                    f && (c = a.childNodes, 1 == c.length && c[0] && 1 == c[0].nodeType && "bookmark" == c[0].getAttribute("data-mce-type") && a.parentNode.insertBefore(c[0], a), c.length || /^(br|hr|input|img)$/i.test(a.nodeName))) return;
                                e.remove(a)
                            }
                            return a
                        }
                    }
                    var e = this,
                        f = e.createRng(),
                        m, p;
                    if (a && b) return f.setStart(a.parentNode, e.nodeIndex(a)), f.setEnd(b.parentNode, e.nodeIndex(b)), m = f.extractContents(), f = e.createRng(), f.setStart(b.parentNode, e.nodeIndex(b) + 1), f.setEnd(a.parentNode, e.nodeIndex(a) + 1), f = f.extractContents(), p = a.parentNode, p.insertBefore(d(m),
                        a), c ? p.insertBefore(c, a) : p.insertBefore(b, a), p.insertBefore(d(f), a), e.remove(a), c || b
                },
                bind: function(a, b, c, d) {
                    if (l.isArray(a)) {
                        for (var e = a.length; e--;) a[e] = this.bind(a[e], b, c, d);
                        return a
                    }!this.settings.collect || a !== this.doc && a !== this.win || this.boundEvents.push([a, b, c, d]);
                    return this.events.bind(a, b, c, d || this)
                },
                unbind: function(a, b, c) {
                    var d;
                    if (l.isArray(a)) {
                        for (d = a.length; d--;) a[d] = this.unbind(a[d], b, c);
                        return a
                    }
                    if (this.boundEvents && (a === this.doc || a === this.win))
                        for (d = this.boundEvents.length; d--;) {
                            var e =
                                this.boundEvents[d];
                            a != e[0] || b && b != e[1] || c && c != e[2] || this.events.unbind(e[0], e[1], e[2])
                        }
                    return this.events.unbind(a, b, c)
                },
                fire: function(a, b, c) {
                    return this.events.fire(a, b, c)
                },
                getContentEditable: function(a) {
                    var b;
                    return a && 1 == a.nodeType ? (b = a.getAttribute("data-mce-contenteditable")) && "inherit" !== b ? b : "inherit" !== a.contentEditable ? a.contentEditable : null : null
                },
                getContentEditableParent: function(a) {
                    for (var b = this.getRoot(), c = null; a && a !== b && (c = this.getContentEditable(a), null === c); a = a.parentNode);
                    return c
                },
                destroy: function() {
                    if (this.boundEvents) {
                        for (var a = this.boundEvents.length; a--;) {
                            var b = this.boundEvents[a];
                            this.events.unbind(b[0], b[1], b[2])
                        }
                        this.boundEvents = null
                    }
                    k.setDocument && k.setDocument();
                    this.win = this.doc = this.root = this.events = this.frag = null
                },
                isChildOf: function(a, b) {
                    for (; a;) {
                        if (b === a) return !0;
                        a = a.parentNode
                    }
                    return !1
                },
                dumpRng: function(a) {
                    return "startContainer: " + a.startContainer.nodeName + ", startOffset: " + a.startOffset + ", endContainer: " + a.endContainer.nodeName + ", endOffset: " + a.endOffset
                },
                _findSib: function(a, b, c) {
                    var d = this,
                        e = b;
                    if (a)
                        for ("string" == typeof e && (e = function(a) {
                                return d.is(a, b)
                            }), a = a[c]; a; a = a[c])
                            if (e(a)) return a;
                    return null
                }
            };
            p.DOM = new p(document);
            p.nodeIndex = n;
            return p
        });
    z("tinymce/dom/ScriptLoader", ["tinymce/dom/DOMUtils", "tinymce/util/Tools"], function(k, g) {
        function f() {
            function b(a, b) {
                function c() {
                    d.remove(p);
                    f && (f.onreadystatechange = f.onload = f = null);
                    b()
                }
                var d = e,
                    f, p;
                p = d.uniqueId();
                f = document.createElement("script");
                f.id = p;
                f.type = "text/javascript";
                f.src = g._addCacheSuffix(a);
                "onreadystatechange" in f ? f.onreadystatechange = function() {
                    /loaded|complete/.test(f.readyState) && c()
                } : f.onload = c;
                f.onerror = function() {
                    "undefined" !== typeof console && console.log && console.log("Failed to load: " + a)
                };
                (document.getElementsByTagName("head")[0] || document.body).appendChild(f)
            }
            var d = {},
                f = [],
                v = {},
                q = [],
                h = 0;
            this.isDone = function(a) {
                return 2 == d[a]
            };
            this.markDone = function(a) {
                d[a] = 2
            };
            this.add = this.load = function(a, b, c) {
                void 0 == d[a] && (f.push(a), d[a] = 0);
                b && (v[a] || (v[a] = []), v[a].push({
                    func: b,
                    scope: c || this
                }))
            };
            this.remove = function(a) {
                delete d[a];
                delete v[a]
            };
            this.loadQueue = function(a, b) {
                this.loadScripts(f, a, b)
            };
            this.loadScripts = function(e, f, l) {
                function p(b) {
                    a(v[b], function(a) {
                        a.func.call(a.scope)
                    });
                    v[b] = void 0
                }
                var m;
                q.push({
                    func: f,
                    scope: l || this
                });
                m = function() {
                    var f = c(e);
                    e.length = 0;
                    a(f, function(a) {
                        2 == d[a] ? p(a) : 1 != d[a] && (d[a] = 1, h++, b(a, function() {
                            d[a] = 2;
                            h--;
                            p(a);
                            m()
                        }))
                    });
                    h || (a(q, function(a) {
                        a.func.call(a.scope)
                    }), q.length = 0)
                };
                m()
            }
        }
        var e = k.DOM,
            a = g.each,
            c = g.grep;
        f.ScriptLoader = new f;
        return f
    });
    z("tinymce/AddOnManager", ["tinymce/dom/ScriptLoader", "tinymce/util/Tools"], function(k, g) {
        function f() {
            this.items = [];
            this.urls = {};
            this.lookup = {}
        }
        var e = g.each;
        f.prototype = {
            get: function(a) {
                return this.lookup[a] ? this.lookup[a].instance : K
            },
            dependencies: function(a) {
                var c;
                this.lookup[a] && (c = this.lookup[a].dependencies);
                return c || []
            },
            requireLangPack: function(a, c) {
                var b = f.language;
                if (b && !1 !== f.languageLoad) {
                    if (c)
                        if (c = "," + c + ",", -1 != c.indexOf("," + b.substr(0, 2) + ",")) b = b.substr(0, 2);
                        else if (-1 == c.indexOf("," + b + ",")) return;
                    k.ScriptLoader.add(this.urls[a] +
                        "/langs/" + b + ".js")
                }
            },
            add: function(a, c, b) {
                this.items.push(c);
                this.lookup[a] = {
                    instance: c,
                    dependencies: b
                };
                return c
            },
            remove: function(a) {
                delete this.urls[a];
                delete this.lookup[a]
            },
            createUrl: function(a, c) {
                return "object" === typeof c ? c : {
                    prefix: a.prefix,
                    resource: c,
                    suffix: a.suffix
                }
            },
            addComponents: function(a, c) {
                var b = this.urls[a];
                e(c, function(a) {
                    k.ScriptLoader.add(b + "/" + a)
                })
            },
            load: function(a, c, b, d) {
                function l() {
                    var f = v.dependencies(a);
                    e(f, function(a) {
                        a = v.createUrl(c, a);
                        v.load(a.resource, a, K, K)
                    });
                    b && (d ? b.call(d) :
                        b.call(k))
                }
                var v = this,
                    q = c;
                v.urls[a] || ("object" === typeof c && (q = c.prefix + c.resource + c.suffix), 0 !== q.indexOf("/") && -1 == q.indexOf("://") && (q = f.baseURL + "/" + q), v.urls[a] = q.substring(0, q.lastIndexOf("/")), v.lookup[a] ? l() : k.ScriptLoader.add(q, l, d))
            }
        };
        f.PluginManager = new f;
        f.ThemeManager = new f;
        return f
    });
    z("tinymce/dom/NodeType", [], function() {
        function k(a) {
            return function(c) {
                return !!c && c.nodeType == a
            }
        }

        function g(a) {
            a = a.toLowerCase().split(" ");
            return function(c) {
                var b;
                if (c && c.nodeType)
                    for (b = c.nodeName.toLowerCase(),
                        c = 0; c < a.length; c++)
                        if (b === a[c]) return !0;
                return !1
            }
        }

        function f(a) {
            return function(c) {
                return !e(c) || c.contentEditable !== a && c.getAttribute("data-mce-contenteditable") !== a ? !1 : !0
            }
        }
        var e = k(1);
        return {
            isText: k(3),
            isElement: e,
            isComment: k(8),
            isBr: g("br"),
            isContentEditableTrue: f("true"),
            isContentEditableFalse: f("false"),
            matchNodeNames: g,
            hasPropValue: function(a, c) {
                return function(b) {
                    return e(b) && b[a] === c
                }
            },
            hasAttributeValue: function(a, c) {
                return function(b) {
                    return e(b) && b.getAttribute(a) === c
                }
            },
            matchStyleValues: function(a,
                c) {
                c = c.toLowerCase().split(" ");
                return function(b) {
                    var d, f;
                    if (e(b))
                        for (d = 0; d < c.length; d++)
                            if (f = getComputedStyle(b, null).getPropertyValue(a), f === c[d]) return !0;
                    return !1
                }
            },
            isBogus: function(a) {
                return e(a) && a.hasAttribute("data-mce-bogus")
            }
        }
    });
    z("tinymce/text/Zwsp", [], function() {
        return {
            isZwsp: function(k) {
                return "\u200b" == k
            },
            ZWSP: "\u200b",
            trim: function(k) {
                return k.replace(/\u200b/g, "")
            }
        }
    });
    z("tinymce/caret/CaretContainer", ["tinymce/dom/NodeType", "tinymce/text/Zwsp"], function(k, g) {
        function f(a) {
            l(a) && (a =
                a.parentNode);
            return d(a) && a.hasAttribute("data-mce-caret")
        }

        function e(a) {
            return l(a) && g.isZwsp(a.data)
        }

        function a(a) {
            return f(a) || e(a)
        }

        function c(a) {
            return l(a) && a.data[0] == g.ZWSP
        }

        function b(a) {
            return l(a) && a.data[a.data.length - 1] == g.ZWSP
        }
        var d = k.isElement,
            l = k.isText;
        return {
            isCaretContainer: a,
            isCaretContainerBlock: f,
            isCaretContainerInline: e,
            insertInline: function(d, e) {
                var f, n;
                f = d.ownerDocument.createTextNode(g.ZWSP);
                n = d.parentNode;
                if (e) {
                    e = d.previousSibling;
                    if (l(e)) {
                        if (a(e)) return e;
                        if (b(e)) return e.splitText(e.data.length -
                            1)
                    }
                    n.insertBefore(f, d)
                } else {
                    e = d.nextSibling;
                    if (l(e)) {
                        if (a(e)) return e;
                        if (c(e)) return e.splitText(1), e
                    }
                    d.nextSibling ? n.insertBefore(f, d.nextSibling) : n.appendChild(f)
                }
                return f
            },
            insertBlock: function(a, b, c) {
                var d;
                d = b.ownerDocument;
                a = d.createElement(a);
                a.setAttribute("data-mce-caret", c ? "before" : "after");
                a.setAttribute("data-mce-bogus", "all");
                a.appendChild(d.createTextNode("\u00a0"));
                d = b.parentNode;
                c ? d.insertBefore(a, b) : b.nextSibling ? d.insertBefore(a, b.nextSibling) : d.appendChild(a);
                return a
            },
            remove: function(b) {
                var c;
                d(b) && a(b) && ("\x26nbsp;" != b.innerHTML ? b.removeAttribute("data-mce-caret") : b.parentNode && b.parentNode.removeChild(b));
                l(b) && (c = g.trim(b.data), 0 === c.length && b.parentNode && b.parentNode.removeChild(b), b.nodeValue = c)
            },
            startsWithCaretContainer: c,
            endsWithCaretContainer: b
        }
    });
    z("tinymce/dom/RangeUtils", ["tinymce/util/Tools", "tinymce/dom/TreeWalker", "tinymce/dom/NodeType", "tinymce/caret/CaretContainer"], function(k, g, f, e) {
        function a(a, b) {
            var c = a.childNodes;
            b--;
            b > c.length - 1 ? b = c.length - 1 : 0 > b && (b = 0);
            return c[b] ||
                a
        }

        function c(b) {
            this.walk = function(c, e) {
                function f(a) {
                    var b;
                    b = a[0];
                    3 === b.nodeType && b === n && y >= b.nodeValue.length && a.splice(0, 1);
                    b = a[a.length - 1];
                    0 === x && 0 < a.length && b === q && 3 === b.nodeType && a.splice(a.length - 1, 1);
                    return a
                }

                function h(a, b, c) {
                    for (var d = []; a && a != c; a = a[b]) d.push(a);
                    return d
                }

                function l(a, b) {
                    do {
                        if (a.parentNode == b) return a;
                        a = a.parentNode
                    } while (a)
                }

                function m(a, b, c) {
                    var d = c ? "nextSibling" : "previousSibling";
                    u = a;
                    for (g = u.parentNode; u && u != b; u = g) g = u.parentNode, v = h(u == a ? u : u[d], d), v.length && (c || v.reverse(),
                        e(f(v)))
                }
                var n = c.startContainer,
                    y = c.startOffset,
                    q = c.endContainer,
                    x = c.endOffset,
                    r, u, g, v;
                c = b.select("td[data-mce-selected],th[data-mce-selected]");
                if (0 < c.length) d(c, function(a) {
                    e([a])
                });
                else {
                    1 == n.nodeType && n.hasChildNodes() && (n = n.childNodes[y]);
                    1 == q.nodeType && q.hasChildNodes() && (q = a(q, x));
                    if (n == q) return e(f([n]));
                    r = b.findCommonAncestor(n, q);
                    for (u = n; u; u = u.parentNode) {
                        if (u === q) return m(n, r, !0);
                        if (u === r) break
                    }
                    for (u = q; u; u = u.parentNode) {
                        if (u === n) return m(q, r);
                        if (u === r) break
                    }
                    c = l(n, r) || n;
                    r = l(q, r) || q;
                    m(n, c, !0);
                    v = h(c == n ? c : c.nextSibling, "nextSibling", r == q ? r.nextSibling : r);
                    v.length && e(f(v));
                    m(q, r)
                }
            };
            this.split = function(a) {
                var b = a.startContainer,
                    c = a.startOffset,
                    d = a.endContainer;
                a = a.endOffset;
                b == d && 3 == b.nodeType ? 0 < c && c < b.nodeValue.length && (d = b.splitText(c), b = d.previousSibling, a > c ? (b = d = d.splitText(a - c).previousSibling, a = d.nodeValue.length, c = 0) : a = 0) : (3 == b.nodeType && 0 < c && c < b.nodeValue.length && (b = b.splitText(c), c = 0), 3 == d.nodeType && 0 < a && a < d.nodeValue.length && (d = d.splitText(a).previousSibling, a = d.nodeValue.length));
                return {
                    startContainer: b,
                    startOffset: c,
                    endContainer: d,
                    endOffset: a
                }
            };
            this.normalize = function(a) {
                function c(c) {
                    function f(a, c) {
                        for (var d = new g(a, b.getParent(a.parentNode, b.isBlock) || u); a = d[c ? "prev" : "next"]();)
                            if ("BR" === a.nodeName) return !0
                    }

                    function p(a) {
                        for (; a && a != u;) {
                            if (l(a)) return !0;
                            a = a.parentNode
                        }
                        return !1
                    }

                    function h(a, c) {
                        var f, m;
                        c = c || n;
                        m = b.getParent(c.parentNode, b.isBlock) || u;
                        if (a && "BR" == c.nodeName && k && b.isEmpty(m)) n = c.parentNode, x = b.nodeIndex(c), d = !0;
                        else {
                            for (c = new g(c, m); w = c[a ? "prev" : "next"]();) {
                                if ("false" ===
                                    b.getContentEditableParent(w) || v(w)) return;
                                if (3 === w.nodeType && 0 < w.nodeValue.length) {
                                    n = w;
                                    x = a ? w.nodeValue.length : 0;
                                    d = !0;
                                    return
                                }
                                if (b.isBlock(w) || q[w.nodeName.toLowerCase()]) return;
                                f = w
                            }
                            e && f && (n = f, d = !0, x = 0)
                        }
                    }
                    var n, x, r, u = b.getRoot(),
                        w, q, A, k;
                    n = a[(c ? "start" : "end") + "Container"];
                    x = a[(c ? "start" : "end") + "Offset"];
                    k = 1 == n.nodeType && x === n.childNodes.length;
                    q = b.schema.getNonEmptyElements();
                    A = c;
                    if (!v(n)) {
                        1 == n.nodeType && x > n.childNodes.length - 1 && (A = !1);
                        9 === n.nodeType && (n = b.getRoot(), x = 0);
                        if (n === u) {
                            if (A && (w = n.childNodes[0 <
                                    x ? x - 1 : 0]) && (v(w) || q[w.nodeName] || "TABLE" == w.nodeName)) return;
                            if (n.hasChildNodes()) {
                                x = Math.min(!A && 0 < x ? x - 1 : x, n.childNodes.length - 1);
                                n = n.childNodes[x];
                                x = 0;
                                if (!e && n === u.lastChild && "TABLE" === n.nodeName || p(n) || v(n)) return;
                                if (n.hasChildNodes() && !/TABLE/.test(n.nodeName)) {
                                    w = n;
                                    r = new g(n, u);
                                    do {
                                        if (l(w) || v(w)) {
                                            d = !1;
                                            break
                                        }
                                        if (3 === w.nodeType && 0 < w.nodeValue.length) {
                                            x = A ? 0 : w.nodeValue.length;
                                            n = w;
                                            d = !0;
                                            break
                                        }
                                        if (q[w.nodeName.toLowerCase()] && (!w || !/^(TD|TH|CAPTION)$/.test(w.nodeName))) {
                                            x = b.nodeIndex(w);
                                            n = w.parentNode;
                                            "IMG" != w.nodeName || A || x++;
                                            d = !0;
                                            break
                                        }
                                    } while (w = A ? r.next() : r.prev())
                                }
                            }
                        }
                        e && (3 === n.nodeType && 0 === x && h(!0), 1 === n.nodeType && ((w = n.childNodes[x]) || (w = n.childNodes[x - 1]), !w || "BR" !== w.nodeName || w.previousSibling && "A" == w.previousSibling.nodeName || f(w) || f(w, !0) || h(!0, w)));
                        A && !e && 3 === n.nodeType && x === n.nodeValue.length && h(!1);
                        if (d) a["set" + (c ? "Start" : "End")](n, x)
                    }
                }
                var d, e;
                e = a.collapsed;
                c(!0);
                e || c();
                d && e && a.collapse(!0);
                return d
            }
        }

        function b(a, b, c) {
            var d, e;
            d = c.elementFromPoint(a, b);
            e = c.body.createTextRange();
            d &&
                "HTML" != d.tagName || (d = c.body);
            e.moveToElementText(d);
            c = k.toArray(e.getClientRects());
            c = c.sort(function(a, c) {
                a = Math.abs(Math.max(a.top - b, a.bottom - b));
                c = Math.abs(Math.max(c.top - b, c.bottom - b));
                return a - c
            });
            if (0 < c.length) {
                b = (c[0].bottom + c[0].top) / 2;
                try {
                    return e.moveToPoint(a, b), e.collapse(!0), e
                } catch (A) {}
            }
            return null
        }
        var d = k.each,
            l = f.isContentEditableFalse,
            v = e.isCaretContainer;
        c.compareRanges = function(a, b) {
            if (a && b)
                if (a.item || a.duplicate) {
                    if (a.item && b.item && a.item(0) === b.item(0) || a.isEqual && b.isEqual &&
                        b.isEqual(a)) return !0
                } else return a.startContainer == b.startContainer && a.startOffset == b.startOffset;
            return !1
        };
        c.getCaretRangeFromPoint = function(a, c, d) {
            var e;
            if (d.caretPositionFromPoint) a = d.caretPositionFromPoint(a, c), e = d.createRange(), e.setStart(a.offsetNode, a.offset), e.collapse(!0);
            else if (d.caretRangeFromPoint) e = d.caretRangeFromPoint(a, c);
            else if (d.body.createTextRange) {
                e = d.body.createTextRange();
                try {
                    e.moveToPoint(a, c), e.collapse(!0)
                } catch (w) {
                    e = b(a, c, d)
                }
            }
            return e
        };
        c.getSelectedNode = function(a) {
            var b =
                a.startContainer,
                c = a.startOffset;
            return b.hasChildNodes() && a.endOffset == c + 1 ? b.childNodes[c] : null
        };
        c.getNode = function(a, b) {
            1 == a.nodeType && a.hasChildNodes() && (b >= a.childNodes.length && (b = a.childNodes.length - 1), a = a.childNodes[b]);
            return a
        };
        return c
    });
    z("tinymce/NodeChange", ["tinymce/dom/RangeUtils", "tinymce/Env", "tinymce/util/Delay"], function(k, g, f) {
        return function(e) {
            var a, c = [];
            if (!("onselectionchange" in e.getDoc())) e.on("NodeChange Click MouseUp KeyUp Focus", function(b) {
                var c;
                c = e.selection.getRng();
                c = {
                    startContainer: c.startContainer,
                    startOffset: c.startOffset,
                    endContainer: c.endContainer,
                    endOffset: c.endOffset
                };
                "nodechange" != b.type && k.compareRanges(c, a) || e.fire("SelectionChange");
                a = c
            });
            e.on("contextmenu", function() {
                e.fire("SelectionChange")
            });
            e.on("SelectionChange", function() {
                var a = e.selection.getStart(!0);
                if (g.range || !e.selection.isCollapsed()) {
                    var d;
                    a: {
                        var f;f = e.$(a).parentsUntil(e.getBody()).add(a);
                        if (f.length === c.length) {
                            for (d = f.length; 0 <= d && f[d] === c[d]; d--);
                            if (-1 === d) {
                                c = f;
                                d = !0;
                                break a
                            }
                        }
                        c =
                        f;d = !1
                    }!d && e.dom.isChildOf(a, e.getBody()) && e.nodeChanged({
                        selectionChange: !0
                    })
                }
            });
            e.on("MouseUp", function(a) {
                a.isDefaultPrevented() || ("IMG" == e.selection.getNode().nodeName ? f.setEditorTimeout(e, function() {
                    e.nodeChanged()
                }) : e.nodeChanged())
            });
            this.nodeChanged = function(a) {
                var b = e.selection,
                    c, f, q;
                e.initialized && b && !e.settings.disable_nodechange && !e.readonly && (q = e.getBody(), c = b.getStart() || q, c.ownerDocument == e.getDoc() && e.dom.isChildOf(c, q) || (c = q), "IMG" == c.nodeName && b.isCollapsed() && (c = c.parentNode), f = [], e.dom.getParent(c, function(a) {
                    if (a === q) return !0;
                    f.push(a)
                }), a = a || {}, a.element = c, a.parents = f, e.fire("NodeChange", a))
            }
        }
    });
    z("tinymce/html/Node", [], function() {
        function k(a, c, b) {
            var d;
            d = b ? "lastChild" : "firstChild";
            b = b ? "prev" : "next";
            if (a[d]) return a[d];
            if (a !== c) {
                if (d = a[b]) return d;
                for (a = a.parent; a && a !== c; a = a.parent)
                    if (d = a[b]) return d
            }
        }

        function g(a, c) {
            this.name = a;
            this.type = c;
            1 === c && (this.attributes = [], this.attributes.map = {})
        }
        var f = /^[ \t\r\n]*$/,
            e = {
                "#text": 3,
                "#comment": 8,
                "#cdata": 4,
                "#pi": 7,
                "#doctype": 10,
                "#document-fragment": 11
            };
        g.prototype = {
            replace: function(a) {
                a.parent && a.remove();
                this.insert(a, this);
                this.remove();
                return this
            },
            attr: function(a, c) {
                var b, d;
                if ("string" !== typeof a) {
                    for (d in a) this.attr(d, a[d]);
                    return this
                }
                if (b = this.attributes) {
                    if (void 0 !== c) {
                        if (null === c) {
                            if (a in b.map)
                                for (delete b.map[a], d = b.length; d--;)
                                    if (b[d].name === a) {
                                        b.splice(d, 1);
                                        break
                                    }
                            return this
                        }
                        if (a in b.map)
                            for (d = b.length; d--;) {
                                if (b[d].name === a) {
                                    b[d].value = c;
                                    break
                                }
                            } else b.push({
                                name: a,
                                value: c
                            });
                        b.map[a] = c;
                        return this
                    }
                    return b.map[a]
                }
            },
            clone: function() {
                var a = new g(this.name, this.type),
                    c, b, d, e, f;
                if (d = this.attributes) {
                    f = [];
                    f.map = {};
                    c = 0;
                    for (b = d.length; c < b; c++) e = d[c], "id" !== e.name && (f[f.length] = {
                        name: e.name,
                        value: e.value
                    }, f.map[e.name] = e.value);
                    a.attributes = f
                }
                a.value = this.value;
                a.shortEnded = this.shortEnded;
                return a
            },
            wrap: function(a) {
                this.parent.insert(a, this);
                a.append(this);
                return this
            },
            unwrap: function() {
                var a, c;
                for (a = this.firstChild; a;) c = a.next, this.insert(a, this, !0), a = c;
                this.remove()
            },
            remove: function() {
                var a = this.parent,
                    c = this.next,
                    b = this.prev;
                if (a) {
                    if (a.firstChild === this) {
                        if (a.firstChild = c) c.prev = null
                    } else b.next = c;
                    if (a.lastChild === this) {
                        if (a.lastChild = b) b.next = null
                    } else c.prev = b;
                    this.parent = this.next = this.prev = null
                }
                return this
            },
            append: function(a) {
                var c;
                a.parent && a.remove();
                (c = this.lastChild) ? (c.next = a, a.prev = c, this.lastChild = a) : this.lastChild = this.firstChild = a;
                a.parent = this;
                return a
            },
            insert: function(a, c, b) {
                var d;
                a.parent && a.remove();
                d = c.parent || this;
                b ? (c === d.firstChild ? d.firstChild = a : c.prev.next = a, a.prev = c.prev, a.next = c, c.prev =
                    a) : (c === d.lastChild ? d.lastChild = a : c.next.prev = a, a.next = c.next, a.prev = c, c.next = a);
                a.parent = d;
                return a
            },
            getAll: function(a) {
                var c, b = [];
                for (c = this.firstChild; c; c = k(c, this)) c.name === a && b.push(c);
                return b
            },
            empty: function() {
                var a, c, b;
                if (this.firstChild) {
                    a = [];
                    for (b = this.firstChild; b; b = k(b, this)) a.push(b);
                    for (c = a.length; c--;) b = a[c], b.parent = b.firstChild = b.lastChild = b.next = b.prev = null
                }
                this.firstChild = this.lastChild = null;
                return this
            },
            isEmpty: function(a) {
                var c = this.firstChild,
                    b, d;
                if (c) {
                    do {
                        if (1 === c.type) {
                            if (c.attributes.map["data-mce-bogus"]) continue;
                            if (a[c.name]) return !1;
                            for (b = c.attributes.length; b--;)
                                if (d = c.attributes[b].name, "name" === d || 0 === d.indexOf("data-mce-bookmark")) return !1
                        }
                        if (8 === c.type || 3 === c.type && !f.test(c.value)) return !1
                    } while (c = k(c, this))
                }
                return !0
            },
            walk: function(a) {
                return k(this, null, a)
            }
        };
        g.create = function(a, c) {
            var b;
            a = new g(a, e[a] || 1);
            if (c)
                for (b in c) a.attr(b, c[b]);
            return a
        };
        return g
    });
    z("tinymce/html/Schema", ["tinymce/util/Tools"], function(k) {
        function g(a, b) {
            return a ? a.split(b || " ") : []
        }

        function f(b) {
            function e(a, b, d) {
                function e(a,
                    b) {
                    var c = {},
                        d, e;
                    d = 0;
                    for (e = a.length; d < e; d++) c[a[d]] = b || {};
                    return c
                }
                var f, m;
                m = arguments;
                d = d || [];
                b = b || "";
                "string" === typeof d && (d = g(d));
                for (f = 3; f < m.length; f++) "string" === typeof m[f] && (m[f] = g(m[f])), d.push.apply(d, m[f]);
                a = g(a);
                for (f = a.length; f--;) m = [].concat(l, g(b)), h[a[f]] = {
                    attributes: e(m),
                    attributesOrder: m,
                    children: e(d, c)
                }
            }

            function f(a, b) {
                var c, d, e, f;
                a = g(a);
                c = a.length;
                for (b = g(b); c--;)
                    for (d = h[a[c]], e = 0, f = b.length; e < f; e++) d.attributes[b[e]] = {}, d.attributesOrder.push(b[e])
            }
            var h = {},
                l, m, t, y, q;
            if (a[b]) return a[b];
            l = g("id accesskey class dir lang style tabindex title");
            m = g("address blockquote div dl fieldset form h1 h2 h3 h4 h5 h6 hr menu ol p pre table ul");
            t = g("a abbr b bdo br button cite code del dfn em embed i iframe img input ins kbd label map noscript object q s samp script select small span strong sub sup textarea u var #text #comment");
            "html4" != b && (l.push.apply(l, g("contenteditable contextmenu draggable dropzone hidden spellcheck translate")), m.push.apply(m, g("article aside details dialog figure header footer hgroup section nav")),
                t.push.apply(t, g("audio canvas command datalist mark meter output picture progress time wbr video ruby bdi keygen")));
            "html5-strict" != b && (l.push("xml:lang"), q = g("acronym applet basefont big font strike tt"), t.push.apply(t, q), d(q, function(a) {
                e(a, "", t)
            }), q = g("center dir isindex noframes"), m.push.apply(m, q), y = [].concat(m, t), d(q, function(a) {
                e(a, "", y)
            }));
            y = y || [].concat(m, t);
            e("html", "manifest", "head body");
            e("head", "", "base command link meta noscript script style title");
            e("title hr noscript br");
            e("base",
                "href target");
            e("link", "href rel media hreflang type sizes hreflang");
            e("meta", "name http-equiv content charset");
            e("style", "media type scoped");
            e("script", "src async defer type charset");
            e("body", "onafterprint onbeforeprint onbeforeunload onblur onerror onfocus onhashchange onload onmessage onoffline ononline onpagehide onpageshow onpopstate onresize onscroll onstorage onunload", y);
            e("address dt dd div caption", "", y);
            e("h1 h2 h3 h4 h5 h6 pre p abbr code var samp kbd sub sup i b u bdo span legend em strong small s cite dfn",
                "", t);
            e("blockquote", "cite", y);
            e("ol", "reversed start type", "li");
            e("ul", "", "li");
            e("li", "value", y);
            e("dl", "", "dt dd");
            e("a", "href target rel media hreflang type", t);
            e("q", "cite", t);
            e("ins del", "cite datetime", y);
            e("img", "src sizes srcset alt usemap ismap width height");
            e("iframe", "src name width height", y);
            e("embed", "src type width height");
            e("object", "data type typemustmatch name usemap form width height", y, "param");
            e("param", "name value");
            e("map", "name", y, "area");
            e("area", "alt coords shape href target rel media hreflang type");
            e("table", "border", "caption colgroup thead tfoot tbody tr" + ("html4" == b ? " col" : ""));
            e("colgroup", "span", "col");
            e("col", "span");
            e("tbody thead tfoot", "", "tr");
            e("tr", "", "td th");
            e("td", "colspan rowspan headers", y);
            e("th", "colspan rowspan headers scope abbr", y);
            e("form", "accept-charset action autocomplete enctype method name novalidate target", y);
            e("fieldset", "disabled form name", y, "legend");
            e("label", "form for", t);
            e("input", "accept alt autocomplete checked dirname disabled form formaction formenctype formmethod formnovalidate formtarget height list max maxlength min multiple name pattern readonly required size src step type value width");
            e("button", "disabled form formaction formenctype formmethod formnovalidate formtarget name type value", "html4" == b ? y : t);
            e("select", "disabled form multiple name required size", "option optgroup");
            e("optgroup", "disabled label", "option");
            e("option", "disabled label selected value");
            e("textarea", "cols dirname disabled form maxlength name readonly required rows wrap");
            e("menu", "type label", y, "li");
            e("noscript", "", y);
            "html4" != b && (e("wbr"), e("ruby", "", t, "rt rp"), e("figcaption", "", y), e("mark rt rp summary bdi",
                "", t), e("canvas", "width height", y), e("video", "src crossorigin poster preload autoplay mediagroup loop muted controls width height buffered", y, "track source"), e("audio", "src crossorigin preload autoplay mediagroup loop muted controls buffered volume", y, "track source"), e("picture", "", "img source"), e("source", "src srcset type media sizes"), e("track", "kind src srclang label default"), e("datalist", "", t, "option"), e("article section nav aside header footer", "", y), e("hgroup", "", "h1 h2 h3 h4 h5 h6"), e("figure",
                "", y, "figcaption"), e("time", "datetime", t), e("dialog", "open", y), e("command", "type label icon disabled checked radiogroup command"), e("output", "for form name", t), e("progress", "value max", t), e("meter", "value min max low high optimum", t), e("details", "open", y, "summary"), e("keygen", "autofocus challenge disabled form keytype name"));
            "html5-strict" != b && (f("script", "language xml:space"), f("style", "xml:space"), f("object", "declare classid code codebase codetype archive standby align border hspace vspace"), f("embed",
                    "align name hspace vspace"), f("param", "valuetype type"), f("a", "charset name rev shape coords"), f("br", "clear"), f("applet", "codebase archive code object alt name width height align hspace vspace"), f("img", "name longdesc align border hspace vspace"), f("iframe", "longdesc frameborder marginwidth marginheight scrolling align"), f("font basefont", "size color face"), f("input", "usemap align"), f("select", "onchange"), f("textarea"), f("h1 h2 h3 h4 h5 h6 div p legend caption", "align"), f("ul", "type compact"), f("li",
                    "type"), f("ol dl menu dir", "compact"), f("pre", "width xml:space"), f("hr", "align noshade size width"), f("isindex", "prompt"), f("table", "summary width frame rules cellspacing cellpadding align bgcolor"), f("col", "width align char charoff valign"), f("colgroup", "width align char charoff valign"), f("thead", "align char charoff valign"), f("tr", "align char charoff valign bgcolor"), f("th", "axis align char charoff valign nowrap bgcolor width height"), f("form", "accept"), f("td", "abbr axis scope align char charoff valign nowrap bgcolor width height"),
                f("tfoot", "align char charoff valign"), f("tbody", "align char charoff valign"), f("area", "nohref"), f("body", "background bgcolor text link vlink alink"));
            "html4" != b && (f("input button select textarea", "autofocus"), f("input textarea", "placeholder"), f("a", "download"), f("link script img", "crossorigin"), f("iframe", "sandbox seamless allowfullscreen"));
            d(g("a form meter progress dfn"), function(a) {
                h[a] && delete h[a].children[a]
            });
            delete h.caption.children.table;
            delete h.script;
            return a[b] = h
        }

        function e(a, c) {
            var e;
            a && (e = {}, "string" == typeof a && (a = {
                "*": a
            }), d(a, function(a, d) {
                e[d] = e[d.toUpperCase()] = "map" == c ? b(a, /[, ]/) : v(a, /[, ]/)
            }));
            return e
        }
        var a = {},
            c = {},
            b = k.makeMap,
            d = k.each,
            l = k.extend,
            v = k.explode,
            q = k.inArray;
        return function(c) {
            function h(d, e, f) {
                var m = c[d];
                m ? m = b(m, /[, ]/, b(m.toUpperCase(), /[, ]/)) : (m = a[d], m || (m = b(e, " ", b(e.toUpperCase(), " ")), m = l(m, f), a[d] = m));
                return m
            }

            function p(a) {
                return new RegExp("^" + a.replace(/([?+*])/g, ".$1") + "$")
            }

            function w(a) {
                var c, d, e, f, m, h, l, u, n, w, t, x, y, A, v, k, F, O, W = /^([#+\-])?([^\[!\/]+)(?:\/([^\[!]+))?(?:(!?)\[([^\]]+)\])?$/,
                    G = /^([!\-])?(\w+::\w+|[^=:<]+)?(?:([=:<])(.*))?$/,
                    z = /[*?+]/;
                if (a)
                    for (a = g(a, ","), B["@"] && (k = B["@"].attributes, F = B["@"].attributesOrder), c = 0, d = a.length; c < d; c++)
                        if (m = W.exec(a[c])) {
                            A = m[1];
                            n = m[2];
                            v = m[3];
                            u = m[5];
                            x = {};
                            y = [];
                            h = {
                                attributes: x,
                                attributesOrder: y
                            };
                            "#" === A && (h.paddEmpty = !0);
                            "-" === A && (h.removeEmpty = !0);
                            "!" === m[4] && (h.removeEmptyAttrs = !0);
                            if (k) {
                                for (O in k) x[O] = k[O];
                                y.push.apply(y, F)
                            }
                            if (u)
                                for (u = g(u, "|"), e = 0, f = u.length; e < f; e++)
                                    if (m = G.exec(u[e])) l = {}, t = m[1], w = m[2].replace(/::/g, ":"), A = m[3], m = m[4], "!" ===
                                        t && (h.attributesRequired = h.attributesRequired || [], h.attributesRequired.push(w), l.required = !0), "-" === t ? (delete x[w], y.splice(q(y, w), 1)) : (A && ("\x3d" === A && (h.attributesDefault = h.attributesDefault || [], h.attributesDefault.push({
                                                name: w,
                                                value: m
                                            }), l.defaultValue = m), ":" === A && (h.attributesForced = h.attributesForced || [], h.attributesForced.push({
                                                name: w,
                                                value: m
                                            }), l.forcedValue = m), "\x3c" === A && (l.validValues = b(m, "?"))), z.test(w) ? (h.attributePatterns = h.attributePatterns || [], l.pattern = p(w), h.attributePatterns.push(l)) :
                                            (x[w] || y.push(w), x[w] = l));
                            k || "@" != n || (k = x, F = y);
                            v && (h.outputName = n, B[v] = h);
                            z.test(n) ? (h.pattern = p(n), r.push(h)) : B[n] = h
                        }
            }

            function A(a) {
                B = {};
                r = [];
                w(a);
                d(O, function(a, b) {
                    x[b] = a.children
                })
            }

            function m(b) {
                var c = /^(~)?(.+)$/;
                b && (a.text_block_elements = a.block_elements = null, d(g(b, ","), function(a) {
                    a = c.exec(a);
                    var b = "~" === a[1],
                        e = b ? "span" : "div",
                        f = a[2];
                    x[f] = x[e];
                    M[f] = e;
                    b || (I[f.toUpperCase()] = {}, I[f] = {});
                    B[f] || (a = B[e], a = l({}, a), delete a.removeEmptyAttrs, delete a.removeEmpty, B[f] = a);
                    d(x, function(a, b) {
                        a[e] && (x[b] =
                            a = l({}, x[b]), a[f] = a[e])
                    })
                }))
            }

            function t(b) {
                var e = /^([+\-]?)(\w+)\[([^\]]+)\]$/;
                a[c.schema] = null;
                b && d(g(b, ","), function(a) {
                    a = e.exec(a);
                    var b, c;
                    a && (b = (c = a[1]) ? x[a[2]] : x[a[2]] = {
                        "#comment": {}
                    }, b = x[a[2]], d(g(a[3], "|"), function(a) {
                        "-" === c ? delete b[a] : b[a] = {}
                    }))
                })
            }

            function y(a) {
                var b = B[a],
                    c;
                if (b) return b;
                for (c = r.length; c--;)
                    if (b = r[c], b.pattern.test(a)) return b
            }
            var B = {},
                x = {},
                r = [],
                u, k, O, W, z, G, N, R, I, H, L, T, E, M = {},
                C = {};
            c = c || {};
            O = f(c.schema);
            !1 === c.verify_html && (c.valid_elements = "*[*]");
            u = e(c.valid_styles);
            k =
                e(c.invalid_styles, "map");
            R = e(c.valid_classes, "map");
            W = h("whitespace_elements", "pre script noscript style textarea video audio iframe object");
            z = h("self_closing_elements", "colgroup dd dt li option p td tfoot th thead tr");
            G = h("short_ended_elements", "area base basefont br col frame hr img input isindex link meta param embed source wbr track");
            N = h("boolean_attributes", "checked compact declare defer disabled ismap multiple nohref noresize noshade nowrap readonly selected autoplay loop controls");
            H = h("non_empty_elements",
                "td th iframe video audio object script", G);
            L = h("move_caret_before_on_enter_elements", "table", H);
            T = h("text_block_elements", "h1 h2 h3 h4 h5 h6 p div address pre form blockquote center dir fieldset header footer article section hgroup aside nav figure");
            I = h("block_elements", "hr table tbody thead tfoot th tr td li ol ul caption dl dt dd noscript menu isindex option datalist select optgroup figcaption", T);
            E = h("text_inline_elements", "span strong b em i font strike u var cite dfn code mark q sup sub samp");
            d((c.special || "script noscript style textarea").split(" "), function(a) {
                C[a] = new RegExp("\x3c/" + a + "[^\x3e]*\x3e", "gi")
            });
            c.valid_elements ? A(c.valid_elements) : (d(O, function(a, b) {
                B[b] = {
                    attributes: a.attributes,
                    attributesOrder: a.attributesOrder
                };
                x[b] = a.children
            }), "html5" != c.schema && d(g("strong/b em/i"), function(a) {
                a = g(a, "/");
                B[a[1]].outputName = a[0]
            }), d(g("ol ul sub sup blockquote span font a table tbody tr strong em b i"), function(a) {
                B[a] && (B[a].removeEmpty = !0)
            }), d(g("p h1 h2 h3 h4 h5 h6 th td pre div address caption"),
                function(a) {
                    B[a].paddEmpty = !0
                }), d(g("span"), function(a) {
                B[a].removeEmptyAttrs = !0
            }));
            m(c.custom_elements);
            t(c.valid_children);
            w(c.extended_valid_elements);
            t("+ol[ul|ol],+ul[ul|ol]");
            c.invalid_elements && d(v(c.invalid_elements), function(a) {
                B[a] && delete B[a]
            });
            y("span") || w("span[!data-mce-type|*]");
            this.children = x;
            this.getValidStyles = function() {
                return u
            };
            this.getInvalidStyles = function() {
                return k
            };
            this.getValidClasses = function() {
                return R
            };
            this.getBoolAttrs = function() {
                return N
            };
            this.getBlockElements = function() {
                return I
            };
            this.getTextBlockElements = function() {
                return T
            };
            this.getTextInlineElements = function() {
                return E
            };
            this.getShortEndedElements = function() {
                return G
            };
            this.getSelfClosingElements = function() {
                return z
            };
            this.getNonEmptyElements = function() {
                return H
            };
            this.getMoveCaretBeforeOnEnterElements = function() {
                return L
            };
            this.getWhiteSpaceElements = function() {
                return W
            };
            this.getSpecialElements = function() {
                return C
            };
            this.isValidChild = function(a, b) {
                a = x[a];
                return !(!a || !a[b])
            };
            this.isValid = function(a, b) {
                var c;
                if (c = y(a))
                    if (b) {
                        if (c.attributes[b]) return !0;
                        if (b = c.attributePatterns)
                            for (c = b.length; c--;)
                                if (b[c].pattern.test(a)) return !0
                    } else return !0;
                return !1
            };
            this.getElementRule = y;
            this.getCustomElements = function() {
                return M
            };
            this.addValidElements = w;
            this.setValidElements = A;
            this.addCustomElements = m;
            this.addValidChildren = t;
            this.elements = B
        }
    });
    z("tinymce/html/SaxParser", ["tinymce/html/Schema", "tinymce/html/Entities", "tinymce/util/Tools"], function(k, g, f) {
        function e(a, c, e) {
            var b = 1,
                d, f;
            f = a.getShortEndedElements();
            a = /<([!?\/])?([A-Za-z0-9\-_\:\.]+)((?:\s+[^"\'>]+(?:(?:"[^"]*")|(?:\'[^\']*\')|[^>]*))*|\/|\s+)>/g;
            for (a.lastIndex = e; d = a.exec(c);) {
                e = a.lastIndex;
                if ("/" === d[1]) b--;
                else if (!d[1]) {
                    if (d[2] in f) continue;
                    b++
                }
                if (0 === b) break
            }
            return e
        }

        function a(a, d) {
            function b() {}
            var v = this;
            a = a || {};
            v.schema = d = d || new k;
            !1 !== a.fix_self_closing && (a.fix_self_closing = !0);
            c("comment cdata text start end pi doctype".split(" "), function(c) {
                c && (v[c] = a[c] || b)
            });
            v.parse = function(b) {
                function c(a) {
                    var b, c;
                    for (b = y.length; b-- && y[b].name !== a;);
                    if (0 <= b) {
                        for (c = y.length - 1; c >= b; c--) a = y[c], a.valid && p.end(a.name);
                        y.length = b
                    }
                }

                function l(b,
                    c, d, e, f) {
                    b = /[\s\u0000-\u001F]+/g;
                    c = c.toLowerCase();
                    d = c in z ? c : Z(d || e || f || "");
                    if (G && !v && 0 !== c.indexOf("data-")) {
                        e = L[c];
                        if (!e && T) {
                            for (f = T.length; f-- && (e = T[f], !e.pattern.test(c));); - 1 === f && (e = null)
                        }
                        if (!e || e.validValues && !(d in e.validValues)) return
                    }
                    if (X[c] && !a.allow_script_urls) {
                        b = d.replace(b, "");
                        try {
                            b = decodeURIComponent(b)
                        } catch (S) {
                            b = unescape(b)
                        }
                        if (V.test(b) || !a.allow_html_data_urls && ca.test(b) && !/^data:image\//i.test(b)) return
                    }
                    B.map[c] = d;
                    B.push({
                        name: c,
                        value: d
                    })
                }
                var p = this,
                    w, q = 0,
                    m, t, y = [],
                    B, x, r, u, v,
                    k, W, z, G, N, R, I, H, L, T, E, M, C, D, P, K, Q = 0,
                    Z = g.decode,
                    aa, X = f.makeMap("src,href,data,background,formaction,poster"),
                    V = /((java|vb)script|mhtml):/i,
                    ca = /^data:/i;
                D = /<(?:(?:!--([\w\W]*?)--\x3e)|(?:!\[CDATA\[([\w\W]*?)\]\]>)|(?:!DOCTYPE([\w\W]*?)>)|(?:\?([^\s\/<>]+) ?([\w\W]*?)[?/]>)|(?:\/([^>]+)>)|(?:([A-Za-z0-9\-_\:\.]+)((?:\s+[^"'>]+(?:(?:"[^"]*")|(?:'[^']*')|[^>]*))*|\/|\s+)>))/g;
                P = /([\w:\-]+)(?:\s*=\s*(?:(?:\"((?:[^\"])*)\")|(?:\'((?:[^\'])*)\')|([^>\s]+)))?/g;
                W = d.getShortEndedElements();
                C = a.self_closing_elements ||
                    d.getSelfClosingElements();
                z = d.getBoolAttrs();
                G = a.validate;
                k = a.remove_internals;
                aa = a.fix_self_closing;
                for (K = d.getSpecialElements(); w = D.exec(b);) {
                    q < w.index && p.text(Z(b.substr(q, w.index - q)));
                    if (m = w[6]) m = m.toLowerCase(), ":" === m.charAt(0) && (m = m.substr(1)), c(m);
                    else if (m = w[7]) {
                        m = m.toLowerCase();
                        ":" === m.charAt(0) && (m = m.substr(1));
                        q = m in W;
                        aa && C[m] && 0 < y.length && y[y.length - 1].name === m && c(m);
                        if (!G || (N = d.getElementRule(m))) {
                            R = !0;
                            G && (L = N.attributes, T = N.attributePatterns);
                            (H = w[8]) ? ((v = -1 !== H.indexOf("data-mce-type")) &&
                                k && (R = !1), B = [], B.map = {}, H.replace(P, l)) : (B = [], B.map = {});
                            if (G && !v) {
                                t = N.attributesRequired;
                                E = N.attributesDefault;
                                M = N.attributesForced;
                                (x = N.removeEmptyAttrs) && !B.length && (R = !1);
                                if (M)
                                    for (x = M.length; x--;) I = M[x], u = I.name, I = I.value, "{$uid}" === I && (I = "mce_" + Q++), B.map[u] = I, B.push({
                                        name: u,
                                        value: I
                                    });
                                if (E)
                                    for (x = E.length; x--;) I = E[x], u = I.name, u in B.map || (I = I.value, "{$uid}" === I && (I = "mce_" + Q++), B.map[u] = I, B.push({
                                        name: u,
                                        value: I
                                    }));
                                if (t) {
                                    for (x = t.length; x-- && !(t[x] in B.map);); - 1 === x && (R = !1)
                                }
                                if (I = B.map["data-mce-bogus"]) {
                                    if ("all" ===
                                        I) {
                                        q = e(d, b, D.lastIndex);
                                        D.lastIndex = q;
                                        continue
                                    }
                                    R = !1
                                }
                            }
                            R && p.start(m, B, q)
                        } else R = !1;
                        if (t = K[m]) {
                            t.lastIndex = q = w.index + w[0].length;
                            (w = t.exec(b)) ? (R && (r = b.substr(q, w.index - q)), q = w.index + w[0].length) : (r = b.substr(q), q = b.length);
                            R && (0 < r.length && p.text(r, !0), p.end(m));
                            D.lastIndex = q;
                            continue
                        }
                        q || (H && H.indexOf("/") == H.length - 1 ? R && p.end(m) : y.push({
                            name: m,
                            valid: R
                        }))
                    } else(m = w[1]) ? ("\x3e" === m.charAt(0) && (m = " " + m), a.allow_conditional_comments || "[if" !== m.substr(0, 3) || (m = " " + m), p.comment(m)) : (m = w[2]) ? p.cdata(m) : (m = w[3]) ?
                        p.doctype(m) : (m = w[4]) && p.pi(m, w[5]);
                    q = w.index + w[0].length
                }
                q < b.length && p.text(Z(b.substr(q)));
                for (x = y.length - 1; 0 <= x; x--) m = y[x], m.valid && p.end(m.name)
            }
        }
        var c = f.each;
        a.findEndTag = e;
        return a
    });
    z("tinymce/html/DomParser", ["tinymce/html/Node", "tinymce/html/Schema", "tinymce/html/SaxParser", "tinymce/util/Tools"], function(k, g, f, e) {
        var a = e.makeMap,
            c = e.each,
            b = e.explode,
            d = e.extend;
        return function(e, v) {
            function l(b) {
                var c, d, e, f, m, p, l, w, n, q, A, g, z;
                q = a("tr,td,th,tbody,thead,tfoot,table");
                n = v.getNonEmptyElements();
                A = v.getTextBlockElements();
                g = v.getSpecialElements();
                for (c = 0; c < b.length; c++)
                    if (d = b[c], d.parent && !d.fixed)
                        if (A[d.name] && "li" == d.parent.name) {
                            for (e = d.next; e;) {
                                if (A[e.name]) e.name = "li", e.fixed = !0, d.parent.insert(e, d.parent);
                                else break;
                                e = e.next
                            }
                            d.unwrap(d)
                        } else {
                            f = [d];
                            for (e = d.parent; e && !v.isValidChild(e.name, d.name) && !q[e.name]; e = e.parent) f.push(e);
                            if (e && 1 < f.length) {
                                f.reverse();
                                m = p = h.filterNode(f[0].clone());
                                for (w = 0; w < f.length - 1; w++) {
                                    v.isValidChild(p.name, f[w].name) ? (l = h.filterNode(f[w].clone()), p.append(l)) :
                                        l = p;
                                    for (p = f[w].firstChild; p && p != f[w + 1];) z = p.next, l.append(p), p = z;
                                    p = l
                                }
                                m.isEmpty(n) ? e.insert(d, f[0], !0) : (e.insert(m, f[0], !0), e.insert(d, m));
                                e = f[0];
                                (e.isEmpty(n) || e.firstChild === e.lastChild && "br" === e.firstChild.name) && e.empty().remove()
                            } else d.parent && ("li" === d.name ? (e = d.prev, !e || "ul" !== e.name && "ul" !== e.name ? (e = d.next, !e || "ul" !== e.name && "ul" !== e.name ? d.wrap(h.filterNode(new k("ul", 1))) : e.insert(d, e.firstChild, !0)) : e.append(d)) : v.isValidChild(d.parent.name, "div") && v.isValidChild("div", d.name) ? d.wrap(h.filterNode(new k("div",
                                1))) : g[d.name] ? d.empty().remove() : d.unwrap())
                        }
            }
            var h = this,
                n = {},
                p = [],
                w = {},
                A = {};
            e = e || {};
            e.validate = "validate" in e ? e.validate : !0;
            e.root_name = e.root_name || "body";
            h.schema = v = v || new g;
            h.filterNode = function(a) {
                var b, c, d;
                c in n && ((d = w[c]) ? d.push(a) : w[c] = [a]);
                for (b = p.length; b--;) c = p[b].name, c in a.attributes.map && ((d = A[c]) ? d.push(a) : A[c] = [a]);
                return a
            };
            h.addNodeFilter = function(a, d) {
                c(b(a), function(a) {
                    var b = n[a];
                    b || (n[a] = b = []);
                    b.push(d)
                })
            };
            h.addAttributeFilter = function(a, d) {
                c(b(a), function(a) {
                    var b;
                    for (b = 0; b <
                        p.length; b++)
                        if (p[b].name === a) {
                            p[b].callbacks.push(d);
                            return
                        }
                    p.push({
                        name: a,
                        callbacks: [d]
                    })
                })
            };
            h.parse = function(b, c) {
                function m() {
                    function a(a) {
                        a && ((b = a.firstChild) && 3 == b.type && (b.value = b.value.replace(I, "")), (b = a.lastChild) && 3 == b.type && (b.value = b.value.replace(T, "")))
                    }
                    var b = u.firstChild,
                        c, d;
                    if (v.isValidChild(u.name, K.toLowerCase())) {
                        for (; b;) c = b.next, 3 == b.type || 1 == b.type && "p" !== b.name && !R[b.name] && !b.attr("data-mce-type") ? (d || (d = h(K, 1), d.attr(e.forced_root_block_attrs), u.insert(d, b)), d.append(b)) :
                            (a(d), d = null), b = c;
                        a(d)
                    }
                }

                function h(a, b) {
                    b = new k(a, b);
                    var c;
                    a in n && ((c = w[a]) ? c.push(b) : w[a] = [b]);
                    return b
                }

                function x(a) {
                    var b, c = v.getBlockElements();
                    for (a = a.prev; a && 3 === a.type;) {
                        b = a.value.replace(T, "");
                        if (0 < b.length) {
                            a.value = b;
                            break
                        }
                        if (b = a.next) {
                            if (3 == b.type && b.value.length) {
                                a = a.prev;
                                continue
                            }
                            if (!c[b.name] && "script" != b.name && "style" != b.name) {
                                a = a.prev;
                                continue
                            }
                        }
                        b = a.prev;
                        a.remove();
                        a = b
                    }
                }
                var r, u, t, q, g, z, G, N, R, I, H = [],
                    L, T, E, M, C, D, P, K;
                c = c || {};
                w = {};
                A = {};
                R = d(a("script,style,head,html,body,title,meta,param"),
                    v.getBlockElements());
                P = v.getNonEmptyElements();
                D = v.children;
                N = e.validate;
                K = "forced_root_block" in c ? c.forced_root_block : e.forced_root_block;
                C = v.getWhiteSpaceElements();
                I = /^[ \t\r\n]+/;
                T = /[ \t\r\n]+$/;
                E = /[ \t\r\n]+/g;
                M = /^[ \t\r\n]+$/;
                r = new f({
                    validate: N,
                    allow_script_urls: e.allow_script_urls,
                    allow_conditional_comments: e.allow_conditional_comments,
                    self_closing_elements: function(a) {
                        var b, c = {};
                        for (b in a) "li" !== b && "p" != b && (c[b] = a[b]);
                        return c
                    }(v.getSelfClosingElements()),
                    cdata: function(a) {
                        t.append(h("#cdata",
                            4)).value = a
                    },
                    text: function(a, b) {
                        var c;
                        L || (a = a.replace(E, " "), t.lastChild && R[t.lastChild.name] && (a = a.replace(I, "")));
                        0 !== a.length && (c = h("#text", 3), c.raw = !!b, t.append(c).value = a)
                    },
                    comment: function(a) {
                        t.append(h("#comment", 8)).value = a
                    },
                    pi: function(a, b) {
                        t.append(h(a, 7)).value = b;
                        x(t)
                    },
                    doctype: function(a) {
                        t.append(h("#doctype", 10)).value = a;
                        x(t)
                    },
                    start: function(a, b, c) {
                        var d, e, f;
                        if (d = N ? v.getElementRule(a) : {}) {
                            d = h(d.outputName || a, 1);
                            d.attributes = b;
                            d.shortEnded = c;
                            t.append(d);
                            (e = D[t.name]) && D[d.name] && !e[d.name] &&
                                H.push(d);
                            for (e = p.length; e--;) f = p[e].name, f in b.map && ((G = A[f]) ? G.push(d) : A[f] = [d]);
                            R[a] && x(d);
                            c || (t = d);
                            !L && C[a] && (L = !0)
                        }
                    },
                    end: function(a) {
                        var b, c, d, e;
                        if (c = N ? v.getElementRule(a) : {}) {
                            if (R[a] && !L) {
                                if ((b = t.firstChild) && 3 === b.type)
                                    if (d = b.value.replace(I, ""), 0 < d.length) b.value = d;
                                    else
                                        for (e = b.next, b.remove(), b = e; b && 3 === b.type;) d = b.value, e = b.next, (0 === d.length || M.test(d)) && b.remove(), b = e;
                                if ((b = t.lastChild) && 3 === b.type)
                                    if (d = b.value.replace(T, ""), 0 < d.length) b.value = d;
                                    else
                                        for (e = b.prev, b.remove(), b = e; b && 3 ===
                                            b.type;) d = b.value, e = b.prev, (0 === d.length || M.test(d)) && b.remove(), b = e
                            }
                            L && C[a] && (L = !1);
                            if ((c.removeEmpty || c.paddEmpty) && t.isEmpty(P))
                                if (c.paddEmpty) t.empty().append(new k("#text", "3")).value = "\u00a0";
                                else if (!t.attributes.map.name && !t.attributes.map.id) {
                                a = t.parent;
                                R[t.name] ? t.empty().remove() : t.unwrap();
                                t = a;
                                return
                            }
                            t = t.parent
                        }
                    }
                }, v);
                u = t = new k(c.context || e.root_name, 11);
                r.parse(b);
                N && H.length && (c.context ? c.invalid = !0 : l(H));
                K && ("body" == u.name || c.isRootContent) && m();
                if (!c.invalid) {
                    for (z in w) {
                        G = n[z];
                        b = w[z];
                        for (g = b.length; g--;) b[g].parent || b.splice(g, 1);
                        r = 0;
                        for (q = G.length; r < q; r++) G[r](b, z, c)
                    }
                    r = 0;
                    for (q = p.length; r < q; r++)
                        if (G = p[r], G.name in A) {
                            b = A[G.name];
                            for (g = b.length; g--;) b[g].parent || b.splice(g, 1);
                            g = 0;
                            for (z = G.callbacks.length; g < z; g++) G.callbacks[g](b, G.name, c)
                        }
                }
                return u
            };
            e.remove_trailing_brs && h.addNodeFilter("br", function(a) {
                var b, c = a.length,
                    e, f = d({}, v.getBlockElements()),
                    m = v.getNonEmptyElements(),
                    p, h, l;
                f.body = 1;
                for (b = 0; b < c; b++)
                    if (e = a[b], p = e.parent, f[e.parent.name] && e === p.lastChild) {
                        for (h = e.prev; h;) {
                            l =
                                h.name;
                            if ("span" !== l || "bookmark" !== h.attr("data-mce-type")) {
                                if ("br" !== l) break;
                                if ("br" === l) {
                                    e = null;
                                    break
                                }
                            }
                            h = h.prev
                        }
                        e && (e.remove(), p.isEmpty(m) && (e = v.getElementRule(p.name))) && (e.removeEmpty ? p.remove() : e.paddEmpty && (p.empty().append(new k("#text", 3)).value = "\u00a0"))
                    } else {
                        for (h = e; p && p.firstChild === h && p.lastChild === h;) {
                            h = p;
                            if (f[p.name]) break;
                            p = p.parent
                        }
                        h === p && (p = new k("#text", 3), p.value = "\u00a0", e.replace(p))
                    }
            });
            e.allow_html_in_named_anchor || h.addAttributeFilter("id,name", function(a) {
                for (var b = a.length,
                        c, d, e, f; b--;)
                    if (f = a[b], "a" === f.name && f.firstChild && !f.attr("href")) {
                        e = f.parent;
                        c = f.lastChild;
                        do d = c.prev, e.insert(c, f), c = d; while (c)
                    }
            });
            e.validate && v.getValidClasses() && h.addAttributeFilter("class", function(a) {
                for (var b = a.length, c, d, e, f, m, p = v.getValidClasses(), h, l; b--;) {
                    c = a[b];
                    d = c.attr("class").split(" ");
                    m = "";
                    for (e = 0; e < d.length; e++) f = d[e], l = !1, (h = p["*"]) && h[f] && (l = !0), h = p[c.name], !l && h && h[f] && (l = !0), l && (m && (m += " "), m += f);
                    m.length || (m = null);
                    c.attr("class", m)
                }
            })
        }
    });
    z("tinymce/html/Writer", ["tinymce/html/Entities",
        "tinymce/util/Tools"
    ], function(k, g) {
        var f = g.makeMap;
        return function(e) {
            var a = [],
                c, b, d, l, g;
            e = e || {};
            c = e.indent;
            b = f(e.indent_before || "");
            d = f(e.indent_after || "");
            l = k.getEncodeFunc(e.entity_encoding || "raw", e.entities);
            g = "html" == e.element_format;
            return {
                start: function(e, f, n) {
                    var p, h, q;
                    c && b[e] && 0 < a.length && (p = a[a.length - 1], 0 < p.length && "\n" !== p && a.push("\n"));
                    a.push("\x3c", e);
                    if (f)
                        for (p = 0, h = f.length; p < h; p++) q = f[p], a.push(" ", q.name, '\x3d"', l(q.value, !0), '"');
                    a[a.length] = !n || g ? "\x3e" : " /\x3e";
                    n && c && d[e] && 0 <
                        a.length && (p = a[a.length - 1], 0 < p.length && "\n" !== p && a.push("\n"))
                },
                end: function(b) {
                    a.push("\x3c/", b, "\x3e");
                    c && d[b] && 0 < a.length && (b = a[a.length - 1], 0 < b.length && "\n" !== b && a.push("\n"))
                },
                text: function(b, c) {
                    0 < b.length && (a[a.length] = c ? b : l(b))
                },
                cdata: function(b) {
                    a.push("\x3c![CDATA[", b, "]]\x3e")
                },
                comment: function(b) {
                    a.push("\x3c!--", b, "--\x3e")
                },
                pi: function(b, d) {
                    d ? a.push("\x3c?", b, " ", l(d), "?\x3e") : a.push("\x3c?", b, "?\x3e");
                    c && a.push("\n")
                },
                doctype: function(b) {
                    a.push("\x3c!DOCTYPE", b, "\x3e", c ? "\n" : "")
                },
                reset: function() {
                    a.length =
                        0
                },
                getContent: function() {
                    return a.join("").replace(/\n$/, "")
                }
            }
        }
    });
    z("tinymce/html/Serializer", ["tinymce/html/Writer", "tinymce/html/Schema"], function(k, g) {
        return function(f, e) {
            var a = new k(f);
            f = f || {};
            f.validate = "validate" in f ? f.validate : !0;
            this.schema = e = e || new g;
            this.writer = a;
            this.serialize = function(c) {
                function b(c) {
                    var f = d[c.type],
                        h, n, p, w, g, m, t, y;
                    if (f) f(c);
                    else {
                        f = c.name;
                        h = c.shortEnded;
                        n = c.attributes;
                        if (l && n && 1 < n.length && (g = [], g.map = {}, y = e.getElementRule(c.name))) {
                            m = 0;
                            for (t = y.attributesOrder.length; m <
                                t; m++) p = y.attributesOrder[m], p in n.map && (w = n.map[p], g.map[p] = w, g.push({
                                name: p,
                                value: w
                            }));
                            m = 0;
                            for (t = n.length; m < t; m++) p = n[m].name, p in g.map || (w = n.map[p], g.map[p] = w, g.push({
                                name: p,
                                value: w
                            }));
                            n = g
                        }
                        a.start(c.name, n, h);
                        if (!h) {
                            if (c = c.firstChild) {
                                do b(c); while (c = c.next)
                            }
                            a.end(f)
                        }
                    }
                }
                var d, l;
                l = f.validate;
                d = {
                    3: function(b) {
                        a.text(b.value, b.raw)
                    },
                    8: function(b) {
                        a.comment(b.value)
                    },
                    7: function(b) {
                        a.pi(b.name, b.value)
                    },
                    10: function(b) {
                        a.doctype(b.value)
                    },
                    4: function(b) {
                        a.cdata(b.value)
                    },
                    11: function(a) {
                        if (a = a.firstChild) {
                            do b(a);
                            while (a = a.next)
                        }
                    }
                };
                a.reset();
                if (1 != c.type || f.inner) d[11](c);
                else b(c);
                return a.getContent()
            }
        }
    });
    z("tinymce/dom/Serializer", "tinymce/dom/DOMUtils tinymce/html/DomParser tinymce/html/SaxParser tinymce/html/Entities tinymce/html/Serializer tinymce/html/Node tinymce/html/Schema tinymce/Env tinymce/util/Tools tinymce/text/Zwsp".split(" "), function(k, g, f, e, a, c, b, d, l, v) {
        function q(a) {
            var b;
            (a = a.lastChild) && "br" === a.name && (b = a.prev) && "br" === b.name && (a.remove(), b.remove())
        }
        var h = l.each,
            n = l.trim,
            p = k.DOM,
            w = ["data-mce-selected"];
        return function(c, m) {
            function t(a) {
                var b = new RegExp(["\x3cspan[^\x3e]+data-mce-bogus[^\x3e]+\x3e[\u200b\ufeff]+\x3c\\/span\x3e", "\\s?(" + w.join("|") + ')\x3d"[^"]+"'].join("|"), "gi");
                return a = v.trim(a.replace(b, ""))
            }
            var y, A, x;
            m && (y = m.dom, A = m.schema);
            y = y || p;
            A = A || new b(c);
            c.entity_encoding = c.entity_encoding || "named";
            c.remove_trailing_brs = "remove_trailing_brs" in c ? c.remove_trailing_brs : !0;
            x = new g(c, A);
            x.addAttributeFilter("data-mce-tabindex", function(a, b) {
                for (var c = a.length, d; c--;) d =
                    a[c], d.attr("tabindex", d.attributes.map["data-mce-tabindex"]), d.attr(b, null)
            });
            x.addAttributeFilter("src,href,style", function(a, b) {
                for (var d = a.length, e, f, m = "data-mce-" + b, p = c.url_converter, h = c.url_converter_scope; d--;) e = a[d], f = e.attributes.map[m], void 0 !== f ? (e.attr(b, 0 < f.length ? f : null), e.attr(m, null)) : (f = e.attributes.map[b], "style" === b ? f = y.serializeStyle(y.parseStyle(f), e.name) : p && (f = p.call(h, f, b, e.name)), e.attr(b, 0 < f.length ? f : null))
            });
            x.addAttributeFilter("class", function(a) {
                for (var b = a.length, c,
                        d; b--;)
                    if (c = a[b], d = c.attr("class")) d = c.attr("class").replace(/(?:^|\s)mce-item-\w+(?!\S)/g, ""), c.attr("class", 0 < d.length ? d : null)
            });
            x.addAttributeFilter("data-mce-type", function(a, b, c) {
                b = a.length;
                for (var d; b--;) d = a[b], "bookmark" !== d.attributes.map["data-mce-type"] || c.cleanup || d.remove()
            });
            x.addNodeFilter("noscript", function(a) {
                for (var b = a.length, c; b--;)
                    if (c = a[b].firstChild) c.value = e.decode(c.value)
            });
            x.addNodeFilter("script,style", function(a, b) {
                function c(a) {
                    return a.replace(/(\x3c!--\[CDATA\[|\]\]--\x3e)/g,
                        "\n").replace(/^[\r\n]*|[\r\n]*$/g, "").replace(/^\s*((\x3c!--)?(\s*\/\/)?\s*<!\[CDATA\[|(\x3c!--\s*)?\/\*\s*<!\[CDATA\[\s*\*\/|(\/\/)?\s*\x3c!--|\/\*\s*\x3c!--\s*\*\/)\s*[\r\n]*/gi, "").replace(/\s*(\/\*\s*\]\]>\s*\*\/(--\x3e)?|\s*\/\/\s*\]\]>(--\x3e)?|\/\/\s*(--\x3e)?|\]\]>|\/\*\s*--\x3e\s*\*\/|\s*--\x3e\s*)\s*$/g, "")
                }
                for (var d = a.length, e, f, m; d--;) e = a[d], f = e.firstChild ? e.firstChild.value : "", "script" === b ? ((m = e.attr("type")) && e.attr("type", "mce-no/type" == m ? null : m.replace(/^mce\-/, "")), 0 < f.length && (e.firstChild.value =
                    "// \x3c![CDATA[\n" + c(f) + "\n// ]]\x3e")) : 0 < f.length && (e.firstChild.value = "\x3c!--\n" + c(f) + "\n--\x3e")
            });
            x.addNodeFilter("#comment", function(a) {
                for (var b = a.length, c; b--;) c = a[b], 0 === c.value.indexOf("[CDATA[") ? (c.name = "#cdata", c.type = 4, c.value = c.value.replace(/^\[CDATA\[|\]\]$/g, "")) : 0 === c.value.indexOf("mce:protected ") && (c.name = "#text", c.type = 3, c.raw = !0, c.value = unescape(c.value).substr(14))
            });
            x.addNodeFilter("xml:namespace,input", function(a, b) {
                for (var c = a.length, d; c--;) d = a[c], 7 === d.type ? d.remove() :
                    1 === d.type && ("input" !== b || "type" in d.attributes.map || d.attr("type", "text"))
            });
            c.fix_list_elements && x.addNodeFilter("ul,ol", function(a) {
                for (var b = a.length, c, d; b--;) c = a[b], d = c.parent, ("ul" === d.name || "ol" === d.name) && c.prev && "li" === c.prev.name && c.prev.append(c)
            });
            x.addAttributeFilter("data-mce-src,data-mce-href,data-mce-style,data-mce-selected,data-mce-expando,data-mce-type,data-mce-resize", function(a, b) {
                for (var c = a.length; c--;) a[c].attr(b, null)
            });
            return {
                schema: A,
                addNodeFilter: x.addNodeFilter,
                addAttributeFilter: x.addAttributeFilter,
                serialize: function(b, e) {
                    var f, m, p;
                    d.ie && 0 < y.select("script,style,select,map").length ? (f = b.innerHTML, b = b.cloneNode(!1), y.setHTML(b, f)) : b = b.cloneNode(!0);
                    f = b.ownerDocument.implementation;
                    f.createHTMLDocument && (m = f.createHTMLDocument(""), h("BODY" == b.nodeName ? b.childNodes : [b], function(a) {
                        m.body.appendChild(m.importNode(a, !0))
                    }), b = "BODY" != b.nodeName ? m.body.firstChild : m.body, p = y.doc, y.doc = m);
                    e = e || {};
                    e.format = e.format || "html";
                    e.selection && (e.forced_root_block = "");
                    e.no_events || (e.node = b, this.onPreProcess(e));
                    f = x.parse(n(e.getInner ? b.innerHTML : y.getOuterHTML(b)), e);
                    q(f);
                    b = new a(c, A);
                    e.content = b.serialize(f);
                    e.cleanup || (e.content = v.trim(e.content), e.content = e.content.replace(/\uFEFF/g, ""));
                    if (!e.no_events) this.onPostProcess(e);
                    p && (y.doc = p);
                    e.node = null;
                    return e.content
                },
                addRules: function(a) {
                    A.addValidElements(a)
                },
                setRules: function(a) {
                    A.setValidElements(a)
                },
                onPreProcess: function(a) {
                    m && m.fire("PreProcess", a)
                },
                onPostProcess: function(a) {
                    m && m.fire("PostProcess", a)
                },
                addTempAttr: function(a) {
                    -1 === l.inArray(w, a) &&
                        (x.addAttributeFilter(a, function(a, b) {
                            for (var c = a.length; c--;) a[c].attr(b, null)
                        }), w.push(a))
                },
                trimHtml: t,
                getTrimmedContent: function() {
                    var a = m.getBody().innerHTML,
                        b = /<(\w+) [^>]*data-mce-bogus="all"[^>]*>/g,
                        c, d, e, p, h = m.schema,
                        a = t(a);
                    for (p = h.getShortEndedElements(); c = b.exec(a);) d = b.lastIndex, e = c[0].length, c = p[c[1]] ? d : f.findEndTag(h, a, d), a = a.substring(0, d - e) + a.substring(c), b.lastIndex = d - e;
                    return n(a)
                }
            }
        }
    });
    z("tinymce/dom/TridentSelection", [], function() {
        return function(k) {
            function g(a, c) {
                var b, d = 0,
                    e, f,
                    q, h, n, p = -1,
                    w;
                b = a.duplicate();
                b.collapse(c);
                w = b.parentElement();
                if (w.ownerDocument === k.dom.doc) {
                    for (;
                        "false" === w.contentEditable;) w = w.parentNode;
                    if (!w.hasChildNodes()) return {
                        node: w,
                        inside: 1
                    };
                    q = w.children;
                    for (e = q.length - 1; d <= e;)
                        if (n = Math.floor((d + e) / 2), h = q[n], b.moveToElementText(h), p = b.compareEndPoints(c ? "StartToStart" : "EndToEnd", a), 0 < p) e = n - 1;
                        else if (0 > p) d = n + 1;
                    else return {
                        node: h
                    };
                    if (0 > p)
                        for (h ? b.collapse(!1) : (b.moveToElementText(w), b.collapse(!0), h = w, f = !0), d = 0; 0 !== b.compareEndPoints(c ? "StartToStart" :
                                "StartToEnd", a) && 0 !== b.move("character", 1) && w == b.parentElement();) d++;
                    else
                        for (b.collapse(!0), d = 0; 0 !== b.compareEndPoints(c ? "StartToStart" : "StartToEnd", a) && 0 !== b.move("character", -1) && w == b.parentElement();) d++;
                    return {
                        node: h,
                        position: p,
                        offset: d,
                        inside: f
                    }
                }
            }
            var f = this,
                e = k.dom;
            this.getBookmark = function(a) {
                function c(a) {
                    var b, c, d, f, l = [];
                    b = a.parentNode;
                    for (c = e.getRoot().parentNode; b != c && 9 !== b.nodeType;) {
                        d = b.children;
                        for (f = d.length; f--;)
                            if (a === d[f]) {
                                l.push(f);
                                break
                            }
                        a = b;
                        b = b.parentNode
                    }
                    return l
                }

                function b(a) {
                    if (a =
                        g(d, a)) return {
                        position: a.position,
                        offset: a.offset,
                        indexes: c(a.node),
                        inside: a.inside
                    }
                }
                var d = k.getRng(),
                    f = {};
                2 === a && (d.item ? f.start = {
                    ctrl: !0,
                    indexes: c(d.item(0))
                } : (f.start = b(!0), k.isCollapsed() || (f.end = b())));
                return f
            };
            this.moveToBookmark = function(a) {
                function c(a) {
                    var b, c, d, f;
                    b = e.getRoot();
                    for (c = a.length - 1; 0 <= c; c--) f = b.children, d = a[c], d <= f.length - 1 && (b = f[d]);
                    return b
                }

                function b(b) {
                    var e = a[b ? "start" : "end"],
                        h, l, p;
                    e && (h = 0 < e.position, l = f.createTextRange(), l.moveToElementText(c(e.indexes)), p = e.offset, void 0 !==
                        p ? (l.collapse(e.inside || h), l.moveStart("character", h ? -p : p)) : l.collapse(b), d.setEndPoint(b ? "StartToStart" : "EndToStart", l), b && d.collapse(!0))
                }
                var d, f = e.doc.body;
                a.start && (a.start.ctrl ? (d = f.createControlRange(), d.addElement(c(a.start.indexes))) : (d = f.createTextRange(), b(!0), b()), d.select())
            };
            this.addRange = function(a) {
                function c(a) {
                    var c, d, m, l, w;
                    m = e.create("a");
                    c = a ? f : q;
                    d = a ? g : h;
                    l = b.duplicate();
                    if (c == n || c == n.documentElement) c = p, d = 0;
                    3 == c.nodeType ? (c.parentNode.insertBefore(m, c), l.moveToElementText(m), l.moveStart("character",
                        d), e.remove(m), b.setEndPoint(a ? "StartToStart" : "EndToEnd", l)) : (w = c.childNodes, w.length ? (d >= w.length ? e.insertAfter(m, w[w.length - 1]) : c.insertBefore(m, w[d]), l.moveToElementText(m)) : c.canHaveHTML && (c.innerHTML = "\x3cspan\x3e\x26#xFEFF;\x3c/span\x3e", m = c.firstChild, l.moveToElementText(m), l.collapse(!1)), b.setEndPoint(a ? "StartToStart" : "EndToEnd", l), e.remove(m))
                }
                var b, d, f, g, q, h, n = k.dom.doc,
                    p = n.body,
                    w, A;
                f = a.startContainer;
                g = a.startOffset;
                q = a.endContainer;
                h = a.endOffset;
                b = p.createTextRange();
                if (f == q && 1 == f.nodeType) {
                    if (g ==
                        h && !f.hasChildNodes()) {
                        if (f.canHaveHTML) {
                            (a = f.previousSibling) && !a.hasChildNodes() && e.isBlock(a) ? a.innerHTML = "\x26#xFEFF;" : a = null;
                            f.innerHTML = "\x3cspan\x3e\x26#xFEFF;\x3c/span\x3e\x3cspan\x3e\x26#xFEFF;\x3c/span\x3e";
                            b.moveToElementText(f.lastChild);
                            b.select();
                            e.doc.selection.clear();
                            f.innerHTML = "";
                            a && (a.innerHTML = "");
                            return
                        }
                        g = e.nodeIndex(f);
                        f = f.parentNode
                    }
                    if (g == h - 1) try {
                        if (A = f.childNodes[g], d = p.createControlRange(), d.addElement(A), d.select(), w = k.getRng(), w.item && A === w.item(0)) return
                    } catch (m) {}
                }
                c(!0);
                c();
                b.select()
            };
            this.getRangeAt = function() {
                function a(a) {
                    var d = g(c, a),
                        e, f, h = 0,
                        m;
                    e = d.node;
                    f = d.offset;
                    if (d.inside && !e.hasChildNodes()) b[a ? "setStart" : "setEnd"](e, 0);
                    else if (void 0 === f) b[a ? "setStartBefore" : "setEndAfter"](e);
                    else {
                        if (0 > d.position) {
                            d = d.inside ? e.firstChild : e.nextSibling;
                            if (!d) {
                                b[a ? "setStartAfter" : "setEndAfter"](e);
                                return
                            }
                            if (!f) {
                                if (3 == d.nodeType) b[a ? "setStart" : "setEnd"](d, 0);
                                else b[a ? "setStartBefore" : "setEndBefore"](d);
                                return
                            }
                            for (; d;) {
                                if (3 == d.nodeType && (m = d.nodeValue, h += m.length, h >= f)) {
                                    e =
                                        d;
                                    h -= f;
                                    h = m.length - h;
                                    break
                                }
                                d = d.nextSibling
                            }
                        } else {
                            d = e.previousSibling;
                            if (!d) return b[a ? "setStartBefore" : "setEndBefore"](e);
                            if (!f) {
                                if (3 == e.nodeType) b[a ? "setStart" : "setEnd"](d, e.nodeValue.length);
                                else b[a ? "setStartAfter" : "setEndAfter"](d);
                                return
                            }
                            for (; d;) {
                                if (3 == d.nodeType && (h += d.nodeValue.length, h >= f)) {
                                    e = d;
                                    h -= f;
                                    break
                                }
                                d = d.previousSibling
                            }
                        }
                        b[a ? "setStart" : "setEnd"](e, h)
                    }
                }
                var c = k.getRng(),
                    b = e.createRng(),
                    d, l, v, q;
                d = c.item ? c.item(0) : c.parentElement();
                if (d.ownerDocument != e.doc) return b;
                l = k.isCollapsed();
                if (c.item) return b.setStart(d.parentNode,
                    e.nodeIndex(d)), b.setEnd(b.startContainer, b.startOffset + 1), b;
                try {
                    a(!0), l || a()
                } catch (h) {
                    if (-2147024809 == h.number) q = f.getBookmark(2), v = c.duplicate(), v.collapse(!0), d = v.parentElement(), l || (v = c.duplicate(), v.collapse(!1), v = v.parentElement(), v.innerHTML = v.innerHTML), d.innerHTML = d.innerHTML, f.moveToBookmark(q), c = k.getRng(), a(!0), l || a();
                    else throw h;
                }
                return b
            }
        }
    });
    z("tinymce/util/VK", ["tinymce/Env"], function(k) {
        return {
            BACKSPACE: 8,
            DELETE: 46,
            DOWN: 40,
            ENTER: 13,
            LEFT: 37,
            RIGHT: 39,
            SPACEBAR: 32,
            TAB: 9,
            UP: 38,
            modifierPressed: function(g) {
                return g.shiftKey ||
                    g.ctrlKey || g.altKey || this.metaKeyPressed(g)
            },
            metaKeyPressed: function(g) {
                return k.mac ? g.metaKey : g.ctrlKey && !g.altKey
            }
        }
    });
    z("tinymce/dom/ControlSelection", ["tinymce/util/VK", "tinymce/util/Tools", "tinymce/util/Delay", "tinymce/Env", "tinymce/dom/NodeType"], function(k, g, f, e, a) {
        var c = a.isContentEditableFalse;
        return function(a, d) {
            function b(a) {
                var b = d.settings.object_resizing;
                if (!1 === b || e.iOS) return !1;
                "string" != typeof b && (b = "table,img,div");
                return "false" === a.getAttribute("data-mce-resize") || a == d.getBody() ?
                    !1 : d.dom.is(a, b)
            }

            function v(a) {
                var b, c, e;
                b = a.screenX - N;
                c = a.screenY - R;
                C = b * z[2] + L;
                D = c * z[3] + T;
                C = 5 > C ? 5 : C;
                D = 5 > D ? 5 : D;
                if ("IMG" == u.nodeName && !1 !== d.settings.resize_img_proportional ? !k.modifierPressed(a) : k.modifierPressed(a) || "IMG" == u.nodeName && 0 !== z[2] * z[3]) Z(b) > Z(c) ? (D = aa(C * E), C = aa(D / E)) : (C = aa(D / E), D = aa(C * E));
                x.setStyles(F, {
                    width: C,
                    height: D
                });
                a = z.startPos.x + b;
                e = z.startPos.y + c;
                a = 0 < a ? a : 0;
                e = 0 < e ? e : 0;
                x.setStyles(O, {
                    left: a,
                    top: e,
                    display: "block"
                });
                O.innerHTML = C + " \x26times; " + D;
                0 > z[2] && F.clientWidth <= C && x.setStyle(F,
                    "left", I + (L - C));
                0 > z[3] && F.clientHeight <= D && x.setStyle(F, "top", H + (T - D));
                b = X.scrollWidth - V;
                c = X.scrollHeight - ca;
                0 !== b + c && x.setStyles(O, {
                    left: a - b,
                    top: e - c
                });
                M || (d.fire("ObjectResizeStart", {
                    target: u,
                    width: L,
                    height: T
                }), M = !0)
            }

            function q() {
                function a(a, b) {
                    b && (u.style[a] || !d.schema.isValid(u.nodeName.toLowerCase(), a) ? x.setStyle(u, a, b) : x.setAttrib(u, a, b))
                }
                M = !1;
                a("width", C);
                a("height", D);
                x.unbind(P, "mousemove", v);
                x.unbind(P, "mouseup", q);
                K != P && (x.unbind(K, "mousemove", v), x.unbind(K, "mouseup", q));
                x.remove(F);
                x.remove(O);
                Q && "TABLE" != u.nodeName || h(u);
                d.fire("ObjectResized", {
                    target: u,
                    width: C,
                    height: D
                });
                x.setAttrib(u, "style", x.getAttrib(u, "style"));
                d.nodeChanged()
            }

            function h(a, c, f) {
                var p, h, l;
                n();
                t();
                p = x.getPos(a, X);
                I = p.x;
                H = p.y;
                p = a.getBoundingClientRect();
                h = p.width || p.right - p.left;
                l = p.height || p.bottom - p.top;
                u != a && (m(), u = a, C = D = 0);
                p = d.fire("ObjectSelected", {
                    target: a
                });
                b(a) && !p.isDefaultPrevented() ? r(W, function(a, b) {
                    function d(b) {
                        N = b.screenX;
                        R = b.screenY;
                        L = u.clientWidth;
                        T = u.clientHeight;
                        E = T / L;
                        z = a;
                        a.startPos = {
                            x: h * a[0] + I,
                            y: l *
                                a[1] + H
                        };
                        V = X.scrollWidth;
                        ca = X.scrollHeight;
                        F = u.cloneNode(!0);
                        x.addClass(F, "mce-clonedresizable");
                        x.setAttrib(F, "data-mce-bogus", "all");
                        F.contentEditable = !1;
                        F.unSelectabe = !0;
                        x.setStyles(F, {
                            left: I,
                            top: H,
                            margin: 0
                        });
                        F.removeAttribute("data-mce-selected");
                        X.appendChild(F);
                        x.bind(P, "mousemove", v);
                        x.bind(P, "mouseup", q);
                        K != P && (x.bind(K, "mousemove", v), x.bind(K, "mouseup", q));
                        O = x.add(X, "div", {
                            "class": "mce-resize-helper",
                            "data-mce-bogus": "all"
                        }, L + " \x26times; " + T)
                    }
                    var m;
                    c ? b == c && d(f) : ((m = x.get("mceResizeHandle" +
                        b)) && x.remove(m), m = x.add(X, "div", {
                        id: "mceResizeHandle" + b,
                        "data-mce-bogus": "all",
                        "class": "mce-resizehandle",
                        unselectable: !0,
                        style: "cursor:" + b + "-resize; margin:0; padding:0"
                    }), e.ie && (m.contentEditable = !1), x.bind(m, "mousedown", function(a) {
                        a.stopImmediatePropagation();
                        a.preventDefault();
                        d(a)
                    }), a.elm = m, x.setStyles(m, {
                        left: h * a[0] + I - m.offsetWidth / 2,
                        top: l * a[1] + H - m.offsetHeight / 2
                    }))
                }) : n();
                u.setAttribute("data-mce-selected", "1")
            }

            function n() {
                var a, b;
                t();
                u && u.removeAttribute("data-mce-selected");
                for (a in W)
                    if (b =
                        x.get("mceResizeHandle" + a)) x.unbind(b), x.remove(b)
            }

            function p(b) {
                function c(a, b) {
                    if (a) {
                        do
                            if (a === b) return !0; while (a = a.parentNode)
                    }
                }
                var e;
                if (!M && !d.removed) {
                    r(x.select("img[data-mce-selected],hr[data-mce-selected]"), function(a) {
                        a.removeAttribute("data-mce-selected")
                    });
                    e = "mousedown" == b.type ? b.target : a.getNode();
                    e = x.$(e).closest(Q ? "table" : "table,img,hr")[0];
                    if (c(e, X) && (y(), b = a.getStart(!0), c(b, e) && c(a.getEnd(!0), e) && (!Q || e != b && "IMG" !== b.nodeName))) {
                        h(e);
                        return
                    }
                    n()
                }
            }

            function w(a) {
                a = a.srcElement;
                var b,
                    c, e, f, m, p;
                b = a.getBoundingClientRect();
                m = G.clientX - b.left;
                p = G.clientY - b.top;
                for (c in W)
                    if (b = W[c], e = a.offsetWidth * b[0], f = a.offsetHeight * b[1], 8 > Z(e - m) && 8 > Z(f - p)) {
                        z = b;
                        break
                    }
                M = !0;
                d.fire("ObjectResizeStart", {
                    target: u,
                    width: u.clientWidth,
                    height: u.clientHeight
                });
                d.getDoc().selection.empty();
                h(a, c, G)
            }

            function A(a) {
                var b = a.srcElement;
                if (c(b)) a.preventDefault ? a.preventDefault() : a.returnValue = !1;
                else if (b != u)
                    if (d.fire("ObjectSelected", {
                            target: b
                        }), m(), 0 === b.id.indexOf("mceResizeHandle")) a.returnValue = !1;
                    else if ("IMG" ==
                    b.nodeName || "TABLE" == b.nodeName) n(), (u = b) && b.attachEvent && b.attachEvent("onresizestart", w)
            }

            function m() {
                u && u.detachEvent && u.detachEvent("onresizestart", w)
            }

            function t() {
                for (var a in W) {
                    var b = W[a];
                    b.elm && (x.unbind(b.elm), delete b.elm)
                }
            }

            function y() {
                try {
                    d.getDoc().execCommand("enableObjectResizing", !1, !1)
                } catch (ea) {}
            }

            function B(a) {
                var b;
                if (Q) {
                    b = P.body.createControlRange();
                    try {
                        return b.addElement(a), b.select(), !0
                    } catch (ia) {}
                }
            }
            var x = d.dom,
                r = g.each,
                u, F, O, W, z, G, N, R, I, H, L, T, E, M, C, D, P = d.getDoc(),
                K = document,
                Q = e.ie && 11 > e.ie,
                Z = Math.abs,
                aa = Math.round,
                X = d.getBody(),
                V, ca;
            W = {
                nw: [0, 0, -1, -1],
                ne: [1, 0, 1, -1],
                se: [1, 1, 1, 1],
                sw: [0, 1, -1, 1]
            };
            d.contentStyles.push(".mce-content-body div.mce-resizehandle {position: absolute;border: 1px solid black;box-sizing: box-sizing;background: #FFF;width: 7px;height: 7px;z-index: 10000}.mce-content-body .mce-resizehandle:hover {background: #000}.mce-content-body img[data-mce-selected],.mce-content-body hr[data-mce-selected] {outline: 1px solid black;resize: none}.mce-content-body .mce-clonedresizable {position: absolute;" +
                (e.gecko ? "" : "outline: 1px dashed black;") + "opacity: .5;filter: alpha(opacity\x3d50);z-index: 10000}.mce-content-body .mce-resize-helper {background: #555;background: rgba(0,0,0,0.75);border-radius: 3px;border: 1px;color: white;display: none;font-family: sans-serif;font-size: 12px;white-space: nowrap;line-height: 14px;margin: 5px 10px;padding: 5px;position: absolute;z-index: 10001}");
            d.on("init", function() {
                Q ? (d.on("ObjectResized", function(a) {
                        "TABLE" != a.target.nodeName && (n(), B(a.target))
                    }), X && X.attachEvent &&
                    X.attachEvent("oncontrolselect", A), d.on("mousedown", function(a) {
                        G = a
                    })) : (y(), 11 <= e.ie && (d.on("mousedown click", function(a) {
                    var b = a.target.nodeName;
                    !M && /^(TABLE|IMG|HR)$/.test(b) && (d.selection.select(a.target, "TABLE" == b), "mousedown" == a.type && d.nodeChanged())
                }), d.dom.bind(X, "mscontrolselect", function(a) {
                    function b(a) {
                        f.setEditorTimeout(d, function() {
                            d.selection.select(a)
                        })
                    }
                    c(a.target) ? (a.preventDefault(), b(a.target)) : /^(TABLE|IMG|HR)$/.test(a.target.nodeName) && (a.preventDefault(), "IMG" == a.target.tagName &&
                        b(a.target))
                })));
                var a = f.throttle(function(a) {
                    d.composing || p(a)
                });
                d.on("nodechange ResizeEditor ResizeWindow drop", a);
                d.on("keyup compositionend", function(b) {
                    u && "TABLE" == u.nodeName && a(b)
                });
                d.on("hide blur", n)
            });
            d.on("remove", t);
            return {
                isResizable: b,
                showResizeRect: h,
                hideResizeRect: n,
                updateResizeRect: p,
                controlSelect: B,
                destroy: function() {
                    u = F = null;
                    Q && (m(), X && X.detachEvent && X.detachEvent("oncontrolselect", A))
                }
            }
        }
    });
    z("tinymce/util/Fun", [], function() {
        function k(f) {
            var e = g.call(arguments);
            return e.length -
                1 >= f.length ? f.apply(this, e.slice(1)) : function() {
                    var a = e.concat([].slice.call(arguments));
                    return k.apply(this, a)
                }
        }
        var g = [].slice;
        return {
            constant: function(f) {
                return function() {
                    return f
                }
            },
            negate: function(f) {
                return function(e) {
                    return !f(e)
                }
            },
            and: function() {
                var f = g.call(arguments);
                return function(e) {
                    for (var a = 0; a < f.length; a++)
                        if (!f[a](e)) return !1;
                    return !0
                }
            },
            or: function() {
                var f = g.call(arguments);
                return function(e) {
                    for (var a = 0; a < f.length; a++)
                        if (f[a](e)) return !0;
                    return !1
                }
            },
            curry: k,
            compose: function(f, e) {
                return function(a) {
                    return f(e(a))
                }
            }
        }
    });
    z("tinymce/caret/CaretCandidate", ["tinymce/dom/NodeType", "tinymce/util/Arr", "tinymce/caret/CaretContainer"], function(k, g, f) {
        function e(a) {
            return p(a) ? !1 : v(a) ? q(a.parentNode) ? !1 : !0 : h(a) || l(a) || n(a) || d(a)
        }

        function a(a, c) {
            for (a = a.parentNode; a && a != c; a = a.parentNode) {
                if (d(a)) return !1;
                if (b(a)) break
            }
            return !0
        }

        function c(a) {
            return d(a) ? !0 !== g.reduce(a.getElementsByTagName("*"), function(a, c) {
                return a || b(c)
            }, !1) : !1
        }
        var b = k.isContentEditableTrue,
            d = k.isContentEditableFalse,
            l = k.isBr,
            v = k.isText,
            q = k.matchNodeNames("script style textarea"),
            h = k.matchNodeNames("img input textarea hr iframe video audio object"),
            n = k.matchNodeNames("table"),
            p = f.isCaretContainer;
        return {
            isCaretCandidate: e,
            isInEditable: a,
            isAtomic: function(a) {
                return h(a) || c(a)
            },
            isEditableCaretCandidate: function(b, c) {
                return e(b) && a(b, c)
            }
        }
    });
    z("tinymce/geom/ClientRect", [], function() {
        function k(a) {
            return a ? {
                left: b(a.left),
                top: b(a.top),
                bottom: b(a.bottom),
                right: b(a.right),
                width: b(a.width),
                height: b(a.height)
            } : {
                left: 0,
                top: 0,
                bottom: 0,
                right: 0,
                width: 0,
                height: 0
            }
        }

        function g(a, b, c) {
            return 0 <=
                a && a <= Math.min(b.height, c.height) / 2
        }

        function f(a, b) {
            return a.bottom < b.top ? !0 : a.top > b.bottom ? !1 : g(b.top - a.bottom, a, b)
        }

        function e(a, b) {
            return a.top > b.bottom ? !0 : a.bottom < b.top ? !1 : g(b.bottom - a.top, a, b)
        }

        function a(a, b) {
            return a.left < b.left
        }

        function c(a, b) {
            return a.right > b.right
        }
        var b = Math.round;
        return {
            clone: k,
            collapse: function(a, b) {
                a = k(a);
                b || (a.left += a.width);
                a.right = a.left;
                a.width = 0;
                return a
            },
            isEqual: function(a, b) {
                return a.left === b.left && a.top === b.top && a.bottom === b.bottom && a.right === b.right
            },
            isAbove: f,
            isBelow: e,
            isLeft: a,
            isRight: c,
            compare: function(b, l) {
                return f(b, l) ? -1 : e(b, l) ? 1 : a(b, l) ? -1 : c(b, l) ? 1 : 0
            },
            containsXY: function(a, b, c) {
                return b >= a.left && b <= a.right && c >= a.top && c <= a.bottom
            }
        }
    });
    z("tinymce/text/ExtendingChar", [], function() {
        var k = /[\u0300-\u036f\u0483-\u0487\u0488-\u0489\u0591-\u05bd\u05bf\u05c1-\u05c2\u05c4-\u05c5\u05c7\u0610-\u061a\u064b-\u065f\u0670\u06d6-\u06dc\u06df-\u06e4\u06e7-\u06e8\u06ea-\u06ed\u0711\u0730-\u074a\u07a6-\u07b0\u07eb-\u07f3\u0816-\u0819\u081b-\u0823\u0825-\u0827\u0829-\u082d\u0859-\u085b\u08e3-\u0902\u093a\u093c\u0941-\u0948\u094d\u0951-\u0957\u0962-\u0963\u0981\u09bc\u09be\u09c1-\u09c4\u09cd\u09d7\u09e2-\u09e3\u0a01-\u0a02\u0a3c\u0a41-\u0a42\u0a47-\u0a48\u0a4b-\u0a4d\u0a51\u0a70-\u0a71\u0a75\u0a81-\u0a82\u0abc\u0ac1-\u0ac5\u0ac7-\u0ac8\u0acd\u0ae2-\u0ae3\u0b01\u0b3c\u0b3e\u0b3f\u0b41-\u0b44\u0b4d\u0b56\u0b57\u0b62-\u0b63\u0b82\u0bbe\u0bc0\u0bcd\u0bd7\u0c00\u0c3e-\u0c40\u0c46-\u0c48\u0c4a-\u0c4d\u0c55-\u0c56\u0c62-\u0c63\u0c81\u0cbc\u0cbf\u0cc2\u0cc6\u0ccc-\u0ccd\u0cd5-\u0cd6\u0ce2-\u0ce3\u0d01\u0d3e\u0d41-\u0d44\u0d4d\u0d57\u0d62-\u0d63\u0dca\u0dcf\u0dd2-\u0dd4\u0dd6\u0ddf\u0e31\u0e34-\u0e3a\u0e47-\u0e4e\u0eb1\u0eb4-\u0eb9\u0ebb-\u0ebc\u0ec8-\u0ecd\u0f18-\u0f19\u0f35\u0f37\u0f39\u0f71-\u0f7e\u0f80-\u0f84\u0f86-\u0f87\u0f8d-\u0f97\u0f99-\u0fbc\u0fc6\u102d-\u1030\u1032-\u1037\u1039-\u103a\u103d-\u103e\u1058-\u1059\u105e-\u1060\u1071-\u1074\u1082\u1085-\u1086\u108d\u109d\u135d-\u135f\u1712-\u1714\u1732-\u1734\u1752-\u1753\u1772-\u1773\u17b4-\u17b5\u17b7-\u17bd\u17c6\u17c9-\u17d3\u17dd\u180b-\u180d\u18a9\u1920-\u1922\u1927-\u1928\u1932\u1939-\u193b\u1a17-\u1a18\u1a1b\u1a56\u1a58-\u1a5e\u1a60\u1a62\u1a65-\u1a6c\u1a73-\u1a7c\u1a7f\u1ab0-\u1abd\u1abe\u1b00-\u1b03\u1b34\u1b36-\u1b3a\u1b3c\u1b42\u1b6b-\u1b73\u1b80-\u1b81\u1ba2-\u1ba5\u1ba8-\u1ba9\u1bab-\u1bad\u1be6\u1be8-\u1be9\u1bed\u1bef-\u1bf1\u1c2c-\u1c33\u1c36-\u1c37\u1cd0-\u1cd2\u1cd4-\u1ce0\u1ce2-\u1ce8\u1ced\u1cf4\u1cf8-\u1cf9\u1dc0-\u1df5\u1dfc-\u1dff\u200c-\u200d\u20d0-\u20dc\u20dd-\u20e0\u20e1\u20e2-\u20e4\u20e5-\u20f0\u2cef-\u2cf1\u2d7f\u2de0-\u2dff\u302a-\u302d\u302e-\u302f\u3099-\u309a\ua66f\ua670-\ua672\ua674-\ua67d\ua69e-\ua69f\ua6f0-\ua6f1\ua802\ua806\ua80b\ua825-\ua826\ua8c4\ua8e0-\ua8f1\ua926-\ua92d\ua947-\ua951\ua980-\ua982\ua9b3\ua9b6-\ua9b9\ua9bc\ua9e5\uaa29-\uaa2e\uaa31-\uaa32\uaa35-\uaa36\uaa43\uaa4c\uaa7c\uaab0\uaab2-\uaab4\uaab7-\uaab8\uaabe-\uaabf\uaac1\uaaec-\uaaed\uaaf6\uabe5\uabe8\uabed\ufb1e\ufe00-\ufe0f\ufe20-\ufe2f\uff9e-\uff9f]/;
        return {
            isExtendingChar: function(g) {
                return "string" == typeof g && 768 <= g.charCodeAt(0) && k.test(g)
            }
        }
    });
    z("tinymce/caret/CaretPosition", "tinymce/util/Fun tinymce/dom/NodeType tinymce/dom/DOMUtils tinymce/dom/RangeUtils tinymce/caret/CaretCandidate tinymce/geom/ClientRect tinymce/text/ExtendingChar".split(" "), function(k, g, f, e, a, c, b) {
        function d(a) {
            return a && /[\r\n\t ]/.test(a)
        }

        function l(a) {
            var b = a.startContainer,
                c = a.startOffset;
            return d(a.toString()) && A(b.parentNode) && (a = b.data, d(a[c - 1]) || d(a[c + 1])) ? !0 :
                !1
        }

        function v(a) {
            function d(a) {
                var b;
                b = a.getClientRects();
                b = 0 < b.length ? c.clone(b[0]) : c.clone(a.getBoundingClientRect());
                if (t(a) && 0 === b.left) {
                    var d = a.ownerDocument;
                    b = d.createRange();
                    var d = d.createTextNode("\u00a0"),
                        e = a.parentNode;
                    e.insertBefore(d, a);
                    b.setStart(d, 0);
                    b.setEnd(d, 1);
                    a = c.clone(b.getBoundingClientRect());
                    e.removeChild(d);
                    return a
                }
                return b
            }

            function e(a, b) {
                a = c.collapse(a, b);
                a.width = 1;
                a.right = a.left + 1;
                return a
            }

            function f(a) {
                0 !== a.height && (0 < g.length && c.isEqual(a, g[g.length - 1]) || g.push(a))
            }

            function p(a, c) {
                var m = a.ownerDocument.createRange();
                if (c < a.data.length) {
                    if (b.isExtendingChar(a.data[c])) return g;
                    if (b.isExtendingChar(a.data[c - 1]) && (m.setStart(a, c), m.setEnd(a, c + 1), !l(m))) return f(e(d(m), !1)), g
                }
                0 < c && (m.setStart(a, c - 1), m.setEnd(a, c), l(m) || f(e(d(m), !1)));
                c < a.data.length && (m.setStart(a, c), m.setEnd(a, c + 1), l(m) || f(e(d(m), !0)))
            }
            var g = [],
                q;
            if (m(a.container())) return p(a.container(), a.offset()), g;
            if (h(a.container()))
                if (a.isAtEnd()) q = B(a.container(), a.offset()), m(q) && p(q, q.data.length), w(q) &&
                    !t(q) && f(e(d(q), !1));
                else {
                    q = B(a.container(), a.offset());
                    m(q) && p(q, 0);
                    if (w(q) && a.isAtEnd()) return f(e(d(q), !1)), g;
                    a = B(a.container(), a.offset() - 1);
                    w(a) && !t(a) && (n(a) || n(q) || !w(q)) && f(e(d(a), !1));
                    w(q) && f(e(d(q), !0))
                }
            return g
        }

        function q(a, b, c) {
            function d() {
                c || (c = v(new q(a, b)));
                return c
            }
            return {
                container: k.constant(a),
                offset: k.constant(b),
                toRange: function() {
                    var c;
                    c = a.ownerDocument.createRange();
                    c.setStart(a, b);
                    c.setEnd(a, b);
                    return c
                },
                getClientRects: d,
                isVisible: function() {
                    return 0 < d().length
                },
                isAtStart: function() {
                    m(a);
                    return 0 === b
                },
                isAtEnd: function() {
                    return m(a) ? b >= a.data.length : b >= a.childNodes.length
                },
                isEqual: function(c) {
                    return c && a === c.container() && b === c.offset()
                },
                getNode: function(c) {
                    return B(a, c ? b - 1 : b)
                }
            }
        }
        var h = g.isElement;
        a = a.isCaretCandidate;
        var n = g.matchStyleValues("display", "block table"),
            p = g.matchStyleValues("float", "left right"),
            w = k.and(h, a, k.negate(p)),
            A = k.negate(g.matchStyleValues("white-space", "pre pre-line pre-wrap")),
            m = g.isText,
            t = g.isBr,
            y = f.nodeIndex,
            B = e.getNode;
        q.fromRangeStart = function(a) {
            return new q(a.startContainer,
                a.startOffset)
        };
        q.fromRangeEnd = function(a) {
            return new q(a.endContainer, a.endOffset)
        };
        q.after = function(a) {
            return new q(a.parentNode, y(a) + 1)
        };
        q.before = function(a) {
            return new q(a.parentNode, y(a))
        };
        return q
    });
    z("tinymce/caret/CaretBookmark", ["tinymce/dom/NodeType", "tinymce/dom/DOMUtils", "tinymce/util/Fun", "tinymce/util/Arr", "tinymce/caret/CaretPosition"], function(k, g, f, e, a) {
        function c(a) {
            a = a.parentNode;
            return A(a) ? c(a) : a
        }

        function b(a) {
            return a ? e.reduce(a.childNodes, function(a, c) {
                A(c) && "BR" != c.nodeName ?
                    a = a.concat(b(c)) : a.push(c);
                return a
            }, []) : []
        }

        function d(a, b) {
            for (;
                (a = a.previousSibling) && w(a);) b += a.data.length;
            return b
        }

        function l(a) {
            return function(b) {
                return a === b
            }
        }

        function v(a) {
            var d, f, m;
            d = b(c(a));
            f = e.findIndex(d, l(a), a);
            d = d.slice(0, f + 1);
            m = e.reduce(d, function(a, b, c) {
                w(b) && w(d[c - 1]) && a++;
                return a
            }, 0);
            d = e.filter(d, k.matchNodeNames(a.nodeName));
            f = e.findIndex(d, l(a), a);
            return f - m
        }

        function q(a) {
            return (w(a) ? "text()" : a.nodeName.toLowerCase()) + "[" + v(a) + "]"
        }

        function h(a, b, c) {
            var d = [];
            for (b = b.parentNode; !(b ==
                    a || c && c(b)); b = b.parentNode) d.push(b);
            return d
        }

        function n(a, c, d) {
            var f = b(a),
                f = e.filter(f, function(a, b) {
                    return !w(a) || !w(f[b - 1])
                }),
                f = e.filter(f, k.matchNodeNames(c));
            return f[d]
        }

        function p(b, c) {
            for (var d = b, e = 0, f; w(d);) {
                f = d.data.length;
                if (c >= e && c <= e + f) {
                    b = d;
                    c -= e;
                    break
                }
                if (!w(d.nextSibling)) {
                    b = d;
                    c = f;
                    break
                }
                e += f;
                d = d.nextSibling
            }
            c > b.data.length && (c = b.data.length);
            return new a(b, c)
        }
        var w = k.isText,
            A = k.isBogus,
            m = g.nodeIndex;
        return {
            create: function(a, b) {
                var c, m, p = [];
                c = b.container();
                m = b.offset();
                w(c) ? b = d(c, m) : (c =
                    c.childNodes, m >= c.length ? (b = "after", m = c.length - 1) : b = "before", c = c[m]);
                p.push(q(c));
                a = h(a, c);
                a = e.filter(a, f.negate(k.isBogus));
                p = p.concat(e.map(a, function(a) {
                    return q(a)
                }));
                return p.reverse().join("/") + "," + b
            },
            resolve: function(b, c) {
                var d;
                if (!c) return null;
                d = c.split(",");
                c = d[0].split("/");
                d = 1 < d.length ? d[1] : "before";
                b = e.reduce(c, function(a, b) {
                    b = /([\w\-\(\)]+)\[([0-9]+)\]/.exec(b);
                    if (!b) return null;
                    "text()" === b[1] && (b[1] = "#text");
                    return n(a, b[1], parseInt(b[2], 10))
                }, b);
                return b ? w(b) ? p(b, parseInt(d, 10)) :
                    (d = "after" === d ? m(b) + 1 : m(b), new a(b.parentNode, d)) : null
            }
        }
    });
    z("tinymce/dom/BookmarkManager", "tinymce/Env tinymce/util/Tools tinymce/caret/CaretContainer tinymce/caret/CaretBookmark tinymce/caret/CaretPosition tinymce/dom/NodeType".split(" "), function(k, g, f, e, a, c) {
        function b(b) {
            var l = b.dom;
            this.getBookmark = function(q, h) {
                function n(a, b) {
                    var c = 0;
                    g.each(l.select(a), function(a) {
                        if ("all" !== a.getAttribute("data-mce-bogus")) {
                            if (a == b) return !1;
                            c++
                        }
                    });
                    return c
                }

                function p(a) {
                    function b(b) {
                        var c, d;
                        d = b ? "start" :
                            "end";
                        c = a[d + "Container"];
                        d = a[d + "Offset"];
                        1 == c.nodeType && "TR" == c.nodeName && (c = c.childNodes, c = c[Math.min(b ? d : d - 1, c.length - 1)]) && (d = b ? 0 : c.childNodes.length, a["set" + (b ? "Start" : "End")](c, d))
                    }
                    b(!0);
                    b();
                    return a
                }

                function w(a) {
                    function c(a, b) {
                        var c = a[b ? "startContainer" : "endContainer"];
                        a = a[b ? "startOffset" : "endOffset"];
                        b = [];
                        var e, f = 0;
                        if (3 == c.nodeType) {
                            if (h)
                                for (e = c.previousSibling; e && 3 == e.nodeType; e = e.previousSibling) a += e.nodeValue.length;
                            b.push(a)
                        } else e = c.childNodes, a >= e.length && e.length && (f = 1, a = Math.max(0,
                            e.length - 1)), b.push(l.nodeIndex(e[a], h) + f);
                        for (; c && c != d; c = c.parentNode) b.push(l.nodeIndex(c, h));
                        return b
                    }
                    var d = l.getRoot(),
                        e = {};
                    e.start = c(a, !0);
                    b.isCollapsed() || (e.end = c(a));
                    return e
                }

                function A(a) {
                    function b(a) {
                        var b;
                        if (f.isCaretContainer(a)) {
                            c.isText(a) && f.isCaretContainerBlock(a) && (a = a.parentNode);
                            b = a.previousSibling;
                            if (d(b)) return b;
                            b = a.nextSibling;
                            if (d(b)) return b
                        }
                    }
                    return b(a.startContainer) || b(a.endContainer)
                }
                var m, t, y, k;
                if (2 == q) return t = (k = b.getNode()) ? k.nodeName : null, m = b.getRng(), d(k) || "IMG" ==
                    t ? {
                        name: t,
                        index: n(t, k)
                    } : b.tridentSel ? b.tridentSel.getBookmark(q) : (k = A(m)) ? (t = k.tagName, {
                        name: t,
                        index: n(t, k)
                    }) : w(m);
                if (3 == q) return m = b.getRng(), {
                    start: e.create(l.getRoot(), a.fromRangeStart(m)),
                    end: e.create(l.getRoot(), a.fromRangeEnd(m))
                };
                if (q) return {
                    rng: b.getRng()
                };
                m = b.getRng();
                q = l.uniqueId();
                y = b.isCollapsed();
                if (m.duplicate || m.item) {
                    if (m.item) return k = m.item(0), t = k.nodeName, {
                        name: t,
                        index: n(t, k)
                    };
                    t = m.duplicate();
                    try {
                        m.collapse(), m.pasteHTML('\x3cspan data-mce-type\x3d"bookmark" id\x3d"' + q + '_start" style\x3d"overflow:hidden;line-height:0px"\x3e\x26#xFEFF;\x3c/span\x3e'),
                            y || (t.collapse(!1), m.moveToElementText(t.parentElement()), 0 === m.compareEndPoints("StartToEnd", t) && t.move("character", -1), t.pasteHTML('\x3cspan data-mce-type\x3d"bookmark" id\x3d"' + q + '_end" style\x3d"overflow:hidden;line-height:0px"\x3e\x26#xFEFF;\x3c/span\x3e'))
                    } catch (x) {
                        return null
                    }
                } else {
                    k = b.getNode();
                    t = k.nodeName;
                    if ("IMG" == t) return {
                        name: t,
                        index: n(t, k)
                    };
                    t = p(m.cloneRange());
                    y || (t.collapse(!1), t.insertNode(l.create("span", {
                            "data-mce-type": "bookmark",
                            id: q + "_end",
                            style: "overflow:hidden;line-height:0px"
                        },
                        "\x26#xFEFF;")));
                    m = p(m);
                    m.collapse(!0);
                    m.insertNode(l.create("span", {
                        "data-mce-type": "bookmark",
                        id: q + "_start",
                        style: "overflow:hidden;line-height:0px"
                    }, "\x26#xFEFF;"))
                }
                b.moveToBookmark({
                    id: q,
                    keep: 1
                });
                return {
                    id: q
                }
            };
            this.moveToBookmark = function(a) {
                function c(b) {
                    var c = a[b ? "start" : "end"],
                        d, e, f;
                    if (c) {
                        f = c[0];
                        e = m;
                        for (d = c.length - 1; 1 <= d; d--) {
                            e = e.childNodes;
                            if (c[d] > e.length - 1) return;
                            e = e[c[d]]
                        }
                        3 === e.nodeType && (f = Math.min(c[0], e.nodeValue.length));
                        1 === e.nodeType && (f = Math.min(c[0], e.childNodes.length));
                        b ? q.setStart(e,
                            f) : q.setEnd(e, f)
                    }
                    return !0
                }

                function d(b) {
                    var c = l.get(a.id + "_" + b),
                        d, e, f;
                    f = a.keep;
                    if (c && (d = c.parentNode, "start" == b ? (f ? (d = c.firstChild, e = 1) : e = l.nodeIndex(c), t = y = d, v = x = e) : (f ? (d = c.firstChild, e = 1) : e = l.nodeIndex(c), y = d, x = e), !f)) {
                        f = c.previousSibling;
                        d = c.nextSibling;
                        for (g.each(g.grep(c.childNodes), function(a) {
                                3 == a.nodeType && (a.nodeValue = a.nodeValue.replace(/\uFEFF/g, ""))
                            }); c = l.get(a.id + "_" + b);) l.remove(c, 1);
                        f && d && f.nodeType == d.nodeType && 3 == f.nodeType && !k.opera && (e = f.nodeValue.length, f.appendData(d.nodeValue),
                            l.remove(d), "start" == b ? (t = y = f, v = x = e) : (y = f, x = e))
                    }
                }

                function f(a) {
                    !l.isBlock(a) || a.innerHTML || k.ie || (a.innerHTML = '\x3cbr data-mce-bogus\x3d"1" /\x3e');
                    return a
                }

                function w() {
                    var b, c;
                    b = l.createRng();
                    c = e.resolve(l.getRoot(), a.start);
                    b.setStart(c.container(), c.offset());
                    c = e.resolve(l.getRoot(), a.end);
                    b.setEnd(c.container(), c.offset());
                    return b
                }
                var q, m, t, y, v, x;
                if (a)
                    if (g.isArray(a.start)) {
                        q = l.createRng();
                        m = l.getRoot();
                        if (b.tridentSel) return b.tridentSel.moveToBookmark(a);
                        c(!0) && c() && b.setRng(q)
                    } else "string" ==
                        typeof a.start ? b.setRng(w(a)) : a.id ? (d("start"), d("end"), t && (q = l.createRng(), q.setStart(f(t), v), q.setEnd(f(y), x), b.setRng(q))) : a.name ? b.select(l.select(a.name)[a.index]) : a.rng && b.setRng(a.rng)
            }
        }
        var d = c.isContentEditableFalse;
        b.isBookmarkNode = function(a) {
            return a && "SPAN" === a.tagName && "bookmark" === a.getAttribute("data-mce-type")
        };
        return b
    });
    z("tinymce/dom/Selection", "tinymce/dom/TreeWalker tinymce/dom/TridentSelection tinymce/dom/ControlSelection tinymce/dom/RangeUtils tinymce/dom/BookmarkManager tinymce/dom/NodeType tinymce/Env tinymce/util/Tools tinymce/caret/CaretPosition".split(" "),
        function(k, g, f, e, a, c, b, d, l) {
            function v(b, c, d, e) {
                this.dom = b;
                this.win = c;
                this.serializer = d;
                this.editor = e;
                this.bookmarkManager = new a(this);
                this.controlSelection = new f(this, e);
                this.win.getSelection || (this.tridentSel = new g(this))
            }
            var q = d.each,
                h = d.trim,
                n = b.ie;
            v.prototype = {
                setCursorLocation: function(a, b) {
                    var c = this.dom.createRng();
                    a ? (c.setStart(a, b), c.setEnd(a, b), this.setRng(c), this.collapse(!1)) : (this._moveEndPoint(c, this.editor.getBody(), !0), this.setRng(c))
                },
                getContent: function(a) {
                    var b = this.getRng(),
                        c =
                        this.dom.create("body"),
                        d = this.getSel(),
                        e, f;
                    a = a || {};
                    e = f = "";
                    a.get = !0;
                    a.format = a.format || "html";
                    a.selection = !0;
                    this.editor.fire("BeforeGetContent", a);
                    if ("text" == a.format) return this.isCollapsed() ? "" : b.text || (d.toString ? d.toString() : "");
                    b.cloneContents ? (b = b.cloneContents()) && c.appendChild(b) : b.item !== K || b.htmlText !== K ? (c.innerHTML = "\x3cbr\x3e" + (b.item ? b.item(0).outerHTML : b.htmlText), c.removeChild(c.firstChild)) : c.innerHTML = b.toString();
                    /^\s/.test(c.innerHTML) && (e = " ");
                    /\s+$/.test(c.innerHTML) && (f = " ");
                    a.getInner = !0;
                    a.content = this.isCollapsed() ? "" : e + this.serializer.serialize(c, a) + f;
                    this.editor.fire("GetContent", a);
                    return a.content
                },
                setContent: function(a, b) {
                    var c = this.getRng(),
                        d = this.win.document,
                        e, f;
                    b = b || {
                        format: "html"
                    };
                    b.set = !0;
                    b.selection = !0;
                    b.content = a;
                    b.no_events || this.editor.fire("BeforeSetContent", b);
                    a = b.content;
                    if (c.insertNode) {
                        a += '\x3cspan id\x3d"__caret"\x3e_\x3c/span\x3e';
                        c.startContainer == d && c.endContainer == d ? d.body.innerHTML = a : (c.deleteContents(), 0 === d.body.childNodes.length ? d.body.innerHTML =
                            a : c.createContextualFragment ? c.insertNode(c.createContextualFragment(a)) : (e = d.createDocumentFragment(), f = d.createElement("div"), e.appendChild(f), f.outerHTML = a, c.insertNode(e)));
                        a = this.dom.get("__caret");
                        c = d.createRange();
                        c.setStartBefore(a);
                        c.setEndBefore(a);
                        this.setRng(c);
                        this.dom.remove("__caret");
                        try {
                            this.setRng(c)
                        } catch (B) {}
                    } else c.item && (d.execCommand("Delete", !1, null), c = this.getRng()), /^\s+/.test(a) ? (c.pasteHTML('\x3cspan id\x3d"__mce_tmp"\x3e_\x3c/span\x3e' + a), this.dom.remove("__mce_tmp")) :
                        c.pasteHTML(a);
                    b.no_events || this.editor.fire("SetContent", b)
                },
                getStart: function(a) {
                    var b = this.getRng(),
                        c;
                    if (b.duplicate || b.item) {
                        if (b.item) return b.item(0);
                        c = b.duplicate();
                        c.collapse(1);
                        c = c.parentElement();
                        c.ownerDocument !== this.dom.doc && (c = this.dom.getRoot());
                        for (b = a = b.parentElement(); a = a.parentNode;)
                            if (a == c) {
                                c = b;
                                break
                            }
                        return c
                    }
                    c = b.startContainer;
                    1 == c.nodeType && c.hasChildNodes() && (a && b.collapsed || (c = c.childNodes[Math.min(c.childNodes.length - 1, b.startOffset)]));
                    return c && 3 == c.nodeType ? c.parentNode :
                        c
                },
                getEnd: function(a) {
                    var b = this.getRng(),
                        c, d;
                    if (b.duplicate || b.item) {
                        if (b.item) return b.item(0);
                        b = b.duplicate();
                        b.collapse(0);
                        c = b.parentElement();
                        c.ownerDocument !== this.dom.doc && (c = this.dom.getRoot());
                        return c && "BODY" == c.nodeName ? c.lastChild || c : c
                    }
                    c = b.endContainer;
                    d = b.endOffset;
                    1 == c.nodeType && c.hasChildNodes() && (a && b.collapsed || (c = c.childNodes[0 < d ? d - 1 : d]));
                    return c && 3 == c.nodeType ? c.parentNode : c
                },
                getBookmark: function(a, b) {
                    return this.bookmarkManager.getBookmark(a, b)
                },
                moveToBookmark: function(a) {
                    return this.bookmarkManager.moveToBookmark(a)
                },
                select: function(a, b) {
                    var c = this.dom,
                        d = c.createRng();
                    this.lastFocusBookmark = null;
                    if (a) {
                        if (!b && this.controlSelection.controlSelect(a)) return;
                        c = c.nodeIndex(a);
                        d.setStart(a.parentNode, c);
                        d.setEnd(a.parentNode, c + 1);
                        b && (this._moveEndPoint(d, a, !0), this._moveEndPoint(d, a));
                        this.setRng(d)
                    }
                    return a
                },
                isCollapsed: function() {
                    var a = this.getRng(),
                        b = this.getSel();
                    return !a || a.item ? !1 : a.compareEndPoints ? 0 === a.compareEndPoints("StartToEnd", a) : !b || a.collapsed
                },
                collapse: function(a) {
                    var b = this.getRng(),
                        c;
                    b.item && (c = b.item(0),
                        b = this.win.document.body.createTextRange(), b.moveToElementText(c));
                    b.collapse(!!a);
                    this.setRng(b)
                },
                getSel: function() {
                    var a = this.win;
                    return a.getSelection ? a.getSelection() : a.document.selection
                },
                getRng: function(a) {
                    function b(a, b, c) {
                        try {
                            return b.compareBoundaryPoints(a, c)
                        } catch (u) {
                            return -1
                        }
                    }
                    var c, d, e, f;
                    if (!this.win) return null;
                    f = this.win.document;
                    if (!a && this.lastFocusBookmark) return e = this.lastFocusBookmark, e.startContainer ? (d = f.createRange(), d.setStart(e.startContainer, e.startOffset), d.setEnd(e.endContainer,
                        e.endOffset)) : d = e, d;
                    if (a && this.tridentSel) return this.tridentSel.getRangeAt(0);
                    try {
                        if (c = this.getSel()) d = 0 < c.rangeCount ? c.getRangeAt(0) : c.createRange ? c.createRange() : f.createRange()
                    } catch (B) {}
                    a = this.editor.fire("GetSelectionRange", {
                        range: d
                    });
                    if (a.range !== d) return a.range;
                    if (n && d && d.setStart && f.selection) {
                        try {
                            e = f.selection.createRange()
                        } catch (B) {}
                        e && e.item && (e = e.item(0), d = f.createRange(), d.setStartBefore(e), d.setEndAfter(e))
                    }
                    d || (d = f.createRange ? f.createRange() : f.body.createTextRange());
                    d.setStart &&
                        9 === d.startContainer.nodeType && d.collapsed && (e = this.dom.getRoot(), d.setStart(e, 0), d.setEnd(e, 0));
                    this.selectedRange && this.explicitRange && (0 === b(d.START_TO_START, d, this.selectedRange) && 0 === b(d.END_TO_END, d, this.selectedRange) ? d = this.explicitRange : this.explicitRange = this.selectedRange = null);
                    return d
                },
                setRng: function(a, c) {
                    var d;
                    if (a)
                        if (a.select) {
                            this.explicitRange = null;
                            try {
                                a.select()
                            } catch (m) {}
                        } else if (!this.tridentSel) {
                        d = this.getSel();
                        a = this.editor.fire("SetSelectionRange", {
                            range: a
                        });
                        a = a.range;
                        if (d) {
                            this.explicitRange =
                                a;
                            try {
                                d.removeAllRanges(), d.addRange(a)
                            } catch (m) {}!1 === c && d.extend && (d.collapse(a.endContainer, a.endOffset), d.extend(a.startContainer, a.startOffset));
                            this.selectedRange = 0 < d.rangeCount ? d.getRangeAt(0) : null
                        }!a.collapsed && a.startContainer == a.endContainer && d.setBaseAndExtent && !b.ie && 2 > a.endOffset - a.startOffset && a.startContainer.hasChildNodes() && (c = a.startContainer.childNodes[a.startOffset]) && "IMG" == c.tagName && this.getSel().setBaseAndExtent(c, 0, c, 1)
                    } else if (a.cloneRange) try {
                        this.tridentSel.addRange(a)
                    } catch (m) {}
                },
                setNode: function(a) {
                    this.setContent(this.dom.getOuterHTML(a));
                    return a
                },
                getNode: function() {
                    function a(a, b) {
                        for (var c = a; a && 3 === a.nodeType && 0 === a.length;) a = b ? a.nextSibling : a.previousSibling;
                        return a || c
                    }
                    var b = this.getRng(),
                        c, d, e, f, h, l = this.dom.getRoot();
                    if (!b) return l;
                    d = b.startContainer;
                    e = b.endContainer;
                    f = b.startOffset;
                    h = b.endOffset;
                    if (b.setStart) return c = b.commonAncestorContainer, !b.collapsed && (d == e && 2 > h - f && d.hasChildNodes() && (c = d.childNodes[f]), 3 === d.nodeType && 3 === e.nodeType && (d = d.length === f ? a(d.nextSibling, !0) : d.parentNode, e = 0 === h ? a(e.previousSibling, !1) : e.parentNode, d && d === e)) ? d : c && 3 == c.nodeType ? c.parentNode : c;
                    c = b.item ? b.item(0) : b.parentElement();
                    c.ownerDocument !== this.win.document && (c = l);
                    return c
                },
                getSelectedBlocks: function(a, b) {
                    var c = this.dom,
                        d, e, f = [];
                    e = c.getRoot();
                    a = c.getParent(a || this.getStart(), c.isBlock);
                    b = c.getParent(b || this.getEnd(), c.isBlock);
                    a && a != e && f.push(a);
                    if (a && b && a != b) {
                        d = a;
                        for (var h = new k(a, e);
                            (d = h.next()) && d != b;) c.isBlock(d) && f.push(d)
                    }
                    b && a != b && b != e && f.push(b);
                    return f
                },
                isForward: function() {
                    var a =
                        this.dom,
                        b = this.getSel(),
                        c;
                    if (!b || !b.anchorNode || !b.focusNode) return !0;
                    c = a.createRng();
                    c.setStart(b.anchorNode, b.anchorOffset);
                    c.collapse(!0);
                    a = a.createRng();
                    a.setStart(b.focusNode, b.focusOffset);
                    a.collapse(!0);
                    return 0 >= c.compareBoundaryPoints(c.START_TO_START, a)
                },
                normalize: function() {
                    var a = this.getRng();
                    b.range && (new e(this.dom)).normalize(a) && this.setRng(a, this.isForward());
                    return a
                },
                selectorChanged: function(a, b) {
                    var c = this,
                        d;
                    c.selectorChangedData || (c.selectorChangedData = {}, d = {}, c.editor.on("NodeChange",
                        function(a) {
                            var b = a.element,
                                e = c.dom,
                                f = e.getParents(b, null, e.getRoot()),
                                m = {};
                            q(c.selectorChangedData, function(a, b) {
                                q(f, function(c) {
                                    if (e.is(c, b)) return d[b] || (q(a, function(a) {
                                        a(!0, {
                                            node: c,
                                            selector: b,
                                            parents: f
                                        })
                                    }), d[b] = a), m[b] = a, !1
                                })
                            });
                            q(d, function(a, c) {
                                m[c] || (delete d[c], q(a, function(a) {
                                    a(!1, {
                                        node: b,
                                        selector: c,
                                        parents: f
                                    })
                                }))
                            })
                        }));
                    c.selectorChangedData[a] || (c.selectorChangedData[a] = []);
                    c.selectorChangedData[a].push(b);
                    return c
                },
                getScrollContainer: function() {
                    for (var a, b = this.dom.getRoot(); b && "BODY" !=
                        b.nodeName;) {
                        if (b.scrollHeight > b.clientHeight) {
                            a = b;
                            break
                        }
                        b = b.parentNode
                    }
                    return a
                },
                scrollIntoView: function(a, b) {
                    function d(a) {
                        for (var b = 0, c = 0; a && a.nodeType;) b += a.offsetLeft || 0, c += a.offsetTop || 0, a = a.offsetParent;
                        return {
                            x: b,
                            y: c
                        }
                    }
                    var e = this.dom,
                        f = e.getRoot(),
                        h;
                    h = 0;
                    if (c.isElement(a)) {
                        !1 === b && (h = a.offsetHeight);
                        if ("BODY" != f.nodeName && (b = this.getScrollContainer())) {
                            a = d(a).y - d(b).y + h;
                            h = b.clientHeight;
                            e = b.scrollTop;
                            if (a < e || a + 25 > e + h) b.scrollTop = a < e ? a : a - h + 25;
                            return
                        }
                        b = e.getViewPort(this.editor.getWin());
                        a = e.getPos(a).y +
                            h;
                        e = b.y;
                        h = b.h;
                        (a < b.y || a + 25 > e + h) && this.editor.getWin().scrollTo(0, a < e ? a : a - h + 25)
                    }
                },
                placeCaretAt: function(a, b) {
                    this.setRng(e.getCaretRangeFromPoint(a, b, this.editor.getDoc()))
                },
                _moveEndPoint: function(a, c, d) {
                    var e = c,
                        f = new k(c, e),
                        l = this.dom.schema.getNonEmptyElements();
                    do {
                        if (3 == c.nodeType && 0 !== h(c.nodeValue).length) {
                            d ? a.setStart(c, 0) : a.setEnd(c, c.nodeValue.length);
                            return
                        }
                        if (l[c.nodeName] && !/^(TD|TH)$/.test(c.nodeName)) {
                            d ? a.setStartBefore(c) : "BR" == c.nodeName ? a.setEndBefore(c) : a.setEndAfter(c);
                            return
                        }
                        if (b.ie &&
                            11 > b.ie && this.dom.isBlock(c) && this.dom.isEmpty(c)) {
                            d ? a.setStart(c, 0) : a.setEnd(c, 0);
                            return
                        }
                    } while (c = d ? f.next() : f.prev());
                    "BODY" == e.nodeName && (d ? a.setStart(e, 0) : a.setEnd(e, e.childNodes.length))
                },
                getBoundingClientRect: function() {
                    var a = this.getRng();
                    return a.collapsed ? l.fromRangeStart(a).getClientRects()[0] : a.getBoundingClientRect()
                },
                destroy: function() {
                    this.win = null;
                    this.controlSelection.destroy()
                }
            };
            return v
        });
    z("tinymce/dom/ElementUtils", ["tinymce/dom/BookmarkManager", "tinymce/util/Tools"], function(k,
        g) {
        var f = g.each;
        return function(e) {
            this.compare = function(a, c) {
                function b(a) {
                    var b = {};
                    f(e.getAttribs(a), function(c) {
                        c = c.nodeName.toLowerCase();
                        0 !== c.indexOf("_") && "style" !== c && "data-mce-style" !== c && "data-mce-fragment" != c && (b[c] = e.getAttrib(a, c))
                    });
                    return b
                }

                function d(a, b) {
                    var c, d;
                    for (d in a)
                        if (a.hasOwnProperty(d)) {
                            c = b[d];
                            if ("undefined" == typeof c || a[d] != c) return !1;
                            delete b[d]
                        }
                    for (d in b)
                        if (b.hasOwnProperty(d)) return !1;
                    return !0
                }
                return a.nodeName == c.nodeName && d(b(a), b(c)) && d(e.parseStyle(e.getAttrib(a,
                    "style")), e.parseStyle(e.getAttrib(c, "style"))) ? !k.isBookmarkNode(a) && !k.isBookmarkNode(c) : !1
            }
        }
    });
    z("tinymce/fmt/Preview", ["tinymce/util/Tools"], function(k) {
        var g = k.each;
        return {
            getCssText: function(f, e) {
                var a, c = f.dom,
                    b = "",
                    d, l;
                l = f.settings.preview_styles;
                if (!1 === l) return "";
                l || (l = "font-family font-size font-weight font-style text-decoration text-transform color background-color border border-radius outline text-shadow");
                if ("string" == typeof e) {
                    e = f.formatter.get(e);
                    if (!e) return;
                    e = e[0]
                }
                a = c.create(e.block ||
                    e.inline || "span");
                g(e.styles, function(b, d) {
                    (b = b.replace(/%(\w+)/g, "")) && c.setStyle(a, d, b)
                });
                g(e.attributes, function(b, d) {
                    (b = b.replace(/%(\w+)/g, "")) && c.setAttrib(a, d, b)
                });
                g(e.classes, function(b) {
                    b = b.replace(/%(\w+)/g, "");
                    c.hasClass(a, b) || c.addClass(a, b)
                });
                f.fire("PreviewFormats");
                c.setStyles(a, {
                    position: "absolute",
                    left: -65535
                });
                f.getBody().appendChild(a);
                d = c.getStyle(f.getBody(), "fontSize", !0);
                d = /px$/.test(d) ? parseInt(d, 10) : 0;
                g(l.split(" "), function(e) {
                    var l = c.getStyle(a, e, !0);
                    if ("background-color" ==
                        e && /transparent|rgba\s*\([^)]+,\s*0\)/.test(l) && (l = c.getStyle(f.getBody(), e, !0), "#ffffff" == c.toHex(l).toLowerCase())) return;
                    if ("color" != e || "#000000" != c.toHex(l).toLowerCase()) {
                        if ("font-size" == e && /em|%$/.test(l)) {
                            if (0 === d) return;
                            l = parseFloat(l, 10) / (/%$/.test(l) ? 100 : 1);
                            l = l * d + "px"
                        }
                        "border" == e && l && (b += "padding:0 2px;");
                        b += e + ":" + l + ";"
                    }
                });
                f.fire("AfterPreviewFormats");
                c.remove(a);
                return b
            }
        }
    });
    z("tinymce/fmt/Hooks", ["tinymce/util/Arr", "tinymce/dom/NodeType", "tinymce/dom/DomQuery"], function(k, g, f) {
        var e = [],
            a = k.filter,
            c = k.each;
        (function(a, c) {
            e[a] || (e[a] = []);
            e[a].push(c)
        })("pre", function(b) {
            function d(a) {
                return v(a.previousSibling) && -1 != k.indexOf(q, a.previousSibling)
            }
            var e = b.selection.getRng(),
                v, q;
            v = g.matchNodeNames("pre");
            e.collapsed || (q = b.selection.getSelectedBlocks(), c(a(a(q, v), d), function(a) {
                var b = a.previousSibling;
                f(a).remove();
                f(b).append("\x3cbr\x3e\x3cbr\x3e").append(a.childNodes)
            }))
        });
        return {
            postProcess: function(a, d) {
                c(e[a], function(a) {
                    a(d)
                })
            }
        }
    });
    z("tinymce/Formatter", "tinymce/dom/TreeWalker tinymce/dom/RangeUtils tinymce/dom/BookmarkManager tinymce/dom/ElementUtils tinymce/util/Tools tinymce/fmt/Preview tinymce/fmt/Hooks".split(" "),
        function(k, g, f, e, a, c, b) {
            return function(d) {
                function l(a) {
                    a.nodeType && (a = a.nodeName);
                    return !!d.schema.getTextBlockElements()[a.toLowerCase()]
                }

                function v(a, b) {
                    return C.getParents(a, b, C.getRoot())
                }

                function q(a) {
                    return 1 === a.nodeType && "_mce_caret" === a.id
                }

                function h(a) {
                    return a ? M[a] : M
                }

                function n(a, b) {
                    a && ("string" !== typeof a ? J(a, function(a, b) {
                        n(b, a)
                    }) : (b = b.length ? b : [b], J(b, function(a) {
                        a.deep === ca && (a.deep = !a.selector);
                        a.split === ca && (a.split = !a.selector || a.inline);
                        a.remove === ca && a.selector && !a.inline && (a.remove =
                            "none");
                        a.selector && a.inline && (a.mixed = !0, a.block_expand = !0);
                        "string" === typeof a.classes && (a.classes = a.classes.split(/\s+/))
                    }), M[a] = b))
                }

                function p(a, b) {
                    if (b = h(b))
                        for (var c = 0; c < b.length; c++)
                            if (!1 === b[c].inherit && C.is(a, b[c].selector)) return !0;
                    return !1
                }

                function w(a) {
                    var b;
                    d.dom.getParent(a, function(a) {
                        return (b = d.dom.getStyle(a, "text-decoration")) && "none" !== b
                    });
                    return b
                }

                function A(a) {
                    var b;
                    1 === a.nodeType && a.parentNode && 1 === a.parentNode.nodeType && (b = w(a.parentNode), d.dom.getStyle(a, "color") && b ? d.dom.setStyle(a,
                        "text-decoration", b) : d.dom.getStyle(a, "text-decoration") === b && d.dom.setStyle(a, "text-decoration", null))
                }

                function m(a, c, e) {
                    function f(a, b) {
                        b = b || u;
                        if (a) {
                            if (b.onformat) b.onformat(a, b, c, e);
                            J(b.styles, function(b, d) {
                                C.setStyle(a, d, O(b, c))
                            });
                            if (b.styles) {
                                var d = C.getAttrib(a, "style");
                                d && a.setAttribute("data-mce-style", d)
                            }
                            J(b.attributes, function(b, d) {
                                C.setAttrib(a, d, O(b, c))
                            });
                            J(b.classes, function(b) {
                                b = O(b, c);
                                C.hasClass(a, b) || C.addClass(a, b)
                            })
                        }
                    }

                    function n() {
                        var a = d.selection.getRng(),
                            b = a.startContainer,
                            c = a.endContainer;
                        if (b != c && 0 === a.endOffset) {
                            a: {
                                c = new k(c);
                                for (e = c.prev2(); e; e = c.prev2()) {
                                    if (3 == e.nodeType && 0 < e.data.length) {
                                        b = e;
                                        break a
                                    }
                                    if (1 < e.childNodes.length || e == b || "BR" == e.tagName) {
                                        b = e;
                                        break a
                                    }
                                }
                                b = void 0
                            }
                            a.setEnd(b, 3 == b.nodeType ? b.data.length : b.childNodes.length)
                        }
                        return a
                    }

                    function p(b, d, e) {
                        var m = [],
                            h, p, n = !0;
                        h = u.inline || u.block;
                        p = C.create(h);
                        f(p);
                        P.walk(b, function(b) {
                            function d(b) {
                                var x, k, v, A, B;
                                B = n;
                                x = b.nodeName.toLowerCase();
                                k = b.parentNode.nodeName.toLowerCase();
                                1 === b.nodeType && ea(b) && (B = n, n = "true" === ea(b), A = !0);
                                if (r(x, "br")) t = 0, u.block && C.remove(b);
                                else if (u.wrapper && y(b, a, c)) t = 0;
                                else if (n && !A && u.block && !u.wrapper && l(x) && da(k, h)) b = C.rename(b, h), f(b), m.push(b), t = 0;
                                else {
                                    if (u.selector && (J(g, function(a) {
                                            if (!("collapsed" in a && a.collapsed !== w) && C.is(b, a.selector) && !q(b)) return f(b, a), v = !0, !1
                                        }), !u.inline || v)) {
                                        t = 0;
                                        return
                                    }!n || A || !da(h, x) || !da(k, h) || !e && 3 === b.nodeType && 1 === b.nodeValue.length && 65279 === b.nodeValue.charCodeAt(0) || q(b) || u.inline && Q(b) ? (t = 0, J(S(b.childNodes), d), A && (n = B), t = 0) : (t || (t = C.clone(p, !1), b.parentNode.insertBefore(t,
                                        b), m.push(t)), t.appendChild(b))
                                }
                            }
                            var t;
                            J(b, d)
                        });
                        !0 === u.links && J(m, function(a) {
                            function b(a) {
                                "A" === a.nodeName && f(a, u);
                                J(S(a.childNodes), b)
                            }
                            b(a)
                        });
                        J(m, function(b) {
                            function d(a) {
                                var b, c;
                                J(a.childNodes, function(a) {
                                    if (1 == a.nodeType && !ba(a) && !q(a)) return b = a, !1
                                });
                                b && !ba(b) && x(b, u) && (c = C.clone(b, !1), f(c), C.replace(c, a, !0), C.remove(b, 1));
                                return c || a
                            }
                            var e;
                            e = function(a) {
                                var b = 0;
                                J(a.childNodes, function(a) {
                                    z(a) || ba(a) || b++
                                });
                                return b
                            }(b);
                            if ((1 < m.length || !Q(b)) && 0 === e) C.remove(b, 1);
                            else if (u.inline || u.wrapper) {
                                u.exact ||
                                    1 !== e || (b = d(b));
                                J(g, function(a) {
                                    J(C.select(a.inline, b), function(b) {
                                        ba(b) || N(a, c, b, a.exact ? b : null)
                                    })
                                });
                                if (y(b.parentNode, a, c)) return C.remove(b, 1), b = 0, !0;
                                u.merge_with_parents && C.getParent(b.parentNode, function(d) {
                                    if (y(d, a, c)) return C.remove(b, 1), b = 0, !0
                                });
                                b && !1 !== u.merge_siblings && (b = H(I(b), b), b = H(b, I(b, !0)))
                            }
                        })
                    }
                    var g = h(a),
                        u = g[0],
                        t, w = !e && D.isCollapsed();
                    if ("false" === ea(D.getNode())) {
                        e = D.getNode();
                        t = 0;
                        for (var v = g.length; t < v; t++)
                            if (g[t].ceFalseOverride && C.is(e, g[t].selector)) {
                                f(e, g[t]);
                                break
                            }
                    } else u &&
                        (e ? e.nodeType ? (t = C.createRng(), t.setStartBefore(e), t.setEndAfter(e), p(G(t, g), null, !0)) : p(e, null, !0) : w && u.inline && !C.select("td[data-mce-selected],th[data-mce-selected]").length ? E("apply", a, c) : (v = d.selection.getNode(), Z || !g[0].defaultBlock || C.getParent(v, C.isBlock) || m(g[0].defaultBlock), d.selection.setRng(n()), t = D.getBookmark(), p(G(D.getRng(!0), g), t), u.styles && (u.styles.color || u.styles.textDecoration) && (ka(v, A, "childNodes"), A(v)), D.moveToBookmark(t), K(D.getRng(!0)), d.nodeChanged()), b.postProcess(a,
                            d))
                }

                function t(a, b, c, e) {
                    function f(a) {
                        var c, d, e, m, h;
                        1 === a.nodeType && ea(a) && (m = u, u = "true" === ea(a), h = !0);
                        c = S(a.childNodes);
                        if (u && !h)
                            for (d = 0, e = g.length; d < e && !N(g[d], b, a, a); d++);
                        if (r.deep && c.length) {
                            d = 0;
                            for (e = c.length; d < e; d++) f(c[d]);
                            h && (u = m)
                        }
                    }

                    function m(c) {
                        var d;
                        J(v(c.parentNode).reverse(), function(c) {
                            var f;
                            d || "_start" == c.id || "_end" == c.id || (f = y(c, a, b, e)) && !1 !== f.split && (d = c)
                        });
                        return d
                    }

                    function l(a) {
                        var c = m(a),
                            d = a,
                            e, f, h, l, n, p;
                        if (c) {
                            p = c.parentNode;
                            for (e = d.parentNode; e && e != p; e = e.parentNode) {
                                f = C.clone(e, !1);
                                for (n = 0; n < g.length; n++)
                                    if (N(g[n], b, f, f)) {
                                        f = 0;
                                        break
                                    }
                                f && (h && f.appendChild(h), l || (l = f), h = f)
                            }
                            r.mixed && Q(c) || (d = C.split(c, d));
                            h && (a.parentNode.insertBefore(h, a), l.appendChild(a))
                        }
                        return d
                    }

                    function n(a) {
                        var b = C.get(a ? "_start" : "_end"),
                            c = b[a ? "firstChild" : "lastChild"];
                        ba(c) && (c = c[a ? "firstChild" : "lastChild"]);
                        3 == c.nodeType && 0 === c.data.length && (c = a ? b.previousSibling || b.nextSibling : b.nextSibling || b.previousSibling);
                        C.remove(b, !0);
                        return c
                    }

                    function p(a) {
                        var b, c, e = a.commonAncestorContainer;
                        a = G(a, g, !0);
                        if (r.split) {
                            b =
                                L(a, !0);
                            c = L(a);
                            if (b != c) {
                                /^(TR|TH|TD)$/.test(b.nodeName) && b.firstChild && (b = "TR" == b.nodeName ? b.firstChild.firstChild || b : b.firstChild || b);
                                e && /^T(HEAD|BODY|FOOT|R)$/.test(e.nodeName) && /^(TH|TD)$/.test(c.nodeName) && c.firstChild && (c = c.firstChild || c);
                                if (C.isChildOf(b, c) && !Q(c) && !/^(TH|TD)$/.test(b.nodeName) && !/^(TH|TD)$/.test(c.nodeName)) {
                                    b = U(b, "span", {
                                        id: "_start",
                                        "data-mce-type": "bookmark"
                                    });
                                    l(b);
                                    b = n(!0);
                                    return
                                }
                                b = U(b, "span", {
                                    id: "_start",
                                    "data-mce-type": "bookmark"
                                });
                                c = U(c, "span", {
                                    id: "_end",
                                    "data-mce-type": "bookmark"
                                });
                                l(b);
                                l(c);
                                b = n(!0);
                                c = n()
                            } else b = c = l(b);
                            a.startContainer = b.parentNode ? b.parentNode : b;
                            a.startOffset = aa(b);
                            a.endContainer = c.parentNode ? c.parentNode : c;
                            a.endOffset = aa(c) + 1
                        }
                        P.walk(a, function(a) {
                            J(a, function(a) {
                                f(a);
                                1 === a.nodeType && "underline" === d.dom.getStyle(a, "text-decoration") && a.parentNode && "underline" === w(a.parentNode) && N({
                                    deep: !1,
                                    exact: !0,
                                    inline: "span",
                                    styles: {
                                        textDecoration: "underline"
                                    }
                                }, null, a)
                            })
                        })
                    }
                    var g = h(a),
                        r = g[0],
                        q, u = !0;
                    if (c) c.nodeType ? (q = C.createRng(), q.setStartBefore(c), q.setEndAfter(c), p(q)) :
                        p(c);
                    else if ("false" === ea(D.getNode())) {
                        c = D.getNode();
                        q = 0;
                        for (var t = g.length; q < t && (!g[q].ceFalseOverride || !N(g[q], b, c, c)); q++);
                    } else D.isCollapsed() && r.inline && !C.select("td[data-mce-selected],th[data-mce-selected]").length ? E("remove", a, b, e) : (c = D.getBookmark(), p(D.getRng(!0)), D.moveToBookmark(c), r.inline && B(a, b, D.getStart()) && K(D.getRng(!0)), d.nodeChanged())
                }

                function y(a, b, c, d) {
                    function e(a, b, e) {
                        var f, m, h = b[e];
                        if (b.onmatch) return b.onmatch(a, b, e);
                        if (h)
                            if (h.length === ca)
                                for (f in h) {
                                    if (h.hasOwnProperty(f) &&
                                        (m = "attributes" === e ? C.getAttrib(a, f) : u(a, f), d && !m && !b.exact || (!d || b.exact) && !r(m, F(O(h[f], c), f)))) return
                                } else
                                    for (f = 0; f < h.length && ("attributes" === e ? !C.getAttrib(a, h[f]) : !u(a, h[f])); f++);
                        return b
                    }
                    var f = h(b),
                        m;
                    if (f && a)
                        for (m = 0; m < f.length; m++)
                            if (b = f[m], x(a, b) && e(a, b, "attributes") && e(a, b, "styles")) {
                                if (f = b.classes)
                                    for (m = 0; m < f.length; m++)
                                        if (!C.hasClass(a, f[m])) return;
                                return b
                            }
                }

                function B(a, b, c) {
                    function d(c) {
                        var d = C.getRoot();
                        if (c === d) return !1;
                        c = C.getParent(c, function(c) {
                            return p(c, a) ? !0 : c.parentNode ===
                                d || !!y(c, a, b, !0)
                        });
                        return y(c, a, b)
                    }
                    var e;
                    if (c) return d(c);
                    c = D.getNode();
                    if (d(c)) return !0;
                    e = D.getStart();
                    return e != c && d(e) ? !0 : !1
                }

                function x(a, b) {
                    if (r(a, b.inline) || r(a, b.block)) return !0;
                    if (b.selector) return 1 == a.nodeType && C.is(a, b.selector)
                }

                function r(a, b) {
                    a = a || "";
                    b = b || "";
                    a = "" + (a.nodeName || a);
                    b = "" + (b.nodeName || b);
                    return a.toLowerCase() == b.toLowerCase()
                }

                function u(a, b) {
                    return F(C.getStyle(a, b), b)
                }

                function F(a, b) {
                    if ("color" == b || "backgroundColor" == b) a = C.toHex(a);
                    "fontWeight" == b && 700 == a && (a = "bold");
                    "fontFamily" ==
                    b && (a = a.replace(/[\'\"]/g, "").replace(/,\s+/g, ","));
                    return "" + a
                }

                function O(a, b) {
                    "string" != typeof a ? a = a(b) : b && (a = a.replace(/%(\w+)/g, function(a, c) {
                        return b[c] || a
                    }));
                    return a
                }

                function z(a) {
                    return a && 3 === a.nodeType && /^([\t \r\n]+|)$/.test(a.nodeValue)
                }

                function U(a, b, c) {
                    b = C.create(b, c);
                    a.parentNode.insertBefore(b, a);
                    b.appendChild(a);
                    return b
                }

                function G(a, b, c) {
                    function e(a) {
                        var c, d, e, f;
                        c = d = a ? q : t;
                        e = a ? "previousSibling" : "nextSibling";
                        f = C.getRoot();
                        if (3 == c.nodeType && !z(c) && (a ? 0 < u : x < c.nodeValue.length)) return c;
                        for (; b[0].block_expand || !Q(d);) {
                            for (a = d[e]; a; a = a[e]) {
                                if (c = !ba(a) && !z(a)) c = a, c = !("BR" == c.nodeName && c.getAttribute("data-mce-bogus") && !c.nextSibling);
                                if (c) return d
                            }
                            if (d == f || d.parentNode == f) break;
                            d = d.parentNode
                        }
                        return d
                    }

                    function f(a, b) {
                        for (b === ca && (b = 3 === a.nodeType ? a.length : a.childNodes.length); a && a.hasChildNodes();)(a = a.childNodes[b]) && (b = 3 === a.nodeType ? a.length : a.childNodes.length);
                        return {
                            node: a,
                            offset: b
                        }
                    }

                    function m(a) {
                        for (var b = a; b;) {
                            if (1 === b.nodeType && ea(b)) return "false" === ea(b) ? b : a;
                            b = b.parentNode
                        }
                        return a
                    }

                    function h(a, b, e) {
                        function f(a, b) {
                            var d = a.nodeValue;
                            "undefined" == typeof b && (b = e ? d.length : 0);
                            e ? (a = d.lastIndexOf(" ", b), b = d.lastIndexOf("\u00a0", b), a = a > b ? a : b, -1 === a || c || a++) : (a = d.indexOf(" ", b), b = d.indexOf("\u00a0", b), a = -1 !== a && (-1 === b || a < b) ? a : b);
                            return a
                        }
                        var m, h;
                        if (3 === a.nodeType) {
                            b = f(a, b);
                            if (-1 !== b) return {
                                container: a,
                                offset: b
                            };
                            h = a
                        }
                        for (a = new k(a, C.getParent(a, Q) || d.getBody()); m = a[e ? "prev" : "next"]();)
                            if (3 === m.nodeType) {
                                if (h = m, b = f(m), -1 !== b) return {
                                    container: m,
                                    offset: b
                                }
                            } else if (Q(m)) break;
                        if (h) return b =
                            e ? 0 : h.length, {
                                container: h,
                                offset: b
                            }
                    }

                    function n(c, d) {
                        var e, f, m;
                        3 == c.nodeType && 0 === c.nodeValue.length && c[d] && (c = c[d]);
                        d = v(c);
                        for (e = 0; e < d.length; e++)
                            for (f = 0; f < b.length; f++)
                                if (m = b[f], !("collapsed" in m && m.collapsed !== a.collapsed) && C.is(d[e], m.selector)) return d[e];
                        return c
                    }

                    function p(a, c) {
                        var d, e = C.getRoot();
                        b[0].wrapper || (d = C.getParent(a, b[0].block, e));
                        d || (d = C.getParent(3 == a.nodeType ? a.parentNode : a, function(a) {
                            return a != e && l(a)
                        }));
                        d && b[0].wrapper && (d = v(d, "ul,ol").reverse()[0] || d);
                        if (!d)
                            for (d = a; d[c] &&
                                !Q(d[c]) && (d = d[c], !r(d, "br")););
                        return d || a
                    }
                    var g, q = a.startContainer,
                        u = a.startOffset,
                        t = a.endContainer,
                        x = a.endOffset;
                    1 == q.nodeType && q.hasChildNodes() && (g = q.childNodes.length - 1, q = q.childNodes[u > g ? g : u], 3 == q.nodeType && (u = 0));
                    1 == t.nodeType && t.hasChildNodes() && (g = t.childNodes.length - 1, t = t.childNodes[x > g ? g : x - 1], 3 == t.nodeType && (x = t.nodeValue.length));
                    q = m(q);
                    t = m(t);
                    if (ba(q.parentNode) || ba(q)) q = ba(q) ? q : q.parentNode, q = q.nextSibling || q, 3 == q.nodeType && (u = 0);
                    if (ba(t.parentNode) || ba(t)) t = ba(t) ? t : t.parentNode, t =
                        t.previousSibling || t, 3 == t.nodeType && (x = t.length);
                    if (b[0].inline) {
                        if (a.collapsed) {
                            if (g = h(q, u, !0)) q = g.container, u = g.offset;
                            if (g = h(t, x)) t = g.container, x = g.offset
                        }
                        g = f(t, x);
                        if (g.node) {
                            for (; g.node && 0 === g.offset && g.node.previousSibling;) g = f(g.node.previousSibling);
                            g.node && 0 < g.offset && 3 === g.node.nodeType && " " === g.node.nodeValue.charAt(g.offset - 1) && 1 < g.offset && (t = g.node, t.splitText(g.offset - 1))
                        }
                    }
                    if (b[0].inline || b[0].block_expand) b[0].inline && 3 == q.nodeType && 0 !== u || (q = e(!0)), b[0].inline && 3 == t.nodeType && x !== t.nodeValue.length ||
                        (t = e());
                    b[0].selector && !1 !== b[0].expand && !b[0].inline && (q = n(q, "previousSibling"), t = n(t, "nextSibling"));
                    if (b[0].block || b[0].selector) q = p(q, "previousSibling"), t = p(t, "nextSibling"), b[0].block && (Q(q) || (q = e(!0)), Q(t) || (t = e()));
                    1 == q.nodeType && (u = aa(q), q = q.parentNode);
                    1 == t.nodeType && (x = aa(t) + 1, t = t.parentNode);
                    return {
                        startContainer: q,
                        startOffset: u,
                        endContainer: t,
                        endOffset: x
                    }
                }

                function N(a, b, c, d) {
                    var e, f, m;
                    if (!(x(c, a) || a.links && "A" == c.tagName)) return !1;
                    if ("all" != a.remove)
                        for (J(a.styles, function(e, f) {
                                e = F(O(e,
                                    b), f);
                                "number" === typeof f && (f = e, d = 0);
                                (a.remove_similar || !d || r(u(d, f), e)) && C.setStyle(c, f, "");
                                m = 1
                            }), m && "" === C.getAttrib(c, "style") && (c.removeAttribute("style"), c.removeAttribute("data-mce-style")), J(a.attributes, function(a, e) {
                                var f;
                                a = O(a, b);
                                "number" === typeof e && (e = a, d = 0);
                                if (!d || r(C.getAttrib(d, e), a)) {
                                    if ("class" == e && (a = C.getAttrib(c, e)) && (f = "", J(a.split(/\s+/), function(a) {
                                            /mce\-\w+/.test(a) && (f += (f ? " " : "") + a)
                                        }), f)) {
                                        C.setAttrib(c, e, f);
                                        return
                                    }
                                    "class" == e && c.removeAttribute("className");
                                    X.test(e) && c.removeAttribute("data-mce-" +
                                        e);
                                    c.removeAttribute(e)
                                }
                            }), J(a.classes, function(a) {
                                a = O(a, b);
                                d && !C.hasClass(d, a) || C.removeClass(c, a)
                            }), f = C.getAttribs(c), e = 0; e < f.length; e++)
                            if (0 !== f[e].nodeName.indexOf("_")) return !1;
                    if ("none" != a.remove) return R(c, a), !0
                }

                function R(a, b) {
                    function c(a, b, c) {
                        a = I(a, b, c);
                        return !a || "BR" == a.nodeName || Q(a)
                    }
                    var e = a.parentNode,
                        f;
                    b.block && (Z ? e == C.getRoot() && (b.list_block && r(a, b.list_block) || J(S(a.childNodes), function(a) {
                        da(Z, a.nodeName.toLowerCase()) ? f ? f.appendChild(a) : (f = U(a, Z), C.setAttribs(f, d.settings.forced_root_block_attrs)) :
                            f = 0
                    })) : Q(a) && !Q(e) && (c(a, !1) || c(a.firstChild, !0, 1) || a.insertBefore(C.create("br"), a.firstChild), c(a, !0) || c(a.lastChild, !1, 1) || a.appendChild(C.create("br"))));
                    b.selector && b.inline && !r(b.inline, a) || C.remove(a, 1)
                }

                function I(a, b, c) {
                    if (a)
                        for (b = b ? "nextSibling" : "previousSibling", a = c ? a : a[b]; a; a = a[b])
                            if (1 == a.nodeType || !z(a)) return a
                }

                function H(a, b) {
                    function c(a, b) {
                        for (d = a; d && (3 != d.nodeType || 0 === d.nodeValue.length); d = d[b])
                            if (1 == d.nodeType && !ba(d)) return d;
                        return a
                    }
                    var d, f;
                    f = new e(C);
                    if (a && b && (a = c(a, "previousSibling"),
                            b = c(b, "nextSibling"), f.compare(a, b))) {
                        for (d = a.nextSibling; d && d != b;) f = d, d = d.nextSibling, a.appendChild(f);
                        C.remove(b);
                        J(S(b.childNodes), function(b) {
                            a.appendChild(b)
                        });
                        return a
                    }
                    return b
                }

                function L(a, b) {
                    var c, e;
                    c = a[b ? "startContainer" : "endContainer"];
                    a = a[b ? "startOffset" : "endOffset"];
                    1 == c.nodeType && (e = c.childNodes.length - 1, !b && a && a--, c = c.childNodes[a > e ? e : a]);
                    3 === c.nodeType && b && a >= c.nodeValue.length && (c = (new k(c, d.getBody())).next() || c);
                    3 !== c.nodeType || b || 0 !== a || (c = (new k(c, d.getBody())).prev() || c);
                    return c
                }

                function E(a, b, c, e) {
                    function f(a) {
                        var b = C.create("span", {
                            id: "_mce_caret",
                            "data-mce-bogus": !0,
                            style: w ? "color:red" : ""
                        });
                        a && b.appendChild(d.getDoc().createTextNode("\ufeff"));
                        return b
                    }

                    function g(a, b) {
                        for (; a;) {
                            if (3 === a.nodeType && "\ufeff" !== a.nodeValue || 1 < a.childNodes.length) return !1;
                            b && 1 === a.nodeType && b.push(a);
                            a = a.firstChild
                        }
                        return !0
                    }

                    function n(a) {
                        for (; a;) {
                            if ("_mce_caret" === a.id) return a;
                            a = a.parentNode
                        }
                    }

                    function p(a) {
                        var b;
                        if (a)
                            for (b = new k(a, a), a = b.current(); a; a = b.next())
                                if (3 === a.nodeType) return a
                    }

                    function q(a,
                        b) {
                        var c;
                        if (a) c = D.getRng(!0), g(a) ? (!1 !== b && (c.setStartBefore(a), c.setEndBefore(a)), C.remove(a)) : (b = p(a), "\ufeff" === b.nodeValue.charAt(0) && (b.deleteData(0, 1), c.startContainer == b && 0 < c.startOffset && c.setStart(b, c.startOffset - 1), c.endContainer == b && 0 < c.endOffset && c.setEnd(b, c.endOffset - 1)), C.remove(a, 1)), D.setRng(c);
                        else if (a = n(D.getStart()), !a)
                            for (; a = C.get("_mce_caret");) q(a, !1)
                    }

                    function r() {
                        var a, d, e, l, g;
                        a = D.getRng(!0);
                        l = a.startOffset;
                        g = a.startContainer.nodeValue;
                        (d = n(D.getStart())) && (e = p(d));
                        g && 0 < l &&
                            l < g.length && /\w/.test(g.charAt(l)) && /\w/.test(g.charAt(l - 1)) ? (d = D.getBookmark(), a.collapse(!0), a = G(a, h(b)), a = P.split(a), m(b, c, a), D.moveToBookmark(d)) : (d && "\ufeff" === e.nodeValue || (d = f(!0), e = d.firstChild, a.insertNode(d), l = 1), m(b, c, d), D.setCursorLocation(e, l))
                    }

                    function u() {
                        var a = D.getRng(!0),
                            d, m, g, n, p, q = [];
                        d = a.startContainer;
                        m = a.startOffset;
                        g = d;
                        3 == d.nodeType && (m != d.nodeValue.length && (n = !0), g = g.parentNode);
                        for (; g;) {
                            if (y(g, b, c, e)) {
                                p = g;
                                break
                            }
                            g.nextSibling && (n = !0);
                            q.push(g);
                            g = g.parentNode
                        }
                        if (p)
                            if (n) g = D.getBookmark(),
                                a.collapse(!0), a = G(a, h(b), !0), a = P.split(a), t(b, c, a), D.moveToBookmark(g);
                            else {
                                g = a = f();
                                for (d = q.length - 1; 0 <= d; d--) g.appendChild(C.clone(q[d], !1)), g = g.firstChild;
                                g.appendChild(C.doc.createTextNode("\ufeff"));
                                g = g.firstChild;
                                (q = C.getParent(p, l)) && C.isEmpty(q) ? p.parentNode.replaceChild(a, p) : C.insertAfter(a, p);
                                D.setCursorLocation(g, 1);
                                C.isEmpty(p) && C.remove(p)
                            }
                    }

                    function x() {
                        var a;
                        (a = n(D.getStart())) && !C.isEmpty(a) && ka(a, function(a) {
                            1 != a.nodeType || "_mce_caret" === a.id || C.isEmpty(a) || C.setAttrib(a, "data-mce-bogus",
                                null)
                        }, "childNodes")
                    }
                    var w = d.settings.caret_debug;
                    d._hasCaretEvents || (ia = function() {
                        var a = [],
                            b;
                        if (g(n(D.getStart()), a))
                            for (b = a.length; b--;) C.setAttrib(a[b], "data-mce-bogus", "1")
                    }, fa = function(a) {
                        a = a.keyCode;
                        q();
                        8 == a && D.isCollapsed() && "\ufeff" == D.getStart().innerHTML && q(n(D.getStart()));
                        37 != a && 39 != a || q(n(D.getStart()));
                        x()
                    }, d.on("SetContent", function(a) {
                        a.selection && x()
                    }), d._hasCaretEvents = !0);
                    "apply" == a ? r() : u()
                }

                function K(a) {
                    var b = a.startContainer,
                        c = a.startOffset,
                        d, e;
                    if (a.startContainer == a.endContainer &&
                        (e = a.startContainer.childNodes[a.startOffset]) && /^(IMG)$/.test(e.nodeName)) return;
                    3 == b.nodeType && c >= b.nodeValue.length && (c = aa(b), b = b.parentNode, d = !0);
                    if (1 == b.nodeType)
                        for (e = b.childNodes, b = e[Math.min(c, e.length - 1)], b = new k(b, C.getParent(b, C.isBlock)), (c > e.length - 1 || d) && b.next(), c = b.current(); c; c = b.next())
                            if (3 == c.nodeType && !z(c)) {
                                d = C.create("a", {
                                    "data-mce-bogus": "all"
                                }, "\ufeff");
                                c.parentNode.insertBefore(d, c);
                                a.setStart(c, 0);
                                D.setRng(a);
                                C.remove(d);
                                break
                            }
                }
                var M = {},
                    C = d.dom,
                    D = d.selection,
                    P = new g(C),
                    da =
                    d.schema.isValidChild,
                    Q = C.isBlock,
                    Z = d.settings.forced_root_block,
                    aa = C.nodeIndex,
                    X = /^(src|href|style)$/,
                    V, ca, ea = C.getContentEditable,
                    fa, ia, ba = f.isBookmarkNode,
                    J = a.each,
                    S = a.grep,
                    ka = a.walk,
                    ja = a.extend;
                ja(this, {
                    get: h,
                    register: n,
                    unregister: function(a) {
                        a && M[a] && delete M[a];
                        return M
                    },
                    apply: m,
                    remove: t,
                    toggle: function(a, b, c) {
                        var d = h(a);
                        !B(a, b, c) || "toggle" in d[0] && !d[0].toggle ? m(a, b, c) : t(a, b, c)
                    },
                    match: B,
                    matchAll: function(a, b) {
                        var c, d = [],
                            e = {};
                        c = D.getStart();
                        C.getParent(c, function(c) {
                            var f, m;
                            for (f = 0; f < a.length; f++) m =
                                a[f], !e[m] && y(c, m, b) && (e[m] = !0, d.push(m))
                        }, C.getRoot());
                        return d
                    },
                    matchNode: y,
                    canApply: function(a) {
                        a = h(a);
                        var b, c, d, e;
                        if (a)
                            for (b = D.getStart(), b = v(b), d = a.length - 1; 0 <= d; d--) {
                                e = a[d].selector;
                                if (!e || a[d].defaultBlock) return !0;
                                for (c = b.length - 1; 0 <= c; c--)
                                    if (C.is(b[c], e)) return !0
                            }
                        return !1
                    },
                    formatChanged: function(b, c, e) {
                        var f;
                        V || (V = {}, f = {}, d.on("NodeChange", function(b) {
                            var c = v(b.element),
                                d = {},
                                c = a.grep(c, function(a) {
                                    return 1 == a.nodeType && !a.getAttribute("data-mce-bogus")
                                });
                            J(V, function(a, b) {
                                J(c, function(e) {
                                    if (y(e,
                                            b, {}, a.similar)) return f[b] || (J(a, function(a) {
                                        a(!0, {
                                            node: e,
                                            format: b,
                                            parents: c
                                        })
                                    }), f[b] = a), d[b] = a, !1;
                                    if (p(e, b)) return !1
                                })
                            });
                            J(f, function(a, e) {
                                d[e] || (delete f[e], J(a, function(a) {
                                    a(!1, {
                                        node: b.element,
                                        format: e,
                                        parents: c
                                    })
                                }))
                            })
                        }));
                        J(b.split(","), function(a) {
                            V[a] || (V[a] = [], V[a].similar = e);
                            V[a].push(c)
                        });
                        return this
                    },
                    getCssText: function(a) {
                        return c.getCssText(d, a)
                    }
                });
                (function() {
                    n({
                        valigntop: [{
                            selector: "td,th",
                            styles: {
                                verticalAlign: "top"
                            }
                        }],
                        valignmiddle: [{
                            selector: "td,th",
                            styles: {
                                verticalAlign: "middle"
                            }
                        }],
                        valignbottom: [{
                            selector: "td,th",
                            styles: {
                                verticalAlign: "bottom"
                            }
                        }],
                        alignleft: [{
                            selector: "figure.image",
                            collapsed: !1,
                            classes: "align-left",
                            ceFalseOverride: !0
                        }, {
                            selector: "figure,p,h1,h2,h3,h4,h5,h6,td,th,tr,div,ul,ol,li",
                            styles: {
                                textAlign: "left"
                            },
                            inherit: !1,
                            defaultBlock: "div"
                        }, {
                            selector: "img,table",
                            collapsed: !1,
                            styles: {
                                "float": "left"
                            }
                        }],
                        aligncenter: [{
                            selector: "figure,p,h1,h2,h3,h4,h5,h6,td,th,tr,div,ul,ol,li",
                            styles: {
                                textAlign: "center"
                            },
                            inherit: !1,
                            defaultBlock: "div"
                        }, {
                            selector: "figure.image",
                            collapsed: !1,
                            classes: "align-center",
                            ceFalseOverride: !0
                        }, {
                            selector: "img",
                            collapsed: !1,
                            styles: {
                                display: "block",
                                marginLeft: "auto",
                                marginRight: "auto"
                            }
                        }, {
                            selector: "table",
                            collapsed: !1,
                            styles: {
                                marginLeft: "auto",
                                marginRight: "auto"
                            }
                        }],
                        alignright: [{
                            selector: "figure.image",
                            collapsed: !1,
                            classes: "align-right",
                            ceFalseOverride: !0
                        }, {
                            selector: "figure,p,h1,h2,h3,h4,h5,h6,td,th,tr,div,ul,ol,li",
                            styles: {
                                textAlign: "right"
                            },
                            inherit: !1,
                            defaultBlock: "div"
                        }, {
                            selector: "img,table",
                            collapsed: !1,
                            styles: {
                                "float": "right"
                            }
                        }],
                        alignjustify: [{
                            selector: "figure,p,h1,h2,h3,h4,h5,h6,td,th,tr,div,ul,ol,li",
                            styles: {
                                textAlign: "justify"
                            },
                            inherit: !1,
                            defaultBlock: "div"
                        }],
                        bold: [{
                            inline: "strong",
                            remove: "all"
                        }, {
                            inline: "span",
                            styles: {
                                fontWeight: "bold"
                            }
                        }, {
                            inline: "b",
                            remove: "all"
                        }],
                        italic: [{
                            inline: "em",
                            remove: "all"
                        }, {
                            inline: "span",
                            styles: {
                                fontStyle: "italic"
                            }
                        }, {
                            inline: "i",
                            remove: "all"
                        }],
                        underline: [{
                            inline: "span",
                            styles: {
                                textDecoration: "underline"
                            },
                            exact: !0
                        }, {
                            inline: "u",
                            remove: "all"
                        }],
                        strikethrough: [{
                            inline: "span",
                            styles: {
                                textDecoration: "line-through"
                            },
                            exact: !0
                        }, {
                            inline: "strike",
                            remove: "all"
                        }],
                        forecolor: {
                            inline: "span",
                            styles: {
                                color: "%value"
                            },
                            links: !0,
                            remove_similar: !0
                        },
                        hilitecolor: {
                            inline: "span",
                            styles: {
                                backgroundColor: "%value"
                            },
                            links: !0,
                            remove_similar: !0
                        },
                        fontname: {
                            inline: "span",
                            styles: {
                                fontFamily: "%value"
                            }
                        },
                        fontsize: {
                            inline: "span",
                            styles: {
                                fontSize: "%value"
                            }
                        },
                        fontsize_class: {
                            inline: "span",
                            attributes: {
                                "class": "%value"
                            }
                        },
                        blockquote: {
                            block: "blockquote",
                            wrapper: 1,
                            remove: "all"
                        },
                        subscript: {
                            inline: "sub"
                        },
                        superscript: {
                            inline: "sup"
                        },
                        code: {
                            inline: "code"
                        },
                        link: {
                            inline: "a",
                            selector: "a",
                            remove: "all",
                            split: !0,
                            deep: !0,
                            onmatch: function() {
                                return !0
                            },
                            onformat: function(a, b, c) {
                                J(c, function(b, c) {
                                    C.setAttrib(a, c, b)
                                })
                            }
                        },
                        removeformat: [{
                            selector: "b,strong,em,i,font,u,strike,sub,sup,dfn,code,samp,kbd,var,cite,mark,q,del,ins",
                            remove: "all",
                            split: !0,
                            expand: !1,
                            block_expand: !0,
                            deep: !0
                        }, {
                            selector: "span",
                            attributes: ["style", "class"],
                            remove: "empty",
                            split: !0,
                            expand: !1,
                            deep: !0
                        }, {
                            selector: "*",
                            attributes: ["style", "class"],
                            split: !1,
                            expand: !1,
                            deep: !0
                        }]
                    });
                    J("p h1 h2 h3 h4 h5 h6 div address pre div dt dd samp".split(/\s/), function(a) {
                        n(a, {
                            block: a,
                            remove: "all"
                        })
                    });
                    n(d.settings.formats)
                })();
                (function() {
                    d.addShortcut("meta+b", "bold_desc", "Bold");
                    d.addShortcut("meta+i", "italic_desc", "Italic");
                    d.addShortcut("meta+u", "underline_desc", "Underline");
                    for (var a = 1; 6 >= a; a++) d.addShortcut("access+" + a, "", ["FormatBlock", !1, "h" + a]);
                    d.addShortcut("access+7", "", ["FormatBlock", !1, "p"]);
                    d.addShortcut("access+8", "", ["FormatBlock", !1, "div"]);
                    d.addShortcut("access+9", "", ["FormatBlock", !1, "address"])
                })();
                d.on("BeforeGetContent", function(a) {
                    ia && "raw" != a.format && ia()
                });
                d.on("mouseup keydown", function(a) {
                    fa &&
                        fa(a)
                })
            }
        });
    z("tinymce/UndoManager", ["tinymce/util/VK", "tinymce/Env"], function(k, g) {
        return function(f) {
            function e(b) {
                a.typing = !1;
                a.add({}, b)
            }
            var a = this,
                c = 0,
                b = [],
                d, l, k = 0;
            f.on("init", function() {
                a.add()
            });
            f.on("BeforeExecCommand", function(b) {
                b = b.command;
                "Undo" != b && "Redo" != b && "mceRepaint" != b && a.beforeChange()
            });
            f.on("ExecCommand", function(a) {
                var b = a.command;
                "Undo" != b && "Redo" != b && "mceRepaint" != b && e(a)
            });
            f.on("ObjectResizeStart Cut", function() {
                a.beforeChange()
            });
            f.on("SaveContent ObjectResized blur", e);
            f.on("DragEnd", e);
            f.on("KeyUp", function(c) {
                var d = c.keyCode;
                if (!c.isDefaultPrevented()) {
                    if (33 <= d && 36 >= d || 37 <= d && 40 >= d || 45 == d || 13 == d || c.ctrlKey) e(), f.nodeChanged();
                    (46 == d || 8 == d || g.mac && (91 == d || 93 == d)) && f.nodeChanged();
                    l && a.typing && (f.isDirty() || (f.setDirty(b[0] && f.serializer.getTrimmedContent() != b[0].content), f.isDirty() && f.fire("change", {
                        level: b[0],
                        lastLevel: null
                    })), f.fire("TypingUndo"), l = !1, f.nodeChanged())
                }
            });
            f.on("KeyDown", function(b) {
                var c = b.keyCode;
                if (!b.isDefaultPrevented())
                    if (33 <= c && 36 >= c ||
                        37 <= c && 40 >= c || 45 == c) a.typing && e(b);
                    else {
                        var d = b.ctrlKey && !b.altKey || b.metaKey;
                        !(16 > c || 20 < c) || 224 == c || 91 == c || a.typing || d || (a.beforeChange(), a.typing = !0, a.add({}, b), l = !0)
                    }
            });
            f.on("MouseDown", function(b) {
                a.typing && e(b)
            });
            f.addShortcut("meta+z", "", "Undo");
            f.addShortcut("meta+y,meta+shift+z", "", "Redo");
            f.on("AddUndo Undo Redo ClearUndos", function(a) {
                a.isDefaultPrevented() || f.nodeChanged()
            });
            return a = {
                data: b,
                typing: !1,
                beforeChange: function() {
                    k || (d = f.selection.getBookmark(2, !0))
                },
                add: function(a, e) {
                    var h;
                    h = f.settings;
                    var l;
                    a = a || {};
                    a.content = f.serializer.getTrimmedContent();
                    if (k || f.removed) return null;
                    l = b[c];
                    if (f.fire("BeforeAddUndo", {
                            level: a,
                            lastLevel: l,
                            originalEvent: e
                        }).isDefaultPrevented() || l && l.content == a.content) return null;
                    b[c] && (b[c].beforeBookmark = d);
                    if (h.custom_undo_redo_levels && b.length > h.custom_undo_redo_levels) {
                        for (h = 0; h < b.length - 1; h++) b[h] = b[h + 1];
                        b.length--;
                        c = b.length
                    }
                    a.bookmark = f.selection.getBookmark(2, !0);
                    c < b.length - 1 && (b.length = c + 1);
                    b.push(a);
                    c = b.length - 1;
                    e = {
                        level: a,
                        lastLevel: l,
                        originalEvent: e
                    };
                    f.fire("AddUndo", e);
                    0 < c && (f.setDirty(!0), f.fire("change", e));
                    return a
                },
                undo: function() {
                    var d;
                    a.typing && (a.add(), a.typing = !1);
                    0 < c && (d = b[--c], f.setContent(d.content, {
                        format: "raw"
                    }), f.selection.moveToBookmark(d.beforeBookmark), f.setDirty(!0), f.fire("undo", {
                        level: d
                    }));
                    return d
                },
                redo: function() {
                    var a;
                    c < b.length - 1 && (a = b[++c], f.setContent(a.content, {
                        format: "raw"
                    }), f.selection.moveToBookmark(a.bookmark), f.setDirty(!0), f.fire("redo", {
                        level: a
                    }));
                    return a
                },
                clear: function() {
                    b = [];
                    c = 0;
                    a.typing = !1;
                    a.data = b;
                    f.fire("ClearUndos")
                },
                hasUndo: function() {
                    return 0 < c || a.typing && b[0] && f.serializer.getTrimmedContent() != b[0].content
                },
                hasRedo: function() {
                    return c < b.length - 1 && !this.typing
                },
                transact: function(b) {
                    a.beforeChange();
                    try {
                        k++, b()
                    } finally {
                        k--
                    }
                    return a.add()
                },
                extra: function(d, e) {
                    var h;
                    a.transact(d) && (h = b[c].bookmark, d = b[c - 1], f.setContent(d.content, {
                        format: "raw"
                    }), f.selection.moveToBookmark(d.beforeBookmark), a.transact(e) && (b[c - 1].beforeBookmark = h))
                }
            }
        }
    });
    z("tinymce/EnterKey", ["tinymce/dom/TreeWalker", "tinymce/dom/RangeUtils", "tinymce/Env"],
        function(k, g, f) {
            var e = f.ie && 11 > f.ie;
            return function(a) {
                function c(c) {
                    function p(a) {
                        return a && b.isBlock(a) && !/^(TD|TH|CAPTION|FORM)$/.test(a.nodeName) && !/^(fixed|absolute)/i.test(a.style.position) && "true" !== b.getContentEditable(a)
                    }

                    function A(a) {
                        var c;
                        b.isBlock(a) && (c = d.getRng(), a.appendChild(b.create("span", null, "\u00a0")), d.select(a), a.lastChild.outerHTML = "", d.setRng(c))
                    }

                    function m(a) {
                        var c = [],
                            d;
                        if (a) {
                            for (; a = a.firstChild;) {
                                if (b.isBlock(a)) return;
                                1 != a.nodeType || h[a.nodeName.toLowerCase()] || c.push(a)
                            }
                            for (d =
                                c.length; d--;) a = c[d], !a.hasChildNodes() || a.firstChild == a.lastChild && "" === a.firstChild.nodeValue ? b.remove(a) : "A" == a.nodeName && " " === (a.innerText || a.textContent) && b.remove(a)
                        }
                    }

                    function t(a) {
                        var c, e, m, h = a,
                            l;
                        if (a) {
                            f.ie && 9 > f.ie && L && L.firstChild && L.firstChild == L.lastChild && "BR" == L.firstChild.tagName && b.remove(L.firstChild);
                            if (/^(LI|DT|DD)$/.test(a.nodeName)) {
                                a: {
                                    for (c = a.firstChild; c;) {
                                        if (1 == c.nodeType || 3 == c.nodeType && c.data && /[\r\n\s]/.test(c.data)) break a;
                                        c = c.nextSibling
                                    }
                                    c = void 0
                                }
                                c && /^(UL|OL|DL)$/.test(c.nodeName) &&
                                a.insertBefore(b.doc.createTextNode("\u00a0"), a.firstChild)
                            }
                            m = b.createRng();
                            f.ie || a.normalize();
                            if (a.hasChildNodes()) {
                                for (c = new k(a, a); e = c.current();) {
                                    if (3 == e.nodeType) {
                                        m.setStart(e, 0);
                                        m.setEnd(e, 0);
                                        break
                                    }
                                    if (n[e.nodeName.toLowerCase()]) {
                                        m.setStartBefore(e);
                                        m.setEndBefore(e);
                                        break
                                    }
                                    h = e;
                                    c.next()
                                }
                                e || (m.setStart(h, 0), m.setEnd(h, 0))
                            } else if ("BR" == a.nodeName)
                                if (a.nextSibling && b.isBlock(a.nextSibling)) {
                                    if (!E || 9 > E) l = b.create("br"), a.parentNode.insertBefore(l, a);
                                    m.setStartBefore(a);
                                    m.setEndBefore(a)
                                } else m.setStartAfter(a),
                                    m.setEndAfter(a);
                            else m.setStart(a, 0), m.setEnd(a, 0);
                            d.setRng(m);
                            b.remove(l);
                            d.scrollIntoView(a)
                        }
                    }

                    function y(a) {
                        var c = l.forced_root_block;
                        c && c.toLowerCase() === a.tagName.toLowerCase() && b.setAttribs(a, l.forced_root_block_attrs)
                    }

                    function B(a) {
                        var c = I,
                            d, f, m = q.getTextInlineElements();
                        a || "TABLE" == P ? (a = b.create(a || Q), y(a)) : a = L.cloneNode(!1);
                        f = a;
                        if (!1 !== l.keep_styles) {
                            do m[c.nodeName] && "_mce_caret" != c.id && (d = c.cloneNode(!1), b.setAttrib(d, "id", ""), a.hasChildNodes() ? d.appendChild(a.firstChild) : f = d, a.appendChild(d));
                            while ((c = c.parentNode) && c != R)
                        }
                        e || (f.innerHTML = '\x3cbr data-mce-bogus\x3d"1"\x3e');
                        return a
                    }

                    function x(a) {
                        var b, c;
                        if (3 == I.nodeType && (a ? 0 < H : H < I.nodeValue.length)) return !1;
                        if (I.parentNode == L && Z && !a || a && 1 == I.nodeType && I == L.firstChild) return !0;
                        if ("TABLE" === I.nodeName || I.previousSibling && "TABLE" == I.previousSibling.nodeName) return Z && !a || !Z && a;
                        b = new k(I, L);
                        for (3 == I.nodeType && (a && 0 === H ? b.prev() : a || H != I.nodeValue.length || b.next()); c = b.current();) {
                            if (1 === c.nodeType) {
                                if (!c.getAttribute("data-mce-bogus") && (c =
                                        c.nodeName.toLowerCase(), h[c] && "br" !== c)) return !1
                            } else if (3 === c.nodeType && !/^[ \t\r\n]*$/.test(c.nodeValue)) return !1;
                            a ? b.prev() : b.next()
                        }
                        return !0
                    }

                    function r(c, d) {
                        var e, f, m, h, l = Q || "P";
                        f = b.getParent(c, b.isBlock);
                        if (!f || !p(f)) {
                            f = f || R;
                            e = f == a.getBody() || f && /^(TD|TH|CAPTION)$/.test(f.nodeName) ? f.nodeName.toLowerCase() : f.parentNode.nodeName.toLowerCase();
                            if (!f.hasChildNodes()) return e = b.create(l), y(e), f.appendChild(e), G.setStart(e, 0), G.setEnd(e, 0), e;
                            for (h = c; h.parentNode != f;) h = h.parentNode;
                            for (; h && !b.isBlock(h);) m =
                                h, h = h.previousSibling;
                            if (m && q.isValidChild(e, l.toLowerCase())) {
                                e = b.create(l);
                                y(e);
                                m.parentNode.insertBefore(e, m);
                                for (h = m; h && !b.isBlock(h);) f = h.nextSibling, e.appendChild(h), h = f;
                                G.setStart(c, d);
                                G.setEnd(c, d)
                            }
                        }
                        return c
                    }

                    function u() {
                        function c(a) {
                            for (var b = D[a ? "firstChild" : "lastChild"]; b && 1 != b.nodeType;) b = b[a ? "nextSibling" : "previousSibling"];
                            return b === L
                        }

                        function d() {
                            var a = D.parentNode;
                            return /^(LI|DT|DD)$/.test(a.nodeName) ? a : D
                        }
                        if (D != a.getBody()) {
                            var e = D.parentNode.nodeName;
                            /^(OL|UL|LI)$/.test(e) && (Q =
                                "LI");
                            M = Q ? B(Q) : b.create("BR");
                            c(!0) && c() ? "LI" == e ? b.insertAfter(M, d()) : b.replace(M, D) : c(!0) ? "LI" == e ? (b.insertAfter(M, d()), M.appendChild(b.doc.createTextNode(" ")), M.appendChild(D)) : D.parentNode.insertBefore(M, D) : c() ? (b.insertAfter(M, d()), A(M)) : (D = d(), N = G.cloneRange(), N.setStartAfter(L), N.setEndAfter(D), C = N.extractContents(), "LI" == Q && "LI" == C.firstChild.nodeName ? (M = C.firstChild, b.insertAfter(C, D)) : (b.insertAfter(C, D), b.insertAfter(M, D)));
                            b.remove(L);
                            t(M);
                            v.add()
                        }
                    }

                    function F() {
                        a.execCommand("InsertLineBreak", !1, c)
                    }

                    function O(a) {
                        do 3 === a.nodeType && (a.nodeValue = a.nodeValue.replace(/^[\r\n]+/, "")), a = a.firstChild; while (a)
                    }

                    function z(a) {
                        var c;
                        e || (a.normalize(), (c = a.lastChild) && !/^(left|right)$/gi.test(b.getStyle(c, "float", !0)) || b.add(a, "br"))
                    }

                    function U() {
                        M = /^(H[1-6]|PRE|FIGURE)$/.test(P) && "HGROUP" != da ? B(Q) : B();
                        l.end_container_on_empty_block && p(D) && b.isEmpty(L) ? M = b.split(D, L) : b.insertAfter(M, L);
                        t(M)
                    }
                    var G, N, R, I, H, L, E, K, M, C, D, P, da, Q, Z;
                    G = d.getRng(!0);
                    if (!c.isDefaultPrevented())
                        if (G.collapsed) {
                            if ((new g(b)).normalize(G),
                                I = G.startContainer, H = G.startOffset, Q = (Q = (l.force_p_newlines ? "p" : "") || l.forced_root_block) ? Q.toUpperCase() : "", E = b.doc.documentMode, K = c.shiftKey, 1 == I.nodeType && I.hasChildNodes() && (Z = H > I.childNodes.length - 1, I = I.childNodes[Math.min(H, I.childNodes.length - 1)] || I, H = Z && 3 == I.nodeType ? I.nodeValue.length : 0), R = function(a) {
                                    for (var c = b.getRoot(), d; a !== c && "false" !== b.getContentEditable(a);) "true" === b.getContentEditable(a) && (d = a), a = a.parentNode;
                                    return a !== c ? d : c
                                }(I))
                                if (v.beforeChange(), b.isBlock(R) || R == b.getRoot()) {
                                    if (Q &&
                                        !K || !Q && K) I = r(I, H);
                                    D = (L = b.getParent(I, b.isBlock)) ? b.getParent(L.parentNode, b.isBlock) : null;
                                    P = L ? L.nodeName.toUpperCase() : "";
                                    da = D ? D.nodeName.toUpperCase() : "";
                                    "LI" != da || c.ctrlKey || (L = D, P = da);
                                    if (/^(LI|DT|DD)$/.test(P)) {
                                        if (!Q && K) {
                                            F();
                                            return
                                        }
                                        if (b.isEmpty(L)) {
                                            u();
                                            return
                                        }
                                    }
                                    if ("PRE" == P && !1 !== l.br_in_pre) {
                                        if (!K) {
                                            F();
                                            return
                                        }
                                    } else if (!Q && !K && "LI" != P || Q && K) {
                                        F();
                                        return
                                    }
                                    Q && L === a.getBody() || (Q = Q || "P", x() ? U() : x(!0) ? (M = L.parentNode.insertBefore(B(), L), A(M), t(L)) : (N = G.cloneRange(), N.setEndAfter(L), C = N.extractContents(),
                                        O(C), M = C.firstChild, b.insertAfter(C, L), m(M), z(L), b.isEmpty(L) && (L.innerHTML = e ? "" : '\x3cbr data-mce-bogus\x3d"1"\x3e'), M.normalize(), b.isEmpty(M) ? (b.remove(M), U()) : t(M)), b.setAttrib(M, "id", ""), a.fire("NewBlock", {
                                        newBlock: M
                                    }), v.add())
                                } else Q && !K || F()
                        } else a.execCommand("Delete")
                }
                var b = a.dom,
                    d = a.selection,
                    l = a.settings,
                    v = a.undoManager,
                    q = a.schema,
                    h = q.getNonEmptyElements(),
                    n = q.getMoveCaretBeforeOnEnterElements();
                a.on("keydown", function(a) {
                    13 == a.keyCode && !1 !== c(a) && a.preventDefault()
                })
            }
        });
    z("tinymce/ForceBlocks", [], function() {
        return function(k) {
            function g() {
                var d = a.getStart(),
                    l = k.getBody(),
                    g, q, h, n, p, w, A, m, t, y, B;
                B = f.forced_root_block;
                if (d && 1 === d.nodeType && B) {
                    for (; d && d != l;) {
                        if (b[d.nodeName]) return;
                        d = d.parentNode
                    }
                    g = a.getRng();
                    if (g.setStart) {
                        q = g.startContainer;
                        h = g.startOffset;
                        n = g.endContainer;
                        p = g.endOffset;
                        try {
                            t = k.getDoc().activeElement === l
                        } catch (x) {}
                    } else g.item && (d = g.item(0), g = k.getDoc().body.createTextRange(), g.moveToElementText(d)), t = g.parentElement().ownerDocument === k.getDoc(), d = g.duplicate(), d.collapse(!0),
                        h = -1 * d.move("character", -16777215), d.collapsed || (d = g.duplicate(), d.collapse(!1), p = -1 * d.move("character", -16777215) - h);
                    d = l.firstChild;
                    for (y = l.nodeName.toLowerCase(); d;)(3 === d.nodeType || 1 == d.nodeType && !b[d.nodeName]) && c.isValidChild(y, B.toLowerCase()) ? 3 === d.nodeType && 0 === d.nodeValue.length ? (A = d, d = d.nextSibling, e.remove(A)) : (w || (w = e.create(B, k.settings.forced_root_block_attrs), d.parentNode.insertBefore(w, d), m = !0), A = d, d = d.nextSibling, w.appendChild(A)) : (w = null, d = d.nextSibling);
                    if (m && t) {
                        if (g.setStart) g.setStart(q,
                            h), g.setEnd(n, p), a.setRng(g);
                        else try {
                            g = k.getDoc().body.createTextRange(), g.moveToElementText(l), g.collapse(!0), g.moveStart("character", h), 0 < p && g.moveEnd("character", p), g.select()
                        } catch (x) {}
                        k.nodeChanged()
                    }
                }
            }
            var f = k.settings,
                e = k.dom,
                a = k.selection,
                c = k.schema,
                b = c.getBlockElements();
            if (f.forced_root_block) k.on("NodeChange", g)
        }
    });
    z("tinymce/caret/CaretUtils", "tinymce/util/Fun tinymce/dom/TreeWalker tinymce/dom/NodeType tinymce/caret/CaretPosition tinymce/caret/CaretContainer tinymce/caret/CaretCandidate".split(" "),
        function(k, g, f, e, a, c) {
            function b(a, b) {
                for (a = a.parentNode; a && a != b; a = a.parentNode)
                    if (h(a)) return a;
                return b
            }

            function d(a, b) {
                for (; a && a != b;) {
                    if (p(a)) return a;
                    a = a.parentNode
                }
                return null
            }

            function l(a, b) {
                var c = b.ownerDocument.createRange();
                a ? (c.setStartBefore(b), c.setEndBefore(b)) : (c.setStartAfter(b), c.setEndAfter(b));
                return c
            }

            function v(a, b, c) {
                var e;
                for (e = a ? "previousSibling" : "nextSibling"; c && c != b;) {
                    a = c[e];
                    w(a) && (a = a[e]);
                    if (n(a)) {
                        if (d(a, b) == d(c, b)) return a;
                        break
                    }
                    if (t(a)) break;
                    c = c.parentNode
                }
                return null
            }

            function q(a, b) {
                var c;
                b ? (c = b.container(), b = b.offset(), a = m(c) ? c.childNodes[b + a] : null) : a = null;
                return n(a)
            }
            var h = f.isContentEditableTrue,
                n = f.isContentEditableFalse,
                p = f.matchStyleValues("display", "block table table-cell table-caption"),
                w = a.isCaretContainer,
                A = k.curry,
                m = f.isElement,
                t = c.isCaretCandidate,
                y = A(l, !0),
                B = A(l, !1);
            return {
                isForwards: function(a) {
                    return 0 < a
                },
                isBackwards: function(a) {
                    return 0 > a
                },
                findNode: function(a, b, c, d, e) {
                    d = new g(a, d);
                    if (0 > b) {
                        if (n(a) && (a = d.prev(!0), c(a))) return a;
                        for (; a = d.prev(e);)
                            if (c(a)) return a
                    }
                    if (0 <
                        b) {
                        if (n(a) && (a = d.next(!0), c(a))) return a;
                        for (; a = d.next(e);)
                            if (c(a)) return a
                    }
                    return null
                },
                getEditingHost: b,
                getParentBlock: d,
                isInSameBlock: function(a, b, c) {
                    return d(a.container(), c) == d(b.container(), c)
                },
                isInSameEditingHost: function(a, c, d) {
                    return b(a.container(), d) == b(c.container(), d)
                },
                isBeforeContentEditableFalse: A(q, 0),
                isAfterContentEditableFalse: A(q, -1),
                normalizeRange: function(b, c, d) {
                    var e, h, l, g = A(v, !0, c),
                        p = A(v, !1, c);
                    e = d.startContainer;
                    h = d.startOffset;
                    if (a.isCaretContainerBlock(e)) {
                        m(e) || (e = e.parentNode);
                        l = e.getAttribute("data-mce-caret");
                        if ("before" == l && (c = e.nextSibling, n(c))) return y(c);
                        if ("after" == l && (c = e.previousSibling, n(c))) return B(c)
                    }
                    if (!d.collapsed) return d;
                    if (f.isText(e)) {
                        if (w(e)) {
                            if (1 === b) {
                                if (c = p(e)) return y(c);
                                if (c = g(e)) return B(c)
                            }
                            if (-1 === b) {
                                if (c = g(e)) return B(c);
                                if (c = p(e)) return y(c)
                            }
                            return d
                        }
                        if (a.endsWithCaretContainer(e) && h >= e.data.length - 1) return 1 === b && (c = p(e)) ? y(c) : d;
                        if (a.startsWithCaretContainer(e) && 1 >= h) return -1 === b && (c = g(e)) ? B(c) : d;
                        if (h === e.data.length) return (c = p(e)) ? y(c) : d;
                        if (0 === h && (c = g(e))) return B(c)
                    }
                    return d
                }
            }
        });
    z("tinymce/caret/CaretWalker", "tinymce/dom/NodeType tinymce/caret/CaretCandidate tinymce/caret/CaretPosition tinymce/caret/CaretUtils tinymce/util/Arr tinymce/util/Fun".split(" "), function(k, g, f, e, a, c) {
        function b(a, b) {
            for (var c = []; a && a != b;) c.push(a), a = a.parentNode;
            return c
        }

        function d(a, b) {
            return a.hasChildNodes() && b < a.childNodes.length ? a.childNodes[b] : null
        }

        function l(a, b) {
            if (w(a)) {
                if (m(b.previousSibling) && !h(b.previousSibling)) return f.before(b);
                if (h(b)) return f(b,
                    0)
            }
            if (A(a)) {
                if (m(b.nextSibling) && !h(b.nextSibling)) return f.after(b);
                if (h(b)) return f(b, b.data.length)
            }
            return A(a) ? p(b) ? f.before(b) : f.after(b) : f.before(b)
        }

        function v(g, p, r) {
            var u, x, B;
            if (!n(r) || !p) return null;
            B = p;
            u = B.container();
            x = B.offset();
            if (h(u)) {
                if (A(g) && 0 < x) return f(u, --x);
                if (w(g) && x < u.length) return f(u, ++x);
                p = u
            } else {
                if (A(g) && 0 < x && (p = d(u, x - 1), m(p))) return !t(p) && (g = e.findNode(p, g, y, p)) ? h(g) ? f(g, g.data.length) : f.after(g) : h(p) ? f(p, p.data.length) : f.before(p);
                if (w(g) && x < u.childNodes.length && (p = d(u,
                        x), m(p))) return u = p, u = k.isBr(u) ? (B = v(1, f.after(u), r)) ? !e.isInSameBlock(f.before(u), f.before(B), r) : !1 : !1, u ? v(g, f.after(p), r) : !t(p) && (g = e.findNode(p, g, y, p)) ? h(g) ? f(g, 0) : f.before(g) : h(p) ? f(p, 0) : f.after(p);
                p = B.getNode()
            }
            if (w(g) && B.isAtEnd() || A(g) && B.isAtStart())
                if (p = e.findNode(p, g, c.constant(!0), r, !0), y(p)) return l(g, p);
            p = e.findNode(p, g, y, r);
            return !(r = a.last(a.filter(b(u, r), q))) || p && r.contains(p) ? p ? l(g, p) : null : B = w(g) ? f.after(r) : f.before(r)
        }
        var q = k.isContentEditableFalse,
            h = k.isText,
            n = k.isElement,
            p = k.isBr,
            w = e.isForwards,
            A = e.isBackwards,
            m = g.isCaretCandidate,
            t = g.isAtomic,
            y = g.isEditableCaretCandidate;
        return function(a) {
            return {
                next: function(b) {
                    return v(1, b, a)
                },
                prev: function(b) {
                    return v(-1, b, a)
                }
            }
        }
    });
    z("tinymce/InsertList", ["tinymce/util/Tools", "tinymce/caret/CaretWalker", "tinymce/caret/CaretPosition"], function(k, g, f) {
        var e = function(a, b, c) {
                b = b.serialize(c);
                a = a.createFragment(b);
                b = a.firstChild;
                c = a.lastChild;
                b && "META" === b.nodeName && b.parentNode.removeChild(b);
                c && "mce_marker" === c.id && c.parentNode.removeChild(c);
                return a
            },
            a = function(a) {
                return k.grep(a.childNodes, function(a) {
                    return "LI" === a.nodeName
                })
            },
            c = function(a) {
                return 0 < a.length && !a[a.length - 1].firstChild ? a.slice(0, -1) : a
            },
            b = function(a, b) {
                return (a = a.getParent(b, a.isBlock)) && "LI" === a.nodeName ? a : null
            },
            d = function(a, b) {
                var c = b.cloneRange();
                b = b.cloneRange();
                c.setStartBefore(a);
                b.setEndAfter(a);
                return [c.cloneContents(), b.cloneContents()]
            },
            l = function(a, b) {
                a = f.before(a);
                return (b = (new g(b)).next(a)) ? b.toRange() : null
            },
            v = function(a, b) {
                a = f.after(a);
                return (b = (new g(b)).prev(a)) ?
                    b.toRange() : null
            },
            q = function(a, b, c, e) {
                e = d(a, e);
                var f = a.parentNode;
                f.insertBefore(e[0], a);
                k.each(b, function(b) {
                    f.insertBefore(b, a)
                });
                f.insertBefore(e[1], a);
                f.removeChild(a);
                return v(b[b.length - 1], c)
            },
            h = function(a, b, c) {
                var d = a.parentNode;
                k.each(b, function(b) {
                    d.insertBefore(b, a)
                });
                return l(a, c)
            },
            n = function(a, b, c, d) {
                d.insertAfter(b.reverse(), a);
                return v(b[0], c)
            };
        return {
            isListFragment: function(a) {
                var b = a.firstChild;
                a = a.lastChild;
                b && "meta" === b.name && (b = b.next);
                a && "mce_marker" === a.attr("id") && (a = a.prev);
                return b && b === a ? "ul" === b.name || "ol" === b.name : !1
            },
            insertAtCaret: function(d, l, k, m) {
                d = e(l, d, m);
                var p = b(l, k.startContainer);
                d = c(a(d.firstChild));
                m = l.getRoot();
                var y = function(a) {
                    var c = f.fromRangeStart(k),
                        d = new g(l.getRoot());
                    return (a = 1 === a ? d.prev(c) : d.next(c)) ? b(l, a.getNode()) !== p : !0
                };
                return y(1) ? h(p, d, m) : y(2) ? n(p, d, m, l) : q(p, d, m, k)
            },
            isParentBlockLi: function(a, c) {
                return !!b(a, c)
            },
            trimListItems: c,
            listItems: a
        }
    });
    z("tinymce/InsertContent", "tinymce/Env tinymce/util/Tools tinymce/html/Serializer tinymce/caret/CaretWalker tinymce/caret/CaretPosition tinymce/dom/ElementUtils tinymce/dom/NodeType tinymce/InsertList".split(" "),
        function(k, g, f, e, a, c, b, d) {
            var l = b.matchNodeNames("td th"),
                v = function(b, h, n) {
                    function p(a) {
                        function b(a) {
                            return d[a] && 3 == d[a].nodeType
                        }
                        var c, d;
                        c = U.getRng(!0);
                        d = c.startContainer;
                        c = c.startOffset;
                        3 == d.nodeType && (0 < c ? a = a.replace(/^&nbsp;/, " ") : b("previousSibling") || (a = a.replace(/^ /, "\x26nbsp;")), c < d.length ? a = a.replace(/&nbsp;(<br>|)$/, " ") : b("nextSibling") || (a = a.replace(/(&nbsp;| )(<br>|)$/, "\x26nbsp;")));
                        return a
                    }

                    function q() {
                        var a, b, c;
                        a = U.getRng(!0);
                        b = a.startContainer;
                        c = a.startOffset;
                        3 == b.nodeType &&
                            a.collapsed && ("\u00a0" === b.data[c] ? (b.deleteData(c, 1), /[\u00a0| ]$/.test(h) || (h += " ")) : "\u00a0" === b.data[c - 1] && (b.deleteData(c - 1, 1), /[\u00a0| ]$/.test(h) || (h = " " + h)))
                    }

                    function v(a) {
                        return a && !b.schema.getShortEndedElements()[a.nodeName]
                    }
                    var m, t, y, B, x, r, u, F, O, z = b.schema.getTextInlineElements(),
                        U = b.selection,
                        G = b.dom;
                    /^ | $/.test(h) && (h = p(h));
                    m = b.parser;
                    O = n.merge;
                    t = new f({
                        validate: b.settings.validate
                    }, b.schema);
                    x = {
                        content: h,
                        format: "html",
                        selection: !0
                    };
                    b.fire("BeforeSetContent", x);
                    h = x.content; - 1 == h.indexOf("{$caret}") &&
                        (h += "{$caret}");
                    h = h.replace(/\{\$caret\}/, '\x3cspan id\x3d"mce_marker" data-mce-type\x3d"bookmark"\x3e\x26#xFEFF;\x26#x200B;\x3c/span\x3e');
                    r = U.getRng();
                    y = r.startContainer || (r.parentElement ? r.parentElement() : null);
                    var N = b.getBody();
                    y === N && U.isCollapsed() && G.isBlock(N.firstChild) && v(N.firstChild) && G.isEmpty(N.firstChild) && (r = G.createRng(), r.setStart(N.firstChild, 0), r.setEnd(N.firstChild, 0), U.setRng(r));
                    U.isCollapsed() || (b.selection.setRng(b.selection.getRng()), b.getDoc().execCommand("Delete", !1, null),
                        q());
                    y = U.getNode();
                    N = {
                        context: y.nodeName.toLowerCase(),
                        data: n.data
                    };
                    B = m.parse(h, N);
                    if (d.isListFragment(B) && d.isParentBlockLi(G, y)) r = d.insertAtCaret(t, G, b.selection.getRng(), B), b.selection.setRng(r), b.fire("SetContent", x);
                    else {
                        (function(a) {
                            for (; a = a.walk();) 1 === a.type && a.attr("data-mce-fragment", "1")
                        })(B);
                        u = B.lastChild;
                        if ("mce_marker" == u.attr("id"))
                            for (n = u, u = u.prev; u; u = u.walk(!0))
                                if (3 == u.type || !G.isBlock(u.name)) {
                                    b.schema.isValidChild(u.parent.name, "span") && u.parent.insert(n, u, "br" === u.name);
                                    break
                                }
                        b._selectionOverrides.showBlockCaretContainer(y);
                        if (N.invalid) {
                            U.setContent('\x3cspan id\x3d"mce_marker" data-mce-type\x3d"bookmark"\x3e\x26#xFEFF;\x26#x200B;\x3c/span\x3e');
                            y = U.getNode();
                            n = b.getBody();
                            for (9 == y.nodeType ? y = u = n : u = y; u !== n;) y = u, u = u.parentNode;
                            h = y == n ? n.innerHTML : G.getOuterHTML(y);
                            h = t.serialize(m.parse(h.replace(/<span (id="mce_marker"|id=mce_marker).+?<\/span>/i, function() {
                                return t.serialize(B)
                            })));
                            y == n ? G.setHTML(n, h) : G.setOuterHTML(y, h)
                        } else h = t.serialize(B), u = y.firstChild, F = y.lastChild, !u || u === F && "BR" === u.nodeName ? G.setHTML(y, h) : U.setContent(h);
                        (function() {
                            if (O) {
                                var a = b.getBody(),
                                    d = new c(G);
                                g.each(G.select("*[data-mce-fragment]"), function(b) {
                                    for (var c = b.parentNode; c && c != a; c = c.parentNode) z[b.nodeName.toLowerCase()] && d.compare(c, b) && G.remove(b, !0)
                                })
                            }
                        })();
                        (function(c) {
                            function d(c) {
                                c = a.fromRangeStart(c);
                                if (c = (new e(b.getBody())).next(c)) return c.toRange()
                            }
                            var f, m;
                            c && (U.scrollIntoView(c), (f = function(a) {
                                for (var c = b.getBody(); a && a !== c; a = a.parentNode)
                                    if ("false" === b.dom.getContentEditable(a)) return a;
                                return null
                            }(c)) ? (G.remove(c), U.select(f)) : (r =
                                G.createRng(), (u = c.previousSibling) && 3 == u.nodeType ? (r.setStart(u, u.nodeValue.length), k.ie || (F = c.nextSibling) && 3 == F.nodeType && (u.appendData(F.data), F.parentNode.removeChild(F))) : (r.setStartBefore(c), r.setEndBefore(c)), f = G.getParent(c, G.isBlock), G.remove(c), f && G.isEmpty(f) && (b.$(f).empty(), r.setStart(f, 0), r.setEnd(f, 0), l(f) || f.getAttribute("data-mce-fragment") || !(m = d(r)) ? G.add(f, G.create("br", {
                                    "data-mce-bogus": "1"
                                })) : (r = m, G.remove(f))), U.setRng(r)))
                        })(G.get("mce_marker"));
                        (function(a) {
                            g.each(a.getElementsByTagName("*"),
                                function(a) {
                                    a.removeAttribute("data-mce-fragment")
                                })
                        })(b.getBody());
                        b.fire("SetContent", x);
                        b.addVisual()
                    }
                };
            return {
                insertAtCaret: function(a, b) {
                    var c;
                    "string" !== typeof b ? (c = g.extend({
                        paste: b.paste,
                        data: {
                            paste: b.paste
                        }
                    }, b), b = b.content) : c = {};
                    v(a, b, c)
                }
            }
        });
    z("tinymce/EditorCommands", ["tinymce/Env", "tinymce/util/Tools", "tinymce/dom/RangeUtils", "tinymce/dom/TreeWalker", "tinymce/InsertContent"], function(k, g, f, e, a) {
        var c = g.each,
            b = g.extend,
            d = g.map,
            l = g.inArray,
            v = g.explode,
            q = k.ie && 11 > k.ie;
        return function(h) {
            function g(a) {
                var b;
                if (!h.quirks.isHidden()) {
                    a = a.toLowerCase();
                    if (b = B.state[a]) return b(a);
                    try {
                        return h.getDoc().queryCommandState(a)
                    } catch (O) {}
                    return !1
                }
            }

            function p(a, b) {
                b = b || "exec";
                c(a, function(a, d) {
                    c(d.toLowerCase().split(","), function(c) {
                        B[b][c] = a
                    })
                })
            }

            function w(a, b, c) {
                b === K && (b = !1);
                c === K && (c = null);
                return h.getDoc().execCommand(a, b, c)
            }

            function A(a, b) {
                y.toggle(a, b ? {
                    value: b
                } : K);
                h.nodeChanged()
            }
            var m, t, y, B = {
                    state: {},
                    exec: {},
                    value: {}
                },
                x = h.settings,
                r;
            h.on("PreInit", function() {
                m = h.dom;
                t = h.selection;
                x = h.settings;
                y = h.formatter
            });
            b(this, {
                execCommand: function(a, b, d, e) {
                    var f, m = 0;
                    /^(mceAddUndoLevel|mceEndUndoLevel|mceBeginUndoLevel|mceRepaint)$/.test(a) || e && e.skip_focus || h.focus();
                    e = h.fire("BeforeExecCommand", {
                        command: a,
                        ui: b,
                        value: d
                    });
                    if (e.isDefaultPrevented()) return !1;
                    f = a.toLowerCase();
                    if (e = B.exec[f]) return e(f, b, d), h.fire("ExecCommand", {
                        command: a,
                        ui: b,
                        value: d
                    }), !0;
                    c(h.plugins, function(c) {
                        if (c.execCommand && c.execCommand(a, b, d)) return h.fire("ExecCommand", {
                            command: a,
                            ui: b,
                            value: d
                        }), m = !0, !1
                    });
                    if (m) return m;
                    if (h.theme && h.theme.execCommand &&
                        h.theme.execCommand(a, b, d)) return h.fire("ExecCommand", {
                        command: a,
                        ui: b,
                        value: d
                    }), !0;
                    try {
                        m = h.getDoc().execCommand(a, b, d)
                    } catch (N) {}
                    return m ? (h.fire("ExecCommand", {
                        command: a,
                        ui: b,
                        value: d
                    }), !0) : !1
                },
                queryCommandState: g,
                queryCommandValue: function(a) {
                    var b;
                    if (!h.quirks.isHidden()) {
                        a = a.toLowerCase();
                        if (b = B.value[a]) return b(a);
                        try {
                            return h.getDoc().queryCommandValue(a)
                        } catch (O) {}
                    }
                },
                queryCommandSupported: function(a) {
                    a = a.toLowerCase();
                    if (B.exec[a]) return !0;
                    try {
                        return h.getDoc().queryCommandSupported(a)
                    } catch (F) {}
                    return !1
                },
                addCommands: p,
                addCommand: function(a, b, c) {
                    a = a.toLowerCase();
                    B.exec[a] = function(a, d, e, f) {
                        return b.call(c || h, d, e, f)
                    }
                },
                addQueryStateHandler: function(a, b, c) {
                    a = a.toLowerCase();
                    B.state[a] = function() {
                        return b.call(c || h)
                    }
                },
                addQueryValueHandler: function(a, b, c) {
                    a = a.toLowerCase();
                    B.value[a] = function() {
                        return b.call(c || h)
                    }
                },
                hasCustomCommand: function(a) {
                    a = a.toLowerCase();
                    return !!B.exec[a]
                }
            });
            p({
                "mceResetDesignMode,mceBeginUndoLevel": function() {},
                "mceEndUndoLevel,mceAddUndoLevel": function() {
                    h.undoManager.add()
                },
                "Cut,Copy,Paste": function(a) {
                    var b = h.getDoc(),
                        c;
                    try {
                        w(a)
                    } catch (W) {
                        c = !0
                    }
                    "paste" !== a || b.queryCommandEnabled(a) || (c = !0);
                    if (c || !b.queryCommandSupported(a)) a = h.translate("Your browser doesn't support direct access to the clipboard. Please use the Ctrl+X/C/V keyboard shortcuts instead."), k.mac && (a = a.replace(/Ctrl\+/g, "\u2318+")), h.notificationManager.open({
                        text: a,
                        type: "error"
                    })
                },
                unlink: function() {
                    if (t.isCollapsed()) {
                        var a = t.getNode();
                        "A" == a.tagName && h.dom.remove(a, !0)
                    } else y.remove("link")
                },
                "JustifyLeft,JustifyCenter,JustifyRight,JustifyFull,JustifyNone": function(a) {
                    var b =
                        a.substring(7);
                    "full" == b && (b = "justify");
                    c(["left", "center", "right", "justify"], function(a) {
                        b != a && y.remove("align" + a)
                    });
                    "none" != b && A("align" + b)
                },
                "InsertUnorderedList,InsertOrderedList": function(a) {
                    var b;
                    w(a);
                    if (a = m.getParent(t.getNode(), "ol,ul")) b = a.parentNode, /^(H[1-6]|P|ADDRESS|PRE)$/.test(b.nodeName) && (r = t.getBookmark(void 0), m.split(b, a), t.moveToBookmark(r))
                },
                "Bold,Italic,Underline,Strikethrough,Superscript,Subscript": function(a) {
                    A(a)
                },
                "ForeColor,HiliteColor,FontName": function(a, b, c) {
                    A(a, c)
                },
                FontSize: function(a,
                    b, c) {
                    var d;
                    1 <= c && 7 >= c && (d = v(x.font_size_style_values), c = (b = v(x.font_size_classes)) ? b[c - 1] || c : d[c - 1] || c);
                    A(a, c)
                },
                RemoveFormat: function(a) {
                    y.remove(a)
                },
                mceBlockQuote: function() {
                    A("blockquote")
                },
                FormatBlock: function(a, b, c) {
                    return A(c || "p")
                },
                mceCleanup: function() {
                    var a = t.getBookmark();
                    h.setContent(h.getContent({
                        cleanup: !0
                    }), {
                        cleanup: !0
                    });
                    t.moveToBookmark(a)
                },
                mceRemoveNode: function(a, b, c) {
                    a = c || t.getNode();
                    a != h.getBody() && (r = t.getBookmark(void 0), h.dom.remove(a, !0), t.moveToBookmark(r))
                },
                mceSelectNodeDepth: function(a,
                    b, c) {
                    var d = 0;
                    m.getParent(t.getNode(), function(a) {
                        if (1 == a.nodeType && d++ == c) return t.select(a), !1
                    }, h.getBody())
                },
                mceSelectNode: function(a, b, c) {
                    t.select(c)
                },
                mceInsertContent: function(b, c, d) {
                    a.insertAtCaret(h, d)
                },
                mceInsertRawHTML: function(a, b, c) {
                    t.setContent("tiny_mce_marker");
                    h.setContent(h.getContent().replace(/tiny_mce_marker/g, function() {
                        return c
                    }))
                },
                mceToggleFormat: function(a, b, c) {
                    A(c)
                },
                mceSetContent: function(a, b, c) {
                    h.setContent(c)
                },
                "Indent,Outdent": function(a) {
                    var b, d, e;
                    b = x.indentation;
                    d = /[a-z%]+$/i.exec(b);
                    b = parseInt(b, 10);
                    g("InsertUnorderedList") || g("InsertOrderedList") ? w(a) : (x.forced_root_block || m.getParent(t.getNode(), m.isBlock) || y.apply("div"), c(t.getSelectedBlocks(), function(c) {
                        if ("false" !== m.getContentEditable(c) && "LI" != c.nodeName) {
                            var f = h.getParam("indent_use_margin", !1) ? "margin" : "padding",
                                f = f + ("rtl" == m.getStyle(c, "direction", !0) ? "Right" : "Left");
                            "outdent" == a ? (e = Math.max(0, parseInt(c.style[f] || 0, 10) - b), m.setStyle(c, f, e ? e + d : "")) : (e = parseInt(c.style[f] || 0, 10) + b + d, m.setStyle(c, f, e))
                        }
                    }))
                },
                mceRepaint: function() {},
                InsertHorizontalRule: function() {
                    h.execCommand("mceInsertContent", !1, "\x3chr /\x3e")
                },
                mceToggleVisualAid: function() {
                    h.hasVisual = !h.hasVisual;
                    h.addVisual()
                },
                mceReplaceContent: function(a, b, c) {
                    h.execCommand("mceInsertContent", !1, c.replace(/\{\$selection\}/g, t.getContent({
                        format: "text"
                    })))
                },
                mceInsertLink: function(a, b, c) {
                    "string" == typeof c && (c = {
                        href: c
                    });
                    a = m.getParent(t.getNode(), "a");
                    c.href = c.href.replace(" ", "%20");
                    a && c.href || y.remove("link");
                    c.href && y.apply("link", c, a)
                },
                selectAll: function() {
                    var a = m.getRoot(),
                        b;
                    t.getRng().setStart ? (b = m.createRng(), b.setStart(a, 0), b.setEnd(a, a.childNodes.length), t.setRng(b)) : (b = t.getRng(), b.item || (b.moveToElementText(a), b.select()))
                },
                "delete": function() {
                    w("Delete");
                    var a = h.getBody();
                    m.isEmpty(a) && (h.setContent(""), a.firstChild && m.isBlock(a.firstChild) ? h.selection.setCursorLocation(a.firstChild, 0) : h.selection.setCursorLocation(a, 0))
                },
                mceNewDocument: function() {
                    h.setContent("")
                },
                InsertLineBreak: function(a, b, c) {
                    var d;
                    a = t.getRng(!0);
                    (new f(m)).normalize(a);
                    var g = a.startOffset,
                        l = a.startContainer;
                    1 == l.nodeType && l.hasChildNodes() && (b = g > l.childNodes.length - 1, l = l.childNodes[Math.min(g, l.childNodes.length - 1)] || l, g = b && 3 == l.nodeType ? l.nodeValue.length : 0);
                    var n = m.getParent(l, m.isBlock);
                    b = n ? n.nodeName.toUpperCase() : "";
                    var p = n ? m.getParent(n.parentNode, m.isBlock) : null,
                        r = p ? p.nodeName.toUpperCase() : "";
                    c = c && c.ctrlKey;
                    "LI" != r || c || (n = p, b = r);
                    if (l && 3 == l.nodeType && g >= l.nodeValue.length) {
                        if (c = !q) {
                            a: {
                                c = new e(l, n);
                                for (g = h.schema.getNonEmptyElements(); l = c.next();)
                                    if (g[l.nodeName.toLowerCase()] ||
                                        0 < l.length) {
                                        c = !0;
                                        break a
                                    }
                                c = void 0
                            }
                            c = !c
                        }
                        c && (c = m.create("br"), a.insertNode(c), a.setStartAfter(c), a.setEndAfter(c), d = !0)
                    }
                    c = m.create("br");
                    a.insertNode(c);
                    l = m.doc.documentMode;
                    q && "PRE" == b && (!l || 8 > l) && c.parentNode.insertBefore(m.doc.createTextNode("\r"), c);
                    b = m.create("span", {}, "\x26nbsp;");
                    c.parentNode.insertBefore(b, c);
                    t.scrollIntoView(b);
                    m.remove(b);
                    d ? (a.setStartBefore(c), a.setEndBefore(c)) : (a.setStartAfter(c), a.setEndAfter(c));
                    t.setRng(a);
                    h.undoManager.add();
                    return !0
                }
            });
            p({
                "JustifyLeft,JustifyCenter,JustifyRight,JustifyFull": function(a) {
                    var b =
                        "align" + a.substring(7);
                    a = t.isCollapsed() ? [m.getParent(t.getNode(), m.isBlock)] : t.getSelectedBlocks();
                    a = d(a, function(a) {
                        return !!y.matchNode(a, b)
                    });
                    return -1 !== l(a, !0)
                },
                "Bold,Italic,Underline,Strikethrough,Superscript,Subscript": function(a) {
                    return y.match(a)
                },
                mceBlockQuote: function() {
                    return y.match("blockquote")
                },
                Outdent: function() {
                    var a;
                    return x.inline_styles && ((a = m.getParent(t.getStart(), m.isBlock)) && 0 < parseInt(a.style.paddingLeft, 10) || (a = m.getParent(t.getEnd(), m.isBlock)) && 0 < parseInt(a.style.paddingLeft,
                        10)) ? !0 : g("InsertUnorderedList") || g("InsertOrderedList") || !x.inline_styles && !!m.getParent(t.getNode(), "BLOCKQUOTE")
                },
                "InsertUnorderedList,InsertOrderedList": function(a) {
                    var b = m.getParent(t.getNode(), "ul,ol");
                    return b && ("insertunorderedlist" === a && "UL" === b.tagName || "insertorderedlist" === a && "OL" === b.tagName)
                }
            }, "state");
            p({
                "FontSize,FontName": function(a) {
                    var b = 0,
                        c;
                    if (c = m.getParent(t.getNode(), "span")) b = "fontsize" == a ? c.style.fontSize : c.style.fontFamily.replace(/, /g, ",").replace(/[\'\"]/g, "").toLowerCase();
                    return b
                }
            }, "value");
            p({
                Undo: function() {
                    h.undoManager.undo()
                },
                Redo: function() {
                    h.undoManager.redo()
                }
            })
        }
    });
    z("tinymce/util/URI", ["tinymce/util/Tools"], function(k) {
        function g(b, c) {
            var d = this,
                k, q;
            b = e(b);
            c = d.settings = c || {};
            k = c.base_uri;
            if (/^([\w\-]+):([^\/]{2})/i.test(b) || /^\s*#/.test(b)) d.source = b;
            else {
                var h = 0 === b.indexOf("//");
                0 !== b.indexOf("/") || h || (b = (k ? k.protocol || "http" : "http") + "://mce_host" + b);
                /^[\w\-]*:?\/\//.test(b) || (q = c.base_uri ? c.base_uri.path : (new g(location.href)).directory, "" === c.base_uri.protocol ?
                    b = "//mce_host" + d.toAbsPath(q, b) : (b = /([^#?]*)([#?]?.*)/.exec(b), b = (k && k.protocol || "http") + "://mce_host" + d.toAbsPath(q, b[1]) + b[2]));
                b = b.replace(/@@/g, "(mce_at)");
                b = /^(?:(?![^:@]+:[^:@\/]*@)([^:\/?#.]+):)?(?:\/\/)?((?:(([^:@\/]*):?([^:@\/]*))?@)?([^:\/?#]*)(?::(\d*))?)(((\/(?:[^?#](?![^?#\/]*\.[^?#\/.]+(?:[?#]|$)))*\/?)?([^?#\/]*))(?:\?([^#]*))?(?:#(.*))?)/.exec(b);
                f(a, function(a, c) {
                    (c = b[c]) && (c = c.replace(/\(mce_at\)/g, "@@"));
                    d[a] = c
                });
                k && (d.protocol || (d.protocol = k.protocol), d.userInfo || (d.userInfo = k.userInfo),
                    d.port || "mce_host" !== d.host || (d.port = k.port), d.host && "mce_host" !== d.host || (d.host = k.host), d.source = "");
                h && (d.protocol = "")
            }
        }
        var f = k.each,
            e = k.trim,
            a = "source protocol authority userInfo user password host port relative path directory file query anchor".split(" "),
            c = {
                ftp: 21,
                http: 80,
                https: 443,
                mailto: 25
            };
        g.prototype = {
            setPath: function(a) {
                a = /^(.*?)\/?(\w+)?$/.exec(a);
                this.path = a[0];
                this.directory = a[1];
                this.file = a[2];
                this.source = "";
                this.getURI()
            },
            toRelative: function(a) {
                var b;
                if ("./" === a) return a;
                a = new g(a, {
                    base_uri: this
                });
                if ("mce_host" != a.host && this.host != a.host && a.host || this.port != a.port || this.protocol != a.protocol && "" !== a.protocol) return a.getURI();
                b = this.getURI();
                var c = a.getURI();
                if (b == c || "/" == b.charAt(b.length - 1) && b.substr(0, b.length - 1) == c) return b;
                b = this.toRelPath(this.path, a.path);
                a.query && (b += "?" + a.query);
                a.anchor && (b += "#" + a.anchor);
                return b
            },
            toAbsolute: function(a, c) {
                a = new g(a, {
                    base_uri: this
                });
                return a.getURI(c && this.isSameOrigin(a))
            },
            isSameOrigin: function(a) {
                if (this.host == a.host && this.protocol ==
                    a.protocol) {
                    if (this.port == a.port) return !0;
                    var b = c[this.protocol];
                    if (b && (this.port || b) == (a.port || b)) return !0
                }
                return !1
            },
            toRelPath: function(a, c) {
                var b, d = 0,
                    e = "",
                    f, g;
                a = a.substring(0, a.lastIndexOf("/"));
                a = a.split("/");
                b = c.split("/");
                if (a.length >= b.length)
                    for (f = 0, g = a.length; f < g; f++)
                        if (f >= b.length || a[f] != b[f]) {
                            d = f + 1;
                            break
                        }
                if (a.length < b.length)
                    for (f = 0, g = b.length; f < g; f++)
                        if (f >= a.length || a[f] != b[f]) {
                            d = f + 1;
                            break
                        }
                if (1 === d) return c;
                f = 0;
                for (g = a.length - (d - 1); f < g; f++) e += "../";
                f = d - 1;
                for (g = b.length; f < g; f++) e = f != d - 1 ?
                    e + ("/" + b[f]) : e + b[f];
                return e
            },
            toAbsPath: function(a, c) {
                var b, d = 0,
                    e = [],
                    h;
                h = /\/$/.test(c) ? "/" : "";
                a = a.split("/");
                c = c.split("/");
                f(a, function(a) {
                    a && e.push(a)
                });
                a = e;
                b = c.length - 1;
                for (e = []; 0 <= b; b--) 0 !== c[b].length && "." !== c[b] && (".." === c[b] ? d++ : 0 < d ? d-- : e.push(c[b]));
                b = a.length - d;
                a = 0 >= b ? e.reverse().join("/") : a.slice(0, b).join("/") + "/" + e.reverse().join("/");
                0 !== a.indexOf("/") && (a = "/" + a);
                h && a.lastIndexOf("/") !== a.length - 1 && (a += h);
                return a
            },
            getURI: function(a) {
                var b;
                if (!this.source || a) b = "", a || (b = this.protocol ?
                    b + (this.protocol + "://") : b + "//", this.userInfo && (b += this.userInfo + "@"), this.host && (b += this.host), this.port && (b += ":" + this.port)), this.path && (b += this.path), this.query && (b += "?" + this.query), this.anchor && (b += "#" + this.anchor), this.source = b;
                return this.source
            }
        };
        g.parseDataUri = function(a) {
            var b, c;
            a = decodeURIComponent(a).split(",");
            (c = /data:([^;]+)/.exec(a[0])) && (b = c[1]);
            return {
                type: b,
                data: a[1]
            }
        };
        g.getDocumentBaseUrl = function(a) {
            a = 0 !== a.protocol.indexOf("http") && "file:" !== a.protocol ? a.href : a.protocol + "//" + a.host +
                a.pathname;
            /^[^:]+:\/\/\/?[^\/]+\//.test(a) && (a = a.replace(/[\?#].*$/, "").replace(/[\/\\][^\/]+$/, ""), /[\/\\]$/.test(a) || (a += "/"));
            return a
        };
        return g
    });
    z("tinymce/util/Class", ["tinymce/util/Tools"], function(k) {
        function g() {}
        var f = k.each,
            e = k.extend,
            a, c;
        g.extend = a = function(b) {
            function d() {
                var a, b, d;
                if (!c && (this.init && this.init.apply(this, arguments), b = this.Mixins))
                    for (a = b.length; a--;) d = b[a], d.init && d.init.apply(this, arguments)
            }

            function g() {
                return this
            }

            function k(a, b) {
                return function() {
                    var c = this._super,
                        d;
                    this._super = q[a];
                    d = b.apply(this, arguments);
                    this._super = c;
                    return d
                }
            }
            var q = this.prototype,
                h, n, p;
            c = !0;
            h = new this;
            c = !1;
            b.Mixins && (f(b.Mixins, function(a) {
                for (var c in a) "init" !== c && (b[c] = a[c])
            }), q.Mixins && (b.Mixins = q.Mixins.concat(b.Mixins)));
            b.Methods && f(b.Methods.split(","), function(a) {
                b[a] = g
            });
            b.Properties && f(b.Properties.split(","), function(a) {
                var c = "_" + a;
                b[a] = function(a) {
                    return void 0 !== a ? (this[c] = a, this) : this[c]
                }
            });
            b.Statics && f(b.Statics, function(a, b) {
                d[b] = a
            });
            b.Defaults && q.Defaults && (b.Defaults =
                e({}, q.Defaults, b.Defaults));
            for (n in b) p = b[n], h[n] = "function" == typeof p && q[n] ? k(n, p) : p;
            d.prototype = h;
            d.constructor = d;
            d.extend = a;
            return d
        };
        return g
    });
    z("tinymce/util/EventDispatcher", ["tinymce/util/Tools"], function(k) {
        function g(e) {
            function a() {
                return !1
            }

            function c() {
                return !0
            }

            function b(b, c, d, e) {
                var m, g;
                !1 === c && (c = a);
                if (c)
                    for (c = {
                            func: c
                        }, e && k.extend(c, e), m = b.toLowerCase().split(" "), g = m.length; g--;) b = m[g], e = q[b], e || (e = q[b] = [], h(b, !0)), d ? e.unshift(c) : e.push(c);
                return f
            }

            function d(a, b) {
                var c, d, e, g, l;
                if (a)
                    for (g = a.toLowerCase().split(" "), c = g.length; c--;) {
                        a = g[c];
                        d = q[a];
                        if (!a) {
                            for (e in q) h(e, !1), delete q[e];
                            break
                        }
                        if (d) {
                            if (b)
                                for (l = d.length; l--;) d[l].func === b && (d = d.slice(0, l).concat(d.slice(l + 1)), q[a] = d);
                            else d.length = 0;
                            d.length || (h(a, !1), delete q[a])
                        }
                    } else {
                        for (a in q) h(a, !1);
                        q = {}
                    }
                return f
            }
            var f = this,
                g, q = {},
                h;
            e = e || {};
            g = e.scope || f;
            h = e.toggleEvent || a;
            f.fire = function(b, f) {
                var h, l, m, n;
                b = b.toLowerCase();
                f = f || {};
                f.type = b;
                f.target || (f.target = g);
                f.preventDefault || (f.preventDefault = function() {
                    f.isDefaultPrevented =
                        c
                }, f.stopPropagation = function() {
                    f.isPropagationStopped = c
                }, f.stopImmediatePropagation = function() {
                    f.isImmediatePropagationStopped = c
                }, f.isDefaultPrevented = a, f.isPropagationStopped = a, f.isImmediatePropagationStopped = a);
                e.beforeFire && e.beforeFire(f);
                if (h = q[b])
                    for (l = 0, m = h.length; l < m; l++) {
                        n = h[l];
                        n.once && d(b, n.func);
                        if (f.isImmediatePropagationStopped()) {
                            f.stopPropagation();
                            break
                        }
                        if (!1 === n.func.call(g, f)) {
                            f.preventDefault();
                            break
                        }
                    }
                return f
            };
            f.on = b;
            f.off = d;
            f.once = function(a, c, d) {
                return b(a, c, d, {
                    once: !0
                })
            };
            f.has =
                function(a) {
                    a = a.toLowerCase();
                    return !(!q[a] || 0 === q[a].length)
                }
        }
        var f = k.makeMap("focus blur focusin focusout click dblclick mousedown mouseup mousemove mouseover beforepaste paste cut copy selectionchange mouseout mouseenter mouseleave wheel keydown keypress keyup input contextmenu dragstart dragend dragover draggesture dragdrop drop drag submit compositionstart compositionend compositionupdate touchstart touchend", " ");
        g.isNative = function(e) {
            return !!f[e.toLowerCase()]
        };
        return g
    });
    z("tinymce/data/Binding", [], function() {
        function k(g) {
            this.create = g.create
        }
        k.create = function(g, f) {
            return new k({
                create: function(e, a) {
                    function c(b) {
                        e.set(a, b.value)
                    }
                    var b;
                    e.on("change:" + a, function(a) {
                        g.set(f, a.value)
                    });
                    g.on("change:" + f, c);
                    b = e._bindings;
                    b || (b = e._bindings = [], e.on("destroy", function() {
                        for (var a = b.length; a--;) b[a]()
                    }));
                    b.push(function() {
                        g.off("change:" + f, c)
                    });
                    return g.get(f)
                }
            })
        };
        return k
    });
    z("tinymce/util/Observable", ["tinymce/util/EventDispatcher"], function(k) {
        function g(f) {
            f._eventDispatcher || (f._eventDispatcher =
                new k({
                    scope: f,
                    toggleEvent: function(e, a) {
                        k.isNative(e) && f.toggleNativeEvent && f.toggleNativeEvent(e, a)
                    }
                }));
            return f._eventDispatcher
        }
        return {
            fire: function(f, e, a) {
                if (this.removed && "remove" !== f) return e;
                e = g(this).fire(f, e, a);
                if (!1 !== a && this.parent)
                    for (a = this.parent(); a && !e.isPropagationStopped();) a.fire(f, e, !1), a = a.parent();
                return e
            },
            on: function(f, e, a) {
                return g(this).on(f, e, a)
            },
            off: function(f, e) {
                return g(this).off(f, e)
            },
            once: function(f, e) {
                return g(this).once(f, e)
            },
            hasEventListeners: function(f) {
                return g(this).has(f)
            }
        }
    });
    z("tinymce/data/ObservableObject", ["tinymce/data/Binding", "tinymce/util/Observable", "tinymce/util/Class", "tinymce/util/Tools"], function(k, g, f, e) {
        function a(c, b) {
            var d, f;
            if (c === b) return !0;
            if (null === c || null === b || "object" !== typeof c || "object" !== typeof b) return c === b;
            if (e.isArray(b)) {
                if (c.length !== b.length) return !1;
                for (d = c.length; d--;)
                    if (!a(c[d], b[d])) return !1
            }
            if (0 < c.nodeType || 0 < b.nodeType) return c === b;
            f = {};
            for (d in b) {
                if (!a(c[d], b[d])) return !1;
                f[d] = !0
            }
            for (d in c)
                if (!f[d] && !a(c[d], b[d])) return !1;
            return !0
        }
        return f.extend({
            Mixins: [g],
            init: function(a) {
                var b, c;
                a = a || {};
                for (b in a) c = a[b], c instanceof k && (a[b] = c.create(this, b));
                this.data = a
            },
            set: function(c, b) {
                var d, e = this.data[c];
                b instanceof k && (b = b.create(this, c));
                if ("object" === typeof c) {
                    for (d in c) this.set(d, c[d]);
                    return this
                }
                a(e, b) || (this.data[c] = b, b = {
                    target: this,
                    name: c,
                    value: b,
                    oldValue: e
                }, this.fire("change:" + c, b), this.fire("change", b));
                return this
            },
            get: function(a) {
                return this.data[a]
            },
            has: function(a) {
                return a in this.data
            },
            bind: function(a) {
                return k.create(this,
                    a)
            },
            destroy: function() {
                this.fire("destroy")
            }
        })
    });
    z("tinymce/ui/Selector", ["tinymce/util/Class"], function(k) {
        function g(a) {
            for (var b = [], c = a.length, d; c--;) d = a[c], d.__checked || (b.push(d), d.__checked = 1);
            for (c = b.length; c--;) delete b[c].__checked;
            return b
        }
        var f = /^([\w\\*]+)?(?:#([\w\-\\]+))?(?:\.([\w\\\.]+))?(?:\[\@?([\w\\]+)([\^\$\*!~]?=)([\w\\]+)\])?(?:\:(.+))?/i,
            e = /((?:\((?:\([^()]+\)|[^()]+)+\)|\[(?:\[[^\[\]]*\]|['"][^'"]*['"]|[^\[\]'"]+)+\]|\\.|[^ >+~,(\[\\]+)+|[>+~])(\s*,\s*)?((?:.|\r|\n)*)/g,
            a = /^\s*|\s*$/g,
            c, b = k.extend({
                init: function(b) {
                    function c(a) {
                        if (a) return a = a.toLowerCase(),
                            function(b) {
                                return "*" === a || b.type === a
                            }
                    }

                    function d(a) {
                        if (a) return function(b) {
                            return b._name === a
                        }
                    }

                    function g(a) {
                        if (a) return a = a.split("."),
                            function(b) {
                                for (var c = a.length; c--;)
                                    if (!b.classes.contains(a[c])) return !1;
                                return !0
                            }
                    }

                    function h(a, b, c) {
                        if (a) return function(d) {
                            d = d[a] ? d[a]() : "";
                            return b ? "\x3d" === b ? d === c : "*\x3d" === b ? 0 <= d.indexOf(c) : "~\x3d" === b ? 0 <= (" " + d + " ").indexOf(" " + c + " ") : "!\x3d" === b ? d != c : "^\x3d" === b ? 0 === d.indexOf(c) :
                                "$\x3d" === b ? d.substr(d.length - c.length) === c : !1 : !!c
                        }
                    }

                    function n(a) {
                        var b;
                        if (a) {
                            a = /(?:not\((.+)\))|(.+)/i.exec(a);
                            if (!a[1]) return a = a[2],
                                function(b, c, d) {
                                    return "first" === a ? 0 === c : "last" === a ? c === d - 1 : "even" === a ? 0 === c % 2 : "odd" === a ? 1 === c % 2 : b[a] ? b[a]() : !1
                                };
                            b = k(a[1], []);
                            return function(a) {
                                return !A(a, b)
                            }
                        }
                    }

                    function p(b, e, l) {
                        function m(a) {
                            a && e.push(a)
                        }
                        b = f.exec(b.replace(a, ""));
                        m(c(b[1]));
                        m(d(b[2]));
                        m(g(b[3]));
                        m(h(b[4], b[5], b[6]));
                        m(n(b[7]));
                        e.pseudo = !!b[7];
                        e.direct = l;
                        return e
                    }

                    function k(a, b) {
                        var c = [],
                            d, f;
                        do
                            if (e.exec(""),
                                f = e.exec(a))
                                if (a = f[3], c.push(f[1]), f[2]) {
                                    d = f[3];
                                    break
                                }
                        while (f);
                        d && k(d, b);
                        a = [];
                        for (d = 0; d < c.length; d++) "\x3e" != c[d] && a.push(p(c[d], [], "\x3e" === c[d - 1]));
                        b.push(a);
                        return b
                    }
                    var A = this.match;
                    this._selectors = k(b, [])
                },
                match: function(a, b) {
                    var c, d, e, f, g, l, k, m, t, y, B, x;
                    b = b || this._selectors;
                    c = 0;
                    for (d = b.length; c < d; c++) {
                        g = b[c];
                        f = g.length;
                        x = a;
                        B = 0;
                        for (e = f - 1; 0 <= e; e--)
                            for (m = g[e]; x;) {
                                if (m.pseudo)
                                    for (l = x.parent().items(), t = y = l.length; t-- && l[t] !== x;);
                                l = 0;
                                for (k = m.length; l < k; l++)
                                    if (!m[l](x, t, y)) {
                                        l = k + 1;
                                        break
                                    }
                                if (l === k) {
                                    B++;
                                    break
                                } else if (e === f - 1) break;
                                x = x.parent()
                            }
                        if (B === f) return !0
                    }
                    return !1
                },
                find: function(a) {
                    function d(a, b, c) {
                        var f, g, h, l, n, p = b[c];
                        f = 0;
                        for (g = a.length; f < g; f++) {
                            n = a[f];
                            h = 0;
                            for (l = p.length; h < l; h++)
                                if (!p[h](n, f, g)) {
                                    h = l + 1;
                                    break
                                }
                            if (h === l) c == b.length - 1 ? e.push(n) : n.items && d(n.items(), b, c + 1);
                            else if (p.direct) break;
                            n.items && d(n.items(), b, c)
                        }
                    }
                    var e = [],
                        f, h, n = this._selectors;
                    if (a.items) {
                        f = 0;
                        for (h = n.length; f < h; f++) d(a.items(), n[f], 0);
                        1 < h && (e = g(e))
                    }
                    c || (c = b.Collection);
                    return new c(e)
                }
            });
        return b
    });
    z("tinymce/ui/Collection", ["tinymce/util/Tools", "tinymce/ui/Selector", "tinymce/util/Class"], function(k, g, f) {
        var e, a, c = Array.prototype.push,
            b = Array.prototype.slice;
        a = {
            length: 0,
            init: function(a) {
                a && this.add(a)
            },
            add: function(a) {
                k.isArray(a) ? c.apply(this, a) : a instanceof e ? this.add(a.toArray()) : c.call(this, a);
                return this
            },
            set: function(a) {
                var b = this.length;
                this.length = 0;
                this.add(a);
                for (a = this.length; a < b; a++) delete this[a];
                return this
            },
            filter: function(a) {
                var b, c, d = [],
                    f, n;
                "string" === typeof a ? (a = new g(a), n = function(b) {
                        return a.match(b)
                    }) :
                    n = a;
                b = 0;
                for (c = this.length; b < c; b++) f = this[b], n(f) && d.push(f);
                return new e(d)
            },
            slice: function() {
                return new e(b.apply(this, arguments))
            },
            eq: function(a) {
                return -1 === a ? this.slice(a) : this.slice(a, +a + 1)
            },
            each: function(a) {
                k.each(this, a);
                return this
            },
            toArray: function() {
                return k.toArray(this)
            },
            indexOf: function(a) {
                for (var b = this.length; b-- && this[b] !== a;);
                return b
            },
            reverse: function() {
                return new e(k.toArray(this).reverse())
            },
            hasClass: function(a) {
                return this[0] ? this[0].classes.contains(a) : !1
            },
            prop: function(a, b) {
                var c;
                if (void 0 !== b) return this.each(function(c) {
                    if (c[a]) c[a](b)
                }), this;
                if ((c = this[0]) && c[a]) return c[a]()
            },
            exec: function(a) {
                var b = k.toArray(arguments).slice(1);
                this.each(function(c) {
                    c[a] && c[a].apply(c, b)
                });
                return this
            },
            remove: function() {
                for (var a = this.length; a--;) this[a].remove();
                return this
            },
            addClass: function(a) {
                return this.each(function(b) {
                    b.classes.add(a)
                })
            },
            removeClass: function(a) {
                return this.each(function(b) {
                    b.classes.remove(a)
                })
            }
        };
        k.each("fire on off show hide append prepend before after reflow".split(" "),
            function(b) {
                a[b] = function() {
                    var a = k.toArray(arguments);
                    this.each(function(c) {
                        b in c && c[b].apply(c, a)
                    });
                    return this
                }
            });
        k.each("text name disabled active selected checked visible parent value data".split(" "), function(b) {
            a[b] = function(a) {
                return this.prop(b, a)
            }
        });
        e = f.extend(a);
        return g.Collection = e
    });
    z("tinymce/ui/DomUtils", ["tinymce/util/Tools", "tinymce/dom/DOMUtils"], function(k, g) {
        var f = 0;
        return {
            id: function() {
                return "mceu_" + f++
            },
            create: function(e, a, c) {
                var b = document.createElement(e);
                g.DOM.setAttribs(b,
                    a);
                "string" === typeof c ? b.innerHTML = c : k.each(c, function(a) {
                    a.nodeType && b.appendChild(a)
                });
                return b
            },
            createFragment: function(e) {
                return g.DOM.createFragment(e)
            },
            getWindowSize: function() {
                return g.DOM.getViewPort()
            },
            getSize: function(e) {
                var a;
                if (e.getBoundingClientRect) {
                    var c = e.getBoundingClientRect();
                    a = Math.max(c.width || c.right - c.left, e.offsetWidth);
                    e = Math.max(c.height || c.bottom - c.bottom, e.offsetHeight)
                } else a = e.offsetWidth, e = e.offsetHeight;
                return {
                    width: a,
                    height: e
                }
            },
            getPos: function(e, a) {
                return g.DOM.getPos(e,
                    a)
            },
            getViewPort: function(e) {
                return g.DOM.getViewPort(e)
            },
            get: function(e) {
                return document.getElementById(e)
            },
            addClass: function(e, a) {
                return g.DOM.addClass(e, a)
            },
            removeClass: function(e, a) {
                return g.DOM.removeClass(e, a)
            },
            hasClass: function(e, a) {
                return g.DOM.hasClass(e, a)
            },
            toggleClass: function(e, a, c) {
                return g.DOM.toggleClass(e, a, c)
            },
            css: function(e, a, c) {
                return g.DOM.setStyle(e, a, c)
            },
            getRuntimeStyle: function(e, a) {
                return g.DOM.getStyle(e, a, !0)
            },
            on: function(e, a, c, b) {
                return g.DOM.bind(e, a, c, b)
            },
            off: function(e,
                a, c) {
                return g.DOM.unbind(e, a, c)
            },
            fire: function(e, a, c) {
                return g.DOM.fire(e, a, c)
            },
            innerHtml: function(e, a) {
                g.DOM.setHTML(e, a)
            }
        }
    });
    z("tinymce/ui/BoxUtils", [], function() {
        return {
            parseBox: function(k) {
                var g;
                if (k) {
                    if ("number" === typeof k) return k = k || 0, {
                        top: k,
                        left: k,
                        bottom: k,
                        right: k
                    };
                    k = k.split(" ");
                    g = k.length;
                    1 === g ? k[1] = k[2] = k[3] = k[0] : 2 === g ? (k[2] = k[0], k[3] = k[1]) : 3 === g && (k[3] = k[1]);
                    return {
                        top: parseInt(k[0], 10) || 0,
                        right: parseInt(k[1], 10) || 0,
                        bottom: parseInt(k[2], 10) || 0,
                        left: parseInt(k[3], 10) || 0
                    }
                }
            },
            measureBox: function(k,
                g) {
                function f(a) {
                    var c = document.defaultView;
                    return c ? (a = a.replace(/[A-Z]/g, function(a) {
                        return "-" + a
                    }), c.getComputedStyle(k, null).getPropertyValue(a)) : k.currentStyle[a]
                }

                function e(a) {
                    a = parseFloat(f(a), 10);
                    return isNaN(a) ? 0 : a
                }
                return {
                    top: e(g + "TopWidth"),
                    right: e(g + "RightWidth"),
                    bottom: e(g + "BottomWidth"),
                    left: e(g + "LeftWidth")
                }
            }
        }
    });
    z("tinymce/ui/ClassList", ["tinymce/util/Tools"], function(k) {
        function g() {}

        function f(e) {
            this.cls = [];
            this.cls._map = {};
            this.onchange = e || g;
            this.prefix = ""
        }
        k.extend(f.prototype, {
            add: function(e) {
                e &&
                    !this.contains(e) && (this.cls._map[e] = !0, this.cls.push(e), this._change());
                return this
            },
            remove: function(e) {
                if (this.contains(e)) {
                    for (var a = 0; a < this.cls.length && this.cls[a] !== e; a++);
                    this.cls.splice(a, 1);
                    delete this.cls._map[e];
                    this._change()
                }
                return this
            },
            toggle: function(e, a) {
                var c = this.contains(e);
                c !== a && (c ? this.remove(e) : this.add(e), this._change());
                return this
            },
            contains: function(e) {
                return !!this.cls._map[e]
            },
            _change: function() {
                delete this.clsValue;
                this.onchange.call(this)
            }
        });
        f.prototype.toString = function() {
            var e;
            if (this.clsValue) return this.clsValue;
            e = "";
            for (var a = 0; a < this.cls.length; a++) 0 < a && (e += " "), e += this.prefix + this.cls[a];
            return e
        };
        return f
    });
    z("tinymce/ui/ReflowQueue", ["tinymce/util/Delay"], function(k) {
        var g = {},
            f;
        return {
            add: function(e) {
                (e = e.parent()) && e._layout && !e._layout.isNative() && (g[e._id] || (g[e._id] = e), f || (f = !0, k.requestAnimationFrame(function() {
                    var a, c;
                    f = !1;
                    for (a in g) c = g[a], c.state.get("rendered") && c.reflow();
                    g = {}
                }, document.body)))
            },
            remove: function(e) {
                g[e._id] && delete g[e._id]
            }
        }
    });
    z("tinymce/ui/Control",
        "tinymce/util/Class tinymce/util/Tools tinymce/util/EventDispatcher tinymce/data/ObservableObject tinymce/ui/Collection tinymce/ui/DomUtils tinymce/dom/DomQuery tinymce/ui/BoxUtils tinymce/ui/ClassList tinymce/ui/ReflowQueue".split(" "),
        function(k, g, f, e, a, c, b, d, l, v) {
            function q(a) {
                a._eventDispatcher || (a._eventDispatcher = new f({
                    scope: a,
                    toggleEvent: function(b, c) {
                        c && f.isNative(b) && (a._nativeEvents || (a._nativeEvents = {}), a._nativeEvents[b] = !0, a.state.get("rendered") && h(a))
                    }
                }));
                return a._eventDispatcher
            }

            function h(a) {
                function c(b) {
                    var c = a.getParentCtrl(b.target);
                    c && c.fire(b.type, b)
                }

                function d() {
                    var a = l._lastHoverCtrl;
                    a && (a.fire("mouseleave", {
                        target: a.getEl()
                    }), a.parents().each(function(a) {
                        a.fire("mouseleave", {
                            target: a.getEl()
                        })
                    }), l._lastHoverCtrl = null)
                }

                function e(b) {
                    var c = a.getParentCtrl(b.target);
                    b = l._lastHoverCtrl;
                    var d = 0,
                        e, f;
                    if (c !== b) {
                        l._lastHoverCtrl = c;
                        f = c.parents().toArray().reverse();
                        f.push(c);
                        if (b) {
                            c = b.parents().toArray().reverse();
                            c.push(b);
                            for (d = 0; d < c.length && f[d] === c[d]; d++);
                            for (e = c.length -
                                1; e >= d; e--) b = c[e], b.fire("mouseleave", {
                                target: b.getEl()
                            })
                        }
                        for (e = d; e < f.length; e++) c = f[e], c.fire("mouseenter", {
                            target: c.getEl()
                        })
                    }
                }

                function f(b) {
                    b.preventDefault();
                    "mousewheel" == b.type ? (b.deltaY = -1 / 40 * b.wheelDelta, b.wheelDeltaX && (b.deltaX = -1 / 40 * b.wheelDeltaX)) : (b.deltaX = 0, b.deltaY = b.detail);
                    a.fire("wheel", b)
                }
                var m, g, h, l, p, k;
                if (p = a._nativeEvents) {
                    h = a.parents().toArray();
                    h.unshift(a);
                    m = 0;
                    for (g = h.length; !l && m < g; m++) l = h[m]._eventsRoot;
                    l || (l = h[h.length - 1] || a);
                    a._eventsRoot = l;
                    g = m;
                    for (m = 0; m < g; m++) h[m]._eventsRoot =
                        l;
                    m = l._delegates;
                    m || (m = l._delegates = {});
                    for (k in p) {
                        if (!p) return !1;
                        if ("wheel" === k)
                            if (n) b(a.getEl()).on("mousewheel", f);
                            else b(a.getEl()).on("DOMMouseScroll", f);
                        else "mouseenter" === k || "mouseleave" === k ? l._hasMouseEnter || (b(l.getEl()).on("mouseleave", d).on("mouseover", e), l._hasMouseEnter = 1) : m[k] || (b(l.getEl()).on(k, c), m[k] = !0), p[k] = !1
                    }
                }
            }
            var n = "onmousewheel" in document,
                p, w = 0,
                A = {
                    Statics: {
                        classPrefix: "mce-"
                    },
                    isRtl: function() {
                        return p.rtl
                    },
                    classPrefix: "mce-",
                    init: function(a) {
                        function c(a) {
                            var b;
                            a = a.split(" ");
                            for (b = 0; b < a.length; b++) f.classes.add(a[b])
                        }
                        var f = this,
                            m, h;
                        f.settings = a = g.extend({}, f.Defaults, a);
                        f._id = a.id || "mceu_" + w++;
                        f._aria = {
                            role: a.role
                        };
                        f._elmCache = {};
                        f.$ = b;
                        f.state = new e({
                            visible: !0,
                            active: !1,
                            disabled: !1,
                            value: ""
                        });
                        f.data = new e(a.data);
                        f.classes = new l(function() {
                            f.state.get("rendered") && (f.getEl().className = this.toString())
                        });
                        f.classes.prefix = f.classPrefix;
                        if (m = a.classes) f.Defaults && (h = f.Defaults.classes) && m != h && c(h), c(m);
                        g.each("title text name visible disabled active value".split(" "), function(b) {
                            if (b in
                                a) f[b](a[b])
                        });
                        f.on("click", function() {
                            if (f.disabled()) return !1
                        });
                        f.settings = a;
                        f.borderBox = d.parseBox(a.border);
                        f.paddingBox = d.parseBox(a.padding);
                        f.marginBox = d.parseBox(a.margin);
                        a.hidden && f.hide()
                    },
                    Properties: "parent,name",
                    getContainerElm: function() {
                        return document.body
                    },
                    getParentCtrl: function(a) {
                        for (var b, c = this.getRoot().controlIdLookup; a && c && !(b = c[a.id]);) a = a.parentNode;
                        return b
                    },
                    initLayoutRect: function() {
                        var a = this.settings,
                            b, e = this.getEl(),
                            f, g, h, l, n, p;
                        b = this.borderBox = this.borderBox || d.measureBox(e,
                            "border");
                        this.paddingBox = this.paddingBox || d.measureBox(e, "padding");
                        this.marginBox = this.marginBox || d.measureBox(e, "margin");
                        f = c.getSize(e);
                        n = a.minWidth;
                        p = a.minHeight;
                        e = n || f.width;
                        h = p || f.height;
                        f = a.width;
                        g = a.height;
                        l = a.autoResize;
                        l = "undefined" != typeof l ? l : !f && !g;
                        f = f || e;
                        g = g || h;
                        var k = b.left + b.right;
                        b = b.top + b.bottom;
                        var q = a.maxWidth || 65535,
                            w = a.maxHeight || 65535;
                        this._layoutRect = a = {
                            x: a.x || 0,
                            y: a.y || 0,
                            w: f,
                            h: g,
                            deltaW: k,
                            deltaH: b,
                            contentW: f - k,
                            contentH: g - b,
                            innerW: f - k,
                            innerH: g - b,
                            startMinWidth: n || 0,
                            startMinHeight: p ||
                                0,
                            minW: Math.min(e, q),
                            minH: Math.min(h, w),
                            maxW: q,
                            maxH: w,
                            autoResize: l,
                            scrollW: 0
                        };
                        this._lastLayoutRect = {};
                        return a
                    },
                    layoutRect: function(a) {
                        var b = this._layoutRect,
                            c, d, e;
                        b || (b = this.initLayoutRect());
                        if (a) {
                            d = b.deltaW;
                            e = b.deltaH;
                            void 0 !== a.x && (b.x = a.x);
                            void 0 !== a.y && (b.y = a.y);
                            void 0 !== a.minW && (b.minW = a.minW);
                            void 0 !== a.minH && (b.minH = a.minH);
                            c = a.w;
                            void 0 !== c && (c = c < b.minW ? b.minW : c, c = c > b.maxW ? b.maxW : c, b.w = c, b.innerW = c - d);
                            c = a.h;
                            void 0 !== c && (c = c < b.minH ? b.minH : c, c = c > b.maxH ? b.maxH : c, b.h = c, b.innerH = c - e);
                            c = a.innerW;
                            void 0 !== c && (c = c < b.minW - d ? b.minW - d : c, c = c > b.maxW - d ? b.maxW - d : c, b.innerW = c, b.w = c + d);
                            c = a.innerH;
                            void 0 !== c && (c = c < b.minH - e ? b.minH - e : c, c = c > b.maxH - e ? b.maxH - e : c, b.innerH = c, b.h = c + e);
                            void 0 !== a.contentW && (b.contentW = a.contentW);
                            void 0 !== a.contentH && (b.contentH = a.contentH);
                            a = this._lastLayoutRect;
                            if (a.x !== b.x || a.y !== b.y || a.w !== b.w || a.h !== b.h)(c = p.repaintControls) && c.map && !c.map[this._id] && (c.push(this), c.map[this._id] = !0), a.x = b.x, a.y = b.y, a.w = b.w, a.h = b.h;
                            return this
                        }
                        return b
                    },
                    repaint: function() {
                        var a, b, c, d, e,
                            f, g, h;
                        h = document.createRange ? function(a) {
                            return a
                        } : Math.round;
                        a = this.getEl().style;
                        d = this._layoutRect;
                        g = this._lastRepaintRect || {};
                        e = this.borderBox;
                        f = e.left + e.right;
                        e = e.top + e.bottom;
                        d.x !== g.x && (a.left = h(d.x) + "px", g.x = d.x);
                        d.y !== g.y && (a.top = h(d.y) + "px", g.y = d.y);
                        d.w !== g.w && (f = h(d.w - f), a.width = (0 <= f ? f : 0) + "px", g.w = d.w);
                        d.h !== g.h && (f = h(d.h - e), a.height = (0 <= f ? f : 0) + "px", g.h = d.h);
                        if (this._hasBody && d.innerW !== g.innerW) {
                            f = h(d.innerW);
                            if (c = this.getEl("body")) b = c.style, b.width = (0 <= f ? f : 0) + "px";
                            g.innerW = d.innerW
                        }
                        if (this._hasBody &&
                            d.innerH !== g.innerH) {
                            f = h(d.innerH);
                            if (c = c || this.getEl("body")) b = b || c.style, b.height = (0 <= f ? f : 0) + "px";
                            g.innerH = d.innerH
                        }
                        this._lastRepaintRect = g;
                        this.fire("repaint", {}, !1)
                    },
                    updateLayoutRect: function() {
                        this.parent()._lastRect = null;
                        c.css(this.getEl(), {
                            width: "",
                            height: ""
                        });
                        this._layoutRect = this._lastRepaintRect = this._lastLayoutRect = null;
                        this.initLayoutRect()
                    },
                    on: function(a, b) {
                        var c = this;
                        q(c).on(a, function(a) {
                            var b, d;
                            return "string" != typeof a ? a : function(e) {
                                b || c.parentsAndSelf().each(function(c) {
                                    var e = c.settings.callbacks;
                                    if (e && (b = e[a])) return d = c, !1
                                });
                                if (b) return b.call(d, e);
                                e.action = a;
                                this.fire("execute", e)
                            }
                        }(b));
                        return c
                    },
                    off: function(a, b) {
                        q(this).off(a, b);
                        return this
                    },
                    fire: function(a, b, c) {
                        b = b || {};
                        b.control || (b.control = this);
                        b = q(this).fire(a, b);
                        if (!1 !== c && this.parent)
                            for (c = this.parent(); c && !b.isPropagationStopped();) c.fire(a, b, !1), c = c.parent();
                        return b
                    },
                    hasEventListeners: function(a) {
                        return q(this).has(a)
                    },
                    parents: function(b) {
                        var c, d = new a;
                        for (c = this.parent(); c; c = c.parent()) d.add(c);
                        b && (d = d.filter(b));
                        return d
                    },
                    parentsAndSelf: function(b) {
                        return (new a(this)).add(this.parents(b))
                    },
                    next: function() {
                        var a = this.parent().items();
                        return a[a.indexOf(this) + 1]
                    },
                    prev: function() {
                        var a = this.parent().items();
                        return a[a.indexOf(this) - 1]
                    },
                    innerHtml: function(a) {
                        this.$el.html(a);
                        return this
                    },
                    getEl: function(a) {
                        a = a ? this._id + "-" + a : this._id;
                        this._elmCache[a] || (this._elmCache[a] = b("#" + a)[0]);
                        return this._elmCache[a]
                    },
                    show: function() {
                        return this.visible(!0)
                    },
                    hide: function() {
                        return this.visible(!1)
                    },
                    focus: function() {
                        try {
                            this.getEl().focus()
                        } catch (m) {}
                        return this
                    },
                    blur: function() {
                        this.getEl().blur();
                        return this
                    },
                    aria: function(a, b) {
                        var c = this.getEl(this.ariaTarget);
                        if ("undefined" === typeof b) return this._aria[a];
                        this._aria[a] = b;
                        this.state.get("rendered") && c.setAttribute("role" == a ? a : "aria-" + a, b);
                        return this
                    },
                    encode: function(a, b) {
                        !1 !== b && (a = this.translate(a));
                        return (a || "").replace(/[&<>"]/g, function(a) {
                            return "\x26#" + a.charCodeAt(0) + ";"
                        })
                    },
                    translate: function(a) {
                        return p.translate ? p.translate(a) : a
                    },
                    before: function(a) {
                        var b = this.parent();
                        b && b.insert(a, b.items().indexOf(this), !0);
                        return this
                    },
                    after: function(a) {
                        var b = this.parent();
                        b && b.insert(a, b.items().indexOf(this));
                        return this
                    },
                    remove: function() {
                        var a = this,
                            c = a.getEl(),
                            d = a.parent(),
                            e, f;
                        if (a.items) {
                            var g = a.items().toArray();
                            for (f = g.length; f--;) g[f].remove()
                        }
                        d && d.items && (e = [], d.items().each(function(b) {
                            b !== a && e.push(b)
                        }), d.items().set(e), d._lastRect = null);
                        a._eventsRoot && a._eventsRoot == a && b(c).off();
                        (d = a.getRoot().controlIdLookup) && delete d[a._id];
                        c && c.parentNode && c.parentNode.removeChild(c);
                        a.state.set("rendered", !1);
                        a.state.destroy();
                        a.fire("remove");
                        return a
                    },
                    renderBefore: function(a) {
                        b(a).before(this.renderHtml());
                        this.postRender();
                        return this
                    },
                    renderTo: function(a) {
                        b(a || this.getContainerElm()).append(this.renderHtml());
                        this.postRender();
                        return this
                    },
                    preRender: function() {},
                    render: function() {},
                    renderHtml: function() {
                        return '\x3cdiv id\x3d"' + this._id + '" class\x3d"' + this.classes + '"\x3e\x3c/div\x3e'
                    },
                    postRender: function() {
                        var a = this,
                            c = a.settings,
                            d, e, f;
                        a.$el = b(a.getEl());
                        a.state.set("rendered", !0);
                        for (e in c)
                            if (0 ===
                                e.indexOf("on")) a.on(e.substr(2), c[e]);
                        if (a._eventsRoot) {
                            for (d = a.parent(); !f && d; d = d.parent()) f = d._eventsRoot;
                            if (f)
                                for (e in f._nativeEvents) a._nativeEvents[e] = !0
                        }
                        h(a);
                        c.style && (d = a.getEl()) && (d.setAttribute("style", c.style), d.style.cssText = c.style);
                        a.settings.border && (c = a.borderBox, a.$el.css({
                            "border-top-width": c.top,
                            "border-right-width": c.right,
                            "border-bottom-width": c.bottom,
                            "border-left-width": c.left
                        }));
                        c = a.getRoot();
                        c.controlIdLookup || (c.controlIdLookup = {});
                        c.controlIdLookup[a._id] = a;
                        for (var g in a._aria) a.aria(g,
                            a._aria[g]);
                        !1 === a.state.get("visible") && (a.getEl().style.display = "none");
                        a.bindStates();
                        a.state.on("change:visible", function(b) {
                            b = b.value;
                            var c;
                            a.state.get("rendered") && (a.getEl().style.display = !1 === b ? "none" : "", a.getEl().getBoundingClientRect());
                            if (c = a.parent()) c._lastRect = null;
                            a.fire(b ? "show" : "hide");
                            v.add(a)
                        });
                        a.fire("postrender", {}, !1)
                    },
                    bindStates: function() {},
                    scrollIntoView: function(a) {
                        var b = this.getEl(),
                            c = b.parentNode,
                            d, e, f, g, h;
                        f = b;
                        for (d = e = 0; f && f != c && f.nodeType;) d += f.offsetLeft || 0, e += f.offsetTop ||
                            0, f = f.offsetParent;
                        f = b.offsetWidth;
                        b = b.offsetHeight;
                        g = c.clientWidth;
                        h = c.clientHeight;
                        "end" == a ? (d -= g - f, e -= h - b) : "center" == a && (d -= g / 2 - f / 2, e -= h / 2 - b / 2);
                        c.scrollLeft = d;
                        c.scrollTop = e;
                        return this
                    },
                    getRoot: function() {
                        for (var a = this, b, c = []; a;) {
                            if (a.rootControl) {
                                b = a.rootControl;
                                break
                            }
                            c.push(a);
                            b = a;
                            a = a.parent()
                        }
                        b || (b = this);
                        for (a = c.length; a--;) c[a].rootControl = b;
                        return b
                    },
                    reflow: function() {
                        v.remove(this);
                        var a = this.parent();
                        a._layout && !a._layout.isNative() && a.reflow();
                        return this
                    }
                };
            g.each("text title visible disabled active value".split(" "),
                function(a) {
                    A[a] = function(b) {
                        if (0 === arguments.length) return this.state.get(a);
                        "undefined" != typeof b && this.state.set(a, b);
                        return this
                    }
                });
            return p = k.extend(A)
        });
    z("tinymce/ui/Factory", [], function() {
        var k = {},
            g;
        return {
            add: function(f, e) {
                k[f.toLowerCase()] = e
            },
            has: function(f) {
                return !!k[f.toLowerCase()]
            },
            create: function(f, e) {
                var a, c;
                if (!g) {
                    c = tinymce.ui;
                    for (a in c) k[a.toLowerCase()] = c[a];
                    g = !0
                }
                "string" == typeof f ? (e = e || {}, e.type = f) : (e = f, f = e.type);
                f = f.toLowerCase();
                a = k[f];
                if (!a) throw Error("Could not find control by type: " +
                    f);
                a = new a(e);
                a.type = f;
                return a
            }
        }
    });
    z("tinymce/ui/KeyboardNavigation", [], function() {
        return function(k) {
            function g(a) {
                return (a = a || m) && 1 === a.nodeType ? a.getAttribute("role") : null
            }

            function f(a) {
                for (var b = a || m; b = b.parentNode;)
                    if (a = g(b)) return a
            }

            function e(a) {
                var b = m;
                if (b && 1 === b.nodeType) return b.getAttribute("aria-" + a)
            }

            function a(a) {
                a = a.tagName.toUpperCase();
                return "INPUT" == a || "TEXTAREA" == a || "SELECT" == a
            }

            function c(b) {
                function c(b) {
                    if (1 == b.nodeType && "none" != b.style.display) {
                        var e;
                        e = a(b) && !b.hidden ? !0 : /^(button|menuitem|checkbox|tab|menuitemcheckbox|option|gridcell|slider)$/.test(g(b)) ?
                            !0 : !1;
                        e && d.push(b);
                        for (e = 0; e < b.childNodes.length; e++) c(b.childNodes[e])
                    }
                }
                var d = [];
                c(b || A.getEl());
                return d
            }

            function b(a) {
                var b, c;
                a = a || t;
                c = a.parents().toArray();
                c.unshift(a);
                for (a = 0; a < c.length && (b = c[a], !b.settings.ariaRoot); a++);
                return b
            }

            function d(a, b) {
                0 > a ? a = b.length - 1 : a >= b.length && (a = 0);
                b[a] && b[a].focus();
                return a
            }

            function l(a, e) {
                var f = -1,
                    g = b();
                e = e || c(g.getEl());
                for (var h = 0; h < e.length; h++) e[h] === m && (f = h);
                g.lastAriaIndex = d(f + a, e)
            }

            function v() {
                "tablist" == f() ? l(-1, c(m.parentNode)) : t.parent().submenu ?
                    t.fire("cancel") : l(-1)
            }

            function q() {
                var a = g(),
                    b = f();
                "tablist" == b ? l(1, c(m.parentNode)) : "menuitem" == a && "menu" == b && e("haspopup") ? w() : l(1)
            }

            function h() {
                l(-1)
            }

            function n() {
                var a = g(),
                    b = f();
                "menuitem" == a && "menubar" == b ? w() : "button" == a && e("haspopup") ? w({
                    key: "down"
                }) : l(1)
            }

            function p(a) {
                "tablist" == f() ? (a = c(t.getEl("body"))[0]) && a.focus() : l(a.shiftKey ? -1 : 1)
            }

            function w(a) {
                a = a || {};
                t.fire("click", {
                    target: m,
                    aria: a
                })
            }
            var A = k.root,
                m, t;
            try {
                m = document.activeElement
            } catch (y) {
                m = document.body
            }
            t = A.getParentCtrl(m);
            A.on("keydown",
                function(b) {
                    function c(b, c) {
                        a(m) || "slider" !== g(m) && !1 !== c(b) && b.preventDefault()
                    }
                    if (!b.isDefaultPrevented()) switch (b.keyCode) {
                        case 37:
                            c(b, v);
                            break;
                        case 39:
                            c(b, q);
                            break;
                        case 38:
                            c(b, h);
                            break;
                        case 40:
                            c(b, n);
                            break;
                        case 27:
                            t.fire("cancel");
                            break;
                        case 14:
                        case 13:
                        case 32:
                            c(b, w);
                            break;
                        case 9:
                            !1 !== p(b) && b.preventDefault()
                    }
                });
            A.on("focusin", function(a) {
                m = a.target;
                t = a.control
            });
            return {
                focusFirst: function(a) {
                    a = b(a);
                    var e = c(a.getEl());
                    a.settings.ariaRemember && "lastAriaIndex" in a ? d(a.lastAriaIndex, e) : d(0, e)
                }
            }
        }
    });
    z("tinymce/ui/Container", "tinymce/ui/Control tinymce/ui/Collection tinymce/ui/Selector tinymce/ui/Factory tinymce/ui/KeyboardNavigation tinymce/util/Tools tinymce/dom/DomQuery tinymce/ui/ClassList tinymce/ui/ReflowQueue".split(" "), function(k, g, f, e, a, c, b, d, l) {
        var v = {};
        return k.extend({
            init: function(a) {
                var b = this;
                b._super(a);
                a = b.settings;
                a.fixed && b.state.set("fixed", !0);
                b._items = new g;
                b.isRtl() && b.classes.add("rtl");
                b.bodyClasses = new d(function() {
                    b.state.get("rendered") && (b.getEl("body").className =
                        this.toString())
                });
                b.bodyClasses.prefix = b.classPrefix;
                b.classes.add("container");
                b.bodyClasses.add("container-body");
                a.containerCls && b.classes.add(a.containerCls);
                b._layout = e.create((a.layout || "") + "layout");
                b.settings.items ? b.add(b.settings.items) : b.add(b.render());
                b._hasBody = !0
            },
            items: function() {
                return this._items
            },
            find: function(a) {
                a = v[a] = v[a] || new f(a);
                return a.find(this)
            },
            add: function(a) {
                this.items().add(this.create(a)).parent(this);
                return this
            },
            focus: function(a) {
                var b;
                if (a && (a = this.keyboardNav ||
                        this.parents().eq(-1)[0].keyboardNav)) {
                    a.focusFirst(this);
                    return
                }
                a = this.find("*");
                this.statusbar && a.add(this.statusbar.items());
                a.each(function(a) {
                    if (a.settings.autofocus) return b = null, !1;
                    a.canFocus && (b = b || a)
                });
                b && b.focus();
                return this
            },
            replace: function(a, b) {
                var c;
                c = this.items();
                for (var d = c.length; d--;)
                    if (c[d] === a) {
                        c[d] = b;
                        break
                    }
                0 <= d && ((c = b.getEl()) && c.parentNode.removeChild(c), (c = a.getEl()) && c.parentNode.removeChild(c));
                b.parent(this)
            },
            create: function(a) {
                var b = this,
                    d, f = [];
                c.isArray(a) || (a = [a]);
                c.each(a,
                    function(a) {
                        a && (a instanceof k || ("string" == typeof a && (a = {
                            type: a
                        }), d = c.extend({}, b.settings.defaults, a), a.type = d.type = d.type || a.type || b.settings.defaultType || (d.defaults ? d.defaults.type : null), a = e.create(d)), f.push(a))
                    });
                return f
            },
            renderNew: function() {
                var a = this;
                a.items().each(function(c, d) {
                    var e;
                    c.parent(a);
                    c.state.get("rendered") || (e = a.getEl("body"), e.hasChildNodes() && d <= e.childNodes.length - 1 ? b(e.childNodes[d]).before(c.renderHtml()) : b(e).append(c.renderHtml()), c.postRender(), l.add(c))
                });
                a._layout.applyClasses(a.items().filter(":visible"));
                a._lastRect = null;
                return a
            },
            append: function(a) {
                return this.add(a).renderNew()
            },
            prepend: function(a) {
                this.items().set(this.create(a).concat(this.items().toArray()));
                return this.renderNew()
            },
            insert: function(a, b, c) {
                var d;
                a = this.create(a);
                d = this.items();
                !c && b < d.length - 1 && (b += 1);
                0 <= b && b < d.length && (c = d.slice(0, b).toArray(), b = d.slice(b).toArray(), d.set(c.concat(a, b)));
                return this.renderNew()
            },
            fromJSON: function(a) {
                for (var b in a) this.find("#" + b).value(a[b]);
                return this
            },
            toJSON: function() {
                var a = {};
                this.find("*").each(function(b) {
                    var c =
                        b.name();
                    b = b.value();
                    c && "undefined" != typeof b && (a[c] = b)
                });
                return a
            },
            renderHtml: function() {
                var a = this._layout,
                    b = this.settings.role;
                this.preRender();
                a.preRender(this);
                return '\x3cdiv id\x3d"' + this._id + '" class\x3d"' + this.classes + '"' + (b ? ' role\x3d"' + this.settings.role + '"' : "") + '\x3e\x3cdiv id\x3d"' + this._id + '-body" class\x3d"' + this.bodyClasses + '"\x3e' + (this.settings.html || "") + a.renderHtml(this) + "\x3c/div\x3e\x3c/div\x3e"
            },
            postRender: function() {
                var b;
                this.items().exec("postRender");
                this._super();
                this._layout.postRender(this);
                this.state.set("rendered", !0);
                this.settings.style && this.$el.css(this.settings.style);
                this.settings.border && (b = this.borderBox, this.$el.css({
                    "border-top-width": b.top,
                    "border-right-width": b.right,
                    "border-bottom-width": b.bottom,
                    "border-left-width": b.left
                }));
                this.parent() || (this.keyboardNav = new a({
                    root: this
                }));
                return this
            },
            initLayoutRect: function() {
                var a = this._super();
                this._layout.recalc(this);
                return a
            },
            recalc: function() {
                var a = this._layoutRect,
                    b = this._lastRect;
                if (!b || b.w != a.w || b.h != a.h) return this._layout.recalc(this),
                    a = this.layoutRect(), this._lastRect = {
                        x: a.x,
                        y: a.y,
                        w: a.w,
                        h: a.h
                    }, !0
            },
            reflow: function() {
                var a;
                l.remove(this);
                if (this.visible()) {
                    k.repaintControls = [];
                    k.repaintControls.map = {};
                    this.recalc();
                    for (a = k.repaintControls.length; a--;) k.repaintControls[a].repaint();
                    "flow" !== this.settings.layout && "stack" !== this.settings.layout && this.repaint();
                    k.repaintControls = []
                }
                return this
            }
        })
    });
    z("tinymce/ui/DragHelper", ["tinymce/dom/DomQuery"], function(k) {
        function g(f) {
            var e, a;
            if (f.changedTouches)
                for (e = "screenX screenY pageX pageY clientX clientY".split(" "),
                    a = 0; a < e.length; a++) f[e[a]] = f.changedTouches[0][e[a]]
        }
        return function(f, e) {
            var a, c = e.document || document,
                b, d, l, v, q;
            e = e || {};
            l = function(a) {
                g(a);
                if (a.button !== b) return d(a);
                a.deltaX = a.screenX - v;
                a.deltaY = a.screenY - q;
                a.preventDefault();
                e.drag(a)
            };
            d = function(b) {
                g(b);
                k(c).off("mousemove touchmove", l).off("mouseup touchend", d);
                a.remove();
                e.stop && e.stop(b)
            };
            this.destroy = function() {
                k(c.getElementById(e.handle || f)).off()
            };
            k(c.getElementById(e.handle || f)).on("mousedown touchstart", function(h) {
                var n, p, w, A, m, t, y,
                    B = Math.max;
                w = c.documentElement;
                A = c.body;
                n = B(w.scrollWidth, A.scrollWidth);
                m = B(w.clientWidth, A.clientWidth);
                t = B(w.offsetWidth, A.offsetWidth);
                p = B(w.scrollHeight, A.scrollHeight);
                y = B(w.clientHeight, A.clientHeight);
                w = B(w.offsetHeight, A.offsetHeight);
                n = n < t ? m : n;
                p = p < w ? y : p;
                g(h);
                h.preventDefault();
                b = h.button;
                y = c.getElementById(e.handle || f);
                v = h.screenX;
                q = h.screenY;
                y = window.getComputedStyle ? window.getComputedStyle(y, null).getPropertyValue("cursor") : y.runtimeStyle.cursor;
                a = k("\x3cdiv\x3e").css({
                    position: "absolute",
                    top: 0,
                    left: 0,
                    width: n,
                    height: p,
                    zIndex: 2147483647,
                    opacity: .0001,
                    cursor: y
                }).appendTo(c.body);
                k(c).on("mousemove touchmove", l).on("mouseup touchend", d);
                e.start(h)
            })
        }
    });
    z("tinymce/ui/Scrollable", ["tinymce/dom/DomQuery", "tinymce/ui/DragHelper"], function(k, g) {
        return {
            init: function() {
                this.on("repaint", this.renderScroll)
            },
            renderScroll: function() {
                function f() {
                    function c(c, e, f, g, l, w) {
                        var h, m, n;
                        if (h = a.getEl("scroll" + c)) m = e.toLowerCase(), n = f.toLowerCase(), k(a.getEl("absend")).css(m, a.layoutRect()[g] - 1), l ? (k(h).css("display",
                            "block"), g = a.getEl("body"), c = a.getEl("scroll" + c + "t"), l = g["client" + f] - 4, l -= b && d ? h["client" + w] : 0, f = g["scroll" + f], f = l / f, w = {}, w[m] = g["offset" + e] + 2, w[n] = l, k(h).css(w), w = {}, w[m] = g["scroll" + e] * f, w[n] = l * f, k(c).css(w)) : k(h).css("display", "none")
                    }
                    var b, d, e;
                    e = a.getEl("body");
                    b = e.scrollWidth > e.clientWidth;
                    d = e.scrollHeight > e.clientHeight;
                    c("h", "Left", "Width", "contentW", b, "Height");
                    c("v", "Top", "Height", "contentH", d, "Width")
                }

                function e() {
                    function c(b, c, e, f, q) {
                        var d, l = a._id + "-scroll" + b,
                            p = a.classPrefix;
                        k(a.getEl()).append('\x3cdiv id\x3d"' +
                            l + '" class\x3d"' + p + "scrollbar " + p + "scrollbar-" + b + '"\x3e\x3cdiv id\x3d"' + l + 't" class\x3d"' + p + 'scrollbar-thumb"\x3e\x3c/div\x3e\x3c/div\x3e');
                        a.draghelper = new g(l + "t", {
                            start: function() {
                                d = a.getEl("body")["scroll" + c];
                                k("#" + l).addClass(p + "active")
                            },
                            drag: function(g) {
                                var l, h, n;
                                h = a.layoutRect();
                                l = h.contentW > h.innerW;
                                h = h.contentH > h.innerH;
                                n = a.getEl("body")["client" + e] - 4;
                                n -= l && h ? a.getEl("scroll" + b)["client" + q] : 0;
                                l = n / a.getEl("body")["scroll" + e];
                                a.getEl("body")["scroll" + c] = d + g["delta" + f] / l
                            },
                            stop: function() {
                                k("#" +
                                    l).removeClass(p + "active")
                            }
                        })
                    }
                    a.classes.add("scroll");
                    c("v", "Top", "Height", "Y", "Width");
                    c("h", "Left", "Width", "X", "Height")
                }
                var a = this;
                a.settings.autoScroll && (a._hasScroll || (a._hasScroll = !0, e(), a.on("wheel", function(c) {
                    var b = a.getEl("body");
                    b.scrollLeft += 10 * (c.deltaX || 0);
                    b.scrollTop += 10 * c.deltaY;
                    f()
                }), k(a.getEl("body")).on("scroll", f)), f())
            }
        }
    });
    z("tinymce/ui/Panel", ["tinymce/ui/Container", "tinymce/ui/Scrollable"], function(k, g) {
        return k.extend({
            Defaults: {
                layout: "fit",
                containerCls: "panel"
            },
            Mixins: [g],
            renderHtml: function() {
                var f = this._layout,
                    e = this.settings.html;
                this.preRender();
                f.preRender(this);
                "undefined" == typeof e ? e = '\x3cdiv id\x3d"' + this._id + '-body" class\x3d"' + this.bodyClasses + '"\x3e' + f.renderHtml(this) + "\x3c/div\x3e" : ("function" == typeof e && (e = e.call(this)), this._hasBody = !1);
                return '\x3cdiv id\x3d"' + this._id + '" class\x3d"' + this.classes + '" hidefocus\x3d"1" tabindex\x3d"-1" role\x3d"group"\x3e' + (this._preBodyHtml || "") + e + "\x3c/div\x3e"
            }
        })
    });
    z("tinymce/ui/Movable", ["tinymce/ui/DomUtils"], function(k) {
        function g(f,
            e, a) {
            var c, b, d, g;
            d = k.getViewPort();
            c = k.getPos(e);
            b = c.x;
            c = c.y;
            f.state.get("fixed") && "static" == k.getRuntimeStyle(document.body, "position") && (b -= d.x, c -= d.y);
            f = f.getEl();
            g = k.getSize(f);
            f = g.width;
            d = g.height;
            g = k.getSize(e);
            e = g.width;
            g = g.height;
            a = (a || "").split("");
            "b" === a[0] && (c += g);
            "r" === a[1] && (b += e);
            "c" === a[0] && (c += Math.round(g / 2));
            "c" === a[1] && (b += Math.round(e / 2));
            "b" === a[3] && (c -= d);
            "r" === a[4] && (b -= f);
            "c" === a[3] && (c -= Math.round(d / 2));
            "c" === a[4] && (b -= Math.round(f / 2));
            return {
                x: b,
                y: c,
                w: f,
                h: d
            }
        }
        return {
            testMoveRel: function(f,
                e) {
                for (var a = k.getViewPort(), c = 0; c < e.length; c++) {
                    var b = g(this, f, e[c]);
                    if (this.state.get("fixed")) {
                        if (0 < b.x && b.x + b.w < a.w && 0 < b.y && b.y + b.h < a.h) return e[c]
                    } else if (b.x > a.x && b.x + b.w < a.w + a.x && b.y > a.y && b.y + b.h < a.h + a.y) return e[c]
                }
                return e[0]
            },
            moveRel: function(f, e) {
                "string" != typeof e && (e = this.testMoveRel(f, e));
                f = g(this, f, e);
                return this.moveTo(f.x, f.y)
            },
            moveBy: function(f, e) {
                var a = this.layoutRect();
                this.moveTo(a.x + f, a.y + e);
                return this
            },
            moveTo: function(f, e) {
                function a(a, b, c) {
                    return 0 > a ? 0 : a + c > b ? (a = b - c, 0 > a ? 0 :
                        a) : a
                }
                if (this.settings.constrainToViewport) {
                    var c = k.getViewPort(window),
                        b = this.layoutRect();
                    f = a(f, c.w + c.x, b.w);
                    e = a(e, c.h + c.y, b.h)
                }
                this.state.get("rendered") ? this.layoutRect({
                    x: f,
                    y: e
                }).repaint() : (this.settings.x = f, this.settings.y = e);
                this.fire("move", {
                    x: f,
                    y: e
                });
                return this
            }
        }
    });
    z("tinymce/ui/Resizable", ["tinymce/ui/DomUtils"], function(k) {
        return {
            resizeToContent: function() {
                this._layoutRect.autoResize = !0;
                this._lastRect = null;
                this.reflow()
            },
            resizeTo: function(g, f) {
                if (1 >= g || 1 >= f) {
                    var e = k.getWindowSize();
                    g = 1 >=
                        g ? g * e.w : g;
                    f = 1 >= f ? f * e.h : f
                }
                this._layoutRect.autoResize = !1;
                return this.layoutRect({
                    minW: g,
                    minH: f,
                    w: g,
                    h: f
                }).reflow()
            },
            resizeBy: function(g, f) {
                var e = this.layoutRect();
                return this.resizeTo(e.w + g, e.h + f)
            }
        }
    });
    z("tinymce/ui/FloatPanel", "tinymce/ui/Panel tinymce/ui/Movable tinymce/ui/Resizable tinymce/ui/DomUtils tinymce/dom/DomQuery tinymce/util/Delay".split(" "), function(k, g, f, e, a, c) {
        function b() {
            n || (n = function(a) {
                if (2 != a.button)
                    for (var b = A.length; b--;) {
                        var c = A[b],
                            d = c.getParentCtrl(a.target),
                            e;
                        if ((e = c.settings.autohide) &&
                            !(e = !d)) {
                            a: {
                                e = d;
                                for (var f = c; e;) {
                                    if (e == f) {
                                        e = !0;
                                        break a
                                    }
                                    e = e.parent()
                                }
                                e = void 0
                            }
                            e = !e && c.parent() !== d
                        }
                        e && (a = c.fire("autohide", {
                            target: a.target
                        }), a.isDefaultPrevented() || c.hide())
                    }
            }, a(document).on("click touchstart", n))
        }

        function d() {
            p || (p = function() {
                var a;
                for (a = A.length; a--;) v(A[a])
            }, a(window).on("scroll", p))
        }

        function l() {
            if (!w) {
                var b = document.documentElement,
                    c = b.clientWidth,
                    d = b.clientHeight;
                w = function() {
                    document.all && c == b.clientWidth && d == b.clientHeight || (c = b.clientWidth, d = b.clientHeight, y.hideAll())
                };
                a(window).on("resize", w)
            }
        }

        function v(a) {
            function b(b, c) {
                for (var d, e = 0; e < A.length; e++)
                    if (A[e] != a)
                        for (d = A[e].parent(); d && (d = d.parent());) d == a && A[e].fixed(b).moveBy(0, c).repaint()
            }
            var c = e.getViewPort().y;
            a.settings.autofix && (a.state.get("fixed") ? a._autoFixY > c && (a.fixed(!1).layoutRect({
                y: a._autoFixY
            }).repaint(), b(!1, a._autoFixY - c)) : (a._autoFixY = a.layoutRect().y, a._autoFixY < c && (a.fixed(!0).layoutRect({
                y: 0
            }).repaint(), b(!0, c - a._autoFixY))))
        }

        function q(b, c) {
            var d = y.zIndex || 65535,
                e;
            if (b) m.push(c);
            else
                for (b =
                    m.length; b--;) m[b] === c && m.splice(b, 1);
            if (m.length)
                for (b = 0; b < m.length; b++) m[b].modal && (d++, e = m[b]), m[b].getEl().style.zIndex = d, m[b].zIndex = d, d++;
            c = a("#" + c.classPrefix + "modal-block", c.getContainerElm())[0];
            e ? a(c).css("z-index", e.zIndex - 1) : c && (c.parentNode.removeChild(c), t = !1);
            y.currentZIndex = d
        }

        function h(a) {
            var b;
            for (b = A.length; b--;) A[b] === a && A.splice(b, 1);
            for (b = m.length; b--;) m[b] === a && m.splice(b, 1)
        }
        var n, p, w, A = [],
            m = [],
            t, y = k.extend({
                Mixins: [g, f],
                init: function(e) {
                    var f = this;
                    f._super(e);
                    f._eventsRoot =
                        f;
                    f.classes.add("floatpanel");
                    e.autohide && (b(), l(), A.push(f));
                    e.autofix && (d(), f.on("move", function() {
                        v(this)
                    }));
                    f.on("postrender show", function(b) {
                        if (b.control == f) {
                            var d, e = f.classPrefix;
                            f.modal && !t && (d = a("#" + e + "modal-block", f.getContainerElm()), d[0] || (d = a('\x3cdiv id\x3d"' + e + 'modal-block" class\x3d"' + e + "reset " + e + 'fade"\x3e\x3c/div\x3e').appendTo(f.getContainerElm())), c.setTimeout(function() {
                                d.addClass(e + "in");
                                a(f.getEl()).addClass(e + "in")
                            }), t = !0);
                            q(!0, f)
                        }
                    });
                    f.on("show", function() {
                        f.parents().each(function(a) {
                            if (a.state.get("fixed")) return f.fixed(!0), !1
                        })
                    });
                    e.popover && (f._preBodyHtml = '\x3cdiv class\x3d"' + f.classPrefix + 'arrow"\x3e\x3c/div\x3e', f.classes.add("popover").add("bottom").add(f.isRtl() ? "end" : "start"));
                    f.aria("label", e.ariaLabel);
                    f.aria("labelledby", f._id);
                    f.aria("describedby", f.describedBy || f._id + "-none")
                },
                fixed: function(a) {
                    if (this.state.get("fixed") != a) {
                        if (this.state.get("rendered")) {
                            var b = e.getViewPort();
                            a ? this.layoutRect().y -= b.y : this.layoutRect().y += b.y
                        }
                        this.classes.toggle("fixed", a);
                        this.state.set("fixed", a)
                    }
                    return this
                },
                show: function() {
                    var a,
                        b = this._super();
                    for (a = A.length; a-- && A[a] !== this;); - 1 === a && A.push(this);
                    return b
                },
                hide: function() {
                    h(this);
                    q(!1, this);
                    return this._super()
                },
                hideAll: function() {
                    y.hideAll()
                },
                close: function() {
                    this.fire("close").isDefaultPrevented() || (this.remove(), q(!1, this));
                    return this
                },
                remove: function() {
                    h(this);
                    this._super()
                },
                postRender: function() {
                    this.settings.bodyRole && this.getEl("body").setAttribute("role", this.settings.bodyRole);
                    return this._super()
                }
            });
        y.hideAll = function() {
            for (var a = A.length; a--;) {
                var b = A[a];
                b &&
                    b.settings.autohide && (b.hide(), A.splice(a, 1))
            }
        };
        return y
    });
    z("tinymce/ui/Window", "tinymce/ui/FloatPanel tinymce/ui/Panel tinymce/ui/DomUtils tinymce/dom/DomQuery tinymce/ui/DragHelper tinymce/ui/BoxUtils tinymce/Env tinymce/util/Delay".split(" "), function(k, g, f, e, a, c, b, d) {
        function l(a) {
            var c = e("meta[name\x3dviewport]")[0],
                d;
            !1 !== b.overrideViewPort && (c || (c = document.createElement("meta"), c.setAttribute("name", "viewport"), document.getElementsByTagName("head")[0].appendChild(c)), (d = c.getAttribute("content")) &&
                "undefined" != typeof q && (q = d), c.setAttribute("content", a ? "width\x3ddevice-width,initial-scale\x3d1.0,user-scalable\x3d0,minimum-scale\x3d1.0,maximum-scale\x3d1.0" : q))
        }
        var v = [],
            q = "";
        k = k.extend({
            modal: !0,
            Defaults: {
                border: 1,
                layout: "flex",
                containerCls: "panel",
                role: "dialog",
                callbacks: {
                    submit: function() {
                        this.fire("submit", {
                            data: this.toJSON()
                        })
                    },
                    close: function() {
                        this.close()
                    }
                }
            },
            init: function(a) {
                var b = this;
                b._super(a);
                b.isRtl() && b.classes.add("rtl");
                b.classes.add("window");
                b.bodyClasses.add("window-body");
                b.state.set("fixed", !0);
                a.buttons && (b.statusbar = new g({
                    layout: "flex",
                    border: "1 0 0 0",
                    spacing: 3,
                    padding: 10,
                    align: "center",
                    pack: b.isRtl() ? "start" : "end",
                    defaults: {
                        type: "button"
                    },
                    items: a.buttons
                }), b.statusbar.classes.add("foot"), b.statusbar.parent(b));
                b.on("click", function(a) {
                    var c = b.classPrefix + "close";
                    (f.hasClass(a.target, c) || f.hasClass(a.target.parentNode, c)) && b.close()
                });
                b.on("cancel", function() {
                    b.close()
                });
                b.aria("describedby", b.describedBy || b._id + "-none");
                b.aria("label", a.title);
                b._fullscreen = !1
            },
            recalc: function() {
                var a = this.statusbar,
                    b, c, d;
                this._fullscreen && (this.layoutRect(f.getWindowSize()), this.layoutRect().contentH = this.layoutRect().innerH);
                this._super();
                b = this.layoutRect();
                this.settings.title && !this._fullscreen && (c = b.headerW, c > b.w && (d = b.x - Math.max(0, c / 2), this.layoutRect({
                    w: c,
                    x: d
                }), d = !0));
                a && (a.layoutRect({
                    w: this.layoutRect().innerW
                }).recalc(), c = a.layoutRect().minW + b.deltaW, c > b.w && (d = b.x - Math.max(0, c - b.w), this.layoutRect({
                    w: c,
                    x: d
                }), d = !0));
                d && this.recalc()
            },
            initLayoutRect: function() {
                var a =
                    this._super(),
                    b = 0,
                    c;
                this.settings.title && !this._fullscreen && (c = this.getEl("head"), c = f.getSize(c), a.headerW = c.width, a.headerH = c.height, b += a.headerH);
                this.statusbar && (b += this.statusbar.layoutRect().h);
                a.deltaH += b;
                a.minH += b;
                a.h += b;
                b = f.getWindowSize();
                a.x = this.settings.x || Math.max(0, b.w / 2 - a.w / 2);
                a.y = this.settings.y || Math.max(0, b.h / 2 - a.h / 2);
                return a
            },
            renderHtml: function() {
                var a = this._layout,
                    b = this._id,
                    c = this.classPrefix,
                    d = this.settings,
                    e = "",
                    f = "",
                    g = d.html;
                this.preRender();
                a.preRender(this);
                d.title && (e =
                    '\x3cdiv id\x3d"' + b + '-head" class\x3d"' + c + 'window-head"\x3e\x3cdiv id\x3d"' + b + '-title" class\x3d"' + c + 'title"\x3e' + this.encode(d.title) + '\x3c/div\x3e\x3cdiv id\x3d"' + b + '-dragh" class\x3d"' + c + 'dragh"\x3e\x3c/div\x3e\x3cbutton type\x3d"button" class\x3d"' + c + 'close" aria-hidden\x3d"true"\x3e\x3ci class\x3d"mce-ico mce-i-remove"\x3e\x3c/i\x3e\x3c/button\x3e\x3c/div\x3e');
                d.url && (g = '\x3ciframe src\x3d"' + d.url + '" tabindex\x3d"-1"\x3e\x3c/iframe\x3e');
                "undefined" == typeof g && (g = a.renderHtml(this));
                this.statusbar &&
                    (f = this.statusbar.renderHtml());
                return '\x3cdiv id\x3d"' + b + '" class\x3d"' + this.classes + '" hidefocus\x3d"1"\x3e\x3cdiv class\x3d"' + this.classPrefix + 'reset" role\x3d"application"\x3e' + e + '\x3cdiv id\x3d"' + b + '-body" class\x3d"' + this.bodyClasses + '"\x3e' + g + "\x3c/div\x3e" + f + "\x3c/div\x3e\x3c/div\x3e"
            },
            fullscreen: function(a) {
                var b = this,
                    g = document.documentElement,
                    l, h = b.classPrefix,
                    m;
                a != b._fullscreen && (e(window).on("resize", function() {
                    var a;
                    if (b._fullscreen)
                        if (l) b._timer || (b._timer = d.setTimeout(function() {
                            var a =
                                f.getWindowSize();
                            b.moveTo(0, 0).resizeTo(a.w, a.h);
                            b._timer = 0
                        }, 50));
                        else {
                            a = (new Date).getTime();
                            var c = f.getWindowSize();
                            b.moveTo(0, 0).resizeTo(c.w, c.h);
                            50 < (new Date).getTime() - a && (l = !0)
                        }
                }), m = b.layoutRect(), (b._fullscreen = a) ? (b._initial = {
                    x: m.x,
                    y: m.y,
                    w: m.w,
                    h: m.h
                }, b.borderBox = c.parseBox("0"), b.getEl("head").style.display = "none", m.deltaH -= m.headerH + 2, e([g, document.body]).addClass(h + "fullscreen"), b.classes.add("fullscreen"), a = f.getWindowSize(), b.moveTo(0, 0).resizeTo(a.w, a.h)) : (b.borderBox = c.parseBox(b.settings.border),
                    b.getEl("head").style.display = "", m.deltaH += m.headerH, e([g, document.body]).removeClass(h + "fullscreen"), b.classes.remove("fullscreen"), b.moveTo(b._initial.x, b._initial.y).resizeTo(b._initial.w, b._initial.h)));
                return b.reflow()
            },
            postRender: function() {
                var b, c, d = this;
                setTimeout(function() {
                    d.classes.add("in");
                    d.fire("open")
                }, 0);
                d._super();
                d.statusbar && d.statusbar.postRender();
                d.focus();
                this.dragHelper = new a(d._id + "-dragh", {
                    start: function() {
                        b = d.layoutRect().x;
                        c = d.layoutRect().y
                    },
                    drag: function(a) {
                        d.moveTo(b +
                            a.deltaX, c + a.deltaY)
                    }
                });
                d.on("submit", function(a) {
                    a.isDefaultPrevented() || d.close()
                });
                v.push(d);
                l(!0)
            },
            submit: function() {
                return this.fire("submit", {
                    data: this.toJSON()
                })
            },
            remove: function() {
                var a;
                this.dragHelper.destroy();
                this._super();
                this.statusbar && this.statusbar.remove();
                for (a = v.length; a--;) v[a] === this && v.splice(a, 1);
                l(0 < v.length);
                a: {
                    a = this.classPrefix;
                    for (var b = 0; b < v.length; b++)
                        if (v[b]._fullscreen) break a;e([document.documentElement, document.body]).removeClass(a + "fullscreen")
                }
            },
            getContentWindow: function() {
                var a =
                    this.getEl().getElementsByTagName("iframe")[0];
                return a ? a.contentWindow : null
            }
        });
        (function() {
            if (!b.desktop) {
                var a = window.innerWidth,
                    c = window.innerHeight;
                d.setInterval(function() {
                    var b = window.innerWidth,
                        d = window.innerHeight;
                    if (a != b || c != d) a = b, c = d, e(window).trigger("resize")
                }, 100)
            }
            e(window).on("resize", function() {
                var a, b = f.getWindowSize(),
                    c;
                for (a = 0; a < v.length; a++) c = v[a].layoutRect(), v[a].moveTo(v[a].settings.x || Math.max(0, b.w / 2 - c.w / 2), v[a].settings.y || Math.max(0, b.h / 2 - c.h / 2))
            })
        })();
        return k
    });
    z("tinymce/ui/MessageBox", ["tinymce/ui/Window"], function(k) {
        var g = k.extend({
            init: function(f) {
                f = {
                    border: 1,
                    padding: 20,
                    layout: "flex",
                    pack: "center",
                    align: "center",
                    containerCls: "panel",
                    autoScroll: !0,
                    buttons: {
                        type: "button",
                        text: "Ok",
                        action: "ok"
                    },
                    items: {
                        type: "label",
                        multiline: !0,
                        maxWidth: 500,
                        maxHeight: 200
                    }
                };
                this._super(f)
            },
            Statics: {
                OK: 1,
                OK_CANCEL: 2,
                YES_NO: 3,
                YES_NO_CANCEL: 4,
                msgBox: function(f) {
                    function e(a, d, e) {
                        return {
                            type: "button",
                            text: a,
                            subtype: e ? "primary" : "",
                            onClick: function(a) {
                                a.control.parents()[1].close();
                                c(d)
                            }
                        }
                    }
                    var a, c = f.callback ||
                        function() {};
                    switch (f.buttons) {
                        case g.OK_CANCEL:
                            a = [e("Ok", !0, !0), e("Cancel", !1)];
                            break;
                        case g.YES_NO:
                        case g.YES_NO_CANCEL:
                            a = [e("Yes", 1, !0), e("No", 0)];
                            f.buttons == g.YES_NO_CANCEL && a.push(e("Cancel", -1));
                            break;
                        default:
                            a = [e("Ok", !0, !0)]
                    }
                    return (new k({
                        padding: 20,
                        x: f.x,
                        y: f.y,
                        minWidth: 300,
                        minHeight: 100,
                        layout: "flex",
                        pack: "center",
                        align: "center",
                        buttons: a,
                        title: f.title,
                        role: "alertdialog",
                        items: {
                            type: "label",
                            multiline: !0,
                            maxWidth: 500,
                            maxHeight: 200,
                            text: f.text
                        },
                        onPostRender: function() {
                            this.aria("describedby",
                                this.items()[0]._id)
                        },
                        onClose: f.onClose,
                        onCancel: function() {
                            c(!1)
                        }
                    })).renderTo(document.body).reflow()
                },
                alert: function(f, e) {
                    "string" == typeof f && (f = {
                        text: f
                    });
                    f.callback = e;
                    return g.msgBox(f)
                },
                confirm: function(f, e) {
                    "string" == typeof f && (f = {
                        text: f
                    });
                    f.callback = e;
                    f.buttons = g.OK_CANCEL;
                    return g.msgBox(f)
                }
            }
        });
        return g
    });
    z("tinymce/WindowManager", ["tinymce/ui/Window", "tinymce/ui/MessageBox"], function(k, g) {
        return function(f) {
            function e() {
                if (b.length) return b[b.length - 1]
            }

            function a(a) {
                f.fire("OpenWindow", {
                    win: a
                })
            }

            function c(a) {
                f.fire("CloseWindow", {
                    win: a
                })
            }
            var b = [];
            this.windows = b;
            f.on("remove", function() {
                for (var a = b.length; a--;) b[a].close()
            });
            this.open = function(d, e) {
                var g;
                f.editorManager.setActive(f);
                d.title = d.title || " ";
                d.url = d.url || d.file;
                d.url && (d.width = parseInt(d.width || 320, 10), d.height = parseInt(d.height || 240, 10));
                d.body && (d.items = {
                    defaults: d.defaults,
                    type: d.bodyType || "form",
                    items: d.body,
                    data: d.data,
                    callbacks: d.commands
                });
                d.url || d.buttons || (d.buttons = [{
                        text: "Ok",
                        subtype: "primary",
                        onclick: function() {
                            g.find("form")[0].submit()
                        }
                    },
                    {
                        text: "Cancel",
                        onclick: function() {
                            g.close()
                        }
                    }
                ]);
                g = new k(d);
                b.push(g);
                g.on("close", function() {
                    for (var a = b.length; a--;) b[a] === g && b.splice(a, 1);
                    b.length || f.focus();
                    c(g)
                });
                if (d.data) g.on("postRender", function() {
                    this.find("*").each(function(a) {
                        var b = a.name();
                        b in d.data && a.value(d.data[b])
                    })
                });
                g.features = d || {};
                g.params = e || {};
                1 === b.length && f.nodeChanged();
                g = g.renderTo().reflow();
                a(g);
                return g
            };
            this.alert = function(b, e, k) {
                var d;
                d = g.alert(b, function() {
                    e ? e.call(k || this) : f.focus()
                });
                d.on("close", function() {
                    c(d)
                });
                a(d)
            };
            this.confirm = function(b, e, f) {
                var d;
                d = g.confirm(b, function(a) {
                    e.call(f || this, a)
                });
                d.on("close", function() {
                    c(d)
                });
                a(d)
            };
            this.close = function() {
                e() && e().close()
            };
            this.getParams = function() {
                return e() ? e().params : null
            };
            this.setParams = function(a) {
                e() && (e().params = a)
            };
            this.getWindows = function() {
                return b
            }
        }
    });
    z("tinymce/ui/Tooltip", ["tinymce/ui/Control", "tinymce/ui/Movable"], function(k, g) {
        return k.extend({
            Mixins: [g],
            Defaults: {
                classes: "widget tooltip tooltip-n"
            },
            renderHtml: function() {
                var f = this.classPrefix;
                return '\x3cdiv id\x3d"' + this._id + '" class\x3d"' + this.classes + '" role\x3d"presentation"\x3e\x3cdiv class\x3d"' + f + 'tooltip-arrow"\x3e\x3c/div\x3e\x3cdiv class\x3d"' + f + 'tooltip-inner"\x3e' + this.encode(this.state.get("text")) + "\x3c/div\x3e\x3c/div\x3e"
            },
            bindStates: function() {
                var f = this;
                f.state.on("change:text", function(e) {
                    f.getEl().lastChild.innerHTML = f.encode(e.value)
                });
                return f._super()
            },
            repaint: function() {
                var f, e;
                f = this.getEl().style;
                e = this._layoutRect;
                f.left = e.x + "px";
                f.top = e.y + "px";
                f.zIndex = 131070
            }
        })
    });
    z("tinymce/ui/Widget", ["tinymce/ui/Control", "tinymce/ui/Tooltip"], function(k, g) {
        var f, e = k.extend({
            init: function(a) {
                var c = this;
                c._super(a);
                a = c.settings;
                c.canFocus = !0;
                a.tooltip && !1 !== e.tooltips && (c.on("mouseenter", function(b) {
                        var d = c.tooltip().moveTo(-65535);
                        b.control == c ? (b = d.text(a.tooltip).show().testMoveRel(c.getEl(), ["bc-tc", "bc-tl", "bc-tr"]), d.classes.toggle("tooltip-n", "bc-tc" == b), d.classes.toggle("tooltip-nw", "bc-tl" == b), d.classes.toggle("tooltip-ne", "bc-tr" == b), d.moveRel(c.getEl(), b)) : d.hide()
                    }),
                    c.on("mouseleave mousedown click", function() {
                        c.tooltip().hide()
                    }));
                c.aria("label", a.ariaLabel || a.tooltip)
            },
            tooltip: function() {
                f || (f = new g({
                    type: "tooltip"
                }), f.renderTo());
                return f
            },
            postRender: function() {
                var a = this.settings;
                this._super();
                this.parent() || !a.width && !a.height || (this.initLayoutRect(), this.repaint());
                a.autofocus && this.focus()
            },
            bindStates: function() {
                function a(a) {
                    b.aria("disabled", a);
                    b.classes.toggle("disabled", a)
                }

                function c(a) {
                    b.aria("pressed", a);
                    b.classes.toggle("active", a)
                }
                var b = this;
                b.state.on("change:disabled",
                    function(b) {
                        a(b.value)
                    });
                b.state.on("change:active", function(a) {
                    c(a.value)
                });
                b.state.get("disabled") && a(!0);
                b.state.get("active") && c(!0);
                return b._super()
            },
            remove: function() {
                this._super();
                f && (f.remove(), f = null)
            }
        });
        return e
    });
    z("tinymce/ui/Progress", ["tinymce/ui/Widget"], function(k) {
        return k.extend({
            Defaults: {
                value: 0
            },
            init: function(g) {
                this._super(g);
                this.classes.add("progress");
                this.settings.filter || (this.settings.filter = function(f) {
                    return Math.round(f)
                })
            },
            renderHtml: function() {
                var g = this.classPrefix;
                return '\x3cdiv id\x3d"' + this._id + '" class\x3d"' + this.classes + '"\x3e\x3cdiv class\x3d"' + g + 'bar-container"\x3e\x3cdiv class\x3d"' + g + 'bar"\x3e\x3c/div\x3e\x3c/div\x3e\x3cdiv class\x3d"' + g + 'text"\x3e0%\x3c/div\x3e\x3c/div\x3e'
            },
            postRender: function() {
                this._super();
                this.value(this.settings.value);
                return this
            },
            bindStates: function() {
                function g(e) {
                    e = f.settings.filter(e);
                    f.getEl().lastChild.innerHTML = e + "%";
                    f.getEl().firstChild.firstChild.style.width = e + "%"
                }
                var f = this;
                f.state.on("change:value", function(e) {
                    g(e.value)
                });
                g(f.state.get("value"));
                return f._super()
            }
        })
    });
    z("tinymce/ui/Notification", ["tinymce/ui/Control", "tinymce/ui/Movable", "tinymce/ui/Progress", "tinymce/util/Delay"], function(k, g, f, e) {
        return k.extend({
            Mixins: [g],
            Defaults: {
                classes: "widget notification"
            },
            init: function(a) {
                var c = this;
                c._super(a);
                a.text && c.text(a.text);
                a.icon && (c.icon = a.icon);
                a.color && (c.color = a.color);
                a.type && c.classes.add("notification-" + a.type);
                a.timeout && (0 > a.timeout || 0 < a.timeout) && !a.closeButton ? c.closeButton = !1 : (c.classes.add("has-close"),
                    c.closeButton = !0);
                a.progressBar && (c.progressBar = new f);
                c.on("click", function(a) {
                    -1 != a.target.className.indexOf(c.classPrefix + "close") && c.close()
                })
            },
            renderHtml: function() {
                var a = this.classPrefix,
                    c = "",
                    b = "",
                    d = "",
                    e = "";
                this.icon && (c = '\x3ci class\x3d"' + a + "ico " + a + "i-" + this.icon + '"\x3e\x3c/i\x3e');
                this.color && (e = ' style\x3d"background-color: ' + this.color + '"');
                this.closeButton && (b = '\x3cbutton type\x3d"button" class\x3d"' + a + 'close" aria-hidden\x3d"true"\x3e\u00d7\x3c/button\x3e');
                this.progressBar && (d = this.progressBar.renderHtml());
                return '\x3cdiv id\x3d"' + this._id + '" class\x3d"' + this.classes + '"' + e + ' role\x3d"presentation"\x3e' + c + '\x3cdiv class\x3d"' + a + 'notification-inner"\x3e' + this.state.get("text") + "\x3c/div\x3e" + d + b + "\x3c/div\x3e"
            },
            postRender: function() {
                var a = this;
                e.setTimeout(function() {
                    a.$el.addClass(a.classPrefix + "in")
                });
                return a._super()
            },
            bindStates: function() {
                var a = this;
                a.state.on("change:text", function(c) {
                    a.getEl().childNodes[1].innerHTML = c.value
                });
                a.progressBar && a.progressBar.bindStates();
                return a._super()
            },
            close: function() {
                this.fire("close").isDefaultPrevented() ||
                    this.remove();
                return this
            },
            repaint: function() {
                var a, c;
                a = this.getEl().style;
                c = this._layoutRect;
                a.left = c.x + "px";
                a.top = c.y + "px";
                a.zIndex = 131070
            }
        })
    });
    z("tinymce/NotificationManager", ["tinymce/ui/Notification", "tinymce/util/Delay"], function(k, g) {
        return function(f) {
            function e() {
                if (c.length) return c[c.length - 1]
            }

            function a() {
                if (0 < c.length) {
                    var a = c.slice(0, 1)[0],
                        d = f.inline ? f.getElement() : f.getContentAreaContainer();
                    a.moveRel(d, "tc-tc");
                    if (1 < c.length)
                        for (a = 1; a < c.length; a++) c[a].moveRel(c[a - 1].getEl(), "bc-tc")
                }
            }
            var c = [];
            this.notifications = c;
            f.on("remove", function() {
                for (var a = c.length; a--;) c[a].close()
            });
            f.on("ResizeEditor", a);
            f.on("ResizeWindow", function() {
                g.requestAnimationFrame(function() {
                    for (var b = 0; b < c.length; b++) c[b].moveTo(0, 0);
                    a()
                })
            });
            this.open = function(b) {
                var d;
                f.editorManager.setActive(f);
                d = new k(b);
                c.push(d);
                0 < b.timeout && (d.timer = setTimeout(function() {
                    d.close()
                }, b.timeout));
                d.on("close", function() {
                    var b = c.length;
                    for (d.timer && f.getWin().clearTimeout(d.timer); b--;) c[b] === d && c.splice(b, 1);
                    a()
                });
                d.renderTo();
                a();
                return d
            };
            this.close = function() {
                e() && e().close()
            };
            this.getNotifications = function() {
                return c
            };
            f.on("SkinLoaded", function() {
                var a = f.settings.service_message;
                a && f.notificationManager.open({
                    text: a,
                    type: "warning",
                    timeout: 0,
                    icon: ""
                })
            })
        }
    });
    z("tinymce/dom/NodePath", ["tinymce/dom/DOMUtils"], function(k) {
        return {
            create: function(g, f, e) {
                for (var a = []; f && f != g; f = f.parentNode) a.push(k.nodeIndex(f, e));
                return a
            },
            resolve: function(g, f) {
                var e;
                e = g;
                for (g = f.length - 1; 0 <= g; g--) {
                    e = e.childNodes;
                    if (f[g] > e.length - 1) return null;
                    e = e[f[g]]
                }
                return e
            }
        }
    });
    z("tinymce/util/Quirks", "tinymce/util/VK tinymce/dom/RangeUtils tinymce/dom/TreeWalker tinymce/dom/NodePath tinymce/html/Node tinymce/html/Entities tinymce/Env tinymce/util/Tools tinymce/util/Delay tinymce/caret/CaretContainer tinymce/caret/CaretPosition tinymce/caret/CaretWalker".split(" "), function(k, g, f, e, a, c, b, d, l, v, q, h) {
        return function(n) {
            function p(a, b) {
                try {
                    n.getDoc().execCommand(a, !1, b)
                } catch (va) {}
            }

            function w() {
                var a = n.getDoc().documentMode;
                return a ? a : 6
            }

            function A(a) {
                var b;
                a.dataTransfer && (n.selection.isCollapsed() && "IMG" == a.target.tagName && S.select(a.target), b = n.selection.getContent(), 0 < b.length && (b = "data:text/mce-internal," + escape(n.id) + "," + escape(b), a.dataTransfer.setData(ra, b)))
            }

            function m(a) {
                return a.dataTransfer && (a = a.dataTransfer.getData(ra)) && 0 <= a.indexOf("data:text/mce-internal,") ? (a = a.substr(23).split(","), {
                    id: unescape(a[0]),
                    html: unescape(a[1])
                }) : null
            }

            function t(a) {
                n.queryCommandSupported("mceInsertClipboardContent") ? n.execCommand("mceInsertClipboardContent", !1, {
                    content: a
                }) : n.execCommand("mceInsertContent", !1, a)
            }

            function y() {
                function a(a) {
                    var b = u.schema.getBlockElements(),
                        c = n.getBody();
                    if ("BR" != a.nodeName) return !1;
                    for (; a != c && !b[a.nodeName]; a = a.parentNode)
                        if (a.nextSibling) return !1;
                    return !0
                }

                function b(a, b) {
                    for (a = a.nextSibling; a && a != b; a = a.nextSibling)
                        if ((3 != a.nodeType || 0 !== fa.trim(a.data).length) && a !== b) return !1;
                    return a === b
                }

                function c(b, c, d) {
                    var e;
                    e = u.schema.getNonEmptyElements();
                    for (b = new f(d || b, b); d = b[c ? "next" : "prev"]();)
                        if (e[d.nodeName] && !a(d) || 3 == d.nodeType &&
                            0 < d.data.length) return d
                }

                function h(a, d) {
                    var e, f, l, m;
                    if (!a.collapsed) return a;
                    e = a.startContainer;
                    f = a.startOffset;
                    if (3 == e.nodeType)
                        if (d) {
                            if (f < e.data.length) return a
                        } else if (0 < f) return a;
                    e = g.getNode(a.startContainer, a.startOffset);
                    l = u.getParent(e, u.isBlock);
                    f = c(n.getBody(), d, e);
                    m = u.getParent(f, u.isBlock);
                    if (!e || !f) return a;
                    if (m && l != m)
                        if (d) {
                            if (!b(l, m)) return a;
                            1 == e.nodeType ? "BR" == e.nodeName ? a.setStartBefore(e) : a.setStartAfter(e) : a.setStart(e, e.data.length);
                            1 == f.nodeType ? a.setEnd(f, 0) : a.setEndBefore(f)
                        } else {
                            if (!b(m,
                                    l)) return a;
                            1 == f.nodeType ? "BR" == f.nodeName ? a.setStartBefore(f) : a.setStartAfter(f) : a.setStart(f, f.data.length);
                            1 == e.nodeType ? a.setEnd(e, 0) : a.setEndBefore(e)
                        }
                    return a
                }

                function p(a) {
                    var b = v.getRng(),
                        b = h(b, a);
                    a: {
                        a = b;
                        var d, e, f;
                        if (!a.collapsed && (b = u.getParent(g.getNode(a.startContainer, a.startOffset), u.isBlock), d = u.getParent(g.getNode(a.endContainer, a.endOffset), u.isBlock), e = n.schema.getTextBlockElements(), b != d && e[b.nodeName] && e[d.nodeName] && "false" !== u.getContentEditable(b) && "false" !== u.getContentEditable(d))) {
                            a.deleteContents();
                            e = c(b, !1);
                            f = c(d, !0);
                            u.isEmpty(d) || fa(b).append(d.childNodes);
                            fa(d).remove();
                            e ? 1 == e.nodeType ? "BR" == e.nodeName ? (a.setStartBefore(e), a.setEndBefore(e)) : (a.setStartAfter(e), a.setEndAfter(e)) : (a.setStart(e, e.data.length), a.setEnd(e, e.data.length)) : f && (1 == f.nodeType ? (a.setStartBefore(f), a.setEndBefore(f)) : (a.setStart(f, 0), a.setEnd(f, 0)));
                            v.setRng(a);
                            a = !0;
                            break a
                        }
                        a = void 0
                    }
                    if (a) return !0
                }

                function r(a, b) {
                    function c(a, c) {
                        r = fa(c).parents().filter(function(a, b) {
                            return !!n.schema.getTextInlineElements()[b.nodeName]
                        });
                        m = a.cloneNode(!1);
                        r = d.map(r, function(a) {
                            a = a.cloneNode(!1);
                            m.hasChildNodes() && a.appendChild(m.firstChild);
                            m.appendChild(a);
                            m.appendChild(a);
                            return a
                        });
                        return r.length ? (p = u.create("br"), r[0].appendChild(p), u.replace(m, a), b.setStartBefore(p), b.setEndBefore(p), n.selection.setRng(b), p) : null
                    }

                    function f(a) {
                        return a && n.schema.getTextBlockElements()[a.tagName]
                    }
                    var g, l, m, h, k, p, r;
                    if (b.collapsed && (g = b.startContainer, k = b.startOffset, l = u.getParent(g, u.isBlock), f(l)))
                        if (1 == g.nodeType) {
                            if (g = g.childNodes[k], !g || "BR" ==
                                g.tagName)
                                if (a = a ? l.nextSibling : l.previousSibling, u.isEmpty(l) && f(a) && u.isEmpty(a) && c(l, g)) return u.remove(a), !0
                        } else if (3 == g.nodeType) {
                        g = e.create(l, g);
                        h = l.cloneNode(!0);
                        g = e.resolve(h, g);
                        if (a) {
                            if (k >= g.data.length) return;
                            g.deleteData(k, 1)
                        } else {
                            if (0 >= k) return;
                            g.deleteData(k - 1, 1)
                        }
                        if (u.isEmpty(h)) return c(l, g)
                    }
                }

                function q(a) {
                    var b, c, e;
                    p(a) || (d.each(n.getBody().getElementsByTagName("*"), function(a) {
                        "SPAN" == a.tagName && a.setAttribute("mce-data-marked", 1);
                        !a.hasAttribute("data-mce-style") && a.hasAttribute("style") &&
                            n.dom.setAttrib(a, "style", n.dom.getAttrib(a, "style"))
                    }), b = new x(function() {}), b.observe(n.getDoc(), {
                        childList: !0,
                        attributes: !0,
                        subtree: !0,
                        attributeFilter: ["style"]
                    }), n.getDoc().execCommand(a ? "ForwardDelete" : "Delete", !1, null), c = n.selection.getRng(), e = c.startContainer.parentNode, d.each(b.takeRecords(), function(a) {
                        if (u.isChildOf(a.target, n.getBody())) {
                            if ("style" == a.attributeName) {
                                var b = a.target.getAttribute("data-mce-style");
                                b ? a.target.setAttribute("style", b) : a.target.removeAttribute("style")
                            }
                            d.each(a.addedNodes,
                                function(a) {
                                    if ("SPAN" == a.nodeName && !a.getAttribute("mce-data-marked")) {
                                        var b, d;
                                        a == e && (b = c.startOffset, d = a.firstChild);
                                        u.remove(a, !0);
                                        d && (c.setStart(d, b), c.setEnd(d, b), n.selection.setRng(c))
                                    }
                                })
                        }
                    }), b.disconnect(), d.each(n.dom.select("span[mce-data-marked]"), function(a) {
                        a.removeAttribute("mce-data-marked")
                    }))
                }
                var w = n.getDoc(),
                    u = n.dom,
                    v = n.selection,
                    x = window.MutationObserver,
                    y, V;
                x || (y = !0, x = function() {
                    function a(a) {
                        a = a.relatedNode || a.target;
                        c.push({
                            target: a,
                            addedNodes: [a]
                        })
                    }

                    function b(a) {
                        c.push({
                            target: a.relatedNode ||
                                a.target,
                            attributeName: a.attrName
                        })
                    }
                    var c = [],
                        d;
                    this.observe = function(c) {
                        d = c;
                        d.addEventListener("DOMSubtreeModified", a, !1);
                        d.addEventListener("DOMNodeInsertedIntoDocument", a, !1);
                        d.addEventListener("DOMNodeInserted", a, !1);
                        d.addEventListener("DOMAttrModified", b, !1)
                    };
                    this.disconnect = function() {
                        d.removeEventListener("DOMSubtreeModified", a, !1);
                        d.removeEventListener("DOMNodeInsertedIntoDocument", a, !1);
                        d.removeEventListener("DOMNodeInserted", a, !1);
                        d.removeEventListener("DOMAttrModified", b, !1)
                    };
                    this.takeRecords =
                        function() {
                            return c
                        }
                });
                n.on("keydown", function(a) {
                    var b = a.keyCode == ba,
                        c = a.ctrlKey || a.metaKey;
                    if (!a.isDefaultPrevented() && (b || a.keyCode == ia)) {
                        var d = n.selection.getRng(),
                            e = d.startContainer,
                            f = d.startOffset;
                        b && a.shiftKey || (r(b, d) ? a.preventDefault() : !c && d.collapsed && 3 == e.nodeType && (b ? f < e.data.length : 0 < f) || (a.preventDefault(), c && n.selection.getSel().modify("extend", b ? "forward" : "backward", a.metaKey ? "lineboundary" : "word"), q(b)))
                    }
                });
                n.on("keypress", function(a) {
                    if (!a.isDefaultPrevented() && !v.isCollapsed() &&
                        31 < a.charCode && !k.metaKeyPressed(a)) {
                        var b, c, d, e;
                        b = n.selection.getRng();
                        d = String.fromCharCode(a.charCode);
                        a.preventDefault();
                        a = fa(b.startContainer).parents().filter(function(a, b) {
                            return !!n.schema.getTextInlineElements()[b.nodeName]
                        });
                        q(!0);
                        a = a.filter(function(a, b) {
                            return !fa.contains(n.getBody(), b)
                        });
                        a.length ? (c = u.createFragment(), a.each(function(a, b) {
                                b = b.cloneNode(!1);
                                c.hasChildNodes() ? b.appendChild(c.firstChild) : e = b;
                                c.appendChild(b);
                                c.appendChild(b)
                            }), e.appendChild(n.getDoc().createTextNode(d)),
                            d = u.getParent(b.startContainer, u.isBlock), u.isEmpty(d) ? fa(d).empty().append(c) : b.insertNode(c), b.setStart(e.firstChild, 1), b.setEnd(e.firstChild, 1), n.selection.setRng(b)) : n.selection.setContent(d)
                    }
                });
                n.addCommand("Delete", function() {
                    q()
                });
                n.addCommand("ForwardDelete", function() {
                    q(!0)
                });
                y || (n.on("dragstart", function(a) {
                    V = v.getRng();
                    A(a)
                }), n.on("drop", function(a) {
                    if (!a.isDefaultPrevented()) {
                        var b = m(a);
                        b && (a.preventDefault(), l.setEditorTimeout(n, function() {
                            var c = g.getCaretRangeFromPoint(a.x, a.y, w);
                            V &&
                                (v.setRng(V), V = null);
                            q();
                            v.setRng(c);
                            t(b.html)
                        }))
                    }
                }), n.on("cut", function(a) {
                    a.isDefaultPrevented() || !a.clipboardData || n.selection.isCollapsed() || (a.preventDefault(), a.clipboardData.clearData(), a.clipboardData.setData("text/html", n.selection.getContent()), a.clipboardData.setData("text/plain", n.selection.getContent({
                        format: "text"
                    })), l.setEditorTimeout(n, function() {
                        q(!0)
                    }))
                }))
            }

            function B() {
                n.shortcuts.add("meta+a", null, "SelectAll")
            }

            function x() {
                n.settings.content_editable || J.bind(n.getDoc(), "mousedown mouseup",
                    function(a) {
                        var b;
                        a.target == n.getDoc().documentElement && (b = S.getRng(), n.getBody().focus(), "mousedown" == a.type ? v.isCaretContainer(b.startContainer) || S.placeCaretAt(a.clientX, a.clientY) : S.setRng(b))
                    })
            }

            function r() {
                n.on("keydown", function(a) {
                    if (!a.isDefaultPrevented() && a.keyCode === ia && n.getBody().getElementsByTagName("hr").length && S.isCollapsed() && 0 === S.getRng(!0).startOffset) {
                        var b = S.getNode(),
                            c = b.previousSibling;
                        "HR" == b.nodeName ? (J.remove(b), a.preventDefault()) : c && c.nodeName && "hr" === c.nodeName.toLowerCase() &&
                            (J.remove(c), a.preventDefault())
                    }
                })
            }

            function u() {
                if (!window.Range.prototype.getClientRects) n.on("mousedown", function(a) {
                    if (!a.isDefaultPrevented() && "HTML" === a.target.nodeName) {
                        var b = n.getBody();
                        b.blur();
                        l.setEditorTimeout(n, function() {
                            b.focus()
                        })
                    }
                })
            }

            function z() {
                n.on("click", function(a) {
                    var b = a.target;
                    /^(IMG|HR)$/.test(b.nodeName) && "false" !== J.getContentEditableParent(b) && (a.preventDefault(), S.getSel().setBaseAndExtent(b, 0, b, 1), n.nodeChanged());
                    "A" == b.nodeName && J.hasClass(b, "mce-item-anchor") && (a.preventDefault(),
                        S.select(b))
                })
            }

            function O() {
                function a() {
                    var a = J.getAttribs(S.getStart().cloneNode(!1));
                    return function() {
                        var b = S.getStart();
                        b !== n.getBody() && (J.setAttrib(b, "style", null), ea(a, function(a) {
                            b.setAttributeNode(a.cloneNode(!0))
                        }))
                    }
                }

                function b() {
                    return !S.isCollapsed() && J.getParent(S.getStart(), J.isBlock) != J.getParent(S.getEnd(), J.isBlock)
                }
                n.on("keypress", function(c) {
                    var d;
                    if (!c.isDefaultPrevented() && (8 == c.keyCode || 46 == c.keyCode) && b()) return d = a(), n.getDoc().execCommand("delete", !1, null), d(), c.preventDefault(), !1
                });
                J.bind(n.getDoc(), "cut", function(c) {
                    var d;
                    !c.isDefaultPrevented() && b() && (d = a(), l.setEditorTimeout(n, function() {
                        d()
                    }))
                })
            }

            function W() {
                n.on("keydown", function(a) {
                    if (!a.isDefaultPrevented() && a.keyCode === ia && S.isCollapsed() && 0 === S.getRng(!0).startOffset) {
                        var b = S.getNode().previousSibling;
                        if (b && b.nodeName && "table" === b.nodeName.toLowerCase()) return a.preventDefault(), !1
                    }
                })
            }

            function E() {
                7 < w() || (p("RespectVisibilityInDesign", !0), n.contentStyles.push(".mceHideBrInPre pre br {display: none}"), J.addClass(n.getBody(),
                    "mceHideBrInPre"), ja.addNodeFilter("pre", function(b) {
                    for (var c = b.length, d, e, f, g; c--;)
                        for (d = b[c].getAll("br"), e = d.length; e--;) f = d[e], (g = f.prev) && 3 === g.type && "\n" != g.value.charAt(g.value - 1) ? g.value += "\n" : f.parent.insert(new a("#text", 3), f, !0).value = "\n"
                }), na.addNodeFilter("pre", function(a) {
                    for (var b = a.length, c, d, e; b--;)
                        for (c = a[b].getAll("br"), d = c.length; d--;) e = c[d], (e = e.prev) && 3 == e.type && (e.value = e.value.replace(/\r?\n$/, ""))
                }))
            }

            function G() {
                J.bind(n.getBody(), "mouseup", function() {
                    var a, b = S.getNode();
                    if ("IMG" == b.nodeName) {
                        if (a = J.getStyle(b, "width")) J.setAttrib(b, "width", a.replace(/[^0-9%]+/g, "")), J.setStyle(b, "width", "");
                        if (a = J.getStyle(b, "height")) J.setAttrib(b, "height", a.replace(/[^0-9%]+/g, "")), J.setStyle(b, "height", "")
                    }
                })
            }

            function N() {
                function a() {
                    X();
                    p("StyleWithCSS", !1);
                    p("enableInlineTableEditing", !1);
                    ka.object_resizing || p("enableObjectResizing", !1)
                }
                if (!ka.readonly) n.on("BeforeExecCommand MouseDown", a)
            }

            function K() {
                function a() {
                    ea(J.select("a"), function(a) {
                        var b = a.parentNode,
                            c = J.getRoot();
                        if (b.lastChild === a) {
                            for (; b && !J.isBlock(b);) {
                                if (b.parentNode.lastChild !== b || b === c) return;
                                b = b.parentNode
                            }
                            J.add(b, "br", {
                                "data-mce-bogus": 1
                            })
                        }
                    })
                }
                n.on("SetContent ExecCommand", function(b) {
                    "setcontent" != b.type && "mceInsertLink" !== b.command || a()
                })
            }

            function I() {
                if (ka.forced_root_block) n.on("init", function() {
                    p("DefaultParagraphSeparator", ka.forced_root_block)
                })
            }

            function H() {
                n.on("keydown", function(a) {
                    var b;
                    a.isDefaultPrevented() || a.keyCode != ia || (b = n.getDoc().selection.createRange()) && b.item && (a.preventDefault(),
                        n.undoManager.beforeChange(), J.remove(b.item(0)), n.undoManager.add())
                })
            }

            function L() {
                var a;
                10 <= w() && (a = "", ea("p div h1 h2 h3 h4 h5 h6".split(" "), function(b, c) {
                    a += (0 < c ? "," : "") + b + ":empty"
                }), n.contentStyles.push(a + "{padding-right: 1px !important}"))
            }

            function T() {
                9 > w() && (ja.addNodeFilter("noscript", function(a) {
                    for (var b = a.length, c, d; b--;) c = a[b], (d = c.firstChild) && c.attr("data-mce-innertext", d.value)
                }), na.addNodeFilter("noscript", function(b) {
                    for (var d = b.length, e, f, g; d--;)
                        if (e = b[d], f = b[d].firstChild) f.value =
                            c.decode(f.value);
                        else if (g = e.attributes.map["data-mce-innertext"]) e.attr("data-mce-innertext", null), f = new a("#text", 3), f.value = g, f.raw = !0, e.append(f)
                }))
            }

            function ha() {
                function a(a, b) {
                    var c = e.createTextRange();
                    try {
                        c.moveToPoint(a, b)
                    } catch (wa) {
                        c = null
                    }
                    return c
                }

                function b(b) {
                    if (b.button) {
                        if (b = a(b.x, b.y)) 0 < b.compareEndPoints("StartToStart", g) ? b.setEndPoint("StartToStart", g) : b.setEndPoint("EndToEnd", g), b.select()
                    } else c()
                }

                function c() {
                    var a = d.selection.createRange();
                    g && !a.item && 0 === a.compareEndPoints("StartToEnd",
                        a) && g.select();
                    J.unbind(d, "mouseup", c);
                    J.unbind(d, "mousemove", b);
                    g = f = 0
                }
                var d = J.doc,
                    e = d.body,
                    f, g, l;
                d.documentElement.unselectable = !0;
                J.bind(d, "mousedown contextmenu", function(e) {
                    "HTML" === e.target.nodeName && (f && c(), l = d.documentElement, !(l.scrollHeight > l.clientHeight) && (f = 1, g = a(e.x, e.y))) && (J.bind(d, "mouseup", c), J.bind(d, "mousemove", b), J.getRoot().focus(), g.select())
                })
            }

            function M() {
                n.on("keyup focusin mouseup", function(a) {
                    65 == a.keyCode && k.metaKeyPressed(a) || S.normalize()
                }, !0)
            }

            function C() {
                if (!n.inline) n.on("keydown",
                    function() {
                        document.activeElement == document.body && n.getWin().focus()
                    })
            }

            function D() {
                n.inline || (n.contentStyles.push("body {min-height: 150px}"), n.on("click", function(a) {
                    "HTML" == a.target.nodeName && (11 < b.ie ? n.getBody().focus() : (a = n.selection.getRng(), n.getBody().focus(), n.selection.setRng(a), n.selection.normalize(), n.nodeChanged()))
                }))
            }

            function P() {
                if (b.mac) n.on("keydown", function(a) {
                    !k.metaKeyPressed(a) || a.shiftKey || 37 != a.keyCode && 39 != a.keyCode || (a.preventDefault(), n.selection.getSel().modify("move",
                        37 == a.keyCode ? "backward" : "forward", "lineboundary"))
                })
            }

            function da() {
                n.on("click", function(a) {
                    var b = a.target;
                    do
                        if ("A" === b.tagName) {
                            a.preventDefault();
                            break
                        }
                    while (b = b.parentNode)
                });
                n.contentStyles.push(".mce-content-body {-webkit-touch-callout: none}")
            }

            function Q() {
                n.on("init", function() {
                    n.dom.bind(n.getBody(), "submit", function(a) {
                        a.preventDefault()
                    })
                })
            }

            function Z() {
                ja.addNodeFilter("br", function(a) {
                    for (var b = a.length; b--;) "Apple-interchange-newline" == a[b].attr("class") && a[b].remove()
                })
            }

            function aa() {
                n.on("dragstart",
                    function(a) {
                        A(a)
                    });
                n.on("drop", function(a) {
                    if (!a.isDefaultPrevented()) {
                        var b = m(a);
                        b && b.id != n.id && (a.preventDefault(), a = g.getCaretRangeFromPoint(a.x, a.y, n.getDoc()), S.setRng(a), t(b.html))
                    }
                })
            }

            function X() {
                var a, b;
                V() && (a = n.getBody(), b = a.parentNode, b.removeChild(a), b.appendChild(a), a.focus())
            }

            function V() {
                var a;
                if (!pa) return 0;
                a = n.selection.getSel();
                return !a || !a.rangeCount || 0 === a.rangeCount
            }

            function ca() {
                function a(a) {
                    var b = new h(a.getBody()),
                        c = a.selection.getRng(),
                        d = q.fromRangeStart(c),
                        c = q.fromRangeEnd(c);
                    return !a.selection.isCollapsed() && !b.prev(d) && !b.next(c)
                }
                n.on("keypress", function(b) {
                    !b.isDefaultPrevented() && !S.isCollapsed() && 31 < b.charCode && !k.metaKeyPressed(b) && a(n) && (b.preventDefault(), n.setContent(String.fromCharCode(b.charCode)), n.selection.select(n.getBody(), !0), n.selection.collapse(!1), n.nodeChanged())
                });
                n.on("keydown", function(b) {
                    var c = b.keyCode;
                    b.isDefaultPrevented() || c != ba && c != ia || !a(n) || (b.preventDefault(), n.setContent(""), n.nodeChanged())
                })
            }
            var ea = d.each,
                fa = n.$,
                ia = k.BACKSPACE,
                ba = k.DELETE,
                J = n.dom,
                S = n.selection,
                ka = n.settings,
                ja = n.parser,
                na = n.serializer,
                pa = b.gecko,
                qa = b.ie,
                ua = b.webkit,
                ra = qa ? "Text" : "URL";
            (function() {
                n.on("keydown", function(a) {
                    var b, c, d, e;
                    if (!a.isDefaultPrevented() && a.keyCode == k.BACKSPACE && (a = S.getRng(), b = a.startContainer, c = a.startOffset, d = J.getRoot(), e = b, a.collapsed && 0 === c)) {
                        for (; e && e.parentNode && e.parentNode.firstChild == e && e.parentNode != d;) e = e.parentNode;
                        "BLOCKQUOTE" === e.tagName && (n.formatter.toggle("blockquote", null, e), a = J.createRng(), a.setStart(b, 0), a.setEnd(b, 0), S.setRng(a))
                    }
                })
            })();
            (function() {
                function a(a) {
                    var b = J.create("body");
                    a = a.cloneContents();
                    b.appendChild(a);
                    return S.serializer.serialize(b, {
                        format: "html"
                    })
                }
                n.on("keydown", function(b) {
                    var c = b.keyCode,
                        d;
                    if (!b.isDefaultPrevented() && (c == ba || c == ia) && (d = n.selection.isCollapsed(), c = n.getBody(), !d || J.isEmpty(c))) {
                        if (d = !d) {
                            d = n.selection.getRng();
                            if (d.setStart) {
                                d = a(d);
                                var e = J.createRng();
                                e.selectNode(n.getBody());
                                e = a(e);
                                d = d === e
                            } else d.item ? d = !1 : (e = d.duplicate(), e.moveToElementText(n.getBody()), d = g.compareRanges(d, e));
                            d = !d
                        }
                        d || (b.preventDefault(),
                            n.setContent(""), c.firstChild && J.isBlock(c.firstChild) ? n.selection.setCursorLocation(c.firstChild, 0) : n.selection.setCursorLocation(c, 0), n.nodeChanged())
                    }
                })
            })();
            b.windowsPhone || M();
            ua && (ca(), y(), x(), z(), I(), Q(), W(), Z(), b.iOS ? (C(), D(), da()) : B());
            qa && 11 > b.ie && (r(), document.body.setAttribute("role", "application"), E(), G(), H(), L(), T(), ha());
            11 <= b.ie && (D(), W());
            b.ie && (B(), p("AutoUrlDetect", !1), aa());
            pa && (ca(), r(), u(), O(), N(), K(), n.contentStyles.push("img:-moz-broken {-moz-force-broken-image-icon:1;min-width:24px;min-height:24px}"),
                P(), W());
            return {
                refreshContentEditable: X,
                isHidden: V
            }
        }
    });
    z("tinymce/EditorObservable", ["tinymce/util/Observable", "tinymce/dom/DOMUtils", "tinymce/util/Tools"], function(k, g, f) {
        function e(a, b) {
            return "selectionchange" == b ? a.getDoc() : !a.inline && /^mouse|click|contextmenu|drop|dragover|dragend/.test(b) ? a.getDoc().documentElement : a.settings.event_root ? (a.eventRoot || (a.eventRoot = c.select(a.settings.event_root)[0]), a.eventRoot) : a.getBody()
        }

        function a(a, f) {
            var d = e(a, f),
                g;
            a.delegates || (a.delegates = {});
            a.delegates[f] ||
                (a.settings.event_root ? (b || (b = {}, a.editorManager.on("removeEditor", function() {
                    var c;
                    if (!a.editorManager.activeEditor && b) {
                        for (c in b) a.dom.unbind(e(a, c));
                        b = null
                    }
                })), b[f] || (g = function(b) {
                    for (var d = b.target, e = a.editorManager.editors, g = e.length; g--;) {
                        var l = e[g].getBody();
                        if (l === d || c.isChildOf(d, l)) l = e[g], l.hidden || l.readonly || e[g].fire(f, b)
                    }
                }, b[f] = g, c.bind(d, f, g))) : (g = function(b) {
                    a.hidden || a.readonly || a.fire(f, b)
                }, c.bind(d, f, g), a.delegates[f] = g))
        }
        var c = g.DOM,
            b;
        g = {
            bindPendingEventDelegates: function() {
                var b =
                    this;
                f.each(b._pendingNativeEvents, function(c) {
                    a(b, c)
                })
            },
            toggleNativeEvent: function(b, c) {
                "focus" != b && "blur" != b && (c ? this.initialized ? a(this, b) : this._pendingNativeEvents ? this._pendingNativeEvents.push(b) : this._pendingNativeEvents = [b] : this.initialized && (this.dom.unbind(e(this, b), b, this.delegates[b]), delete this.delegates[b]))
            },
            unbindAllNativeEvents: function() {
                var a;
                if (this.delegates) {
                    for (a in this.delegates) this.dom.unbind(e(this, a), a, this.delegates[a]);
                    delete this.delegates
                }
                this.inline || (this.getBody().onload =
                    null, this.dom.unbind(this.getWin()), this.dom.unbind(this.getDoc()));
                this.dom.unbind(this.getBody());
                this.dom.unbind(this.getContainer())
            }
        };
        return g = f.extend({}, k, g)
    });
    z("tinymce/Mode", [], function() {
        function k(e, a, c) {
            try {
                e.getDoc().execCommand(a, !1, c)
            } catch (b) {}
        }

        function g(e) {
            var a, c;
            a = e.getBody();
            c = function(a) {
                0 < e.dom.getParents(a.target, "a").length && a.preventDefault()
            };
            e.dom.bind(a, "click", c);
            return {
                unbind: function() {
                    e.dom.unbind(a, "click", c)
                }
            }
        }

        function f(e, a) {
            e._clickBlocker && (e._clickBlocker.unbind(),
                e._clickBlocker = null);
            a ? (e._clickBlocker = g(e), e.selection.controlSelection.hideResizeRect(), e.readonly = !0, e.getBody().contentEditable = !1) : (e.readonly = !1, e.getBody().contentEditable = !0, k(e, "StyleWithCSS", !1), k(e, "enableInlineTableEditing", !1), k(e, "enableObjectResizing", !1), e.focus(), e.nodeChanged())
        }
        return {
            setMode: function(e, a) {
                if (a != (e.readonly ? "readonly" : "design")) {
                    if (e.initialized) f(e, "readonly" == a);
                    else e.on("init", function() {
                        f(e, "readonly" == a)
                    });
                    e.fire("SwitchMode", {
                        mode: a
                    })
                }
            }
        }
    });
    z("tinymce/Shortcuts", ["tinymce/util/Tools", "tinymce/Env"], function(k, g) {
        var f = k.each,
            e = k.explode,
            a = {
                f9: 120,
                f10: 121,
                f11: 122
            },
            c = k.makeMap("alt,ctrl,shift,meta,access");
        return function(b) {
            function d(b) {
                var d, l = {};
                f(e(b, "+"), function(b) {
                    b in c ? l[b] = !0 : /^[0-9]{2,}$/.test(b) ? l.keyCode = parseInt(b, 10) : (l.charCode = b.charCodeAt(0), l.keyCode = a[b] || b.toUpperCase().charCodeAt(0))
                });
                b = [l.keyCode];
                for (d in c) l[d] ? b.push(d) : l[d] = !1;
                l.id = b.join(",");
                l.access && (l.alt = !0, g.mac ? l.ctrl = !0 : l.shift = !0);
                l.meta && (g.mac ? l.meta = !0 : (l.ctrl = !0, l.meta = !1));
                return l
            }

            function l(a, c, f, g) {
                a = k.map(e(a, "\x3e"), d);
                a[a.length - 1] = k.extend(a[a.length - 1], {
                    func: f,
                    scope: g || b
                });
                return k.extend(a[0], {
                    desc: b.translate(c),
                    subpatterns: a.slice(1)
                })
            }

            function v(a, b) {
                return b && b.ctrl == a.ctrlKey && b.meta == a.metaKey && b.alt == a.altKey && b.shift == a.shiftKey ? a.keyCode == b.keyCode || a.charCode && a.charCode == b.charCode ? (a.preventDefault(), !0) : !1 : !1
            }

            function q(a) {
                return a.func ? a.func.call(a.scope) : null
            }
            var h = {},
                n = [];
            b.on("keyup keypress keydown", function(a) {
                (a.altKey || a.ctrlKey ||
                    a.metaKey || 112 <= a.keyCode && 123 >= a.keyCode) && !a.isDefaultPrevented() && (f(h, function(b) {
                    if (v(a, b)) return n = b.subpatterns.slice(0), "keydown" == a.type && q(b), !0
                }), v(a, n[0]) && (1 === n.length && "keydown" == a.type && q(n[0]), n.shift()))
            });
            this.add = function(a, c, d, g) {
                var m;
                m = d;
                "string" === typeof d ? d = function() {
                    b.execCommand(m, !1, null)
                } : k.isArray(m) && (d = function() {
                    b.execCommand(m[0], m[1], m[2])
                });
                f(e(k.trim(a.toLowerCase())), function(a) {
                    a = l(a, c, d, g);
                    h[a.id] = a
                });
                return !0
            };
            this.remove = function(a) {
                a = l(a);
                return h[a.id] ?
                    (delete h[a.id], !0) : !1
            }
        }
    });
    z("tinymce/file/Uploader", ["tinymce/util/Promise", "tinymce/util/Tools", "tinymce/util/Fun"], function(k, g, f) {
        return function(e, a) {
            function c(a) {
                var b;
                b = {
                    "image/jpeg": "jpg",
                    "image/jpg": "jpg",
                    "image/gif": "gif",
                    "image/png": "png"
                }[a.blob().type.toLowerCase()] || "dat";
                return a.id() + "." + b
            }

            function b(b, d, e, f) {
                var g, l;
                g = new XMLHttpRequest;
                g.open("POST", a.url);
                g.withCredentials = a.credentials;
                g.upload.onprogress = function(a) {
                    f(a.loaded / a.total * 100)
                };
                g.onerror = function() {
                    e("Image upload failed due to a XHR Transport error. Code: " +
                        g.status)
                };
                g.onload = function() {
                    var b;
                    if (200 != g.status) e("HTTP Error: " + g.status);
                    else if ((b = JSON.parse(g.responseText)) && "string" == typeof b.location) {
                        var c;
                        c = a.basePath;
                        b = b.location;
                        c = c ? c.replace(/\/$/, "") + "/" + b.replace(/^\//, "") : b;
                        d(c)
                    } else e("Invalid JSON: " + g.responseText)
                };
                l = new FormData;
                l.append("file", b.blob(), c(b));
                g.send(l)
            }

            function d() {
                return new k(function(a) {
                    a([])
                })
            }

            function l(a, b) {
                return {
                    url: "",
                    blobInfo: a,
                    status: !1,
                    error: b
                }
            }

            function v(a, b) {
                g.each(p[a], function(a) {
                    a(b)
                });
                delete p[a]
            }

            function q(a,
                b, d) {
                e.markPending(a.blobUri());
                return new k(function(g) {
                    var m, h, k = function() {};
                    try {
                        var n = function() {
                            m && (m.close(), h = k);
                            e.removeFailed(a.blobUri());
                            v(a.blobUri(), l(a, n));
                            g(l(a, n))
                        };
                        h = function(a) {
                            0 > a || 100 < a || (m || (m = d()), m.progressBar.value(a))
                        };
                        b({
                            id: a.id,
                            blob: a.blob,
                            base64: a.base64,
                            filename: f.constant(c(a))
                        }, function(b) {
                            m && (m.close(), h = k);
                            e.markUploaded(a.blobUri(), b);
                            v(a.blobUri(), {
                                url: b,
                                blobInfo: a,
                                status: !0
                            });
                            g({
                                url: b,
                                blobInfo: a,
                                status: !0
                            })
                        }, n, h)
                    } catch (u) {
                        g(l(a, u.message))
                    }
                })
            }

            function h(a) {
                var b =
                    a.blobUri();
                return new k(function(a) {
                    p[b] = p[b] || [];
                    p[b].push(a)
                })
            }

            function n(b, c) {
                b = g.grep(b, function(a) {
                    return !e.isUploaded(a.blobUri())
                });
                return k.all(g.map(b, function(b) {
                    return e.isPending(b.blobUri()) ? h(b) : q(b, a.handler, c)
                }))
            }
            var p = {};
            a = g.extend({
                credentials: !1,
                handler: b
            }, a);
            return {
                upload: function(c, e) {
                    return a.url || a.handler !== b ? n(c, e) : d()
                }
            }
        }
    });
    z("tinymce/file/Conversions", ["tinymce/util/Promise"], function(k) {
        function g(a) {
            return new k(function(c) {
                var b = new XMLHttpRequest;
                b.open("GET", a, !0);
                b.responseType = "blob";
                b.onload = function() {
                    200 == this.status && c(this.response)
                };
                b.send()
            })
        }

        function f(a) {
            var c, b;
            a = decodeURIComponent(a).split(",");
            (b = /data:([^;]+)/.exec(a[0])) && (c = b[1]);
            return {
                type: c,
                data: a[1]
            }
        }

        function e(a) {
            return new k(function(c) {
                var b, d, e;
                a = f(a);
                try {
                    b = atob(a.data)
                } catch (v) {
                    c(new Blob([]));
                    return
                }
                d = new Uint8Array(b.length);
                for (e = 0; e < d.length; e++) d[e] = b.charCodeAt(e);
                c(new Blob([d], {
                    type: a.type
                }))
            })
        }
        return {
            uriToBlob: function(a) {
                return 0 === a.indexOf("blob:") ? g(a) : 0 === a.indexOf("data:") ?
                    e(a) : null
            },
            blobToDataUri: function(a) {
                return new k(function(c) {
                    var b = new FileReader;
                    b.onloadend = function() {
                        c(b.result)
                    };
                    b.readAsDataURL(a)
                })
            },
            parseDataUri: f
        }
    });
    z("tinymce/file/ImageScanner", ["tinymce/util/Promise", "tinymce/util/Arr", "tinymce/util/Fun", "tinymce/file/Conversions", "tinymce/Env"], function(k, g, f, e, a) {
        var c = 0;
        return function(b, d) {
            var l = {};
            return {
                findAll: function(v, q) {
                    function h(a, b) {
                        var f, g;
                        0 === a.src.indexOf("blob:") ? (g = d.getByUri(a.src)) && b({
                            image: a,
                            blobInfo: g
                        }) : (f = e.parseDataUri(a.src).data,
                            (g = d.findFirst(function(a) {
                                return a.base64() === f
                            })) ? b({
                                image: a,
                                blobInfo: g
                            }) : e.uriToBlob(a.src).then(function(e) {
                                var g = "blobid" + c++;
                                e = d.create(g, e, f);
                                d.add(e);
                                b({
                                    image: a,
                                    blobInfo: e
                                })
                            }))
                    }
                    q || (q = f.constant(!0));
                    v = g.filter(v.getElementsByTagName("img"), function(c) {
                        var d = c.src;
                        return !a.fileApi || c.hasAttribute("data-mce-bogus") || c.hasAttribute("data-mce-placeholder") || !d || d == a.transparentSrc ? !1 : 0 === d.indexOf("blob:") ? !b.isUploaded(d) : 0 === d.indexOf("data:") ? q(c) : !1
                    });
                    v = g.map(v, function(a) {
                        var b;
                        if (l[a.src]) return new k(function(b) {
                            l[a.src].then(function(c) {
                                b({
                                    image: a,
                                    blobInfo: c.blobInfo
                                })
                            })
                        });
                        b = (new k(function(b) {
                            h(a, b)
                        })).then(function(a) {
                            delete l[a.image.src];
                            return a
                        })["catch"](function(b) {
                            delete l[a.src];
                            return b
                        });
                        return l[a.src] = b
                    });
                    return k.all(v)
                }
            }
        }
    });
    z("tinymce/file/BlobCache", ["tinymce/util/Arr", "tinymce/util/Fun"], function(k, g) {
        return function() {
            function f(a) {
                return e(function(b) {
                    return b.id() === a
                })
            }

            function e(b) {
                return k.filter(a, b)[0]
            }
            var a = [],
                c = g.constant;
            return {
                create: function(a, d, e) {
                    return {
                        id: c(a),
                        blob: c(d),
                        base64: c(e),
                        blobUri: c(URL.createObjectURL(d))
                    }
                },
                add: function(b) {
                    f(b.id()) || a.push(b)
                },
                get: f,
                getByUri: function(a) {
                    return e(function(b) {
                        return b.blobUri() == a
                    })
                },
                findFirst: e,
                removeByUri: function(b) {
                    a = k.filter(a, function(a) {
                        return a.blobUri() === b ? (URL.revokeObjectURL(a.blobUri()), !1) : !0
                    })
                },
                destroy: function() {
                    k.each(a, function(a) {
                        URL.revokeObjectURL(a.blobUri())
                    });
                    a = []
                }
            }
        }
    });
    z("tinymce/file/UploadStatus", [], function() {
        return function() {
            function k(f) {
                return f in g
            }
            var g = {};
            return {
                hasBlobUri: k,
                getResultUri: function(f) {
                    return (f = g[f]) ? f.resultUri : null
                },
                isPending: function(f) {
                    return k(f) ? 1 === g[f].status : !1
                },
                isUploaded: function(f) {
                    return k(f) ? 2 === g[f].status : !1
                },
                markPending: function(f) {
                    g[f] = {
                        status: 1,
                        resultUri: null
                    }
                },
                markUploaded: function(f, e) {
                    g[f] = {
                        status: 2,
                        resultUri: e
                    }
                },
                removeFailed: function(f) {
                    delete g[f]
                },
                destroy: function() {
                    g = {}
                }
            }
        }
    });
    z("tinymce/EditorUpload", ["tinymce/util/Arr", "tinymce/file/Uploader", "tinymce/file/ImageScanner", "tinymce/file/BlobCache", "tinymce/file/UploadStatus"], function(k, g, f, e, a) {
        return function(c) {
            function b(a) {
                return function(b) {
                    return c.selection ?
                        a(b) : []
                }
            }

            function d(a, b, c) {
                var d = 0;
                do d = a.indexOf(b, d), -1 !== d && (a = a.substring(0, d) + c + a.substr(d + b.length), d += c.length - b.length + 1); while (-1 !== d);
                return a
            }

            function l(a, b) {
                k.each(c.undoManager.data, function(c) {
                    var e = c.content,
                        e = d(e, 'src\x3d"' + a + '"', 'src\x3d"' + b + '"'),
                        e = d(e, 'data-mce-src\x3d"' + a + '"', 'data-mce-src\x3d"' + b + '"');
                    c.content = e
                })
            }

            function v() {
                return c.notificationManager.open({
                    text: c.translate("Image uploading..."),
                    type: "info",
                    timeout: -1,
                    progressBar: !0
                })
            }

            function q(a) {
                m || (m = new g(B, {
                    url: y.images_upload_url,
                    basePath: y.images_upload_base_path,
                    credentials: y.images_upload_credentials,
                    handler: y.images_upload_handler
                }));
                return p().then(b(function(d) {
                    var e;
                    e = k.map(d, function(a) {
                        return a.blobInfo
                    });
                    return m.upload(e, v).then(b(function(b) {
                        b = k.map(b, function(a, b) {
                            b = d[b].image;
                            if (a.status && !1 !== c.settings.images_replace_blob_uris) {
                                var e = a.url;
                                A.removeByUri(b.src);
                                l(b.src, e);
                                c.$(b).attr({
                                    src: e,
                                    "data-mce-src": c.convertURL(e, "src")
                                })
                            }
                            return {
                                element: b,
                                status: a.status
                            }
                        });
                        a && a(b);
                        return b
                    }))
                }))
            }

            function h(a) {
                if (!1 !==
                    y.automatic_uploads) return q(a)
            }

            function n(a) {
                return y.images_dataimg_filter ? y.images_dataimg_filter(a) : !0
            }

            function p() {
                t || (t = new f(B, A));
                return t.findAll(c.getBody(), n).then(b(function(a) {
                    k.each(a, function(a) {
                        l(a.image.src, a.blobInfo.blobUri());
                        a.image.src = a.blobInfo.blobUri();
                        a.image.removeAttribute("data-mce-src")
                    });
                    return a
                }))
            }

            function w(a) {
                return a.replace(/src="(blob:[^"]+)"/g, function(a, b) {
                    var d = B.getResultUri(b);
                    if (d) return 'src\x3d"' + d + '"';
                    (d = A.getByUri(b)) || (d = k.reduce(c.editorManager.editors,
                        function(a, c) {
                            return a || c.editorUpload.blobCache.getByUri(b)
                        }, null));
                    return d ? 'src\x3d"data:' + d.blob().type + ";base64," + d.base64() + '"' : a
                })
            }
            var A = new e,
                m, t, y = c.settings,
                B = new a;
            c.on("setContent", function() {
                !1 !== c.settings.automatic_uploads ? h() : p()
            });
            c.on("RawSaveContent", function(a) {
                a.content = w(a.content)
            });
            c.on("getContent", function(a) {
                a.source_view || "raw" == a.format || (a.content = w(a.content))
            });
            c.on("PostRender", function() {
                c.parser.addNodeFilter("img", function(a) {
                    k.each(a, function(a) {
                        var b = a.attr("src");
                        A.getByUri(b) || (b = B.getResultUri(b)) && a.attr("src", b)
                    })
                })
            });
            return {
                blobCache: A,
                uploadImages: q,
                uploadImagesAuto: h,
                scanForImages: p,
                destroy: function() {
                    A.destroy();
                    B.destroy();
                    t = m = null
                }
            }
        }
    });
    z("tinymce/caret/FakeCaret", "tinymce/caret/CaretContainer tinymce/caret/CaretPosition tinymce/dom/NodeType tinymce/dom/RangeUtils tinymce/dom/DomQuery tinymce/geom/ClientRect tinymce/util/Delay".split(" "), function(k, g, f, e, a, c, b) {
        var d = f.isContentEditableFalse;
        return function(e, f) {
            function g() {
                var b, c, d, f, g;
                b = a("*[contentEditable\x3dfalse]",
                    e);
                for (f = 0; f < b.length; f++) c = b[f], d = c.previousSibling, k.endsWithCaretContainer(d) && (g = d.data, 1 == g.length ? d.parentNode.removeChild(d) : d.deleteData(g.length - 1, 1)), d = c.nextSibling, k.startsWithCaretContainer(d) && (g = d.data, 1 == g.length ? d.parentNode.removeChild(d) : d.deleteData(0, 1));
                v && (k.remove(v), v = null);
                p && (p.remove(), p = null);
                clearInterval(n)
            }

            function l() {
                n = b.setInterval(function() {
                    a("div.mce-visual-caret", e).toggleClass("mce-visual-caret-hidden")
                }, 500)
            }
            var n, p, v;
            return {
                show: function(b, h) {
                    g();
                    if (f(h)) {
                        v =
                            k.insertBlock("p", h, b);
                        var m = c.collapse(h.getBoundingClientRect(), b),
                            n, q;
                        "BODY" == e.tagName ? (n = e.ownerDocument.documentElement, q = e.scrollLeft || n.scrollLeft, n = e.scrollTop || n.scrollTop) : (n = e.getBoundingClientRect(), q = e.scrollLeft - n.left, n = e.scrollTop - n.top);
                        m.left += q;
                        m.right += q;
                        m.top += n;
                        m.bottom += n;
                        m.width = 1;
                        q = h.offsetWidth - h.clientWidth;
                        0 < q && (b && (q *= -1), m.left += q, m.right += q);
                        a(v).css("top", m.top);
                        p = a('\x3cdiv class\x3d"mce-visual-caret" data-mce-bogus\x3d"all"\x3e\x3c/div\x3e').css(m).appendTo(e);
                        b &&
                            p.addClass("mce-visual-caret-before");
                        l();
                        b = h.ownerDocument.createRange();
                        h = v.firstChild;
                        b.setStart(h, 0);
                        b.setEnd(h, 1)
                    } else v = k.insertInline(h, b), b = h.ownerDocument.createRange(), d(v.nextSibling) ? (b.setStart(v, 0), b.setEnd(v, 0)) : (b.setStart(v, 1), b.setEnd(v, 1));
                    return b
                },
                hide: g,
                getCss: function() {
                    return ".mce-visual-caret {position: absolute;background-color: black;background-color: currentcolor;}.mce-visual-caret-hidden {display: none;}*[data-mce-caret] {position: absolute;left: -1000px;right: auto;top: 0;margin: 0;padding: 0;}"
                },
                destroy: function() {
                    b.clearInterval(n)
                }
            }
        }
    });
    z("tinymce/dom/Dimensions", ["tinymce/util/Arr", "tinymce/dom/NodeType", "tinymce/geom/ClientRect"], function(k, g, f) {
        function e(a) {
            function c(b) {
                return k.map(b, function(b) {
                    b = f.clone(b);
                    b.node = a;
                    return b
                })
            }
            if (k.isArray(a)) return k.reduce(a, function(a, b) {
                return a.concat(e(b))
            }, []);
            if (g.isElement(a)) return c(a.getClientRects());
            if (g.isText(a)) {
                var b = a.ownerDocument.createRange();
                b.setStart(a, 0);
                b.setEnd(a, a.data.length);
                return c(b.getClientRects())
            }
        }
        return {
            getClientRects: e
        }
    });
    z("tinymce/caret/LineWalker", "tinymce/util/Fun tinymce/util/Arr tinymce/dom/Dimensions tinymce/caret/CaretCandidate tinymce/caret/CaretUtils tinymce/caret/CaretWalker tinymce/caret/CaretPosition tinymce/geom/ClientRect".split(" "), function(k, g, f, e, a, c, b, d) {
        function l(b, c, d, f) {
            for (;
                (f = a.findNode(f, b, e.isEditableCaretCandidate, c)) && !d(f););
        }

        function v(a, b, c, d, e, h) {
            function m(d) {
                var l, h;
                h = f.getClientRects(d); - 1 == a && (h = h.reverse());
                for (d = 0; d < h.length; d++)
                    if (l = h[d], !c(l, p)) {
                        0 < n.length && b(l, g.last(n)) &&
                            k++;
                        l.line = k;
                        if (e(l)) return !0;
                        n.push(l)
                    }
            }
            var k = 0,
                n = [],
                p;
            p = g.last(h.getClientRects());
            if (!p) return n;
            h = h.getNode();
            m(h);
            l(a, d, m, h);
            return n
        }
        k = k.curry;
        var q = k(v, -1, d.isAbove, d.isBelow),
            h = k(v, 1, d.isBelow, d.isAbove);
        return {
            upUntil: q,
            downUntil: h,
            positionsUntil: function(a, e, f, l) {
                var h = new c(e),
                    k;
                e = [];
                var n = 0,
                    p, q;
                1 == a ? (a = h.next, h = d.isBelow, k = d.isAbove, l = b.after(l)) : (a = h.prev, h = d.isAbove, k = d.isBelow, l = b.before(l));
                q = g.last(l.getClientRects());
                do
                    if (l.isVisible() && (p = g.last(l.getClientRects()), !k(p, q))) {
                        0 <
                            e.length && h(p, g.last(e)) && n++;
                        p = d.clone(p);
                        p.position = l;
                        p.line = n;
                        if (f(p)) break;
                        e.push(p)
                    }
                while (l = a(l));
                return e
            },
            isAboveLine: k(function(a, b) {
                return b.line > a
            }),
            isLine: k(function(a, b) {
                return b.line === a
            })
        }
    });
    z("tinymce/caret/LineUtils", "tinymce/util/Fun tinymce/util/Arr tinymce/dom/NodeType tinymce/dom/Dimensions tinymce/geom/ClientRect tinymce/caret/CaretUtils tinymce/caret/CaretCandidate".split(" "), function(k, g, f, e, a, c, b) {
        function d(a, b) {
            return g.reduce(a, function(a, c) {
                var d, e;
                d = Math.min(Math.abs(a.left -
                    b), Math.abs(a.right - b));
                e = Math.min(Math.abs(c.left - b), Math.abs(c.right - b));
                return b >= c.left && b <= c.right ? c : b >= a.left && b <= a.right ? a : e == d && q(c.node) || e < d ? c : a
            })
        }

        function l(a, c, d, e) {
            for (;
                (e = h(e, a, b.isEditableCaretCandidate, c)) && !d(e););
        }

        function v(b, c) {
            function d(a, b) {
                b = g.filter(e.getClientRects(b), function(b) {
                    return !a(b, c)
                });
                f = f.concat(b);
                return 0 === b.length
            }
            var f = [];
            f.push(c);
            l(-1, b, n(d, a.isAbove), c.node);
            l(1, b, n(d, a.isBelow), c.node);
            return f
        }
        var q = f.isContentEditableFalse,
            h = c.findNode,
            n = k.curry;
        return {
            findClosestClientRect: d,
            findLineNodeRects: v,
            closestCaret: function(a, b, c) {
                var f;
                f = e.getClientRects(g.filter(g.toArray(a.getElementsByTagName("*")), q));
                f = g.filter(f, function(a) {
                    return c >= a.top && c <= a.bottom
                });
                return (f = d(f, b)) && (f = d(v(a, f), b)) && q(f.node) ? {
                    node: f.node,
                    before: Math.abs(f.left - b) < Math.abs(f.right - b)
                } : null
            }
        }
    });
    z("tinymce/DragDropOverrides", ["tinymce/dom/NodeType", "tinymce/util/Arr", "tinymce/util/Fun"], function(k, g, f) {
        var e = k.isContentEditableFalse,
            a = k.isContentEditableTrue;
        return {
            init: function(c) {
                function b(a) {
                    return a ==
                        w.element || c.dom.isChildOf(a, w.element) || e(a) ? !1 : !0
                }

                function d(a) {
                    var b, d, e, f = 0,
                        g = 0;
                    0 === a.button && (b = a.screenX - w.screenX, d = a.screenY - w.screenY, e = Math.max(Math.abs(b), Math.abs(d)), !w.dragging && 10 < e && (w.dragging = !0, q(c.getBody()).css("cursor", "default"), w.clone = w.element.cloneNode(!0), e = p.getPos(w.element), w.relX = w.clientX - e.x, w.relY = w.clientY - e.y, w.width = w.element.offsetWidth, w.height = w.element.offsetHeight, q(w.clone).css({
                        width: w.width,
                        height: w.height
                    }).removeAttr("data-mce-selected"), w.ghost = q("\x3cdiv\x3e").css({
                        position: "absolute",
                        opacity: .5,
                        overflow: "hidden",
                        width: w.width,
                        height: w.height
                    }).attr({
                        "data-mce-bogus": "all",
                        unselectable: "on",
                        contenteditable: "false"
                    }).addClass("mce-drag-container mce-reset").append(w.clone).appendTo(c.getBody())[0], e = c.dom.getViewPort(c.getWin()), w.maxX = e.w, w.maxY = e.h), w.dragging && (c._selectionOverrides.hideFakeCaret(), c.selection.placeCaretAt(a.clientX, a.clientY), a = w.clientX + b - w.relX, d = w.clientY + d + 5, a + w.width > w.maxX && (f = a + w.width - w.maxX), d + w.height > w.maxY && (g = d + w.height - w.maxY), b = "BODY" != c.getBody().nodeName ?
                        c.getBody().getBoundingClientRect() : {
                            left: 0,
                            top: 0
                        }, q(w.ghost).css({
                            left: a - b.left,
                            top: d - b.top,
                            width: w.width - f,
                            height: w.height - g
                        })))
                }

                function l(a) {
                    if (w.dragging && (c.selection.setRng(c.selection.getSel().getRangeAt(0)), b(c.selection.getNode()))) {
                        var d = w.element;
                        a = c.fire("drop", {
                            targetClone: d,
                            clientX: a.clientX,
                            clientY: a.clientY
                        });
                        if (a.isDefaultPrevented()) return;
                        d = a.targetClone;
                        c.undoManager.transact(function() {
                            c.insertContent(p.getOuterHTML(d));
                            q(w.element).remove()
                        })
                    }
                    k()
                }

                function k() {
                    q(w.ghost).remove();
                    q(c.getBody()).css("cursor", null);
                    c.off("mousemove", d);
                    c.off("mouseup", k);
                    h != n && (p.unbind(h, "mousemove", d), p.unbind(h, "mouseup", k));
                    w = {}
                }
                var q = c.$,
                    h = document,
                    n = c.getDoc(),
                    p = c.dom,
                    w = {};
                c.on("mousedown", function(b) {
                    var m, q;
                    k();
                    0 === b.button && (m = g.find(c.dom.getParents(b.target), f.or(e, a)), e(m) && (q = c.fire("dragstart", {
                        target: m
                    }), q.isDefaultPrevented() || (c.on("mousemove", d), c.on("mouseup", l), h != n && (p.bind(h, "mousemove", d), p.bind(h, "mouseup", l)), w = {
                        screenX: b.screenX,
                        screenY: b.screenY,
                        clientX: b.clientX,
                        clientY: b.clientY,
                        element: m
                    })))
                });
                c.on("drop", function(a) {
                    var b = "undefined" !== typeof a.clientX ? c.getDoc().elementFromPoint(a.clientX, a.clientY) : null;
                    (e(b) || e(c.dom.getContentEditableParent(b))) && a.preventDefault()
                })
            }
        }
    });
    z("tinymce/SelectionOverrides", "tinymce/Env tinymce/caret/CaretWalker tinymce/caret/CaretPosition tinymce/caret/CaretContainer tinymce/caret/CaretUtils tinymce/caret/FakeCaret tinymce/caret/LineWalker tinymce/caret/LineUtils tinymce/dom/NodeType tinymce/dom/RangeUtils tinymce/geom/ClientRect tinymce/util/VK tinymce/util/Fun tinymce/util/Arr tinymce/util/Delay tinymce/DragDropOverrides tinymce/text/Zwsp".split(" "),
        function(k, g, f, e, a, c, b, d, l, v, q, h, n, p, w, A, m) {
            function t(a, b) {
                for (;
                    (b = a(b)) && !b.isVisible(););
                return b
            }
            var y = n.curry,
                z = l.isContentEditableTrue,
                x = l.isContentEditableFalse,
                r = l.isElement,
                u = a.isAfterContentEditableFalse,
                F = a.isBeforeContentEditableFalse,
                E = v.getSelectedNode;
            return function(v) {
                function B(a) {
                    return v.dom.isBlock(a)
                }

                function G(a) {
                    a && v.selection.setRng(a)
                }

                function O(a, b, c) {
                    if (v.fire("ShowCaret", {
                            target: b,
                            direction: a,
                            before: c
                        }).isDefaultPrevented()) return null;
                    v.selection.scrollIntoView(b, -1 ===
                        a);
                    return ba.show(c, b)
                }

                function K(a) {
                    ba.hide();
                    if (v.fire("BeforeObjectSelected", {
                            target: a
                        }).isDefaultPrevented()) return null;
                    var b = a.ownerDocument.createRange();
                    b.selectNode(a);
                    return b
                }

                function I(b, c) {
                    c = a.normalizeRange(b, ca, c);
                    return -1 == b ? f.fromRangeStart(c) : f.fromRangeEnd(c)
                }

                function H(b, c, d, f) {
                    var g, h;
                    if (!f.collapsed && (g = E(f), x(g))) return O(b, g, -1 == b);
                    h = e.isCaretContainerBlock(f.startContainer);
                    g = I(b, f);
                    if (d(g)) return K(g.getNode(-1 == b));
                    g = c(g);
                    if (!g) return h ? f : null;
                    if (d(g)) return O(b, g.getNode(-1 ==
                        b), 1 == b);
                    c = c(g);
                    return d(c) && (d = g, f = a.isInSameBlock(d, c), d = !f && l.isBr(d.getNode()) ? !0 : f, d) ? O(b, c.getNode(-1 == b), 1 == b) : h ? D(g.toRange()) : null
                }

                function L(a, b) {
                    var c;
                    if (b.collapsed && v.settings.forced_root_block && (c = v.dom.getParent(b.startContainer, "PRE")) && (b = 1 == a ? fa(f.fromRangeStart(b)) : ia(f.fromRangeStart(b)), !b)) {
                        b = v.dom.create(v.settings.forced_root_block);
                        if (!k.ie || 11 <= k.ie) b.innerHTML = '\x3cbr data-mce-bogus\x3d"1"\x3e';
                        1 == a ? v.$(c).after(b) : v.$(c).before(b);
                        v.selection.select(b, !0);
                        v.selection.collapse()
                    }
                }

                function T(a, b, c, d) {
                    return (b = H(a, b, c, d)) ? b : (b = L(a, d)) ? b : null
                }

                function ha(a, c, e) {
                    var f;
                    a: {
                        var g, l;f = E(e);g = I(a, e);c = c(ca, b.isAboveLine(1), g);l = p.filter(c, b.isLine(1));c = p.last(g.getClientRects());F(g) && (f = g.getNode());u(g) && (f = g.getNode(!0));
                        if (c)
                            if (c = c.left, (g = d.findClosestClientRect(l, c)) && x(g.node)) f = Math.abs(c - g.left), c = Math.abs(c - g.right), f = O(a, g.node, f < c);
                            else {
                                if (f) {
                                    f = b.positionsUntil(a, ca, b.isAboveLine(1), f);
                                    if (g = d.findClosestClientRect(p.filter(f, b.isLine(1)), c)) {
                                        f = D(g.position.toRange());
                                        break a
                                    }
                                    if (g = p.last(p.filter(f, b.isLine(0)))) {
                                        f = D(g.position.toRange());
                                        break a
                                    }
                                }
                                f = void 0
                            }
                        else f = null
                    }
                    return f ? f : (f = L(a, e)) ? f : null
                }

                function M(a) {
                    a = ka(a);
                    a.attr("data-mce-caret") && (ba.hide(), a.removeAttr("data-mce-caret"), a.removeAttr("data-mce-bogus"), a.removeAttr("style"), G(v.selection.getRng()), v.selection.scrollIntoView(a[0], void 0))
                }

                function C(b) {
                    b = a.normalizeRange(1, ca, b);
                    b = f.fromRangeStart(b);
                    if (x(b.getNode())) return O(1, b.getNode(), !b.isAtEnd());
                    if (x(b.getNode(!0))) return O(1, b.getNode(!0), !1);
                    b = v.dom.getParent(b.getNode(), n.or(x, z));
                    if (x(b)) return O(1, b, !1);
                    ba.hide();
                    return null
                }

                function D(a) {
                    var b;
                    return a && a.collapsed ? (b = C(a)) ? b : a : a
                }

                function P(a) {
                    var b, c, d, g;
                    if (!x(a)) return null;
                    x(a.previousSibling) && (d = a.previousSibling);
                    (c = ia(f.before(a))) || (b = fa(f.after(a)));
                    b && r(b.getNode()) && (g = b.getNode());
                    e.remove(a.previousSibling);
                    e.remove(a.nextSibling);
                    v.dom.remove(a);
                    V();
                    if (v.dom.isEmpty(v.getBody())) v.setContent(""), v.focus();
                    else return d ? f.after(d).toRange() : g ? f.before(g).toRange() :
                        c ? c.toRange() : b ? b.toRange() : null
                }

                function W(a, b, c) {
                    var d = v.dom;
                    if (-1 === a) {
                        if (u(c) && B(c.getNode(!0))) return P(c.getNode(!0))
                    } else if (F(b) && B(b.getNode())) return P(b.getNode());
                    a = v.schema.getTextBlockElements();
                    b = d.getParent(b.getNode(), d.isBlock);
                    d = d.getParent(c.getNode(), d.isBlock);
                    if (b === d || !a[b.nodeName] || !a[d.nodeName]) return null;
                    for (; a = b.firstChild;) d.appendChild(a);
                    v.dom.remove(b);
                    return c.toRange()
                }

                function Q(a, b, c, d) {
                    var f;
                    if (!d.collapsed && (f = E(d), x(f))) return D(P(f));
                    f = I(a, d);
                    if (c(f) && e.isCaretContainerBlock(d.startContainer)) return (a = -1 == a ? ea.prev(f) : ea.next(f)) ? D(a.toRange()) : d;
                    if (b(f)) return D(P(f.getNode(-1 == a)));
                    d = -1 == a ? ea.prev(f) : ea.next(f);
                    if (b(d)) return -1 === a ? W(a, f, d) : W(a, d, f)
                }

                function Z() {
                    function a(a, b) {
                        (b = b(v.selection.getRng())) && !a.isDefaultPrevented() && (a.preventDefault(), G(b))
                    }

                    function c(a) {
                        for (var b = v.getBody(); a && a != b;) {
                            if (z(a) || x(a)) return a;
                            a = a.parentNode
                        }
                        return null
                    }

                    function e(a, b, c) {
                        return c.collapsed ? !1 : p.reduce(c.getClientRects(), function(c, d) {
                            return c || q.containsXY(d, a, b)
                        }, !1)
                    }

                    function l() {
                        var a, b = c(v.selection.getNode());
                        z(b) && B(b) && v.dom.isEmpty(b) && (a = v.dom.create("br", {
                            "data-mce-bogus": "1"
                        }), v.$(b).empty().append(a), v.selection.setRng(f.before(a).toRange()))
                    }
                    var m = y(T, 1, fa, F),
                        k = y(T, -1, ia, u),
                        n = y(Q, 1, F, u),
                        r = y(Q, -1, u, F),
                        t = y(ha, -1, b.upUntil),
                        ea = y(ha, 1, b.downUntil);
                    v.on("mouseup", function() {
                        var a = v.selection.getRng();
                        a.collapsed && G(C(a))
                    });
                    v.on("click", function(a) {
                        var b;
                        (b = c(a.target)) && x(b) && a.preventDefault()
                    });
                    v.on("mousedown", function(a) {
                        var b;
                        if (b = c(a.target)) x(b) ? (a.preventDefault(), X(K(b))) : (V(), e(a.clientX,
                            a.clientY, v.selection.getRng()) || v.selection.placeCaretAt(a.clientX, a.clientY));
                        else if (V(), ba.hide(), b = d.closestCaret(ca, a.clientX, a.clientY)) {
                            var l = b.node,
                                h = v.dom.getParent(a.target, v.dom.isBlock),
                                l = v.dom.getParent(l, v.dom.isBlock),
                                m;
                            (m = !h) || (m = v.dom.getParent(h, v.dom.isBlock), l = v.dom.getParent(l, v.dom.isBlock), m = m === l);
                            (l = m) || (l = new g(h), h.firstChild ? (h = f.before(h.firstChild), h = (h = l.next(h)) && !F(h) && !u(h)) : h = !1, l = !h);
                            l && (a.preventDefault(), v.getBody().focus(), G(O(1, b.node, b.before)))
                        }
                    });
                    v.on("keydown",
                        function(b) {
                            if (!h.modifierPressed(b)) switch (b.keyCode) {
                                case h.RIGHT:
                                    a(b, m);
                                    break;
                                case h.DOWN:
                                    a(b, ea);
                                    break;
                                case h.LEFT:
                                    a(b, k);
                                    break;
                                case h.UP:
                                    a(b, t);
                                    break;
                                case h.DELETE:
                                    a(b, n);
                                    break;
                                case h.BACKSPACE:
                                    a(b, r);
                                    break;
                                default:
                                    x(v.selection.getNode()) && b.preventDefault()
                            }
                        });
                    v.on("keyup compositionstart", function(a) {
                        var b = ka("*[data-mce-caret]")[0];
                        b && ("compositionstart" == a.type ? (a.preventDefault(), a.stopPropagation(), M(b)) : "\x26nbsp;" != b.innerHTML && M(b));
                        var c;
                        switch (a.keyCode) {
                            case h.DELETE:
                                c = l();
                                break;
                            case h.BACKSPACE:
                                c = l()
                        }
                        c && a.preventDefault()
                    }, !0);
                    v.on("cut", function() {
                        var a = v.selection.getNode();
                        x(a) && w.setEditorTimeout(v, function() {
                            G(D(P(a)))
                        })
                    });
                    v.on("getSelectionRange", function(a) {
                        var b = a.range;
                        S && (S.parentNode ? (b = b.cloneRange(), b.selectNode(S), a.range = b) : S = null)
                    });
                    v.on("setSelectionRange", function(a) {
                        var b;
                        if (b = X(a.range)) a.range = b
                    });
                    v.on("focus", function() {
                        w.setEditorTimeout(v, function() {
                            v.selection.setRng(D(v.selection.getRng()))
                        }, 0)
                    });
                    A.init(v)
                }

                function aa() {
                    var a = v.contentStyles;
                    a.push(ba.getCss());
                    a.push(".mce-content-body .mce-offscreen-selection {position: absolute;left: -9999999999px;width: 100px;height: 100px;}.mce-content-body *[contentEditable\x3dfalse] {cursor: default;}.mce-content-body *[contentEditable\x3dtrue] {cursor: text;}")
                }

                function X(a) {
                    var b, c = v.$,
                        d = v.dom,
                        f, g, l;
                    if (!a) return V(), null;
                    if (a.collapsed) {
                        V();
                        b = a;
                        if (!e.isCaretContainer(b.startContainer) && !e.isCaretContainer(b.endContainer)) {
                            b = I(1, a);
                            if (x(b.getNode())) return O(1, b.getNode(), !b.isAtEnd());
                            if (x(b.getNode(!0))) return O(1,
                                b.getNode(!0), !1)
                        }
                        return null
                    }
                    g = a.startContainer;
                    f = a.startOffset;
                    a = a.endOffset;
                    3 == g.nodeType && 0 == f && x(g.parentNode) && (g = g.parentNode, f = d.nodeIndex(g), g = g.parentNode);
                    if (1 != g.nodeType) return V(), null;
                    a == f + 1 && (b = g.childNodes[f]);
                    if (!x(b)) return V(), null;
                    l = g = b.cloneNode(!0);
                    a = v.fire("ObjectSelected", {
                        target: b,
                        targetClone: l
                    });
                    if (a.isDefaultPrevented()) return V(), null;
                    l = a.targetClone;
                    f = c("#" + J);
                    0 === f.length && (f = c('\x3cdiv data-mce-bogus\x3d"all" class\x3d"mce-offscreen-selection"\x3e\x3c/div\x3e').attr("id",
                        J), f.appendTo(v.getBody()));
                    a = v.dom.createRng();
                    l === g && k.ie ? (f.empty().append(m.ZWSP).append(l).append(m.ZWSP), a.setStart(f[0].firstChild, 0), a.setEnd(f[0].lastChild, 1)) : (f.empty().append("\u00a0").append(l).append("\u00a0"), a.setStart(f[0].firstChild, 1), a.setEnd(f[0].lastChild, 0));
                    f.css({
                        top: d.getPos(b, v.getBody()).y
                    });
                    f[0].focus();
                    c = v.selection.getSel();
                    c.removeAllRanges();
                    c.addRange(a);
                    v.$("*[data-mce-selected]").removeAttr("data-mce-selected");
                    b.setAttribute("data-mce-selected", 1);
                    S = b;
                    return a
                }

                function V() {
                    S && (S.removeAttribute("data-mce-selected"), v.$("#" + J).remove(), S = null)
                }
                var ca = v.getBody(),
                    ea = new g(ca),
                    fa = y(t, ea.next),
                    ia = y(t, ea.prev),
                    ba = new c(v.getBody(), B),
                    J = "sel-" + v.dom.uniqueId(),
                    S, ka = v.$;
                k.ceFalse && (Z(), aa());
                return {
                    showBlockCaretContainer: M,
                    hideFakeCaret: function() {
                        ba.hide()
                    },
                    destroy: function() {
                        ba.destroy();
                        S = null
                    }
                }
            }
        });
    z("tinymce/util/Uuid", [], function() {
        var k = 0,
            g = function() {
                var f = function() {
                    return Math.round(4294967295 * Math.random()).toString(36)
                };
                return "s" + Date.now().toString(36) +
                    f() + f() + f()
            };
        return {
            uuid: function(f) {
                return f + k++ + g()
            }
        }
    });
    z("tinymce/Editor", "tinymce/dom/DOMUtils tinymce/dom/DomQuery tinymce/AddOnManager tinymce/NodeChange tinymce/html/Node tinymce/dom/Serializer tinymce/html/Serializer tinymce/dom/Selection tinymce/Formatter tinymce/UndoManager tinymce/EnterKey tinymce/ForceBlocks tinymce/EditorCommands tinymce/util/URI tinymce/dom/ScriptLoader tinymce/dom/EventUtils tinymce/WindowManager tinymce/NotificationManager tinymce/html/Schema tinymce/html/DomParser tinymce/util/Quirks tinymce/Env tinymce/util/Tools tinymce/util/Delay tinymce/EditorObservable tinymce/Mode tinymce/Shortcuts tinymce/EditorUpload tinymce/SelectionOverrides tinymce/util/Uuid".split(" "),
        function(k, g, f, e, a, c, b, d, l, v, q, h, n, p, w, A, m, t, y, z, x, r, u, F, E, W, U, G, N, R) {
            function B(a, b, c) {
                var d = this,
                    e, l, h;
                e = d.documentBaseUrl = c.documentBaseURL;
                l = c.baseURI;
                h = c.defaultSettings;
                b = ha({
                    id: a,
                    theme: "modern",
                    delta_width: 0,
                    delta_height: 0,
                    popup_css: "",
                    plugins: "",
                    document_base_url: e,
                    add_form_submit_trigger: !0,
                    submit_patch: !0,
                    add_unload_trigger: !0,
                    convert_urls: !0,
                    relative_urls: !0,
                    remove_script_host: !0,
                    object_resizing: !0,
                    doctype: "\x3c!DOCTYPE html\x3e",
                    visual: !0,
                    font_size_style_values: "xx-small,x-small,small,medium,large,x-large,xx-large",
                    font_size_legacy_values: "xx-small,small,medium,large,x-large,xx-large,300%",
                    forced_root_block: "p",
                    hidden_input: !0,
                    padd_empty_editor: !0,
                    render_ui: !0,
                    indentation: "30px",
                    inline_styles: !0,
                    convert_fonts_to_spans: !0,
                    indent: "simple",
                    indent_before: "p,h1,h2,h3,h4,h5,h6,blockquote,div,title,style,pre,script,td,th,ul,ol,li,dl,dt,dd,area,table,thead,tfoot,tbody,tr,section,article,hgroup,aside,figure,figcaption,option,optgroup,datalist",
                    indent_after: "p,h1,h2,h3,h4,h5,h6,blockquote,div,title,style,pre,script,td,th,ul,ol,li,dl,dt,dd,area,table,thead,tfoot,tbody,tr,section,article,hgroup,aside,figure,figcaption,option,optgroup,datalist",
                    validate: !0,
                    entity_encoding: "named",
                    url_converter: d.convertURL,
                    url_converter_scope: d,
                    ie7_compat: !0
                }, h, b);
                h && h.external_plugins && b.external_plugins && (b.external_plugins = ha({}, h.external_plugins, b.external_plugins));
                d.settings = b;
                f.language = b.language || "en";
                f.languageLoad = b.language_load;
                f.baseURL = c.baseURL;
                d.id = b.id = a;
                d.setDirty(!1);
                d.plugins = {};
                d.documentBaseURI = new p(b.document_base_url || e, {
                    base_uri: l
                });
                d.baseURI = l;
                d.contentCSS = [];
                d.contentStyles = [];
                d.shortcuts = new U(d);
                d.loadedCSS = {};
                d.editorCommands =
                    new n(d);
                b.target && (d.targetElm = b.target);
                d.suffix = c.suffix;
                d.editorManager = c;
                d.inline = b.inline;
                d.settings.content_editable = d.inline;
                b.cache_suffix && (r.cacheSuffix = b.cache_suffix.replace(/^[\?\&]+/, ""));
                !1 === b.override_viewport && (r.overrideViewPort = !1);
                c.fire("SetupEditor", d);
                d.execCallback("setup", d);
                d.$ = g.overrideDefaults(function() {
                    return {
                        context: d.inline ? d.getBody() : d.getDoc(),
                        element: d.getBody()
                    }
                })
            }
            var H = k.DOM,
                L = f.ThemeManager,
                O = f.PluginManager,
                ha = u.extend,
                M = u.each,
                C = u.explode,
                D = u.inArray,
                P =
                u.trim,
                da = u.resolve,
                Q = A.Event,
                Z = r.gecko,
                aa = r.ie;
            B.prototype = {
                render: function() {
                    function a() {
                        H.unbind(window, "ready", a);
                        b.render()
                    }
                    var b = this,
                        c = b.settings,
                        d = b.id,
                        e = b.suffix;
                    if (!Q.domLoaded) H.bind(window, "ready", a);
                    else if (b.getElement() && r.contentEditable) {
                        c.inline ? b.inline = !0 : (b.orgVisibility = b.getElement().style.visibility, b.getElement().style.visibility = "hidden");
                        var f = b.getElement().form || H.getParent(d, "form");
                        f && (b.formElement = f, c.hidden_input && !/TEXTAREA|INPUT/i.test(b.getElement().nodeName) &&
                            (H.insertAfter(H.create("input", {
                                type: "hidden",
                                name: d
                            }), d), b.hasHiddenInput = !0), b.formEventDelegate = function(a) {
                                b.fire(a.type, a)
                            }, H.bind(f, "submit reset", b.formEventDelegate), b.on("reset", function() {
                                b.setContent(b.startContent, {
                                    format: "raw"
                                })
                            }), !c.submit_patch || f.submit.nodeType || f.submit.length || f._mceOldSubmit || (f._mceOldSubmit = f.submit, f.submit = function() {
                                b.editorManager.triggerSave();
                                b.setDirty(!1);
                                return f._mceOldSubmit(f)
                            }));
                        b.windowManager = new m(b);
                        b.notificationManager = new t(b);
                        if ("xml" ==
                            c.encoding) b.on("GetContent", function(a) {
                            a.save && (a.content = H.encode(a.content))
                        });
                        if (c.add_form_submit_trigger) b.on("submit", function() {
                            b.initialized && b.save()
                        });
                        c.add_unload_trigger && (b._beforeUnload = function() {
                            !b.initialized || b.destroyed || b.isHidden() || b.save({
                                format: "raw",
                                no_events: !0,
                                set_dirty: !1
                            })
                        }, b.editorManager.on("BeforeUnload", b._beforeUnload));
                        b.editorManager.add(b);
                        (function() {
                            var a = w.ScriptLoader;
                            c.language && "en" != c.language && !c.language_url && (c.language_url = b.editorManager.baseURL +
                                "/langs/" + c.language + ".js");
                            c.language_url && a.add(c.language_url);
                            if (c.theme && "function" != typeof c.theme && "-" != c.theme.charAt(0) && !L.urls[c.theme]) {
                                var d = c.theme_url,
                                    d = d ? b.documentBaseURI.toAbsolute(d) : "themes/" + c.theme + "/theme" + e + ".js";
                                L.load(c.theme, d)
                            }
                            u.isArray(c.plugins) && (c.plugins = c.plugins.join(" "));
                            M(c.external_plugins, function(a, b) {
                                O.load(b, a);
                                c.plugins += " " + b
                            });
                            M(c.plugins.split(/[ ,]/), function(a) {
                                (a = P(a)) && !O.urls[a] && ("-" == a.charAt(0) ? (a = a.substr(1, a.length), a = O.dependencies(a), M(a,
                                    function(a) {
                                        a = O.createUrl({
                                            prefix: "plugins/",
                                            resource: a,
                                            suffix: "/plugin" + e + ".js"
                                        }, a);
                                        O.load(a.resource, a)
                                    })) : O.load(a, {
                                    prefix: "plugins/",
                                    resource: a,
                                    suffix: "/plugin" + e + ".js"
                                }))
                            });
                            a.loadQueue(function() {
                                b.removed || b.init()
                            })
                        })()
                    }
                },
                init: function() {
                    function a(c) {
                        var d = O.get(c),
                            e;
                        e = O.urls[c] || b.documentBaseUrl.replace(/\/$/, "");
                        c = P(c);
                        d && -1 === D(m, c) && (M(O.dependencies(c), function(b) {
                            a(b)
                        }), b.plugins[c] || (d = new d(b, e, b.$), b.plugins[c] = d, d.init && (d.init(b, e), m.push(c))))
                    }
                    var b = this,
                        c = b.settings,
                        d = b.getElement(),
                        e, f, g, l, h, m = [];
                    this.editorManager.i18n.setCode(c.language);
                    b.rtl = c.rtl_ui || this.editorManager.i18n.rtl;
                    c.aria_label = c.aria_label || H.getAttrib(d, "aria-label", b.getLang("aria.rich_text_area"));
                    c.theme && ("function" != typeof c.theme ? (c.theme = c.theme.replace(/-/, ""), g = L.get(c.theme), b.theme = new g(b, L.urls[c.theme]), b.theme.init && b.theme.init(b, L.urls[c.theme] || b.documentBaseUrl.replace(/\/$/, ""), b.$)) : b.theme = c.theme);
                    M(c.plugins.replace(/\-/g, "").split(/[ ,]/), a);
                    c.render_ui && b.theme && (b.orgDisplay = d.style.display,
                        "function" != typeof c.theme ? (e = c.width || d.style.width || d.offsetWidth, f = c.height || d.style.height || d.offsetHeight, g = c.min_height || 100, h = /^[0-9\.]+(|px)$/i, h.test("" + e) && (e = Math.max(parseInt(e, 10), 100)), h.test("" + f) && (f = Math.max(parseInt(f, 10), g)), e = b.theme.renderUI({
                            targetNode: d,
                            width: e,
                            height: f,
                            deltaWidth: c.delta_width,
                            deltaHeight: c.delta_height
                        }), c.content_editable || (f = (e.iframeHeight || f) + ("number" == typeof f ? e.deltaHeight || 0 : ""), f < g && (f = g))) : (e = c.theme(b, d), e.editorContainer.nodeType && (e.editorContainer =
                            e.editorContainer.id = e.editorContainer.id || b.id + "_parent"), e.iframeContainer.nodeType && (e.iframeContainer = e.iframeContainer.id = e.iframeContainer.id || b.id + "_iframecontainer"), f = e.iframeHeight || d.offsetHeight), b.editorContainer = e.editorContainer);
                    c.content_css && M(C(c.content_css), function(a) {
                        b.contentCSS.push(b.documentBaseURI.toAbsolute(a))
                    });
                    c.content_style && b.contentStyles.push(c.content_style);
                    if (c.content_editable) return d = f = e = null, b.initContentBody();
                    b.iframeHTML = c.doctype + "\x3chtml\x3e\x3chead\x3e";
                    c.document_base_url != b.documentBaseUrl && (b.iframeHTML += '\x3cbase href\x3d"' + b.documentBaseURI.getURI() + '" /\x3e');
                    !r.caretAfter && c.ie7_compat && (b.iframeHTML += '\x3cmeta http-equiv\x3d"X-UA-Compatible" content\x3d"IE\x3d7" /\x3e');
                    b.iframeHTML += '\x3cmeta http-equiv\x3d"Content-Type" content\x3d"text/html; charset\x3dUTF-8" /\x3e';
                    if (!/#$/.test(document.location.href))
                        for (d = 0; d < b.contentCSS.length; d++) g = b.contentCSS[d], b.iframeHTML += '\x3clink type\x3d"text/css" rel\x3d"stylesheet" href\x3d"' + u._addCacheSuffix(g) +
                            '" /\x3e', b.loadedCSS[g] = !0;
                    d = c.body_id || "tinymce"; - 1 != d.indexOf("\x3d") && (d = b.getParam("body_id", "", "hash"), d = d[b.id] || d);
                    g = c.body_class || ""; - 1 != g.indexOf("\x3d") && (g = b.getParam("body_class", "", "hash"), g = g[b.id] || "");
                    c.content_security_policy && (b.iframeHTML += '\x3cmeta http-equiv\x3d"Content-Security-Policy" content\x3d"' + c.content_security_policy + '" /\x3e');
                    b.iframeHTML += '\x3c/head\x3e\x3cbody id\x3d"' + d + '" class\x3d"mce-content-body ' + g + '" data-id\x3d"' + b.id + '"\x3e\x3cbr\x3e\x3c/body\x3e\x3c/html\x3e';
                    c = 'javascript:(function(){document.open();document.domain\x3d"' + document.domain + '";var ed \x3d window.parent.tinymce.get("' + b.id + '");document.write(ed.iframeHTML);document.close();ed.initContentBody(true);})()';
                    document.domain != location.hostname && r.ie && 12 > r.ie && (l = c);
                    var k = H.create("iframe", {
                        id: b.id + "_ifr",
                        frameBorder: "0",
                        allowTransparency: "true",
                        title: b.editorManager.translate("Rich Text Area. Press ALT-F9 for menu. Press ALT-F10 for toolbar. Press ALT-0 for help"),
                        style: {
                            width: "100%",
                            height: f,
                            display: "block"
                        }
                    });
                    k.onload = function() {
                        k.onload = null;
                        b.fire("load")
                    };
                    H.setAttrib(k, "src", l || 'javascript:""');
                    b.contentAreaContainer = e.iframeContainer;
                    b.iframeElement = k;
                    f = H.add(e.iframeContainer, k);
                    if (aa) try {
                        b.getDoc()
                    } catch (na) {
                        f.src = l = c
                    }
                    e.editorContainer && (H.get(e.editorContainer).style.display = b.orgDisplay, b.hidden = H.isHidden(e.editorContainer));
                    b.getElement().style.display = "none";
                    H.setAttrib(b.id, "aria-hidden", !0);
                    l || b.initContentBody();
                    d = f = e = null
                },
                initContentBody: function(b) {
                    var f = this,
                        g = f.settings,
                        m = f.getElement(),
                        n = f.getDoc(),
                        p;
                    g.inline || (f.getElement().style.visibility = f.orgVisibility);
                    b || g.content_editable || (n.open(), n.write(f.iframeHTML), n.close());
                    g.content_editable && (f.on("remove", function() {
                            var a = this.getBody();
                            H.removeClass(a, "mce-content-body");
                            H.removeClass(a, "mce-edit-focus");
                            H.setAttrib(a, "contentEditable", null)
                        }), H.addClass(m, "mce-content-body"), f.contentDocument = n = g.content_document || document, f.contentWindow = g.content_window || window, f.bodyElement = m, g.content_document = g.content_window = null, g.root_name =
                        m.nodeName.toLowerCase());
                    b = f.getBody();
                    b.disabled = !0;
                    f.readonly = g.readonly;
                    f.readonly || (f.inline && "static" == H.getStyle(b, "position", !0) && (b.style.position = "relative"), b.contentEditable = f.getParam("content_editable_state", !0));
                    b.disabled = !1;
                    f.editorUpload = new G(f);
                    f.schema = new y(g);
                    f.dom = new k(n, {
                        keep_values: !0,
                        url_converter: f.convertURL,
                        url_converter_scope: f,
                        hex_colors: g.force_hex_style_colors,
                        class_filter: g.class_filter,
                        update_styles: !0,
                        root_element: f.inline ? f.getBody() : null,
                        collect: g.content_editable,
                        schema: f.schema,
                        onSetAttrib: function(a) {
                            f.fire("SetAttrib", a)
                        }
                    });
                    f.parser = new z(g, f.schema);
                    f.parser.addAttributeFilter("src,href,style,tabindex", function(a, b) {
                        for (var c = a.length, d, e = f.dom, g, l; c--;) d = a[c], g = d.attr(b), l = "data-mce-" + b, d.attributes.map[l] || 0 === g.indexOf("data:") || 0 === g.indexOf("blob:") || ("style" === b ? (g = e.serializeStyle(e.parseStyle(g), d.name), g.length || (g = null), d.attr(l, g), d.attr(b, g)) : "tabindex" === b ? (d.attr(l, g), d.attr(b, null)) : d.attr(l, f.convertURL(g, b, d.name)))
                    });
                    f.parser.addNodeFilter("script",
                        function(a) {
                            for (var b = a.length, c, d; b--;) c = a[b], d = c.attr("type") || "no/type", 0 !== d.indexOf("mce-") && c.attr("type", "mce-" + d)
                        });
                    f.parser.addNodeFilter("#cdata", function(a) {
                        for (var b = a.length, c; b--;) c = a[b], c.type = 8, c.name = "#comment", c.value = "[CDATA[" + c.value + "]]"
                    });
                    f.parser.addNodeFilter("p,h1,h2,h3,h4,h5,h6,div", function(b) {
                        for (var c = b.length, d, e = f.schema.getNonEmptyElements(); c--;) d = b[c], d.isEmpty(e) && (d.append(new a("br", 1)).shortEnded = !0)
                    });
                    f.serializer = new c(g, f);
                    f.selection = new d(f.dom, f.getWin(),
                        f.serializer, f);
                    f.formatter = new l(f);
                    f.undoManager = new v(f);
                    f.forceBlocks = new h(f);
                    f.enterKey = new q(f);
                    f._nodeChangeDispatcher = new e(f);
                    f._selectionOverrides = new N(f);
                    f.fire("PreInit");
                    g.browser_spellcheck || g.gecko_spellcheck || (n.body.spellcheck = !1, H.setAttrib(b, "spellcheck", "false"));
                    f.quirks = new x(f);
                    f.fire("PostRender");
                    g.directionality && (b.dir = g.directionality);
                    g.nowrap && (b.style.whiteSpace = "nowrap");
                    if (g.protect) f.on("BeforeSetContent", function(a) {
                        M(g.protect, function(b) {
                            a.content = a.content.replace(b,
                                function(a) {
                                    return "\x3c!--mce:protected " + escape(a) + "--\x3e"
                                })
                        })
                    });
                    f.on("SetContent", function() {
                        f.addVisual(f.getBody())
                    });
                    if (g.padd_empty_editor) f.on("PostProcess", function(a) {
                        a.content = a.content.replace(/^(<p[^>]*>(&nbsp;|&#160;|\s|\u00a0|)<\/p>[\r\n]*|<br \/>[\r\n]*)$/, "")
                    });
                    f.load({
                        initial: !0,
                        format: "html"
                    });
                    f.startContent = f.getContent({
                        format: "raw"
                    });
                    f.initialized = !0;
                    f.bindPendingEventDelegates();
                    f.fire("init");
                    f.focus(!0);
                    f.nodeChanged({
                        initial: !0
                    });
                    f.execCallback("init_instance_callback", f);
                    f.on("compositionstart compositionend", function(a) {
                        f.composing = "compositionstart" === a.type
                    });
                    0 < f.contentStyles.length && (p = "", M(f.contentStyles, function(a) {
                        p += a + "\r\n"
                    }), f.dom.addStyle(p));
                    M(f.contentCSS, function(a) {
                        f.loadedCSS[a] || (f.dom.loadCSS(a), f.loadedCSS[a] = !0)
                    });
                    g.auto_focus && F.setEditorTimeout(f, function() {
                        var a;
                        a = !0 === g.auto_focus ? f : f.editorManager.get(g.auto_focus);
                        a.destroyed || a.focus()
                    }, 100);
                    m = n = b = null
                },
                focus: function(a) {
                    function b(a) {
                        return c.dom.getParent(a, function(a) {
                            return "true" ===
                                c.dom.getContentEditable(a)
                        })
                    }
                    var c = this,
                        d = c.selection,
                        e = c.settings.content_editable,
                        f, g = c.getDoc(),
                        l = c.getBody();
                    if (!a) {
                        a = d.getRng();
                        a.item && (f = a.item(0));
                        c.quirks.refreshContentEditable();
                        a = b(d.getNode());
                        if (c.$.contains(l, a)) {
                            a.focus();
                            d.normalize();
                            c.editorManager.setActive(c);
                            return
                        }
                        e || (r.opera || c.getBody().focus(), c.getWin().focus());
                        if (Z || e) {
                            if (l.setActive) try {
                                l.setActive()
                            } catch (S) {
                                l.focus()
                            } else l.focus();
                            e && d.normalize()
                        }
                        f && f.ownerDocument == g && (a = g.body.createControlRange(), a.addElement(f),
                            a.select())
                    }
                    c.editorManager.setActive(c)
                },
                execCallback: function(a) {
                    var b = this.settings[a],
                        c;
                    if (b) return this.callbackLookup && (c = this.callbackLookup[a]) && (b = c.func, c = c.scope), "string" === typeof b && (c = (c = b.replace(/\.\w+$/, "")) ? da(c) : 0, b = da(b), this.callbackLookup = this.callbackLookup || {}, this.callbackLookup[a] = {
                        func: b,
                        scope: c
                    }), b.apply(c || this, Array.prototype.slice.call(arguments, 1))
                },
                translate: function(a) {
                    var b = this.settings.language || "en",
                        c = this.editorManager.i18n;
                    if (!a) return "";
                    a = c.data[b + "." + a] ||
                        a.replace(/\{\#([^\}]+)\}/g, function(a, d) {
                            return c.data[b + "." + d] || "{#" + d + "}"
                        });
                    return this.editorManager.translate(a)
                },
                getLang: function(a, b) {
                    return this.editorManager.i18n.data[(this.settings.language || "en") + "." + a] || (b !== K ? b : "{#" + a + "}")
                },
                getParam: function(a, b, c) {
                    a = a in this.settings ? this.settings[a] : b;
                    var d;
                    return "hash" === c ? (d = {}, "string" === typeof a ? M(0 < a.indexOf("\x3d") ? a.split(/[;,](?![^=;,]*(?:[;,]|$))/) : a.split(","), function(a) {
                            a = a.split("\x3d");
                            1 < a.length ? d[P(a[0])] = P(a[1]) : d[P(a[0])] = P(a)
                        }) :
                        d = a, d) : a
                },
                nodeChanged: function(a) {
                    this._nodeChangeDispatcher.nodeChanged(a)
                },
                addButton: function(a, b) {
                    var c = this;
                    b.cmd && (b.onclick = function() {
                        c.execCommand(b.cmd)
                    });
                    b.text || b.icon || (b.icon = a);
                    c.buttons = c.buttons || {};
                    b.tooltip = b.tooltip || b.title;
                    c.buttons[a] = b
                },
                addMenuItem: function(a, b) {
                    var c = this;
                    b.cmd && (b.onclick = function() {
                        c.execCommand(b.cmd)
                    });
                    c.menuItems = c.menuItems || {};
                    c.menuItems[a] = b
                },
                addContextToolbar: function(a, b) {
                    var c = this,
                        d;
                    c.contextToolbars = c.contextToolbars || [];
                    "string" == typeof a && (d =
                        a, a = function(a) {
                            return c.dom.is(a, d)
                        });
                    c.contextToolbars.push({
                        id: R.uuid("mcet"),
                        predicate: a,
                        items: b
                    })
                },
                addCommand: function(a, b, c) {
                    this.editorCommands.addCommand(a, b, c)
                },
                addQueryStateHandler: function(a, b, c) {
                    this.editorCommands.addQueryStateHandler(a, b, c)
                },
                addQueryValueHandler: function(a, b, c) {
                    this.editorCommands.addQueryValueHandler(a, b, c)
                },
                addShortcut: function(a, b, c, d) {
                    this.shortcuts.add(a, b, c, d)
                },
                execCommand: function(a, b, c, d) {
                    return this.editorCommands.execCommand(a, b, c, d)
                },
                queryCommandState: function(a) {
                    return this.editorCommands.queryCommandState(a)
                },
                queryCommandValue: function(a) {
                    return this.editorCommands.queryCommandValue(a)
                },
                queryCommandSupported: function(a) {
                    return this.editorCommands.queryCommandSupported(a)
                },
                show: function() {
                    this.hidden && (this.hidden = !1, this.inline ? this.getBody().contentEditable = !0 : (H.show(this.getContainer()), H.hide(this.id)), this.load(), this.fire("show"))
                },
                hide: function() {
                    var a = this.getDoc();
                    this.hidden || (aa && a && !this.inline && a.execCommand("SelectAll"), this.save(), this.inline ? (this.getBody().contentEditable = !1, this == this.editorManager.focusedEditor &&
                        (this.editorManager.focusedEditor = null)) : (H.hide(this.getContainer()), H.setStyle(this.id, "display", this.orgDisplay)), this.hidden = !0, this.fire("hide"))
                },
                isHidden: function() {
                    return !!this.hidden
                },
                setProgressState: function(a, b) {
                    this.fire("ProgressState", {
                        state: a,
                        time: b
                    })
                },
                load: function(a) {
                    var b = this.getElement(),
                        c;
                    if (b) return a = a || {}, a.load = !0, c = this.setContent(b.value !== K ? b.value : b.innerHTML, a), a.element = b, a.no_events || this.fire("LoadContent", a), a.element = null, c
                },
                save: function(a) {
                    var b = this,
                        c = b.getElement(),
                        d, e;
                    if (c && b.initialized) return a = a || {}, a.save = !0, a.element = c, d = a.content = b.getContent(a), a.no_events || b.fire("SaveContent", a), "raw" == a.format && b.fire("RawSaveContent", a), d = a.content, /TEXTAREA|INPUT/i.test(c.nodeName) ? c.value = d : (b.inline || (c.innerHTML = d), (e = H.getParent(b.id, "form")) && M(e.elements, function(a) {
                        if (a.name == b.id) return a.value = d, !1
                    })), a.element = c = null, !1 !== a.set_dirty && b.setDirty(!1), d
                },
                setContent: function(a, c) {
                    var d = this.getBody(),
                        e, f;
                    c = c || {};
                    c.format = c.format || "html";
                    c.set = !0;
                    c.content =
                        a;
                    c.no_events || this.fire("BeforeSetContent", c);
                    a = c.content;
                    0 === a.length || /^\s+$/.test(a) ? (f = aa && 11 > aa ? "" : '\x3cbr data-mce-bogus\x3d"1"\x3e', "TABLE" == d.nodeName ? a = "\x3ctr\x3e\x3ctd\x3e" + f + "\x3c/td\x3e\x3c/tr\x3e" : /^(UL|OL)$/.test(d.nodeName) && (a = "\x3cli\x3e" + f + "\x3c/li\x3e"), (e = this.settings.forced_root_block) && this.schema.isValidChild(d.nodeName.toLowerCase(), e.toLowerCase()) ? a = this.dom.createHTML(e, this.settings.forced_root_block_attrs, f) : aa || a || (a = '\x3cbr data-mce-bogus\x3d"1"\x3e'), this.dom.setHTML(d,
                        a), this.fire("SetContent", c)) : ("raw" !== c.format && (a = (new b({
                        validate: this.validate
                    }, this.schema)).serialize(this.parser.parse(a, {
                        isRootContent: !0
                    }))), c.content = P(a), this.dom.setHTML(d, c.content), c.no_events || this.fire("SetContent", c));
                    return c.content
                },
                getContent: function(a) {
                    var b;
                    b = this.getBody();
                    a = a || {};
                    a.format = a.format || "html";
                    a.get = !0;
                    a.getInner = !0;
                    a.no_events || this.fire("BeforeGetContent", a);
                    b = "raw" == a.format ? this.serializer.getTrimmedContent() : "text" == a.format ? b.innerText || b.textContent : this.serializer.serialize(b,
                        a);
                    a.content = "text" != a.format ? P(b) : b;
                    a.no_events || this.fire("GetContent", a);
                    return a.content
                },
                insertContent: function(a, b) {
                    b && (a = ha({
                        content: a
                    }, b));
                    this.execCommand("mceInsertContent", !1, a)
                },
                isDirty: function() {
                    return !this.isNotDirty
                },
                setDirty: function(a) {
                    var b = !this.isNotDirty;
                    this.isNotDirty = !a;
                    a && a != b && this.fire("dirty")
                },
                setMode: function(a) {
                    W.setMode(this, a)
                },
                getContainer: function() {
                    this.container || (this.container = H.get(this.editorContainer || this.id + "_parent"));
                    return this.container
                },
                getContentAreaContainer: function() {
                    return this.contentAreaContainer
                },
                getElement: function() {
                    this.targetElm || (this.targetElm = H.get(this.id));
                    return this.targetElm
                },
                getWin: function() {
                    var a;
                    !this.contentWindow && (a = this.iframeElement) && (this.contentWindow = a.contentWindow);
                    return this.contentWindow
                },
                getDoc: function() {
                    var a;
                    !this.contentDocument && (a = this.getWin()) && (this.contentDocument = a.document);
                    return this.contentDocument
                },
                getBody: function() {
                    return this.bodyElement || this.getDoc().body
                },
                convertURL: function(a, b, c) {
                    var d = this.settings;
                    return d.urlconverter_callback ? this.execCallback("urlconverter_callback",
                        a, c, !0, b) : !d.convert_urls || c && "LINK" == c.nodeName || 0 === a.indexOf("file:") || 0 === a.length ? a : d.relative_urls ? this.documentBaseURI.toRelative(a) : a = this.documentBaseURI.toAbsolute(a, d.remove_script_host)
                },
                addVisual: function(a) {
                    var b = this,
                        c = b.settings,
                        d = b.dom,
                        e;
                    a = a || b.getBody();
                    b.hasVisual === K && (b.hasVisual = c.visual);
                    M(d.select("table,a", a), function(a) {
                        var f;
                        switch (a.nodeName) {
                            case "TABLE":
                                e = c.visual_table_class || "mce-item-table";
                                (f = d.getAttrib(a, "border")) && "0" != f || !b.hasVisual ? d.removeClass(a, e) : d.addClass(a,
                                    e);
                                break;
                            case "A":
                                d.getAttrib(a, "href", !1) || (f = d.getAttrib(a, "name") || a.id, e = c.visual_anchor_class || "mce-item-anchor", f && b.hasVisual ? d.addClass(a, e) : d.removeClass(a, e))
                        }
                    });
                    b.fire("VisualAid", {
                        element: a,
                        hasVisual: b.hasVisual
                    })
                },
                remove: function() {
                    this.removed || (this.save(), this.removed = 1, this.unbindAllNativeEvents(), this.hasHiddenInput && H.remove(this.getElement().nextSibling), this.inline || (aa && 10 > aa && this.getDoc().execCommand("SelectAll", !1, null), H.setStyle(this.id, "display", this.orgDisplay), this.getBody().onload =
                        null), this.fire("remove"), this.editorManager.remove(this), H.remove(this.getContainer()), this._selectionOverrides.destroy(), this.editorUpload.destroy(), this.destroy())
                },
                destroy: function(a) {
                    if (!this.destroyed)
                        if (a || this.removed) {
                            a || (this.editorManager.off("beforeunload", this._beforeUnload), this.theme && this.theme.destroy && this.theme.destroy(), this.selection.destroy(), this.dom.destroy());
                            if (a = this.formElement) a._mceOldSubmit && (a.submit = a._mceOldSubmit, a._mceOldSubmit = null), H.unbind(a, "submit reset", this.formEventDelegate);
                            this.iframeElement = this.targetElm = this.bodyElement = this.contentDocument = this.contentWindow = this.contentAreaContainer = this.formElement = this.container = this.editorContainer = null;
                            this.selection && (this.selection = this.selection.win = this.selection.dom = this.selection.dom.doc = null);
                            this.destroyed = 1
                        } else this.remove()
                },
                uploadImages: function(a) {
                    return this.editorUpload.uploadImages(a)
                },
                _scanForImages: function() {
                    return this.editorUpload.scanForImages()
                }
            };
            ha(B.prototype, E);
            return B
        });
    z("tinymce/util/I18n", [],
        function() {
            var k = {},
                g = "en";
            return {
                setCode: function(f) {
                    f && (g = f, this.rtl = this.data[f] ? "rtl" === this.data[f]._dir : !1)
                },
                getCode: function() {
                    return g
                },
                rtl: !1,
                add: function(f, e) {
                    var a = k[f];
                    a || (k[f] = a = {});
                    for (var c in e) a[c] = e[c];
                    this.setCode(f)
                },
                translate: function(f) {
                    var e;
                    (e = k[g]) || (e = {});
                    if ("undefined" == typeof f) return f;
                    if ("string" != typeof f && f.raw) return f.raw;
                    if (f.push) {
                        var a = f.slice(1);
                        f = (e[f[0]] || f[0]).replace(/\{([0-9]+)\}/g, function(c, b) {
                            return a[b]
                        })
                    }
                    return (e[f] || f).replace(/{context:\w+}$/, "")
                },
                data: k
            }
        });
    z("tinymce/FocusManager", ["tinymce/dom/DOMUtils", "tinymce/util/Delay", "tinymce/Env"], function(k, g, f) {
        function e(l) {
            function k() {
                try {
                    return document.activeElement
                } catch (q) {
                    return document.body
                }
            }
            l.on("AddEditor", function(q) {
                var h = q.editor;
                h.on("init", function() {
                    if (h.inline || f.ie) {
                        if ("onbeforedeactivate" in document && 9 > f.ie) h.dom.bind(h.getBody(), "beforedeactivate", function(a) {
                            if (a.target == h.getBody()) try {
                                h.lastRng = h.selection.getRng()
                            } catch (p) {}
                        });
                        else h.on("nodechange mouseup keyup", function(a) {
                            var b =
                                k();
                            "nodechange" == a.type && a.selectionChange || (b && b.id == h.id + "_ifr" && (b = h.getBody()), h.dom.isChildOf(b, h.getBody()) && (h.lastRng = h.selection.getRng()))
                        });
                        f.webkit && !a && (a = function() {
                            var a = l.activeEditor;
                            a && a.selection && (a = a.selection.getRng()) && !a.collapsed && (h.lastRng = a)
                        }, d.bind(document, "selectionchange", a))
                    }
                });
                h.on("setcontent", function() {
                    h.lastRng = null
                });
                h.on("mousedown", function() {
                    h.selection.lastFocusBookmark = null
                });
                h.on("focusin", function() {
                    var a = l.focusedEditor;
                    if (h.selection.lastFocusBookmark) {
                        var b =
                            h.selection.lastFocusBookmark,
                            c;
                        b.startContainer ? (c = h.getDoc().createRange(), c.setStart(b.startContainer, b.startOffset), c.setEnd(b.endContainer, b.endOffset)) : c = b;
                        h.selection.lastFocusBookmark = null;
                        h.selection.setRng(c)
                    }
                    a != h && (a && a.fire("blur", {
                        focusedEditor: h
                    }), l.setActive(h), l.focusedEditor = h, h.fire("focus", {
                        blurredEditor: a
                    }), h.focus(!0));
                    h.lastRng = null
                });
                h.on("focusout", function() {
                    g.setEditorTimeout(h, function() {
                        var a = l.focusedEditor;
                        d.getParent(k(), e.isEditorUIElement) || a != h || (h.fire("blur", {
                                focusedEditor: null
                            }),
                            l.focusedEditor = null, h.selection && (h.selection.lastFocusBookmark = null))
                    })
                });
                c || (c = function(a) {
                    var b = l.activeEditor;
                    a = a.target;
                    if (b && a.ownerDocument == document) {
                        if (b.selection && a != b.getBody()) {
                            var c = b.selection,
                                f;
                            f = b.dom;
                            var g = b.lastRng;
                            f = g && g.startContainer ? f.isChildOf(g.startContainer, f.getRoot()) && f.isChildOf(g.endContainer, f.getRoot()) ? {
                                startContainer: g.startContainer,
                                startOffset: g.startOffset,
                                endContainer: g.endContainer,
                                endOffset: g.endOffset
                            } : void 0 : g;
                            c.lastFocusBookmark = f
                        }
                        a == document.body || d.getParent(a,
                            e.isEditorUIElement) || l.focusedEditor != b || (b.fire("blur", {
                            focusedEditor: null
                        }), l.focusedEditor = null)
                    }
                }, d.bind(document, "focusin", c));
                h.inline && !b && (b = function(a) {
                    var b = l.activeEditor,
                        c = b.dom;
                    b.inline && c && !c.isChildOf(a.target, b.getBody()) && (a = b.selection.getRng(), a.collapsed || (b.lastRng = a))
                }, d.bind(document, "mouseup", b))
            });
            l.on("RemoveEditor", function(e) {
                l.focusedEditor == e.editor && (l.focusedEditor = null);
                l.activeEditor || (d.unbind(document, "selectionchange", a), d.unbind(document, "focusin", c), d.unbind(document,
                    "mouseup", b), a = c = b = null)
            })
        }
        var a, c, b, d = k.DOM;
        e.isEditorUIElement = function(a) {
            return -1 !== a.className.toString().indexOf("mce-")
        };
        return e
    });
    z("tinymce/EditorManager", "tinymce/Editor tinymce/dom/DomQuery tinymce/dom/DOMUtils tinymce/util/URI tinymce/Env tinymce/util/Tools tinymce/util/Promise tinymce/util/Observable tinymce/util/I18n tinymce/FocusManager".split(" "), function(k, g, f, e, a, c, b, d, l, v) {
        function q(a) {
            A(z.editors, function(b) {
                "scroll" === a.type ? b.fire("ScrollWindow", a) : b.fire("ResizeWindow", a)
            })
        }

        function h(a, b) {
            if (b !== x) {
                if (b) g(window).on("resize scroll", q);
                else g(window).off("resize scroll", q);
                x = b
            }
        }

        function n(a) {
            var b = z.editors,
                c;
            delete b[a.id];
            for (var d = 0; d < b.length; d++)
                if (b[d] == a) {
                    b.splice(d, 1);
                    c = !0;
                    break
                }
            z.activeEditor == a && (z.activeEditor = b[0]);
            z.focusedEditor == a && (z.focusedEditor = null);
            return c
        }
        var p = f.DOM,
            w = c.explode,
            A = c.each,
            m = c.extend,
            t = 0,
            y, z, x = !1;
        z = {
            $: g,
            majorVersion: "4",
            minorVersion: "4.0",
            releaseDate: "2016-06-30",
            editors: [],
            i18n: l,
            activeEditor: null,
            setup: function() {
                var a, b, c = "",
                    d;
                b = e.getDocumentBaseUrl(document.location);
                /^[^:]+:\/\/\/?[^\/]+\//.test(b) && (b = b.replace(/[\?#].*$/, "").replace(/[\/\\][^\/]+$/, ""), /[\/\\]$/.test(b) || (b += "/"));
                if (d = window.tinymce || window.tinyMCEPreInit) a = d.base || d.baseURL, c = d.suffix;
                else {
                    for (var f = document.getElementsByTagName("script"), g = 0; g < f.length; g++) {
                        d = f[g].src;
                        var l = d.substring(d.lastIndexOf("/"));
                        if (/tinymce(\.full|\.jquery|)(\.min|\.dev|)\.js/.test(d)) {
                            -1 != l.indexOf(".min") && (c = ".min");
                            a = d.substring(0, d.lastIndexOf("/"));
                            break
                        }
                    }!a && document.currentScript &&
                        (d = document.currentScript.src, -1 != d.indexOf(".min") && (c = ".min"), a = d.substring(0, d.lastIndexOf("/")))
                }
                this.baseURL = (new e(b)).toAbsolute(a);
                this.documentBaseURL = b;
                this.baseURI = new e(this.baseURL);
                this.suffix = c;
                this.focusManager = new v(this)
            },
            overrideDefaults: function(a) {
                var b;
                if (b = a.base_url) this.baseURL = (new e(this.documentBaseURL)).toAbsolute(b.replace(/\/+$/, "")), this.baseURI = new e(this.baseURL);
                if (b = a.suffix) this.suffix = b;
                this.defaultSettings = a
            },
            init: function(a) {
                function d(a) {
                    var b = a.id;
                    b || (b = (b =
                        a.name) && !p.get(b) ? a.name : p.uniqueId(), a.setAttribute("id", b));
                    return b
                }

                function e(b) {
                    var c = a[b];
                    if (c) return c.apply(q, Array.prototype.slice.call(arguments, 2))
                }

                function f(a, b) {
                    return b.constructor === RegExp ? b.test(a.className) : p.hasClass(a, b)
                }

                function l(a) {
                    var b, c = [];
                    if (a.types) return A(a.types, function(a) {
                        c = c.concat(p.select(a.selector))
                    }), c;
                    if (a.selector) return p.select(a.selector);
                    if (a.target) return [a.target];
                    switch (a.mode) {
                        case "exact":
                            b = a.elements || "";
                            0 < b.length && A(w(b), function(a) {
                                var b;
                                (b = p.get(a)) ?
                                c.push(b): A(document.forms, function(b) {
                                    A(b.elements, function(b) {
                                        b.name === a && (a = "mce_editor_" + t++, p.setAttrib(b, "id", a), c.push(b))
                                    })
                                })
                            });
                            break;
                        case "textareas":
                        case "specific_textareas":
                            A(p.select("textarea"), function(b) {
                                a.editor_deselector && f(b, a.editor_deselector) || a.editor_selector && !f(b, a.editor_selector) || c.push(b)
                            })
                    }
                    return c
                }

                function h() {
                    function b(a, b, c) {
                        a = new k(a, b, q);
                        v.push(a);
                        a.on("init", function() {
                            ++f === t.length && x(v)
                        });
                        a.targetElm = a.targetElm || c;
                        a.render()
                    }
                    var f = 0,
                        v = [],
                        t;
                    p.unbind(window,
                        "ready", h);
                    e("onpageload");
                    t = g.unique(l(a));
                    a.types ? A(a.types, function(e) {
                        c.each(t, function(c) {
                            return p.is(c, e.selector) ? (b(d(c), m({}, a, e), c), !1) : !0
                        })
                    }) : (c.each(t, function(a) {
                        (a = q.get(a.id)) && a.initialized && !(a.getContainer() || a.getBody()).parentNode && (n(a), a.unbindAllNativeEvents(), a.destroy(!0), a.removed = !0)
                    }), t = c.grep(t, function(a) {
                        return !q.get(a.id)
                    }), A(t, function(c) {
                        a.inline && c.tagName.toLowerCase() in r ? window.console && !window.test && window.console.log("Could not initialize inline editor on invalid inline target element",
                            c) : b(d(c), a, c)
                    }))
                }
                var q = this,
                    v, r;
                r = c.makeMap("area base basefont br col frame hr img input isindex link meta param embed source wbr track colgroup option tbody tfoot thead tr script noscript style textarea video audio iframe object menu", " ");
                var x = function(a) {
                    v = a
                };
                q.settings = a;
                p.bind(window, "ready", h);
                return new b(function(a) {
                    v ? a(v) : x = function(b) {
                        a(b)
                    }
                })
            },
            get: function(a) {
                return arguments.length ? a in this.editors ? this.editors[a] : null : this.editors
            },
            add: function(a) {
                var b = this,
                    c = b.editors;
                c[a.id] = a;
                c.push(a);
                h(c, !0);
                b.activeEditor = a;
                b.fire("AddEditor", {
                    editor: a
                });
                y || (y = function() {
                    b.fire("BeforeUnload")
                }, p.bind(window, "beforeunload", y));
                return a
            },
            createEditor: function(a, b) {
                return this.add(new k(a, b, this))
            },
            remove: function(a) {
                var b = this,
                    c = b.editors,
                    d;
                if (a)
                    if ("string" == typeof a) a = a.selector || a, A(p.select(a), function(a) {
                        (d = c[a.id]) && b.remove(d)
                    });
                    else {
                        d = a;
                        if (!c[d.id]) return null;
                        n(d) && b.fire("RemoveEditor", {
                            editor: d
                        });
                        c.length || p.unbind(window, "beforeunload", y);
                        d.remove();
                        h(c, 0 < c.length);
                        return d
                    }
                else
                    for (a =
                        c.length - 1; 0 <= a; a--) b.remove(c[a])
            },
            execCommand: function(a, b, c) {
                var d = this.get(c);
                switch (a) {
                    case "mceAddEditor":
                        return this.get(c) || (new k(c, this.settings, this)).render(), !0;
                    case "mceRemoveEditor":
                        return d && d.remove(), !0;
                    case "mceToggleEditor":
                        if (!d) return this.execCommand("mceAddEditor", 0, c), !0;
                        d.isHidden() ? d.show() : d.hide();
                        return !0
                }
                return this.activeEditor ? this.activeEditor.execCommand(a, b, c) : !1
            },
            triggerSave: function() {
                A(this.editors, function(a) {
                    a.save()
                })
            },
            addI18n: function(a, b) {
                l.add(a, b)
            },
            translate: function(a) {
                return l.translate(a)
            },
            setActive: function(a) {
                var b = this.activeEditor;
                this.activeEditor != a && (b && b.fire("deactivate", {
                    relatedTarget: a
                }), a.fire("activate", {
                    relatedTarget: b
                }));
                this.activeEditor = a
            }
        };
        m(z, d);
        z.setup();
        return window.tinymce = window.tinyMCE = z
    });
    z("tinymce/LegacyInput", ["tinymce/EditorManager", "tinymce/util/Tools"], function(k, g) {
        var f = g.each,
            e = g.explode;
        k.on("AddEditor", function(a) {
            var c = a.editor;
            c.on("preInit", function() {
                function a(a, b) {
                    f(b, function(b, c) {
                        b && q.setStyle(a, c, b)
                    });
                    q.rename(a, "span")
                }

                function d(a) {
                    q =
                        c.dom;
                    h.convert_fonts_to_spans && f(q.select("font,u,strike", a.node), function(a) {
                        g[a.nodeName.toLowerCase()](q, a)
                    })
                }
                var g, k, q, h = c.settings;
                h.inline_styles && (k = e(h.font_size_legacy_values), g = {
                    font: function(b, c) {
                        a(c, {
                            backgroundColor: c.style.backgroundColor,
                            color: c.color,
                            fontFamily: c.face,
                            fontSize: k[parseInt(c.size, 10) - 1]
                        })
                    },
                    u: function(b, d) {
                        "html4" === c.settings.schema && a(d, {
                            textDecoration: "underline"
                        })
                    },
                    strike: function(b, c) {
                        a(c, {
                            textDecoration: "line-through"
                        })
                    }
                }, c.on("PreProcess SetContent", d))
            })
        })
    });
    z("tinymce/util/XHR", ["tinymce/util/Observable", "tinymce/util/Tools"], function(k, g) {
        var f = {
            send: function(e) {
                function a() {
                    !e.async || 4 == c.readyState || 1E4 < b++ ? (e.success && 1E4 > b && 200 == c.status ? e.success.call(e.success_scope, "" + c.responseText, c, e) : e.error && e.error.call(e.error_scope, 1E4 < b ? "TIMED_OUT" : "GENERAL", c, e), c = null) : setTimeout(a, 10)
                }
                var c, b = 0;
                e.scope = e.scope || this;
                e.success_scope = e.success_scope || e.scope;
                e.error_scope = e.error_scope || e.scope;
                e.async = !1 === e.async ? !1 : !0;
                e.data = e.data || "";
                f.fire("beforeInitialize", {
                    settings: e
                });
                c = new XMLHttpRequest;
                c.overrideMimeType && c.overrideMimeType(e.content_type);
                c.open(e.type || (e.data ? "POST" : "GET"), e.url, e.async);
                e.crossDomain && (c.withCredentials = !0);
                e.content_type && c.setRequestHeader("Content-Type", e.content_type);
                e.requestheaders && g.each(e.requestheaders, function(a) {
                    c.setRequestHeader(a.key, a.value)
                });
                c.setRequestHeader("X-Requested-With", "XMLHttpRequest");
                c = f.fire("beforeSend", {
                    xhr: c,
                    settings: e
                }).xhr;
                c.send(e.data);
                if (!e.async) return a();
                setTimeout(a, 10)
            }
        };
        g.extend(f,
            k);
        return f
    });
    z("tinymce/util/JSON", [], function() {
        function k(g, f) {
            var e, a, c, b;
            f = f || '"';
            if (null === g) return "null";
            c = typeof g;
            if ("string" == c) return a = "\bb\tt\nn\ff\rr\"\"''\\\\", f + g.replace(/([\u0080-\uFFFF\x00-\x1f\"\'\\])/g, function(b, c) {
                if ('"' === f && "'" === b) return b;
                e = a.indexOf(c);
                if (e + 1) return "\\" + a.charAt(e + 1);
                b = c.charCodeAt().toString(16);
                return "\\u" + "0000".substring(b.length) + b
            }) + f;
            if ("object" == c) {
                if (g.hasOwnProperty && "[object Array]" === Object.prototype.toString.call(g)) {
                    e = 0;
                    for (a = "["; e < g.length; e++) a +=
                        (0 < e ? "," : "") + k(g[e], f);
                    return a + "]"
                }
                a = "{";
                for (b in g) g.hasOwnProperty(b) && (a += "function" != typeof g[b] ? (1 < a.length ? "," + f : f) + b + f + ":" + k(g[b], f) : "");
                return a + "}"
            }
            return "" + g
        }
        return {
            serialize: k,
            parse: function(g) {
                try {
                    return window[String.fromCharCode(101) + "val"]("(" + g + ")")
                } catch (f) {}
            }
        }
    });
    z("tinymce/util/JSONRequest", ["tinymce/util/JSON", "tinymce/util/XHR", "tinymce/util/Tools"], function(k, g, f) {
        function e(c) {
            this.settings = a({}, c);
            this.count = 0
        }
        var a = f.extend;
        e.sendRPC = function(a) {
            return (new e).send(a)
        };
        e.prototype = {
            send: function(c) {
                var b = c.error,
                    d = c.success;
                c = a(this.settings, c);
                c.success = function(a, e) {
                    a = k.parse(a);
                    "undefined" == typeof a && (a = {
                        error: "JSON Parse error."
                    });
                    a.error ? b.call(c.error_scope || c.scope, a.error, e) : d.call(c.success_scope || c.scope, a.result)
                };
                c.error = function(a, d) {
                    b && b.call(c.error_scope || c.scope, a, d)
                };
                c.data = k.serialize({
                    id: c.id || "c" + this.count++,
                    method: c.method,
                    params: c.params
                });
                c.content_type = "application/json";
                g.send(c)
            }
        };
        return e
    });
    z("tinymce/util/JSONP", ["tinymce/dom/DOMUtils"], function(k) {
        return {
            callbacks: {},
            count: 0,
            send: function(g) {
                var f = this,
                    e = k.DOM,
                    a = g.count !== K ? g.count : f.count,
                    c = "tinymce_jsonp_" + a;
                f.callbacks[a] = function(b) {
                    e.remove(c);
                    delete f.callbacks[a];
                    g.callback(b)
                };
                e.add(e.doc.body, "script", {
                    id: c,
                    src: g.url,
                    type: "text/javascript"
                });
                f.count++
            }
        }
    });
    z("tinymce/util/LocalStorage", [], function() {
        function k() {
            c = [];
            for (var b in a) c.push(b);
            f.length = c.length
        }

        function g() {
            var c, f = "";
            if (d) {
                for (var g in a) c = a[g], f += (f ? "," : "") + g.length.toString(32) + "," + g + "," + c.length.toString(32) + "," + c;
                e.setAttribute(b, f);
                try {
                    e.save(b)
                } catch (h) {}
                k()
            }
        }
        var f, e, a, c, b, d;
        try {
            if (window.localStorage) return localStorage
        } catch (l) {}
        b = "tinymce";
        e = document.documentElement;
        (d = !!e.addBehavior) && e.addBehavior("#default#userData");
        f = {
            key: function(a) {
                return c[a]
            },
            getItem: function(b) {
                return b in a ? a[b] : null
            },
            setItem: function(b, c) {
                a[b] = "" + c;
                g()
            },
            removeItem: function(b) {
                delete a[b];
                g()
            },
            clear: function() {
                a = {};
                g()
            }
        };
        (function() {
            function c(a) {
                var b;
                b = a !== K ? n + a : g.indexOf(",", n);
                if (-1 === b || b > g.length) return null;
                a = g.substring(n, b);
                n = b + 1;
                return a
            }
            var f, g, h, n = 0;
            a = {};
            if (d) {
                e.load(b);
                g = e.getAttribute(b) || "";
                do {
                    h = c();
                    if (null === h) break;
                    f = c(parseInt(h, 32) || 0);
                    if (null !== f) {
                        h = c();
                        if (null === h) break;
                        h = c(parseInt(h, 32) || 0);
                        f && (a[f] = h)
                    }
                } while (null !== f);
                k()
            }
        })();
        return f
    });
    z("tinymce/Compat", "tinymce/dom/DOMUtils tinymce/dom/EventUtils tinymce/dom/ScriptLoader tinymce/AddOnManager tinymce/util/Tools tinymce/Env".split(" "), function(k, g, f, e, a, c) {
        var b = window.tinymce;
        b.DOM = k.DOM;
        b.ScriptLoader = f.ScriptLoader;
        b.PluginManager = e.PluginManager;
        b.ThemeManager =
            e.ThemeManager;
        b.dom = b.dom || {};
        b.dom.Event = g.Event;
        a.each(a, function(a, c) {
            b[c] = a
        });
        a.each(["isOpera", "isWebKit", "isIE", "isGecko", "isMac"], function(a) {
            b[a] = c[a.substr(2).toLowerCase()]
        });
        return {}
    });
    z("tinymce/ui/Layout", ["tinymce/util/Class", "tinymce/util/Tools"], function(k, g) {
        return k.extend({
            Defaults: {
                firstControlClass: "first",
                lastControlClass: "last"
            },
            init: function(f) {
                this.settings = g.extend({}, this.Defaults, f)
            },
            preRender: function(f) {
                f.bodyClasses.add(this.settings.containerClass)
            },
            applyClasses: function(f) {
                var e =
                    this.settings,
                    a, c, b, d;
                a = e.firstControlClass;
                c = e.lastControlClass;
                f.each(function(f) {
                    f.classes.remove(a).remove(c).add(e.controlClass);
                    f.visible() && (b || (b = f), d = f)
                });
                b && b.classes.add(a);
                d && d.classes.add(c)
            },
            renderHtml: function(f) {
                var e = "";
                this.applyClasses(f.items());
                f.items().each(function(a) {
                    e += a.renderHtml()
                });
                return e
            },
            recalc: function() {},
            postRender: function() {},
            isNative: function() {
                return !1
            }
        })
    });
    z("tinymce/ui/AbsoluteLayout", ["tinymce/ui/Layout"], function(k) {
        return k.extend({
            Defaults: {
                containerClass: "abs-layout",
                controlClass: "abs-layout-item"
            },
            recalc: function(g) {
                g.items().filter(":visible").each(function(f) {
                    var e = f.settings;
                    f.layoutRect({
                        x: e.x,
                        y: e.y,
                        w: e.w,
                        h: e.h
                    });
                    f.recalc && f.recalc()
                })
            },
            renderHtml: function(g) {
                return '\x3cdiv id\x3d"' + g._id + '-absend" class\x3d"' + g.classPrefix + 'abs-end"\x3e\x3c/div\x3e' + this._super(g)
            }
        })
    });
    z("tinymce/ui/Button", ["tinymce/ui/Widget"], function(k) {
        return k.extend({
            Defaults: {
                classes: "widget btn",
                role: "button"
            },
            init: function(g) {
                var f = this,
                    e;
                f._super(g);
                g = f.settings;
                e = f.settings.size;
                f.on("click mousedown", function(a) {
                    a.preventDefault()
                });
                f.on("touchstart", function(a) {
                    f.fire("click", a);
                    a.preventDefault()
                });
                g.subtype && f.classes.add(g.subtype);
                e && f.classes.add("btn-" + e);
                g.icon && f.icon(g.icon)
            },
            icon: function(g) {
                if (!arguments.length) return this.state.get("icon");
                this.state.set("icon", g);
                return this
            },
            repaint: function() {
                var g = this.getEl().firstChild;
                g && (g = g.style, g.width = g.height = "100%");
                this._super()
            },
            renderHtml: function() {
                var g = this._id,
                    f = this.classPrefix,
                    e = this.state.get("icon"),
                    a, c = this.state.get("text"),
                    b = "";
                (a = this.settings.image) ? (e = "none", "string" != typeof a && (a = window.getSelection ? a[0] : a[1]), a = " style\x3d\"background-image: url('" + a + "')\"") : a = "";
                c && (this.classes.add("btn-has-text"), b = '\x3cspan class\x3d"' + f + 'txt"\x3e' + this.encode(c) + "\x3c/span\x3e");
                e = this.settings.icon ? f + "ico " + f + "i-" + e : "";
                return '\x3cdiv id\x3d"' + g + '" class\x3d"' + this.classes + '" tabindex\x3d"-1" aria-labelledby\x3d"' + g + '"\x3e\x3cbutton role\x3d"presentation" type\x3d"button" tabindex\x3d"-1"\x3e' + (e ?
                    '\x3ci class\x3d"' + e + '"' + a + "\x3e\x3c/i\x3e" : "") + b + "\x3c/button\x3e\x3c/div\x3e"
            },
            bindStates: function() {
                function g(c) {
                    var b = e("span." + a, f.getEl());
                    c ? (b[0] || (e("button:first", f.getEl()).append('\x3cspan class\x3d"' + a + '"\x3e\x3c/span\x3e'), b = e("span." + a, f.getEl())), b.html(f.encode(c))) : b.remove();
                    f.classes.toggle("btn-has-text", !!c)
                }
                var f = this,
                    e = f.$,
                    a = f.classPrefix + "txt";
                f.state.on("change:text", function(a) {
                    g(a.value)
                });
                f.state.on("change:icon", function(a) {
                    a = a.value;
                    var b = f.classPrefix;
                    a = (f.settings.icon =
                        a) ? b + "ico " + b + "i-" + f.settings.icon : "";
                    var b = f.getEl().firstChild,
                        c = b.getElementsByTagName("i")[0];
                    a ? (c && c == b.firstChild || (c = document.createElement("i"), b.insertBefore(c, b.firstChild)), c.className = a) : c && b.removeChild(c);
                    g(f.state.get("text"))
                });
                return f._super()
            }
        })
    });
    z("tinymce/ui/ButtonGroup", ["tinymce/ui/Container"], function(k) {
        return k.extend({
            Defaults: {
                defaultType: "button",
                role: "group"
            },
            renderHtml: function() {
                var g = this._layout;
                this.classes.add("btn-group");
                this.preRender();
                g.preRender(this);
                return '\x3cdiv id\x3d"' +
                    this._id + '" class\x3d"' + this.classes + '"\x3e\x3cdiv id\x3d"' + this._id + '-body"\x3e' + (this.settings.html || "") + g.renderHtml(this) + "\x3c/div\x3e\x3c/div\x3e"
            }
        })
    });
    z("tinymce/ui/Checkbox", ["tinymce/ui/Widget"], function(k) {
        return k.extend({
            Defaults: {
                classes: "checkbox",
                role: "checkbox",
                checked: !1
            },
            init: function(g) {
                var f = this;
                f._super(g);
                f.on("click mousedown", function(e) {
                    e.preventDefault()
                });
                f.on("click", function(e) {
                    e.preventDefault();
                    f.disabled() || f.checked(!f.checked())
                });
                f.checked(f.settings.checked)
            },
            checked: function(g) {
                if (!arguments.length) return this.state.get("checked");
                this.state.set("checked", g);
                return this
            },
            value: function(g) {
                return arguments.length ? this.checked(g) : this.checked()
            },
            renderHtml: function() {
                var g = this._id,
                    f = this.classPrefix;
                return '\x3cdiv id\x3d"' + g + '" class\x3d"' + this.classes + '" unselectable\x3d"on" aria-labelledby\x3d"' + g + '-al" tabindex\x3d"-1"\x3e\x3ci class\x3d"' + f + "ico " + f + 'i-checkbox"\x3e\x3c/i\x3e\x3cspan id\x3d"' + g + '-al" class\x3d"' + f + 'label"\x3e' + this.encode(this.state.get("text")) +
                    "\x3c/span\x3e\x3c/div\x3e"
            },
            bindStates: function() {
                function g(e) {
                    f.classes.toggle("checked", e);
                    f.aria("checked", e)
                }
                var f = this;
                f.state.on("change:text", function(e) {
                    f.getEl("al").firstChild.data = f.translate(e.value)
                });
                f.state.on("change:checked change:value", function(e) {
                    f.fire("change");
                    g(e.value)
                });
                f.state.on("change:icon", function(e) {
                    e = e.value;
                    var a = f.classPrefix;
                    if ("undefined" == typeof e) return f.settings.icon;
                    e = (f.settings.icon = e) ? a + "ico " + a + "i-" + f.settings.icon : "";
                    var a = f.getEl().firstChild,
                        c = a.getElementsByTagName("i")[0];
                    e ? (c && c == a.firstChild || (c = document.createElement("i"), a.insertBefore(c, a.firstChild)), c.className = e) : c && a.removeChild(c)
                });
                f.state.get("checked") && g(!0);
                return f._super()
            }
        })
    });
    z("tinymce/ui/ComboBox", ["tinymce/ui/Widget", "tinymce/ui/Factory", "tinymce/ui/DomUtils", "tinymce/dom/DomQuery"], function(k, g, f, e) {
        return k.extend({
            init: function(a) {
                var c = this;
                c._super(a);
                a = c.settings;
                c.classes.add("combobox");
                c.subinput = !0;
                c.ariaTarget = "inp";
                a.menu = a.menu || a.values;
                a.menu && (a.icon = "caret");
                c.on("click", function(b) {
                    var d =
                        b.target,
                        f = c.getEl();
                    if (e.contains(f, d) || d == f)
                        for (; d && d != f;) d.id && -1 != d.id.indexOf("-open") && (c.fire("action"), a.menu && (c.showMenu(), b.aria && c.menu.items()[0].focus())), d = d.parentNode
                });
                c.on("keydown", function(a) {
                    "INPUT" == a.target.nodeName && 13 == a.keyCode && c.parents().reverse().each(function(b) {
                        var d = c.state.get("value"),
                            e = c.getEl("inp").value;
                        a.preventDefault();
                        c.state.set("value", e);
                        d != e && c.fire("change");
                        if (b.hasEventListeners("submit") && b.toJSON) return b.fire("submit", {
                            data: b.toJSON()
                        }), !1
                    })
                });
                c.on("keyup", function(a) {
                    "INPUT" == a.target.nodeName && c.state.set("value", a.target.value)
                })
            },
            showMenu: function() {
                var a = this,
                    c = a.settings;
                a.menu || (c = c.menu || [], c.length ? c = {
                    type: "menu",
                    items: c
                } : c.type = c.type || "menu", a.menu = g.create(c).parent(a).renderTo(a.getContainerElm()), a.fire("createmenu"), a.menu.reflow(), a.menu.on("cancel", function(b) {
                    b.control === a.menu && a.focus()
                }), a.menu.on("show hide", function(b) {
                    b.control.items().each(function(b) {
                        b.active(b.value() == a.value())
                    })
                }).fire("show"), a.menu.on("select",
                    function(b) {
                        a.value(b.control.value())
                    }), a.on("focusin", function(b) {
                    "INPUT" == b.target.tagName.toUpperCase() && a.menu.hide()
                }), a.aria("expanded", !0));
                a.menu.show();
                a.menu.layoutRect({
                    w: a.layoutRect().w
                });
                a.menu.moveRel(a.getEl(), a.isRtl() ? ["br-tr", "tr-br"] : ["bl-tl", "tl-bl"])
            },
            focus: function() {
                this.getEl("inp").focus()
            },
            repaint: function() {
                var a = this.getEl(),
                    c = this.getEl("open"),
                    b = this.layoutRect(),
                    d, c = c ? b.w - f.getSize(c).width - 10 : b.w - 10,
                    b = document;
                b.all && (!b.documentMode || 8 >= b.documentMode) && (d = this.layoutRect().h -
                    2 + "px");
                e(a.firstChild).css({
                    width: c,
                    lineHeight: d
                });
                this._super();
                return this
            },
            postRender: function() {
                var a = this;
                e(this.getEl("inp")).on("change", function(c) {
                    a.state.set("value", c.target.value);
                    a.fire("change", c)
                });
                return a._super()
            },
            renderHtml: function() {
                var a = this._id,
                    c = this.settings,
                    b = this.classPrefix,
                    d = this.state.get("value") || "",
                    e, f, g = "",
                    h = "";
                "spellcheck" in c && (h += ' spellcheck\x3d"' + c.spellcheck + '"');
                c.maxLength && (h += ' maxlength\x3d"' + c.maxLength + '"');
                c.size && (h += ' size\x3d"' + c.size + '"');
                c.subtype &&
                    (h += ' type\x3d"' + c.subtype + '"');
                this.disabled() && (h += ' disabled\x3d"disabled"');
                (e = c.icon) && "caret" != e && (e = b + "ico " + b + "i-" + c.icon);
                f = this.state.get("text");
                if (e || f) g = '\x3cdiv id\x3d"' + a + '-open" class\x3d"' + b + "btn " + b + 'open" tabIndex\x3d"-1" role\x3d"button"\x3e\x3cbutton id\x3d"' + a + '-action" type\x3d"button" hidefocus\x3d"1" tabindex\x3d"-1"\x3e' + ("caret" != e ? '\x3ci class\x3d"' + e + '"\x3e\x3c/i\x3e' : '\x3ci class\x3d"' + b + 'caret"\x3e\x3c/i\x3e') + (f ? (e ? " " : "") + f : "") + "\x3c/button\x3e\x3c/div\x3e", this.classes.add("has-open");
                return '\x3cdiv id\x3d"' + a + '" class\x3d"' + this.classes + '"\x3e\x3cinput id\x3d"' + a + '-inp" class\x3d"' + b + 'textbox" value\x3d"' + this.encode(d, !1) + '" hidefocus\x3d"1"' + h + ' placeholder\x3d"' + this.encode(c.placeholder) + '" /\x3e' + g + "\x3c/div\x3e"
            },
            value: function(a) {
                if (arguments.length) return this.state.set("value", a), this;
                this.state.get("rendered") && this.state.set("value", this.getEl("inp").value);
                return this.state.get("value")
            },
            bindStates: function() {
                var a = this;
                a.state.on("change:value", function(c) {
                    a.getEl("inp").value !=
                        c.value && (a.getEl("inp").value = c.value)
                });
                a.state.on("change:disabled", function(c) {
                    a.getEl("inp").disabled = c.value
                });
                return a._super()
            },
            remove: function() {
                e(this.getEl("inp")).off();
                this._super()
            }
        })
    });
    z("tinymce/ui/ColorBox", ["tinymce/ui/ComboBox"], function(k) {
        return k.extend({
            init: function(g) {
                var f = this;
                g.spellcheck = !1;
                g.onaction && (g.icon = "none");
                f._super(g);
                f.classes.add("colorbox");
                f.on("change keyup postrender", function() {
                    f.repaintColor(f.value())
                })
            },
            repaintColor: function(g) {
                var f = this.getEl().getElementsByTagName("i")[0];
                if (f) try {
                    f.style.background = g
                } catch (e) {}
            },
            bindStates: function() {
                var g = this;
                g.state.on("change:value", function(f) {
                    g.state.get("rendered") && g.repaintColor(f.value)
                });
                return g._super()
            }
        })
    });
    z("tinymce/ui/PanelButton", ["tinymce/ui/Button", "tinymce/ui/FloatPanel"], function(k, g) {
        return k.extend({
            showPanel: function() {
                var f = this,
                    e = f.settings;
                f.active(!0);
                if (f.panel) f.panel.show();
                else {
                    var a = e.panel;
                    a.type && (a = {
                        layout: "grid",
                        items: a
                    });
                    a.role = a.role || "dialog";
                    a.popover = !0;
                    a.autohide = !0;
                    a.ariaRoot = !0;
                    f.panel =
                        (new g(a)).on("hide", function() {
                            f.active(!1)
                        }).on("cancel", function(a) {
                            a.stopPropagation();
                            f.focus();
                            f.hidePanel()
                        }).parent(f).renderTo(f.getContainerElm());
                    f.panel.fire("show");
                    f.panel.reflow()
                }
                f.panel.moveRel(f.getEl(), e.popoverAlign || (f.isRtl() ? ["bc-tr", "bc-tc"] : ["bc-tl", "bc-tc"]))
            },
            hidePanel: function() {
                this.panel && this.panel.hide()
            },
            postRender: function() {
                var f = this;
                f.aria("haspopup", !0);
                f.on("click", function(e) {
                    e.control === f && (f.panel && f.panel.visible() ? f.hidePanel() : (f.showPanel(), f.panel.focus(!!e.aria)))
                });
                return f._super()
            },
            remove: function() {
                this.panel && (this.panel.remove(), this.panel = null);
                return this._super()
            }
        })
    });
    z("tinymce/ui/ColorButton", ["tinymce/ui/PanelButton", "tinymce/dom/DOMUtils"], function(k, g) {
        var f = g.DOM;
        return k.extend({
            init: function(e) {
                this._super(e);
                this.classes.add("colorbutton")
            },
            color: function(e) {
                return e ? (this._color = e, this.getEl("preview").style.backgroundColor = e, this) : this._color
            },
            resetColor: function() {
                this._color = null;
                this.getEl("preview").style.backgroundColor = null;
                return this
            },
            renderHtml: function() {
                var e = this._id,
                    a = this.classPrefix,
                    c = this.state.get("text"),
                    b = this.settings.icon ? a + "ico " + a + "i-" + this.settings.icon : "",
                    d = this.settings.image ? " style\x3d\"background-image: url('" + this.settings.image + "')\"" : "",
                    f = "";
                c && (this.classes.add("btn-has-text"), f = '\x3cspan class\x3d"' + a + 'txt"\x3e' + this.encode(c) + "\x3c/span\x3e");
                return '\x3cdiv id\x3d"' + e + '" class\x3d"' + this.classes + '" role\x3d"button" tabindex\x3d"-1" aria-haspopup\x3d"true"\x3e\x3cbutton role\x3d"presentation" hidefocus\x3d"1" type\x3d"button" tabindex\x3d"-1"\x3e' +
                    (b ? '\x3ci class\x3d"' + b + '"' + d + "\x3e\x3c/i\x3e" : "") + '\x3cspan id\x3d"' + e + '-preview" class\x3d"' + a + 'preview"\x3e\x3c/span\x3e' + f + '\x3c/button\x3e\x3cbutton type\x3d"button" class\x3d"' + a + 'open" hidefocus\x3d"1" tabindex\x3d"-1"\x3e \x3ci class\x3d"' + a + 'caret"\x3e\x3c/i\x3e\x3c/button\x3e\x3c/div\x3e'
            },
            postRender: function() {
                var e = this,
                    a = e.settings.onclick;
                e.on("click", function(c) {
                    c.aria && "down" == c.aria.key || c.control != e || f.getParent(c.target, "." + e.classPrefix + "open") || (c.stopImmediatePropagation(), a.call(e,
                        c))
                });
                delete e.settings.onclick;
                return e._super()
            }
        })
    });
    z("tinymce/util/Color", [], function() {
        var k = Math.min,
            g = Math.max,
            f = Math.round;
        return function(e) {
            function a(a) {
                var e;
                if ("object" == typeof a)
                    if ("r" in a) b = a.r, d = a.g, l = a.b;
                    else {
                        if ("v" in a) {
                            e = a.h;
                            var h = a.s;
                            a = a.v;
                            var n;
                            e = (parseInt(e, 10) || 0) % 360;
                            h = parseInt(h, 10) / 100;
                            a = parseInt(a, 10) / 100;
                            h = g(0, k(h, 1));
                            a = g(0, k(a, 1));
                            if (0 === h) b = d = l = f(255 * a);
                            else {
                                e /= 60;
                                h *= a;
                                n = h * (1 - Math.abs(e % 2 - 1));
                                a -= h;
                                switch (Math.floor(e)) {
                                    case 0:
                                        b = h;
                                        d = n;
                                        l = 0;
                                        break;
                                    case 1:
                                        b = n;
                                        d = h;
                                        l = 0;
                                        break;
                                    case 2:
                                        b = 0;
                                        d = h;
                                        l = n;
                                        break;
                                    case 3:
                                        b = 0;
                                        d = n;
                                        l = h;
                                        break;
                                    case 4:
                                        b = n;
                                        d = 0;
                                        l = h;
                                        break;
                                    case 5:
                                        b = h;
                                        d = 0;
                                        l = n;
                                        break;
                                    default:
                                        b = d = l = 0
                                }
                                b = f(255 * (b + a));
                                d = f(255 * (d + a));
                                l = f(255 * (l + a))
                            }
                        }
                    }
                else if (e = /rgb\s*\(\s*([0-9]+)\s*,\s*([0-9]+)\s*,\s*([0-9]+)[^\)]*\)/gi.exec(a)) b = parseInt(e[1], 10), d = parseInt(e[2], 10), l = parseInt(e[3], 10);
                else if (e = /#([0-F]{2})([0-F]{2})([0-F]{2})/gi.exec(a)) b = parseInt(e[1], 16), d = parseInt(e[2], 16), l = parseInt(e[3], 16);
                else if (e = /#([0-F])([0-F])([0-F])/gi.exec(a)) b = parseInt(e[1] + e[1], 16), d = parseInt(e[2] +
                    e[2], 16), l = parseInt(e[3] + e[3], 16);
                b = 0 > b ? 0 : 255 < b ? 255 : b;
                d = 0 > d ? 0 : 255 < d ? 255 : d;
                l = 0 > l ? 0 : 255 < l ? 255 : l;
                return c
            }
            var c = this,
                b = 0,
                d = 0,
                l = 0;
            e && a(e);
            c.toRgb = function() {
                return {
                    r: b,
                    g: d,
                    b: l
                }
            };
            c.toHsv = function() {
                var a;
                a = b;
                var c = d,
                    e = l,
                    n, p, w;
                a /= 255;
                c /= 255;
                e /= 255;
                p = k(a, k(c, e));
                w = g(a, g(c, e));
                p == w ? a = {
                    h: 0,
                    s: 0,
                    v: 100 * p
                } : (n = (w - p) / w, a = {
                    h: f(60 * ((a == p ? 3 : e == p ? 1 : 5) - (a == p ? c - e : e == p ? a - c : e - a) / (w - p))),
                    s: f(100 * n),
                    v: f(100 * w)
                });
                return a
            };
            c.toHex = function() {
                function a(a) {
                    a = parseInt(a, 10).toString(16);
                    return 1 < a.length ? a : "0" + a
                }
                return "#" + a(b) +
                    a(d) + a(l)
            };
            c.parse = a
        }
    });
    z("tinymce/ui/ColorPicker", ["tinymce/ui/Widget", "tinymce/ui/DragHelper", "tinymce/ui/DomUtils", "tinymce/util/Color"], function(k, g, f, e) {
        return k.extend({
            Defaults: {
                classes: "widget colorpicker"
            },
            init: function(a) {
                this._super(a)
            },
            postRender: function() {
                function a(a, b) {
                    var c = f.getPos(a),
                        d;
                    d = b.pageX - c.x;
                    b = b.pageY - c.y;
                    d = Math.max(0, Math.min(d / a.clientWidth, 1));
                    b = Math.max(0, Math.min(b / a.clientHeight, 1));
                    return {
                        x: d,
                        y: b
                    }
                }

                function c(a, b) {
                    f.css(n, {
                        top: (360 - a.h) / 360 * 100 + "%"
                    });
                    b || f.css(w, {
                        left: a.s +
                            "%",
                        top: 100 - a.v + "%"
                    });
                    p.style.background = (new e({
                        s: 100,
                        v: 100,
                        h: a.h
                    })).toHex();
                    l.color().parse({
                        s: a.s,
                        v: a.v,
                        h: a.h
                    })
                }

                function b(b) {
                    b = a(p, b);
                    q.s = 100 * b.x;
                    q.v = 100 * (1 - b.y);
                    c(q);
                    l.fire("change")
                }

                function d(b) {
                    b = a(h, b);
                    q = k.toHsv();
                    q.h = 360 * (1 - b.y);
                    c(q, !0);
                    l.fire("change")
                }
                var l = this,
                    k = l.color(),
                    q, h, n, p, w;
                h = l.getEl("h");
                n = l.getEl("hp");
                p = l.getEl("sv");
                w = l.getEl("svp");
                l._repaint = function() {
                    q = k.toHsv();
                    c(q)
                };
                l._super();
                l._svdraghelper = new g(l._id + "-sv", {
                    start: b,
                    drag: b
                });
                l._hdraghelper = new g(l._id + "-h", {
                    start: d,
                    drag: d
                });
                l._repaint()
            },
            rgb: function() {
                return this.color().toRgb()
            },
            value: function(a) {
                if (arguments.length) this.color().parse(a), this._rendered && this._repaint();
                else return this.color().toHex()
            },
            color: function() {
                this._color || (this._color = new e);
                return this._color
            },
            renderHtml: function() {
                var a = this._id,
                    c = this.classPrefix,
                    b, d, e = "",
                    f;
                f = "#ff0000 #ff0080 #ff00ff #8000ff #0000ff #0080ff #00ffff #00ff80 #00ff00 #80ff00 #ffff00 #ff8000 #ff0000".split(" ");
                b = 0;
                for (d = f.length - 1; b < d; b++) e += '\x3cdiv class\x3d"' +
                    c + 'colorpicker-h-chunk" style\x3d"height:' + 100 / d + "%;filter:progid:DXImageTransform.Microsoft.gradient(GradientType\x3d0,startColorstr\x3d" + f[b] + ",endColorstr\x3d" + f[b + 1] + ");-ms-filter:progid:DXImageTransform.Microsoft.gradient(GradientType\x3d0,startColorstr\x3d" + f[b] + ",endColorstr\x3d" + f[b + 1] + ')"\x3e\x3c/div\x3e';
                return '\x3cdiv id\x3d"' + a + '" class\x3d"' + this.classes + '"\x3e\x3cdiv id\x3d"' + a + '-sv" class\x3d"' + c + 'colorpicker-sv"\x3e\x3cdiv class\x3d"' + c + 'colorpicker-overlay1"\x3e\x3cdiv class\x3d"' +
                    c + 'colorpicker-overlay2"\x3e\x3cdiv id\x3d"' + a + '-svp" class\x3d"' + c + 'colorpicker-selector1"\x3e\x3cdiv class\x3d"' + c + 'colorpicker-selector2"\x3e\x3c/div\x3e\x3c/div\x3e\x3c/div\x3e\x3c/div\x3e\x3c/div\x3e' + ('\x3cdiv id\x3d"' + a + '-h" class\x3d"' + c + 'colorpicker-h" style\x3d"background: -ms-linear-gradient(top,#ff0000,#ff0080,#ff00ff,#8000ff,#0000ff,#0080ff,#00ffff,#00ff80,#00ff00,#80ff00,#ffff00,#ff8000,#ff0000);background: linear-gradient(to bottom,#ff0000,#ff0080,#ff00ff,#8000ff,#0000ff,#0080ff,#00ffff,#00ff80,#00ff00,#80ff00,#ffff00,#ff8000,#ff0000);"\x3e' +
                        e + '\x3cdiv id\x3d"' + a + '-hp" class\x3d"' + c + 'colorpicker-h-marker"\x3e\x3c/div\x3e\x3c/div\x3e') + "\x3c/div\x3e"
            }
        })
    });
    z("tinymce/ui/Path", ["tinymce/ui/Widget"], function(k) {
        return k.extend({
            init: function(g) {
                var f = this;
                g.delimiter || (g.delimiter = "\u00bb");
                f._super(g);
                f.classes.add("path");
                f.canFocus = !0;
                f.on("click", function(e) {
                    var a;
                    (a = e.target.getAttribute("data-index")) && f.fire("select", {
                        value: f.row()[a],
                        index: a
                    })
                });
                f.row(f.settings.row)
            },
            focus: function() {
                this.getEl().firstChild.focus();
                return this
            },
            row: function(g) {
                if (!arguments.length) return this.state.get("row");
                this.state.set("row", g);
                return this
            },
            renderHtml: function() {
                return '\x3cdiv id\x3d"' + this._id + '" class\x3d"' + this.classes + '"\x3e' + this._getDataPathHtml(this.state.get("row")) + "\x3c/div\x3e"
            },
            bindStates: function() {
                var g = this;
                g.state.on("change:row", function(f) {
                    g.innerHtml(g._getDataPathHtml(f.value))
                });
                return g._super()
            },
            _getDataPathHtml: function(g) {
                g = g || [];
                var f, e, a = "",
                    c = this.classPrefix;
                f = 0;
                for (e = g.length; f < e; f++) a += (0 < f ? '\x3cdiv class\x3d"' +
                    c + 'divider" aria-hidden\x3d"true"\x3e ' + this.settings.delimiter + " \x3c/div\x3e" : "") + '\x3cdiv role\x3d"button" class\x3d"' + c + "path-item" + (f == e - 1 ? " " + c + "last" : "") + '" data-index\x3d"' + f + '" tabindex\x3d"-1" id\x3d"' + this._id + "-" + f + '" aria-level\x3d"' + f + '"\x3e' + g[f].name + "\x3c/div\x3e";
                a || (a = '\x3cdiv class\x3d"' + c + 'path-item"\x3e\u00a0\x3c/div\x3e');
                return a
            }
        })
    });
    z("tinymce/ui/ElementPath", ["tinymce/ui/Path", "tinymce/EditorManager"], function(k, g) {
        return k.extend({
            postRender: function() {
                var f = this,
                    e = g.activeEditor;
                !1 !== e.settings.elementpath && (f.on("select", function(a) {
                    e.focus();
                    e.selection.select(this.row()[a.index].element);
                    e.nodeChanged()
                }), e.on("nodeChange", function(a) {
                    var c = [];
                    a = a.parents;
                    for (var b = a.length; b--;) {
                        var d;
                        if (d = 1 == a[b].nodeType) {
                            a: {
                                d = a[b];
                                if (1 === d.nodeType) {
                                    if ("BR" == d.nodeName || d.getAttribute("data-mce-bogus")) {
                                        d = !0;
                                        break a
                                    }
                                    if ("bookmark" === d.getAttribute("data-mce-type")) {
                                        d = !0;
                                        break a
                                    }
                                }
                                d = !1
                            }
                            d = !d
                        }
                        if (d && (d = e.fire("ResolveName", {
                                    name: a[b].nodeName.toLowerCase(),
                                    target: a[b]
                                }), d.isDefaultPrevented() ||
                                c.push({
                                    name: d.name,
                                    element: a[b]
                                }), d.isPropagationStopped())) break
                    }
                    f.row(c)
                }));
                return f._super()
            }
        })
    });
    z("tinymce/ui/FormItem", ["tinymce/ui/Container"], function(k) {
        return k.extend({
            Defaults: {
                layout: "flex",
                align: "center",
                defaults: {
                    flex: 1
                }
            },
            renderHtml: function() {
                var g = this._layout,
                    f = this.classPrefix;
                this.classes.add("formitem");
                g.preRender(this);
                return '\x3cdiv id\x3d"' + this._id + '" class\x3d"' + this.classes + '" hidefocus\x3d"1" tabindex\x3d"-1"\x3e' + (this.settings.title ? '\x3cdiv id\x3d"' + this._id + '-title" class\x3d"' +
                    f + 'title"\x3e' + this.settings.title + "\x3c/div\x3e" : "") + '\x3cdiv id\x3d"' + this._id + '-body" class\x3d"' + this.bodyClasses + '"\x3e' + (this.settings.html || "") + g.renderHtml(this) + "\x3c/div\x3e\x3c/div\x3e"
            }
        })
    });
    z("tinymce/ui/Form", ["tinymce/ui/Container", "tinymce/ui/FormItem", "tinymce/util/Tools"], function(k, g, f) {
        return k.extend({
            Defaults: {
                containerCls: "form",
                layout: "flex",
                direction: "column",
                align: "stretch",
                flex: 1,
                padding: 20,
                labelGap: 30,
                spacing: 10,
                callbacks: {
                    submit: function() {
                        this.submit()
                    }
                }
            },
            preRender: function() {
                var e =
                    this,
                    a = e.items();
                e.settings.formItemDefaults || (e.settings.formItemDefaults = {
                    layout: "flex",
                    autoResize: "overflow",
                    defaults: {
                        flex: 1
                    }
                });
                a.each(function(a) {
                    var b;
                    if (b = a.settings.label) b = new g(f.extend({
                        items: {
                            type: "label",
                            id: a._id + "-l",
                            text: b,
                            flex: 0,
                            forId: a._id,
                            disabled: a.disabled()
                        }
                    }, e.settings.formItemDefaults)), b.type = "formitem", a.aria("labelledby", a._id + "-l"), "undefined" == typeof a.settings.flex && (a.settings.flex = 1), e.replace(a, b), b.add(a)
                })
            },
            submit: function() {
                return this.fire("submit", {
                    data: this.toJSON()
                })
            },
            postRender: function() {
                this._super();
                this.fromJSON(this.settings.data)
            },
            bindStates: function() {
                function e() {
                    var c = 0,
                        b = [],
                        d, e;
                    if (!1 !== a.settings.labelGapCalc)
                        for (d = "children" == a.settings.labelGapCalc ? a.find("formitem") : a.items(), d.filter("formitem").each(function(a) {
                                a = a.items()[0];
                                var d = a.getEl().clientWidth;
                                c = d > c ? d : c;
                                b.push(a)
                            }), e = a.settings.labelGap || 0, d = b.length; d--;) b[d].settings.minWidth = c + e
                }
                var a = this;
                a._super();
                a.on("show", e);
                e()
            }
        })
    });
    z("tinymce/ui/FieldSet", ["tinymce/ui/Form"], function(k) {
        return k.extend({
            Defaults: {
                containerCls: "fieldset",
                layout: "flex",
                direction: "column",
                align: "stretch",
                flex: 1,
                padding: "25 15 5 15",
                labelGap: 30,
                spacing: 10,
                border: 1
            },
            renderHtml: function() {
                var g = this._layout,
                    f = this.classPrefix;
                this.preRender();
                g.preRender(this);
                return '\x3cfieldset id\x3d"' + this._id + '" class\x3d"' + this.classes + '" hidefocus\x3d"1" tabindex\x3d"-1"\x3e' + (this.settings.title ? '\x3clegend id\x3d"' + this._id + '-title" class\x3d"' + f + 'fieldset-title"\x3e' + this.settings.title + "\x3c/legend\x3e" : "") + '\x3cdiv id\x3d"' + this._id + '-body" class\x3d"' + this.bodyClasses +
                    '"\x3e' + (this.settings.html || "") + g.renderHtml(this) + "\x3c/div\x3e\x3c/fieldset\x3e"
            }
        })
    });
    z("tinymce/ui/FilePicker", ["tinymce/ui/ComboBox", "tinymce/util/Tools"], function(k, g) {
        return k.extend({
            init: function(f) {
                var e = this,
                    a = tinymce.activeEditor,
                    c = a.settings,
                    b, d, l;
                f.spellcheck = !1;
                (l = c.file_picker_types || c.file_browser_callback_types) && (l = g.makeMap(l, /[, ]/));
                if (!l || l[f.filetype]) d = c.file_picker_callback, !d || l && !l[f.filetype] ? (d = c.file_browser_callback, !d || l && !l[f.filetype] || (b = function() {
                    d(e.getEl("inp").id,
                        e.value(), f.filetype, window)
                })) : b = function() {
                    var b = e.fire("beforecall").meta,
                        b = g.extend({
                            filetype: f.filetype
                        }, b);
                    d.call(a, function(a, b) {
                        e.value(a).fire("change", {
                            meta: b
                        })
                    }, e.value(), b)
                };
                b && (f.icon = "browse", f.onaction = b);
                e._super(f)
            }
        })
    });
    z("tinymce/ui/FitLayout", ["tinymce/ui/AbsoluteLayout"], function(k) {
        return k.extend({
            recalc: function(g) {
                var f = g.layoutRect(),
                    e = g.paddingBox;
                g.items().filter(":visible").each(function(a) {
                    a.layoutRect({
                        x: e.left,
                        y: e.top,
                        w: f.innerW - e.right - e.left,
                        h: f.innerH - e.top - e.bottom
                    });
                    a.recalc && a.recalc()
                })
            }
        })
    });
    z("tinymce/ui/FlexLayout", ["tinymce/ui/AbsoluteLayout"], function(k) {
        return k.extend({
            recalc: function(g) {
                var f, e, a, c, b, d, l, k, q, h, n, p, w, A = [],
                    m, t, y, z, x, r, u, F, E, K, U, G, N, R, I, H, L, T, Y = Math.max,
                    M = Math.min;
                a = g.items().filter(":visible");
                c = g.layoutRect();
                b = g.paddingBox;
                d = g.settings;
                f = g.isRtl() ? d.direction || "row-reversed" : d.direction;
                l = d.align;
                k = g.isRtl() ? d.pack || "end" : d.pack;
                d = d.spacing || 0;
                if ("row-reversed" == f || "column-reverse" == f) a = a.set(a.toArray().reverse()), f = f.split("-")[0];
                "column" == f ? (x = "y", y = "h", z = "minH", r = "maxH", F = "innerH", u = "top", E = "deltaH", K = "contentH", I = "left", N = "w", U = "x", G = "innerW", R = "minW", H = "right", L = "deltaW", T = "contentW") : (x = "x", y = "w", z = "minW", r = "maxW", F = "innerW", u = "left", E = "deltaW", K = "contentW", I = "top", N = "h", U = "y", G = "innerH", R = "minH", H = "bottom", L = "deltaH", T = "contentH");
                h = c[F] - b[u] - b[u];
                f = t = q = 0;
                for (e = a.length; f < e; f++) n = a[f], p = n.layoutRect(), w = n.settings, w = w.flex, h -= f < e - 1 ? d : 0, 0 < w && (q += w, p[r] && A.push(n), p.flex = w), h -= p[z], w = b[I] + p[R] + b[H], w > t && (t = w);
                m = {};
                m[z] =
                    0 > h ? c[z] - h + c[E] : c[F] - h + c[E];
                m[R] = t + c[L];
                m[K] = c[F] - h;
                m[T] = t;
                m.minW = M(m.minW, c.maxW);
                m.minH = M(m.minH, c.maxH);
                m.minW = Y(m.minW, c.startMinWidth);
                m.minH = Y(m.minH, c.startMinHeight);
                if (!c.autoResize || m.minW == c.minW && m.minH == c.minH) {
                    g = h / q;
                    f = 0;
                    for (e = A.length; f < e; f++) n = A[f], p = n.layoutRect(), n = p[r], w = p[z] + p.flex * g, w > n ? (h -= p[r] - p[z], q -= p.flex, p.flex = 0, p.maxFlexSize = n) : p.maxFlexSize = 0;
                    g = h / q;
                    A = b[u];
                    m = {};
                    0 === q && ("end" == k ? A = h + b[u] : "center" == k ? (A = Math.round(c[F] / 2 - (c[F] - h) / 2) + b[u], 0 > A && (A = b[u])) : "justify" == k && (A = b[u],
                        d = Math.floor(h / (a.length - 1))));
                    m[U] = b[I];
                    f = 0;
                    for (e = a.length; f < e; f++) n = a[f], p = n.layoutRect(), w = p.maxFlexSize || p[z], "center" === l ? m[U] = Math.round(c[G] / 2 - p[N] / 2) : "stretch" === l ? (m[N] = Y(p[R] || 0, c[G] - b[I] - b[H]), m[U] = b[I]) : "end" === l && (m[U] = c[G] - p[N] - b.top), 0 < p.flex && (w += p.flex * g), m[y] = w, m[x] = A, n.layoutRect(m), n.recalc && n.recalc(), A += w + d
                } else if (m.w = m.minW, m.h = m.minH, g.layoutRect(m), this.recalc(g), null === g._lastRect && (a = g.parent())) a._lastRect = null, a.recalc()
            }
        })
    });
    z("tinymce/ui/FlowLayout", ["tinymce/ui/Layout"],
        function(k) {
            return k.extend({
                Defaults: {
                    containerClass: "flow-layout",
                    controlClass: "flow-layout-item",
                    endClass: "break"
                },
                recalc: function(g) {
                    g.items().filter(":visible").each(function(f) {
                        f.recalc && f.recalc()
                    })
                },
                isNative: function() {
                    return !0
                }
            })
        });
    z("tinymce/ui/FormatControls", "tinymce/ui/Control tinymce/ui/Widget tinymce/ui/FloatPanel tinymce/util/Tools tinymce/EditorManager tinymce/Env".split(" "), function(k, g, f, e, a, c) {
        function b(a) {
            function b(b, c) {
                return function() {
                    var e = this;
                    a.on("nodeChange", function(f) {
                        var g =
                            a.formatter,
                            h = null;
                        d(f.parents, function(a) {
                            d(b, function(b) {
                                c ? g.matchNode(a, c, {
                                    value: b.value
                                }) && (h = b.value) : g.matchNode(a, b.value) && (h = b.value);
                                if (h) return !1
                            });
                            if (h) return !1
                        });
                        e.value(h)
                    })
                }
            }

            function c(a) {
                a = a.replace(/;$/, "").split(";");
                for (var b = a.length; b--;) a[b] = a[b].split("\x3d");
                return a
            }

            function e(b) {
                return function() {
                    var c = this;
                    if (a.formatter) a.formatter.formatChanged(b, function(a) {
                        c.active(a)
                    });
                    else a.on("init", function() {
                        a.formatter.formatChanged(b, function(a) {
                            c.active(a)
                        })
                    })
                }
            }

            function g(b) {
                return function() {
                    var c =
                        this;
                    b = "redo" == b ? "hasRedo" : "hasUndo";
                    c.disabled(!(a.undoManager && a.undoManager[b]()));
                    a.on("Undo Redo AddUndo TypingUndo ClearUndos SwitchMode", function() {
                        c.disabled(a.readonly || !(a.undoManager && a.undoManager[b]()))
                    })
                }
            }

            function l(b) {
                b.control && (b = b.control.value());
                b && a.execCommand("mceToggleFormat", !1, b)
            }
            var k;
            k = function() {
                function b(a) {
                    var f = [];
                    if (a) return d(a, function(a) {
                        var d = {
                            text: a.title,
                            icon: a.icon
                        };
                        if (a.items) d.menu = b(a.items);
                        else {
                            var g = a.format || "custom" + c++;
                            a.format || (a.name = g, e.push(a));
                            d.format = g;
                            d.cmd = a.cmd
                        }
                        f.push(d)
                    }), f
                }
                var c = 0,
                    e = [],
                    f = [{
                        title: "Headings",
                        items: [{
                            title: "Heading 1",
                            format: "h1"
                        }, {
                            title: "Heading 2",
                            format: "h2"
                        }, {
                            title: "Heading 3",
                            format: "h3"
                        }, {
                            title: "Heading 4",
                            format: "h4"
                        }, {
                            title: "Heading 5",
                            format: "h5"
                        }, {
                            title: "Heading 6",
                            format: "h6"
                        }]
                    }, {
                        title: "Inline",
                        items: [{
                                title: "Bold",
                                icon: "bold",
                                format: "bold"
                            }, {
                                title: "Italic",
                                icon: "italic",
                                format: "italic"
                            }, {
                                title: "Underline",
                                icon: "underline",
                                format: "underline"
                            }, {
                                title: "Strikethrough",
                                icon: "strikethrough",
                                format: "strikethrough"
                            },
                            {
                                title: "Superscript",
                                icon: "superscript",
                                format: "superscript"
                            }, {
                                title: "Subscript",
                                icon: "subscript",
                                format: "subscript"
                            }, {
                                title: "Code",
                                icon: "code",
                                format: "code"
                            }
                        ]
                    }, {
                        title: "Blocks",
                        items: [{
                            title: "Paragraph",
                            format: "p"
                        }, {
                            title: "Blockquote",
                            format: "blockquote"
                        }, {
                            title: "Div",
                            format: "div"
                        }, {
                            title: "Pre",
                            format: "pre"
                        }]
                    }, {
                        title: "Alignment",
                        items: [{
                            title: "Left",
                            icon: "alignleft",
                            format: "alignleft"
                        }, {
                            title: "Center",
                            icon: "aligncenter",
                            format: "aligncenter"
                        }, {
                            title: "Right",
                            icon: "alignright",
                            format: "alignright"
                        }, {
                            title: "Justify",
                            icon: "alignjustify",
                            format: "alignjustify"
                        }]
                    }];
                a.on("init", function() {
                    d(e, function(b) {
                        a.formatter.register(b.name, b)
                    })
                });
                return {
                    type: "menu",
                    items: a.settings.style_formats_merge ? a.settings.style_formats ? b(f.concat(a.settings.style_formats)) : b(f) : b(a.settings.style_formats || f),
                    onPostRender: function(b) {
                        a.fire("renderFormatsMenu", {
                            control: b.control
                        })
                    },
                    itemDefaults: {
                        preview: !0,
                        textStyle: function() {
                            if (this.settings.format) return a.formatter.getCssText(this.settings.format)
                        },
                        onPostRender: function() {
                            var b =
                                this;
                            b.parent().on("show", function() {
                                var c;
                                if (c = b.settings.format) b.disabled(!a.formatter.canApply(c)), b.active(a.formatter.match(c));
                                (c = b.settings.cmd) && b.active(a.queryCommandState(c))
                            })
                        },
                        onclick: function() {
                            this.settings.format && l(this.settings.format);
                            this.settings.cmd && a.execCommand(this.settings.cmd)
                        }
                    }
                }
            }();
            d({
                bold: "Bold",
                italic: "Italic",
                underline: "Underline",
                strikethrough: "Strikethrough",
                subscript: "Subscript",
                superscript: "Superscript"
            }, function(b, c) {
                a.addButton(c, {
                    tooltip: b,
                    onPostRender: e(c),
                    onclick: function() {
                        l(c)
                    }
                })
            });
            d({
                outdent: ["Decrease indent", "Outdent"],
                indent: ["Increase indent", "Indent"],
                cut: ["Cut", "Cut"],
                copy: ["Copy", "Copy"],
                paste: ["Paste", "Paste"],
                help: ["Help", "mceHelp"],
                selectall: ["Select all", "SelectAll"],
                removeformat: ["Clear formatting", "RemoveFormat"],
                visualaid: ["Visual aids", "mceToggleVisualAid"],
                newdocument: ["New document", "mceNewDocument"]
            }, function(b, c) {
                a.addButton(c, {
                    tooltip: b[0],
                    cmd: b[1]
                })
            });
            d({
                blockquote: ["Blockquote", "mceBlockQuote"],
                numlist: ["Numbered list", "InsertOrderedList"],
                bullist: ["Bullet list", "InsertUnorderedList"],
                subscript: ["Subscript", "Subscript"],
                superscript: ["Superscript", "Superscript"],
                alignleft: ["Align left", "JustifyLeft"],
                aligncenter: ["Align center", "JustifyCenter"],
                alignright: ["Align right", "JustifyRight"],
                alignjustify: ["Justify", "JustifyFull"],
                alignnone: ["No alignment", "JustifyNone"]
            }, function(b, c) {
                a.addButton(c, {
                    tooltip: b[0],
                    cmd: b[1],
                    onPostRender: e(c)
                })
            });
            a.addButton("undo", {
                tooltip: "Undo",
                onPostRender: g("undo"),
                cmd: "undo"
            });
            a.addButton("redo", {
                tooltip: "Redo",
                onPostRender: g("redo"),
                cmd: "redo"
            });
            a.addMenuItem("newdocument", {
                text: "New document",
                icon: "newdocument",
                cmd: "mceNewDocument"
            });
            a.addMenuItem("undo", {
                text: "Undo",
                icon: "undo",
                shortcut: "Meta+Z",
                onPostRender: g("undo"),
                cmd: "undo"
            });
            a.addMenuItem("redo", {
                text: "Redo",
                icon: "redo",
                shortcut: "Meta+Y",
                onPostRender: g("redo"),
                cmd: "redo"
            });
            a.addMenuItem("visualaid", {
                text: "Visual aids",
                selectable: !0,
                onPostRender: function() {
                    var b = this;
                    a.on("VisualAid", function(a) {
                        b.active(a.hasVisual)
                    });
                    b.active(a.hasVisual)
                },
                cmd: "mceToggleVisualAid"
            });
            a.addButton("remove", {
                tooltip: "Remove",
                icon: "remove",
                cmd: "Delete"
            });
            d({
                cut: ["Cut", "Cut", "Meta+X"],
                copy: ["Copy", "Copy", "Meta+C"],
                paste: ["Paste", "Paste", "Meta+V"],
                selectall: ["Select all", "SelectAll", "Meta+A"],
                bold: ["Bold", "Bold", "Meta+B"],
                italic: ["Italic", "Italic", "Meta+I"],
                underline: ["Underline", "Underline"],
                strikethrough: ["Strikethrough", "Strikethrough"],
                subscript: ["Subscript", "Subscript"],
                superscript: ["Superscript", "Superscript"],
                removeformat: ["Clear formatting", "RemoveFormat"]
            }, function(b, c) {
                a.addMenuItem(c, {
                    text: b[0],
                    icon: c,
                    shortcut: b[2],
                    cmd: b[1]
                })
            });
            a.on("mousedown", function() {
                f.hideAll()
            });
            a.addButton("styleselect", {
                type: "menubutton",
                text: "Formats",
                menu: k
            });
            a.addButton("formatselect", function() {
                var e = [],
                    f = c(a.settings.block_formats || "Paragraph\x3dp;Heading 1\x3dh1;Heading 2\x3dh2;Heading 3\x3dh3;Heading 4\x3dh4;Heading 5\x3dh5;Heading 6\x3dh6;Preformatted\x3dpre");
                d(f, function(b) {
                    e.push({
                        text: b[0],
                        value: b[1],
                        textStyle: function() {
                            return a.formatter.getCssText(b[1])
                        }
                    })
                });
                return {
                    type: "listbox",
                    text: f[0][0],
                    values: e,
                    fixedWidth: !0,
                    onselect: l,
                    onPostRender: b(e)
                }
            });
            a.addButton("fontselect", function() {
                var e = [],
                    f = c(a.settings.font_formats || "Andale Mono\x3dandale mono,monospace;Arial\x3darial,helvetica,sans-serif;Arial Black\x3darial black,sans-serif;Book Antiqua\x3dbook antiqua,palatino,serif;Comic Sans MS\x3dcomic sans ms,sans-serif;Courier New\x3dcourier new,courier,monospace;Georgia\x3dgeorgia,palatino,serif;Helvetica\x3dhelvetica,arial,sans-serif;Impact\x3dimpact,sans-serif;Symbol\x3dsymbol;Tahoma\x3dtahoma,arial,helvetica,sans-serif;Terminal\x3dterminal,monaco,monospace;Times New Roman\x3dtimes new roman,times,serif;Trebuchet MS\x3dtrebuchet ms,geneva,sans-serif;Verdana\x3dverdana,geneva,sans-serif;Webdings\x3dwebdings;Wingdings\x3dwingdings,zapf dingbats");
                d(f, function(a) {
                    e.push({
                        text: {
                            raw: a[0]
                        },
                        value: a[1],
                        textStyle: -1 == a[1].indexOf("dings") ? "font-family:" + a[1] : ""
                    })
                });
                return {
                    type: "listbox",
                    text: "Font Family",
                    tooltip: "Font Family",
                    values: e,
                    fixedWidth: !0,
                    onPostRender: b(e, "fontname"),
                    onselect: function(b) {
                        b.control.settings.value && a.execCommand("FontName", !1, b.control.settings.value)
                    }
                }
            });
            a.addButton("fontsizeselect", function() {
                var c = [];
                d((a.settings.fontsize_formats || "8pt 10pt 12pt 14pt 18pt 24pt 36pt").split(" "), function(a) {
                    var b = a,
                        d = a;
                    a = a.split("\x3d");
                    1 < a.length && (b = a[0], d = a[1]);
                    c.push({
                        text: b,
                        value: d
                    })
                });
                return {
                    type: "listbox",
                    text: "Font Sizes",
                    tooltip: "Font Sizes",
                    values: c,
                    fixedWidth: !0,
                    onPostRender: b(c, "fontsize"),
                    onclick: function(b) {
                        b.control.settings.value && a.execCommand("FontSize", !1, b.control.settings.value)
                    }
                }
            });
            a.addMenuItem("formats", {
                text: "Formats",
                menu: k
            })
        }
        var d = e.each;
        a.on("AddEditor", function(a) {
            a.editor.rtl && (k.rtl = !0);
            b(a.editor)
        });
        k.translate = function(b) {
            return a.translate(b)
        };
        g.tooltips = !c.iOS
    });
    z("tinymce/ui/GridLayout", ["tinymce/ui/AbsoluteLayout"],
        function(k) {
            return k.extend({
                recalc: function(g) {
                    var f, e, a, c, b, d, l, k, q, h, n, p, w, z, m, t, y, B, x = [],
                        r = [],
                        u;
                    f = g.settings;
                    c = g.items().filter(":visible");
                    b = g.layoutRect();
                    a = f.columns || Math.ceil(Math.sqrt(c.length));
                    e = Math.ceil(c.length / a);
                    m = f.spacingH || f.spacing || 0;
                    t = f.spacingV || f.spacing || 0;
                    y = f.alignH || f.align;
                    B = f.alignV || f.align;
                    w = g.paddingBox;
                    u = "reverseRows" in f ? f.reverseRows : g.isRtl();
                    y && "string" == typeof y && (y = [y]);
                    B && "string" == typeof B && (B = [B]);
                    for (h = 0; h < a; h++) x.push(0);
                    for (l = 0; l < e; l++) r.push(0);
                    for (l =
                        0; l < e; l++)
                        for (h = 0; h < a; h++) {
                            q = c[l * a + h];
                            if (!q) break;
                            k = q.layoutRect();
                            q = k.minW;
                            k = k.minH;
                            x[h] = q > x[h] ? q : x[h];
                            r[l] = k > r[l] ? k : r[l]
                        }
                    k = b.innerW - w.left - w.right;
                    for (h = q = 0; h < a; h++) q += x[h] + (0 < h ? m : 0), k -= (0 < h ? m : 0) + x[h];
                    n = b.innerH - w.top - w.bottom;
                    for (l = h = 0; l < e; l++) h += r[l] + (0 < l ? t : 0), n -= (0 < l ? t : 0) + r[l];
                    q += w.left + w.right;
                    h += w.top + w.bottom;
                    l = {};
                    l.minW = q + (b.w - b.innerW);
                    l.minH = h + (b.h - b.innerH);
                    l.contentW = l.minW - b.deltaW;
                    l.contentH = l.minH - b.deltaH;
                    l.minW = Math.min(l.minW, b.maxW);
                    l.minH = Math.min(l.minH, b.maxH);
                    l.minW = Math.max(l.minW,
                        b.startMinWidth);
                    l.minH = Math.max(l.minH, b.startMinHeight);
                    if (!b.autoResize || l.minW == b.minW && l.minH == b.minH) {
                        b.autoResize && (l = g.layoutRect(l), l.contentW = l.minW - b.deltaW, l.contentH = l.minH - b.deltaH);
                        g = "start" == f.packV ? 0 : 0 < n ? Math.floor(n / e) : 0;
                        l = 0;
                        if (f = f.flexWidths)
                            for (h = 0; h < f.length; h++) l += f[h];
                        else l = a;
                        l = k / l;
                        for (h = 0; h < a; h++) x[h] += f ? f[h] * l : l;
                        n = w.top;
                        for (l = 0; l < e; l++) {
                            b = w.left;
                            f = r[l] + g;
                            for (h = 0; h < a; h++) {
                                k = u ? l * a + a - 1 - h : l * a + h;
                                q = c[k];
                                if (!q) break;
                                p = q.settings;
                                k = q.layoutRect();
                                d = Math.max(x[h], k.startMinWidth);
                                k.x = b;
                                k.y = n;
                                z = p.alignH || (y ? y[h] || y[0] : null);
                                "center" == z ? k.x = b + d / 2 - k.w / 2 : "right" == z ? k.x = b + d - k.w : "stretch" == z && (k.w = d);
                                z = p.alignV || (B ? B[h] || B[0] : null);
                                "center" == z ? k.y = n + f / 2 - k.h / 2 : "bottom" == z ? k.y = n + f - k.h : "stretch" == z && (k.h = f);
                                q.layoutRect(k);
                                b += d + m;
                                q.recalc && q.recalc()
                            }
                            n += f + t
                        }
                    } else if (l.w = l.minW, l.h = l.minH, g.layoutRect(l), this.recalc(g), null === g._lastRect && (e = g.parent())) e._lastRect = null, e.recalc()
                }
            })
        });
    z("tinymce/ui/Iframe", ["tinymce/ui/Widget", "tinymce/util/Delay"], function(k, g) {
        return k.extend({
            renderHtml: function() {
                this.classes.add("iframe");
                this.canFocus = !1;
                return '\x3ciframe id\x3d"' + this._id + '" class\x3d"' + this.classes + '" tabindex\x3d"-1" src\x3d"' + (this.settings.url || "javascript:''") + '" frameborder\x3d"0"\x3e\x3c/iframe\x3e'
            },
            src: function(f) {
                this.getEl().src = f
            },
            html: function(f, e) {
                var a = this,
                    c = this.getEl().contentWindow.document.body;
                c ? (c.innerHTML = f, e && e()) : g.setTimeout(function() {
                    a.html(f)
                });
                return this
            }
        })
    });
    z("tinymce/ui/InfoBox", ["tinymce/ui/Widget"], function(k) {
        return k.extend({
            init: function(g) {
                this._super(g);
                this.classes.add("widget").add("infobox");
                this.canFocus = !1
            },
            severity: function(g) {
                this.classes.remove("error");
                this.classes.remove("warning");
                this.classes.remove("success");
                this.classes.add(g)
            },
            help: function(g) {
                this.state.set("help", g)
            },
            renderHtml: function() {
                var g = this.classPrefix;
                return '\x3cdiv id\x3d"' + this._id + '" class\x3d"' + this.classes + '"\x3e\x3cdiv id\x3d"' + this._id + '-body"\x3e' + this.encode(this.state.get("text")) + '\x3cbutton role\x3d"button" tabindex\x3d"-1"\x3e\x3ci class\x3d"' + g + "ico " + g + 'i-help"\x3e\x3c/i\x3e\x3c/button\x3e\x3c/div\x3e\x3c/div\x3e'
            },
            bindStates: function() {
                var g = this;
                g.state.on("change:text", function(f) {
                    g.getEl("body").firstChild.data = g.encode(f.value);
                    g.state.get("rendered") && g.updateLayoutRect()
                });
                g.state.on("change:help", function(f) {
                    g.classes.toggle("has-help", f.value);
                    g.state.get("rendered") && g.updateLayoutRect()
                });
                return g._super()
            }
        })
    });
    z("tinymce/ui/Label", ["tinymce/ui/Widget", "tinymce/ui/DomUtils"], function(k, g) {
        return k.extend({
            init: function(f) {
                this._super(f);
                this.classes.add("widget").add("label");
                this.canFocus = !1;
                f.multiline &&
                    this.classes.add("autoscroll");
                f.strong && this.classes.add("strong")
            },
            initLayoutRect: function() {
                var f = this._super();
                this.settings.multiline && (g.getSize(this.getEl()).width > f.maxW && (f.minW = f.maxW, this.classes.add("multiline")), this.getEl().style.width = f.minW + "px", f.startMinH = f.h = f.minH = Math.min(f.maxH, g.getSize(this.getEl()).height));
                return f
            },
            repaint: function() {
                this.settings.multiline || (this.getEl().style.lineHeight = this.layoutRect().h + "px");
                return this._super()
            },
            severity: function(f) {
                this.classes.remove("error");
                this.classes.remove("warning");
                this.classes.remove("success");
                this.classes.add(f)
            },
            renderHtml: function() {
                var f, e = this.settings.forId;
                !e && (f = this.settings.forName) && (f = this.getRoot().find("#" + f)[0]) && (e = f._id);
                return e ? '\x3clabel id\x3d"' + this._id + '" class\x3d"' + this.classes + '"' + (e ? ' for\x3d"' + e + '"' : "") + "\x3e" + this.encode(this.state.get("text")) + "\x3c/label\x3e" : '\x3cspan id\x3d"' + this._id + '" class\x3d"' + this.classes + '"\x3e' + this.encode(this.state.get("text")) + "\x3c/span\x3e"
            },
            bindStates: function() {
                var f =
                    this;
                f.state.on("change:text", function(e) {
                    f.innerHtml(f.encode(e.value));
                    f.state.get("rendered") && f.updateLayoutRect()
                });
                return f._super()
            }
        })
    });
    z("tinymce/ui/Toolbar", ["tinymce/ui/Container"], function(k) {
        return k.extend({
            Defaults: {
                role: "toolbar",
                layout: "flow"
            },
            init: function(g) {
                this._super(g);
                this.classes.add("toolbar")
            },
            postRender: function() {
                this.items().each(function(g) {
                    g.classes.add("toolbar-item")
                });
                return this._super()
            }
        })
    });
    z("tinymce/ui/MenuBar", ["tinymce/ui/Toolbar"], function(k) {
        return k.extend({
            Defaults: {
                role: "menubar",
                containerCls: "menubar",
                ariaRoot: !0,
                defaults: {
                    type: "menubutton"
                }
            }
        })
    });
    z("tinymce/ui/MenuButton", ["tinymce/ui/Button", "tinymce/ui/Factory", "tinymce/ui/MenuBar"], function(k, g, f) {
        var e = k.extend({
            init: function(a) {
                this._renderOpen = !0;
                this._super(a);
                a = this.settings;
                this.classes.add("menubtn");
                a.fixedWidth && this.classes.add("fixed-width");
                this.aria("haspopup", !0);
                this.state.set("menu", a.menu || this.render())
            },
            showMenu: function() {
                var a = this,
                    c;
                if (a.menu && a.menu.visible()) return a.hideMenu();
                a.menu || (c = a.state.get("menu") || [], c.length ? c = {
                    type: "menu",
                    items: c
                } : c.type = c.type || "menu", a.menu = c.renderTo ? c.parent(a).show().renderTo() : g.create(c).parent(a).renderTo(), a.fire("createmenu"), a.menu.reflow(), a.menu.on("cancel", function(b) {
                    b.control.parent() === a.menu && (b.stopPropagation(), a.focus(), a.hideMenu())
                }), a.menu.on("select", function() {
                    a.focus()
                }), a.menu.on("show hide", function(b) {
                    b.control == a.menu && a.activeMenu("show" == b.type);
                    a.aria("expanded", "show" == b.type)
                }).fire("show"));
                a.menu.show();
                a.menu.layoutRect({
                    w: a.layoutRect().w
                });
                a.menu.moveRel(a.getEl(), a.isRtl() ? ["br-tr", "tr-br"] : ["bl-tl", "tl-bl"])
            },
            hideMenu: function() {
                this.menu && (this.menu.items().each(function(a) {
                    a.hideMenu && a.hideMenu()
                }), this.menu.hide())
            },
            activeMenu: function(a) {
                this.classes.toggle("active", a)
            },
            renderHtml: function() {
                var a = this._id,
                    c = this.classPrefix,
                    b = this.settings.icon,
                    d, e = this.state.get("text"),
                    g = "";
                (d = this.settings.image) ? (b = "none", "string" != typeof d && (d = window.getSelection ? d[0] : d[1]), d = " style\x3d\"background-image: url('" + d + "')\"") : d = "";
                e && (this.classes.add("btn-has-text"),
                    g = '\x3cspan class\x3d"' + c + 'txt"\x3e' + this.encode(e) + "\x3c/span\x3e");
                b = this.settings.icon ? c + "ico " + c + "i-" + b : "";
                this.aria("role", this.parent() instanceof f ? "menuitem" : "button");
                return '\x3cdiv id\x3d"' + a + '" class\x3d"' + this.classes + '" tabindex\x3d"-1" aria-labelledby\x3d"' + a + '"\x3e\x3cbutton id\x3d"' + a + '-open" role\x3d"presentation" type\x3d"button" tabindex\x3d"-1"\x3e' + (b ? '\x3ci class\x3d"' + b + '"' + d + "\x3e\x3c/i\x3e" : "") + g + ' \x3ci class\x3d"' + c + 'caret"\x3e\x3c/i\x3e\x3c/button\x3e\x3c/div\x3e'
            },
            postRender: function() {
                var a =
                    this;
                a.on("click", function(c) {
                    var b;
                    if (b = c.control === a) a: {
                        b = c.target;
                        for (var d = a.getEl(); b;) {
                            if (d === b) {
                                b = !0;
                                break a
                            }
                            b = b.parentNode
                        }
                        b = !1
                    }
                    b && (a.showMenu(), c.aria && a.menu.items()[0].focus())
                });
                a.on("mouseenter", function(c) {
                    var b = c.control;
                    c = a.parent();
                    var d;
                    b && c && b instanceof e && b.parent() == c && (c.items().filter("MenuButton").each(function(a) {
                        a.hideMenu && a != b && (a.menu && a.menu.visible() && (d = !0), a.hideMenu())
                    }), d && (b.focus(), b.showMenu()))
                });
                return a._super()
            },
            bindStates: function() {
                var a = this;
                a.state.on("change:menu",
                    function() {
                        a.menu && a.menu.remove();
                        a.menu = null
                    });
                return a._super()
            },
            remove: function() {
                this._super();
                this.menu && this.menu.remove()
            }
        });
        return e
    });
    z("tinymce/ui/MenuItem", ["tinymce/ui/Widget", "tinymce/ui/Factory", "tinymce/Env", "tinymce/util/Delay"], function(k, g, f, e) {
        return k.extend({
            Defaults: {
                border: 0,
                role: "menuitem"
            },
            init: function(a) {
                var c;
                this._super(a);
                a = this.settings;
                this.classes.add("menu-item");
                a.menu && this.classes.add("menu-item-expand");
                a.preview && this.classes.add("menu-item-preview");
                c = this.state.get("text");
                if ("-" === c || "|" === c) this.classes.add("menu-item-sep"), this.aria("role", "separator"), this.state.set("text", "-");
                a.selectable && (this.aria("role", "menuitemcheckbox"), this.classes.add("menu-item-checkbox"), a.icon = "selected");
                a.preview || a.selectable || this.classes.add("menu-item-normal");
                this.on("mousedown", function(a) {
                    a.preventDefault()
                });
                a.menu && !a.ariaHideMenu && this.aria("haspopup", !0)
            },
            hasMenus: function() {
                return !!this.settings.menu
            },
            showMenu: function() {
                var a = this,
                    c = a.settings,
                    b, d = a.parent();
                d.items().each(function(b) {
                    b !==
                        a && b.hideMenu()
                });
                c.menu && ((b = a.menu) ? b.show() : (b = c.menu, b.length ? b = {
                        type: "menu",
                        items: b
                    } : b.type = b.type || "menu", d.settings.itemDefaults && (b.itemDefaults = d.settings.itemDefaults), b = a.menu = g.create(b).parent(a).renderTo(), b.reflow(), b.on("cancel", function(c) {
                        c.stopPropagation();
                        a.focus();
                        b.hide()
                    }), b.on("show hide", function(a) {
                        a.control.items().each(function(a) {
                            a.active(a.settings.selected)
                        })
                    }).fire("show"), b.on("hide", function(c) {
                        c.control === b && a.classes.remove("selected")
                    }), b.submenu = !0), b._parentMenu =
                    d, b.classes.add("menu-sub"), c = b.testMoveRel(a.getEl(), a.isRtl() ? ["tl-tr", "bl-br", "tr-tl", "br-bl"] : ["tr-tl", "br-bl", "tl-tr", "bl-br"]), b.moveRel(a.getEl(), c), b.rel = c, c = "menu-sub-" + c, b.classes.remove(b._lastRel).add(c), b._lastRel = c, a.classes.add("selected"), a.aria("expanded", !0))
            },
            hideMenu: function() {
                this.menu && (this.menu.items().each(function(a) {
                    a.hideMenu && a.hideMenu()
                }), this.menu.hide(), this.aria("expanded", !1));
                return this
            },
            renderHtml: function() {
                var a = this._id,
                    c = this.settings,
                    b = this.classPrefix,
                    d = this.encode(this.state.get("text")),
                    e = this.settings.icon,
                    g = "",
                    k = c.shortcut;
                e && this.parent().classes.add("menu-has-icons");
                c.image && (g = " style\x3d\"background-image: url('" + c.image + "')\"");
                if (k) {
                    var h, n, e = f.mac ? {
                            alt: "\x26#x2325;",
                            ctrl: "\x26#x2318;",
                            shift: "\x26#x21E7;",
                            meta: "\x26#x2318;"
                        } : {
                            meta: "Ctrl"
                        },
                        k = k.split("+");
                    for (h = 0; h < k.length; h++)(n = e[k[h].toLowerCase()]) && (k[h] = n);
                    k = k.join("+")
                }
                e = b + "ico " + b + "i-" + (this.settings.icon || "none");
                return '\x3cdiv id\x3d"' + a + '" class\x3d"' + this.classes + '" tabindex\x3d"-1"\x3e' +
                    ("-" !== d ? '\x3ci class\x3d"' + e + '"' + g + "\x3e\x3c/i\x3e\u00a0" : "") + ("-" !== d ? '\x3cspan id\x3d"' + a + '-text" class\x3d"' + b + 'text"\x3e' + d + "\x3c/span\x3e" : "") + (k ? '\x3cdiv id\x3d"' + a + '-shortcut" class\x3d"' + b + 'menu-shortcut"\x3e' + k + "\x3c/div\x3e" : "") + (c.menu ? '\x3cdiv class\x3d"' + b + 'caret"\x3e\x3c/div\x3e' : "") + "\x3c/div\x3e"
            },
            postRender: function() {
                var a = this,
                    c = a.settings,
                    b = c.textStyle;
                "function" == typeof b && (b = b.call(this));
                if (b) {
                    var d = a.getEl("text");
                    d && d.setAttribute("style", b)
                }
                a.on("mouseenter click", function(b) {
                    b.control ===
                        a && (c.menu || "click" !== b.type ? (a.showMenu(), b.aria && a.menu.focus(!0)) : (a.fire("select"), e.requestAnimationFrame(function() {
                            a.parent().hideAll()
                        })))
                });
                a._super();
                return a
            },
            hover: function() {
                this.parent().items().each(function(a) {
                    a.classes.remove("selected")
                });
                this.classes.toggle("selected", !0);
                return this
            },
            active: function(a) {
                "undefined" != typeof a && this.aria("checked", a);
                return this._super(a)
            },
            remove: function() {
                this._super();
                this.menu && this.menu.remove()
            }
        })
    });
    z("tinymce/ui/Throbber", ["tinymce/dom/DomQuery",
        "tinymce/ui/Control", "tinymce/util/Delay"
    ], function(k, g, f) {
        return function(e, a) {
            var c = this,
                b, d = g.classPrefix,
                l;
            c.show = function(g, q) {
                function h() {
                    b && (k(e).append('\x3cdiv class\x3d"' + d + "throbber" + (a ? " " + d + "throbber-inline" : "") + '"\x3e\x3c/div\x3e'), q && q())
                }
                c.hide();
                b = !0;
                g ? l = f.setTimeout(h, g) : h();
                return c
            };
            c.hide = function() {
                var a = e.lastChild;
                f.clearTimeout(l);
                a && -1 != a.className.indexOf("throbber") && a.parentNode.removeChild(a);
                b = !1;
                return c
            }
        }
    });
    z("tinymce/ui/Menu", ["tinymce/ui/FloatPanel", "tinymce/ui/MenuItem",
        "tinymce/ui/Throbber", "tinymce/util/Tools"
    ], function(k, g, f, e) {
        return k.extend({
            Defaults: {
                defaultType: "menuitem",
                border: 1,
                layout: "stack",
                role: "application",
                bodyRole: "menu",
                ariaRoot: !0
            },
            init: function(a) {
                a.autohide = !0;
                a.constrainToViewport = !0;
                "function" === typeof a.items && (a.itemsFactory = a.items, a.items = []);
                if (a.itemDefaults)
                    for (var c = a.items, b = c.length; b--;) c[b] = e.extend({}, a.itemDefaults, c[b]);
                this._super(a);
                this.classes.add("menu")
            },
            repaint: function() {
                this.classes.toggle("menu-align", !0);
                this._super();
                this.getEl().style.height = "";
                this.getEl("body").style.height = "";
                return this
            },
            cancel: function() {
                this.hideAll();
                this.fire("select")
            },
            load: function() {
                function a() {
                    c.throbber && (c.throbber.hide(), c.throbber = null)
                }
                var c = this,
                    b;
                c.settings.itemsFactory && (c.throbber || (c.throbber = new f(c.getEl("body"), !0), 0 === c.items().length ? (c.throbber.show(), c.fire("loading")) : c.throbber.show(100, function() {
                    c.items().remove();
                    c.fire("loading")
                }), c.on("hide close", a)), c.requestTime = b = (new Date).getTime(), c.settings.itemsFactory(function(d) {
                    0 ===
                        d.length ? c.hide() : c.requestTime === b && (c.getEl().style.width = "", c.getEl("body").style.width = "", a(), c.items().remove(), c.getEl("body").innerHTML = "", c.add(d), c.renderNew(), c.fire("loaded"))
                }))
            },
            hideAll: function() {
                this.find("menuitem").exec("hideMenu");
                return this._super()
            },
            preRender: function() {
                var a = this;
                a.items().each(function(c) {
                    c = c.settings;
                    if (c.icon || c.image || c.selectable) return a._hasIcons = !0, !1
                });
                if (a.settings.itemsFactory) a.on("postrender", function() {
                    a.settings.itemsFactory && a.load()
                });
                return a._super()
            }
        })
    });
    z("tinymce/ui/ListBox", ["tinymce/ui/MenuButton", "tinymce/ui/Menu"], function(k, g) {
        return k.extend({
            init: function(f) {
                function e(c) {
                    for (var g = 0; g < c.length; g++) {
                        if (b = c[g].selected || f.value === c[g].value) return d = d || c[g].text, a.state.set("value", c[g].value), !0;
                        if (c[g].menu && e(c[g].menu)) return !0
                    }
                }
                var a = this,
                    c, b, d, g;
                a._super(f);
                f = a.settings;
                if (a._values = c = f.values) "undefined" != typeof f.value && e(c), !b && 0 < c.length && (d = c[0].text, a.state.set("value", c[0].value)), a.state.set("menu", c);
                a.state.set("text", f.text ||
                    d);
                a.classes.add("listbox");
                a.on("select", function(b) {
                    var c = b.control;
                    g && (b.lastControl = g);
                    f.multiple ? c.active(!c.active()) : a.value(b.control.value());
                    g = c
                })
            },
            bindStates: function() {
                function f(a, b) {
                    a instanceof g && a.items().each(function(a) {
                        a.hasMenus() || a.active(a.value() === b)
                    })
                }

                function e(a, b) {
                    var c;
                    if (a)
                        for (var f = 0; f < a.length; f++) {
                            if (a[f].value === b) return a[f];
                            if (a[f].menu && (c = e(a[f].menu, b))) return c
                        }
                }
                var a = this;
                a.on("show", function(c) {
                    f(c.control, a.value())
                });
                a.state.on("change:value", function(c) {
                    (c =
                        e(a.state.get("menu"), c.value)) ? a.text(c.text): a.text(a.settings.text)
                });
                return a._super()
            }
        })
    });
    z("tinymce/ui/Radio", ["tinymce/ui/Checkbox"], function(k) {
        return k.extend({
            Defaults: {
                classes: "radio",
                role: "radio"
            }
        })
    });
    z("tinymce/ui/ResizeHandle", ["tinymce/ui/Widget", "tinymce/ui/DragHelper"], function(k, g) {
        return k.extend({
            renderHtml: function() {
                var f = this.classPrefix;
                this.classes.add("resizehandle");
                "both" == this.settings.direction && this.classes.add("resizehandle-both");
                this.canFocus = !1;
                return '\x3cdiv id\x3d"' +
                    this._id + '" class\x3d"' + this.classes + '"\x3e\x3ci class\x3d"' + f + "ico " + f + 'i-resize"\x3e\x3c/i\x3e\x3c/div\x3e'
            },
            postRender: function() {
                var f = this;
                f._super();
                f.resizeDragHelper = new g(this._id, {
                    start: function() {
                        f.fire("ResizeStart")
                    },
                    drag: function(e) {
                        "both" != f.settings.direction && (e.deltaX = 0);
                        f.fire("Resize", e)
                    },
                    stop: function() {
                        f.fire("ResizeEnd")
                    }
                })
            },
            remove: function() {
                this.resizeDragHelper && this.resizeDragHelper.destroy();
                return this._super()
            }
        })
    });
    z("tinymce/ui/SelectBox", ["tinymce/ui/Widget"], function(k) {
        function g(f) {
            var e =
                "";
            if (f)
                for (var a = 0; a < f.length; a++) e += '\x3coption value\x3d"' + f[a] + '"\x3e' + f[a] + "\x3c/option\x3e";
            return e
        }
        return k.extend({
            Defaults: {
                classes: "selectbox",
                role: "selectbox",
                options: []
            },
            init: function(f) {
                var e = this;
                e._super(f);
                e.settings.size && (e.size = e.settings.size);
                e.settings.options && (e._options = e.settings.options);
                e.on("keydown", function(a) {
                    var c;
                    13 == a.keyCode && (a.preventDefault(), e.parents().reverse().each(function(a) {
                        if (a.toJSON) return c = a, !1
                    }), e.fire("submit", {
                        data: c.toJSON()
                    }))
                })
            },
            options: function(f) {
                if (!arguments.length) return this.state.get("options");
                this.state.set("options", f);
                return this
            },
            renderHtml: function() {
                var f, e = "";
                f = g(this._options);
                this.size && (e = ' size \x3d "' + this.size + '"');
                return '\x3cselect id\x3d"' + this._id + '" class\x3d"' + this.classes + '"' + e + "\x3e" + f + "\x3c/select\x3e"
            },
            bindStates: function() {
                var f = this;
                f.state.on("change:options", function(e) {
                    f.getEl().innerHTML = g(e.value)
                });
                return f._super()
            }
        })
    });
    z("tinymce/ui/Slider", ["tinymce/ui/Widget", "tinymce/ui/DragHelper", "tinymce/ui/DomUtils"], function(k, g, f) {
        function e(a, b, d) {
            a < b && (a = b);
            a >
                d && (a = d);
            return a
        }

        function a(a, b) {
            var c, e, g, k;
            "v" == a.settings.orientation ? (g = "top", e = "height", c = "h") : (g = "left", e = "width", c = "w");
            k = a.getEl("handle");
            c = ((a.layoutRect()[c] || 100) - f.getSize(k)[e]) * ((b - a._minValue) / (a._maxValue - a._minValue)) + "px";
            k.style[g] = c;
            k.style.height = a.layoutRect().h + "px";
            k.setAttribute("aria-valuenow", b);
            b = "" + a.settings.previewFilter(b);
            k.setAttribute("aria-valuetext", b);
            k.setAttribute("aria-valuemin", a._minValue);
            k.setAttribute("aria-valuemax", a._maxValue)
        }
        return k.extend({
            init: function(a) {
                a.previewFilter ||
                    (a.previewFilter = function(a) {
                        return Math.round(100 * a) / 100
                    });
                this._super(a);
                this.classes.add("slider");
                "v" == a.orientation && this.classes.add("vertical");
                this._minValue = a.minValue || 0;
                this._maxValue = a.maxValue || 100;
                this._initValue = this.state.get("value")
            },
            renderHtml: function() {
                var a = this._id;
                return '\x3cdiv id\x3d"' + a + '" class\x3d"' + this.classes + '"\x3e\x3cdiv id\x3d"' + a + '-handle" class\x3d"' + this.classPrefix + 'slider-handle" role\x3d"slider" tabindex\x3d"-1"\x3e\x3c/div\x3e\x3c/div\x3e'
            },
            reset: function() {
                this.value(this._initValue).repaint()
            },
            postRender: function() {
                var a = this,
                    b, d, k, v, q, h;
                b = a._minValue;
                d = a._maxValue;
                "v" == a.settings.orientation ? (k = "screenY", v = "top", q = "height", h = "h") : (k = "screenX", v = "left", q = "width", h = "w");
                a._super();
                (function(b, c) {
                    function d(d) {
                        var f;
                        f = a.value();
                        f = ((f + b) / (c - b) + .05 * d) * (c - b) - b;
                        f = e(f, b, c);
                        a.value(f);
                        a.fire("dragstart", {
                            value: f
                        });
                        a.fire("drag", {
                            value: f
                        });
                        a.fire("dragend", {
                            value: f
                        })
                    }
                    a.on("keydown", function(a) {
                        switch (a.keyCode) {
                            case 37:
                            case 38:
                                d(-1);
                                break;
                            case 39:
                            case 40:
                                d(1)
                        }
                    })
                })(b, d, a.getEl("handle"));
                (function(b,
                    c, d) {
                    var l, m, n, p, w;
                    a._dragHelper = new g(a._id, {
                        handle: a._id + "-handle",
                        start: function(b) {
                            l = b[k];
                            m = parseInt(a.getEl("handle").style[v], 10);
                            n = (a.layoutRect()[h] || 100) - f.getSize(d)[q];
                            a.fire("dragstart", {
                                value: w
                            })
                        },
                        drag: function(f) {
                            p = e(m + (f[k] - l), 0, n);
                            d.style[v] = p + "px";
                            w = b + p / n * (c - b);
                            a.value(w);
                            a.tooltip().text("" + a.settings.previewFilter(w)).show().moveRel(d, "bc tc");
                            a.fire("drag", {
                                value: w
                            })
                        },
                        stop: function() {
                            a.tooltip().hide();
                            a.fire("dragend", {
                                value: w
                            })
                        }
                    })
                })(b, d, a.getEl("handle"))
            },
            repaint: function() {
                this._super();
                a(this, this.value())
            },
            bindStates: function() {
                var c = this;
                c.state.on("change:value", function(b) {
                    a(c, b.value)
                });
                return c._super()
            }
        })
    });
    z("tinymce/ui/Spacer", ["tinymce/ui/Widget"], function(k) {
        return k.extend({
            renderHtml: function() {
                this.classes.add("spacer");
                this.canFocus = !1;
                return '\x3cdiv id\x3d"' + this._id + '" class\x3d"' + this.classes + '"\x3e\x3c/div\x3e'
            }
        })
    });
    z("tinymce/ui/SplitButton", ["tinymce/ui/MenuButton", "tinymce/ui/DomUtils", "tinymce/dom/DomQuery"], function(k, g, f) {
        return k.extend({
            Defaults: {
                classes: "widget btn splitbtn",
                role: "button"
            },
            repaint: function() {
                var e = this.getEl(),
                    a = this.layoutRect(),
                    c;
                this._super();
                c = e.firstChild;
                e = e.lastChild;
                f(c).css({
                    width: a.w - g.getSize(e).width,
                    height: a.h - 2
                });
                f(e).css({
                    height: a.h - 2
                });
                return this
            },
            activeMenu: function(e) {
                f(this.getEl().lastChild).toggleClass(this.classPrefix + "active", e)
            },
            renderHtml: function() {
                var e = this._id,
                    a = this.classPrefix,
                    c, b = this.state.get("icon"),
                    d = this.state.get("text"),
                    f = "";
                (c = this.settings.image) ? (b = "none", "string" != typeof c && (c = window.getSelection ? c[0] : c[1]),
                    c = " style\x3d\"background-image: url('" + c + "')\"") : c = "";
                b = this.settings.icon ? a + "ico " + a + "i-" + b : "";
                d && (this.classes.add("btn-has-text"), f = '\x3cspan class\x3d"' + a + 'txt"\x3e' + this.encode(d) + "\x3c/span\x3e");
                return '\x3cdiv id\x3d"' + e + '" class\x3d"' + this.classes + '" role\x3d"button" tabindex\x3d"-1"\x3e\x3cbutton type\x3d"button" hidefocus\x3d"1" tabindex\x3d"-1"\x3e' + (b ? '\x3ci class\x3d"' + b + '"' + c + "\x3e\x3c/i\x3e" : "") + f + '\x3c/button\x3e\x3cbutton type\x3d"button" class\x3d"' + a + 'open" hidefocus\x3d"1" tabindex\x3d"-1"\x3e' +
                    (this._menuBtnText ? (b ? "\u00a0" : "") + this._menuBtnText : "") + ' \x3ci class\x3d"' + a + 'caret"\x3e\x3c/i\x3e\x3c/button\x3e\x3c/div\x3e'
            },
            postRender: function() {
                var e = this.settings.onclick;
                this.on("click", function(a) {
                    var c = a.target;
                    if (a.control == this)
                        for (; c;) {
                            if (a.aria && "down" != a.aria.key || "BUTTON" == c.nodeName && -1 == c.className.indexOf("open")) {
                                a.stopImmediatePropagation();
                                e && e.call(this, a);
                                break
                            }
                            c = c.parentNode
                        }
                });
                delete this.settings.onclick;
                return this._super()
            }
        })
    });
    z("tinymce/ui/StackLayout", ["tinymce/ui/FlowLayout"],
        function(k) {
            return k.extend({
                Defaults: {
                    containerClass: "stack-layout",
                    controlClass: "stack-layout-item",
                    endClass: "break"
                },
                isNative: function() {
                    return !0
                }
            })
        });
    z("tinymce/ui/TabPanel", ["tinymce/ui/Panel", "tinymce/dom/DomQuery", "tinymce/ui/DomUtils"], function(k, g, f) {
        return k.extend({
            Defaults: {
                layout: "absolute",
                defaults: {
                    type: "panel"
                }
            },
            activateTab: function(e) {
                var a;
                this.activeTabId && (a = this.getEl(this.activeTabId), g(a).removeClass(this.classPrefix + "active"), a.setAttribute("aria-selected", "false"));
                this.activeTabId =
                    "t" + e;
                a = this.getEl("t" + e);
                a.setAttribute("aria-selected", "true");
                g(a).addClass(this.classPrefix + "active");
                this.items()[e].show().fire("showtab");
                this.reflow();
                this.items().each(function(a, b) {
                    e != b && a.hide()
                })
            },
            renderHtml: function() {
                var e = this,
                    a = e._layout,
                    c = "",
                    b = e.classPrefix;
                e.preRender();
                a.preRender(e);
                e.items().each(function(a, f) {
                    f = e._id + "-t" + f;
                    a.aria("role", "tabpanel");
                    a.aria("labelledby", f);
                    c += '\x3cdiv id\x3d"' + f + '" class\x3d"' + b + 'tab" unselectable\x3d"on" role\x3d"tab" aria-controls\x3d"' + a._id +
                        '" aria-selected\x3d"false" tabIndex\x3d"-1"\x3e' + e.encode(a.settings.title) + "\x3c/div\x3e"
                });
                return '\x3cdiv id\x3d"' + e._id + '" class\x3d"' + e.classes + '" hidefocus\x3d"1" tabindex\x3d"-1"\x3e\x3cdiv id\x3d"' + e._id + '-head" class\x3d"' + b + 'tabs" role\x3d"tablist"\x3e' + c + '\x3c/div\x3e\x3cdiv id\x3d"' + e._id + '-body" class\x3d"' + e.bodyClasses + '"\x3e' + a.renderHtml(e) + "\x3c/div\x3e\x3c/div\x3e"
            },
            postRender: function() {
                var e = this;
                e._super();
                e.settings.activeTab = e.settings.activeTab || 0;
                e.activateTab(e.settings.activeTab);
                this.on("click", function(a) {
                    var c = a.target.parentNode;
                    if (a.target.parentNode.id == e._id + "-head")
                        for (var b = c.childNodes.length; b--;) c.childNodes[b] == a.target && e.activateTab(b)
                })
            },
            initLayoutRect: function() {
                var e, a, c;
                a = f.getSize(this.getEl("head")).width;
                a = 0 > a ? 0 : a;
                c = 0;
                this.items().each(function(b) {
                    a = Math.max(a, b.layoutRect().minW);
                    c = Math.max(c, b.layoutRect().minH)
                });
                this.items().each(function(b) {
                    b.settings.x = 0;
                    b.settings.y = 0;
                    b.settings.w = a;
                    b.settings.h = c;
                    b.layoutRect({
                        x: 0,
                        y: 0,
                        w: a,
                        h: c
                    })
                });
                var b = f.getSize(this.getEl("head")).height;
                this.settings.minWidth = a;
                this.settings.minHeight = c + b;
                e = this._super();
                e.deltaH += b;
                e.innerH = e.h - e.deltaH;
                return e
            }
        })
    });
    z("tinymce/ui/TextBox", ["tinymce/ui/Widget", "tinymce/util/Tools", "tinymce/ui/DomUtils"], function(k, g, f) {
        return k.extend({
            init: function(e) {
                var a = this;
                a._super(e);
                a.classes.add("textbox");
                e.multiline ? a.classes.add("multiline") : (a.on("keydown", function(c) {
                        var b;
                        13 == c.keyCode && (c.preventDefault(), a.parents().reverse().each(function(a) {
                            if (a.toJSON) return b = a, !1
                        }), a.fire("submit", {
                            data: b.toJSON()
                        }))
                    }),
                    a.on("keyup", function(c) {
                        a.state.set("value", c.target.value)
                    }))
            },
            repaint: function() {
                var e, a, c, b;
                c = 0;
                var d;
                e = this.getEl().style;
                a = this._layoutRect;
                d = this._lastRepaintRect || {};
                b = document;
                !this.settings.multiline && b.all && (!b.documentMode || 8 >= b.documentMode) && (e.lineHeight = a.h - c + "px");
                c = this.borderBox;
                b = c.left + c.right + 8;
                c = c.top + c.bottom + (this.settings.multiline ? 8 : 0);
                a.x !== d.x && (e.left = a.x + "px", d.x = a.x);
                a.y !== d.y && (e.top = a.y + "px", d.y = a.y);
                a.w !== d.w && (e.width = a.w - b + "px", d.w = a.w);
                a.h !== d.h && (e.height = a.h -
                    c + "px", d.h = a.h);
                this._lastRepaintRect = d;
                this.fire("repaint", {}, !1);
                return this
            },
            renderHtml: function() {
                var e = this.settings,
                    a, c;
                a = {
                    id: this._id,
                    hidefocus: "1"
                };
                g.each("rows spellcheck maxLength size readonly min max step list pattern placeholder required multiple".split(" "), function(b) {
                    a[b] = e[b]
                });
                this.disabled() && (a.disabled = "disabled");
                e.subtype && (a.type = e.subtype);
                c = f.create(e.multiline ? "textarea" : "input", a);
                c.value = this.state.get("value");
                c.className = this.classes;
                return c.outerHTML
            },
            value: function(e) {
                if (arguments.length) return this.state.set("value",
                    e), this;
                this.state.get("rendered") && this.state.set("value", this.getEl().value);
                return this.state.get("value")
            },
            postRender: function() {
                var e = this;
                e.getEl().value = e.state.get("value");
                e._super();
                e.$el.on("change", function(a) {
                    e.state.set("value", a.target.value);
                    e.fire("change", a)
                })
            },
            bindStates: function() {
                var e = this;
                e.state.on("change:value", function(a) {
                    e.getEl().value != a.value && (e.getEl().value = a.value)
                });
                e.state.on("change:disabled", function(a) {
                    e.getEl().disabled = a.value
                });
                return e._super()
            },
            remove: function() {
                this.$el.off();
                this._super()
            }
        })
    });
    z("tinymce/Register", [], function() {
        var k = this || window;
        "function" === typeof k.define && (k.define.amd || k.define("ephox/tinymce", [], function() {
            return k.tinymce
        }));
        return {}
    });
    (function(k) {
        var g, f, e, a;
        for (g = 0; g < k.length; g++) {
            f = E;
            e = k[g];
            a = e.split(/[.\/]/);
            for (var c = 0; c < a.length - 1; ++c) f[a[c]] === K && (f[a[c]] = {}), f = f[a[c]];
            f[a[a.length - 1]] = ga[e]
        }
        if (E.AMDLC_TESTS) {
            f = E.privateModules || {};
            for (e in ga) f[e] = ga[e];
            for (g = 0; g < k.length; g++) delete f[k[g]];
            E.privateModules = f
        }
    })("tinymce/geom/Rect tinymce/util/Promise tinymce/util/Delay tinymce/Env tinymce/dom/EventUtils tinymce/dom/Sizzle tinymce/util/Tools tinymce/dom/DomQuery tinymce/html/Styles tinymce/dom/TreeWalker tinymce/html/Entities tinymce/dom/DOMUtils tinymce/dom/ScriptLoader tinymce/AddOnManager tinymce/dom/RangeUtils tinymce/html/Node tinymce/html/Schema tinymce/html/SaxParser tinymce/html/DomParser tinymce/html/Writer tinymce/html/Serializer tinymce/dom/Serializer tinymce/util/VK tinymce/dom/ControlSelection tinymce/dom/BookmarkManager tinymce/dom/Selection tinymce/Formatter tinymce/UndoManager tinymce/EditorCommands tinymce/util/URI tinymce/util/Class tinymce/util/EventDispatcher tinymce/util/Observable tinymce/ui/Selector tinymce/ui/Collection tinymce/ui/ReflowQueue tinymce/ui/Control tinymce/ui/Factory tinymce/ui/KeyboardNavigation tinymce/ui/Container tinymce/ui/DragHelper tinymce/ui/Scrollable tinymce/ui/Panel tinymce/ui/Movable tinymce/ui/Resizable tinymce/ui/FloatPanel tinymce/ui/Window tinymce/ui/MessageBox tinymce/WindowManager tinymce/ui/Tooltip tinymce/ui/Widget tinymce/ui/Progress tinymce/ui/Notification tinymce/NotificationManager tinymce/EditorObservable tinymce/Shortcuts tinymce/Editor tinymce/util/I18n tinymce/FocusManager tinymce/EditorManager tinymce/util/XHR tinymce/util/JSON tinymce/util/JSONRequest tinymce/util/JSONP tinymce/util/LocalStorage tinymce/Compat tinymce/ui/Layout tinymce/ui/AbsoluteLayout tinymce/ui/Button tinymce/ui/ButtonGroup tinymce/ui/Checkbox tinymce/ui/ComboBox tinymce/ui/ColorBox tinymce/ui/PanelButton tinymce/ui/ColorButton tinymce/util/Color tinymce/ui/ColorPicker tinymce/ui/Path tinymce/ui/ElementPath tinymce/ui/FormItem tinymce/ui/Form tinymce/ui/FieldSet tinymce/ui/FilePicker tinymce/ui/FitLayout tinymce/ui/FlexLayout tinymce/ui/FlowLayout tinymce/ui/FormatControls tinymce/ui/GridLayout tinymce/ui/Iframe tinymce/ui/InfoBox tinymce/ui/Label tinymce/ui/Toolbar tinymce/ui/MenuBar tinymce/ui/MenuButton tinymce/ui/MenuItem tinymce/ui/Throbber tinymce/ui/Menu tinymce/ui/ListBox tinymce/ui/Radio tinymce/ui/ResizeHandle tinymce/ui/SelectBox tinymce/ui/Slider tinymce/ui/Spacer tinymce/ui/SplitButton tinymce/ui/StackLayout tinymce/ui/TabPanel tinymce/ui/TextBox".split(" "))
})(this);